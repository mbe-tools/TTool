# Preparation

- create a .gitlab-ci.yml at the root of the gitlab

- create an eval script

- create a docker file

- build the docker image
docker build -f Dockerfile -t ttoolci:latest .

- to test if the docker file runs as expected:
docker run -it --rm ttoolci:latest bash


# yvrac

- launch the gitlab-runner so as to start a docker machine for each pull

sudo gitlab-runner register -n --name ttool@cadillac -r GR13489416h8xwG1z62GLNe5Q1phV -u https://gitlab.telecom-paris.fr/ --executor docker --docker-image ttoolci:latest --docker-pull-policy never

#  cadillac

- create a new runner in the gitlab interface:
https://gitlab.telecom-paris.fr/mbe-tools/TTool/-/settings/ci_cd

- edit gitlab-runner configuration in cadillac

emacs -nw sudo more /etc/gitlab-runner/config.toml
[[runners]]
  name = "ttool@cadillac"
  url = "https://gitlab.telecom-paris.fr/"
  id = 112
  token = "glrt-8xpskVidT7YyKkj7edmH"
  token_obtained_at = 2024-06-28T10:11:08Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "ttoolci"
    pull_policy = ["never"]
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mtu = 0

- restart gitlab-runner
systemctl restart gitlab-runner.service

