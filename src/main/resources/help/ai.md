# AI
AI intelligence in TTool intends to help designers to make better diagrams.

## Configuration
TTool must first be configured before using AI. Add the following information to the configuration file (e.g., config.xml) of TTool:
```xml
<OPENAIKey data="<put your secret key there>"/>
<OPENAIModel data="gpt-3.5-turbo gpt-4-turbo gpt-4o gpt-4o-mini"/>
```
This is required to have a valid openai account and available tokens.

Similarly, you can use a mistralAI key.
```xml
<MistralAIKey data="put your key here"/>
<MistralAIModel data="pixtral-12b-2409"/>
```


## How to use AI?

First, select what you intend to do at the top of the AI window:

### Chat
This option makes it possible to directly interact with the AI.

Enter your question in the "Question" textarea, and click on "Start"

### Classify requirements
This options intends to automatically identify the "kind" attributes of requirements. Open a requirement diagram with at least one requirement, and click on "start". If the classification proposed by the AI (see the "Answer text area") suits you, you can click on "Apply response" to update the opened requirement diagram.

### Generate block diagrams
Provide a system specification. The AI will compute a liste of blocks (including their atributes and signals), and will connect them. Once the AI has finished computed a block diagram, click on "apply" to get the block diagram drawn in TTool

### Generate state machine diagrams
Provide a system specification, and select in TTool a block diagram. The AI will compute states machines. Once computed, click on "apply" to get the state machines drawn in TTool (TTool duplicated the block diagram to be certain not to erase your former state machines).


### A(I)MULET

This option enables to automatically generate AMULET commands for modifying AVATAR models (AMULET is a language enabling the description of AVATAR
models mutations - e.g., adding a state in a state-machine diagram, changing the parameters of a signal, removing a block, etc.). Make sure to
follow these steps:
1. Open an AVATAR model (design).
2. Perform a syntax checking.
3. Write your request in the AI window, for instance "add four integer attributes to [name of a block in your model] with names of Crocodilia or
   Zebra species."
4. If the mutation list generated by the AI suits you, click on "Apply response" to update the opened AVATAR model.

### CAPECs

To use CAPECs, you may need to position the path to the python version you wish to use. For instance:
```xml
<PythonPathForCapecs data="/usr/bin/python3.10" />
```

