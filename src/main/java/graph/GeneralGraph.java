/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

/**
 * Class GeneralGraph
 * Creation : 19/07/2014
 * * @version 1.0 19/07/2024
 *
 * @author Ludovic APVRILLE
 */
public class GeneralGraph  {

    public ArrayList<GeneralState> states;
    public Object referenceObject;



    public GeneralGraph() {
        states = new ArrayList<>();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for(GeneralState st: states) {
            sb.append(st.toString() + "\n");
        }
        return sb.toString();
    }

    public static GeneralGraph makeGeneralGraph(AUTGraph graph, ArrayList<AUTState> toBeRemoved) {

        GeneralGraph gg = new GeneralGraph();

        // Making states
        HashMap<AUTState, GeneralState> mapOfStates = new HashMap<>();
        for(AUTState state: graph.getStates()) {
            if(!toBeRemoved.contains(state)) {
                GeneralState gs = new GeneralState(state.info);
                gs.referenceObject = state.referenceObject;
                gs.makeAllTags(state.tags);
                gg.states.add(gs);
                mapOfStates.put(state, gs);
            }
        }

        // Making transitions
        for(AUTState state: graph.getStates()) {
            if(!toBeRemoved.contains(state)) {
                for (AUTTransition trout : state.outTransitions) {
                    // We need to know if the destination was removed or not
                    AUTState destinationState = graph.states.get(trout.destination);
                    if(!toBeRemoved.contains(destinationState)) {
                        GeneralState stO = mapOfStates.get(state);
                        GeneralState stD = mapOfStates.get(destinationState);
                        if ((stO != null) && (stD != null)) {
                            GeneralTransition gt = new GeneralTransition(stO, trout.transition, stD);
                            gt.makeAllTags(trout.tags);
                            stO.outTransitions.add(gt);
                            stD.inTransitions.add(gt);
                        }
                    }
                }
            }
        }

        return gg;
    }

    public AUTGraph makeAUTGraph() {
        ArrayList<AUTState> autStates = new ArrayList<>() ;
        ArrayList<AUTTransition> autTransitions = new ArrayList<>() ;

        int id = 0;
        for(GeneralState state: states) {
            AUTState newSt = new AUTState(id);
            newSt.info = state.info;
            newSt.makeAllTags(state.tags);
            newSt.referenceObject = state.referenceObject;
            autStates.add(newSt);
            id++;
        }

        id = 0;
        for(GeneralState state: states) {
            for(GeneralTransition trOut: state.outTransitions) {
                AUTState state1 = autStates.get(id);
                AUTState state2 = autStates.get(states.indexOf(trOut.destination));
                AUTTransition newTr = new AUTTransition(id, trOut.transition, states.indexOf(trOut.destination));
                newTr.makeAllTags(trOut.tags);
                state1.addOutTransition(newTr);
                state2.addInTransition(newTr);
                autTransitions.add(newTr);
            }
            id ++;
        }

        AUTGraph autg = new AUTGraph(autStates, autTransitions);
        return autg;
    }


}
