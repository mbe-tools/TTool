/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package avatartranslator;

import avatartranslator.intboolsolver.AvatarIBSolver;
import myutil.NameChecker;
import myutil.TraceManager;
import myutil.intboolsolver.IBSParamComp;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;


/**
 * Class AvatarDataType
 * Creation: 31/05/2024
 *
 * @author Ludovic APVRILLE
 */
public class AvatarDataType extends AvatarElement implements NameChecker.NameStartWithUpperCase, IBSParamComp {

    private List<AvatarAttribute> attributes;


    public AvatarDataType(String _name, Object _referenceObject) {
        super(_name, _referenceObject);
        TraceManager.addDev("DataType created with name: " + _name);
        attributes = new LinkedList<AvatarAttribute>();
    }


    public void clearAttributes() {
        attributes.clear();
    }


    public List<AvatarAttribute> getAttributes() {
        return attributes;
    }


    public void addAttribute(AvatarAttribute _aa) {
        if (getAvatarAttributeWithName(_aa.getName()) == null) {
            //TraceManager.addDevStackTrace("Adding attribute " + _aa.getName() + " to block " + getName());
            attributes.add(_aa);
        }
    }


    public String toString() {
        //Thread.currentThread().dumpStack();
        StringBuffer sb = new StringBuffer("DataType:" + getName()  + " \n");

        for (AvatarAttribute attribute : attributes) {
            sb.append("  attribute: " + attribute.toString()  + "\n");
        }

        return sb.toString();
    }

    public String toStringWithID() {
        //Thread.currentThread().dumpStack();
        StringBuffer sb = new StringBuffer("DataType:" + getName() + " ID=" + getID() + " \n");

        for (AvatarAttribute attribute : attributes) {
            sb.append("  attribute: " + attribute.toString() + " ID=" + attribute.getID() + "\n");
        }

        return sb.toString();
    }

    public String toStringRecursive() {

        return toString();
    }



    public String toShortString() {
        return getName();
    }

    public int attributeNb() {
        return attributes.size();
    }


    public AvatarAttribute getAttribute(int _index) {
        return attributes.get(_index);
    }

    public boolean setAttributeValue(int _index, String _value) {
        AvatarAttribute aa = attributes.get(_index);
        if (aa == null) {
            return false;
        }
        aa.setInitialValue(_value);
        return true;
    }

    public int getIndexOfAvatarAttributeWithName(String _name) {
        int cpt = 0;
        for (AvatarAttribute attribute : attributes) {
            if (attribute.getName().compareTo(_name) == 0) {
                return cpt;
            }
            cpt++;
        }
        return -1;
    }


    /**
     * Look for an attribute with the provided name.
     *
     * @param _name The name of the attribute to look for.
     * @return The attribute if found, or null otherwise
     */
    public AvatarAttribute getAvatarAttributeWithName(String _name) {
        if ((attributes == null) || (_name == null)) {
            return null;
        }
        for (AvatarAttribute attribute : attributes) {
            if (attribute.getName().compareTo(_name) == 0) {
                return attribute;
            }
        }
        return null;
    }



    public NameChecker.NamedElement[] getSubNamedElements() {
        NameChecker.NamedElement[] nes = new NameChecker.NamedElement[attributes.size()];
        int index = 0;
        for (AvatarAttribute aa : attributes) {
            nes[index] = aa;
            index++;
        }
        return nes;
    }

    public AvatarDataType advancedClone() {
        AvatarDataType adt = new AvatarDataType(getName(), getReferenceObject());
        for(AvatarAttribute aa: attributes) {
            adt.addAttribute(aa.advancedClone(null));
        }
        return adt;
    }





}
