/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 * 
 * ludovic.apvrille AT enst.fr
 * 
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package avatartranslator.avatarucd;

import avatartranslator.AvatarElement;
import avatartranslator.AvatarSpecification;
import myutil.Conversion;
import myutil.NameChecker;
import myutil.TraceManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

/**
   * Class AvatarUseCaseDiagram
   * Creation: 01/03/2024
   * @author Ludovic APVRILLE
 */
public class AvatarUseCaseDiagram extends AvatarElement {

    private ArrayList<AvatarActor> actors;
    private ArrayList<AvatarUseCase> useCases;
    private ArrayList<AvatarUCDConnection> connections;

    public AvatarUseCaseDiagram(String _name, Object _referenceObject) {
        super(_name, _name);
        actors = new ArrayList<>();
        useCases = new ArrayList<>();
        connections = new ArrayList<>();
    }

    public ArrayList<String> makeActorsFromJson(String _json) {
        ArrayList<String> errors = new ArrayList<>();

        if (_json == null) {
            errors.add("No \"actors\" array in json");
            return errors;
        }
        int indexStart = _json.indexOf('{');
        int indexStop = _json.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            errors.add("Invalid JSON object (start or stop)");
            return errors;
        }

        _json = _json.substring(indexStart, indexStop + 1);

        //TraceManager.addDev("Cut spec: " + _spec);

        JSONObject mainObject = new JSONObject(_json);
        JSONArray actorsJ = mainObject.getJSONArray("actors");

        if (actorsJ == null) {
            TraceManager.addDev("No \"actors\" array in json");
            errors.add("No \"actors\" array in json");
            return errors;
        }

        for (int i = 0; i < actorsJ.length(); i++) {
            JSONObject actor = actorsJ.getJSONObject(i);
            String actorName = actor.getString("name");
            if (actorName == null) {
                errors.add("Actor #" + i + " has no name");
            } else {
                actorName = Conversion.replaceAllString(actor.getString("name").trim(), " ", "");
                actors.add(new AvatarActor(actorName, null));
            }
        }


        return errors;
    }

    public ArrayList<String> makeUseCasesFromJson(String _json) {
        ArrayList<String> errors = new ArrayList<>();

        if (_json == null) {
            errors.add("No \"usecases\" array in json");
            return errors;
        }
        int indexStart = _json.indexOf('{');
        int indexStop = _json.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            errors.add("Invalid JSON object (start or stop)");
            return errors;
        }

        _json = _json.substring(indexStart, indexStop + 1);

        //TraceManager.addDev("Cut spec: " + _spec);

        JSONObject mainObject = new JSONObject(_json);
        JSONArray useCasesJ = mainObject.getJSONArray("usecases");

        if (useCasesJ == null) {
            TraceManager.addDev("No \"usecase\" array in json");
            errors.add("No \"usecase\" array in json");
            return errors;
        }

        for (int i = 0; i < useCasesJ.length(); i++) {
            JSONObject useCase = useCasesJ.getJSONObject(i);
            String useCaseName = useCase.getString("name");
            if (useCaseName == null) {
                errors.add("Usecase #" + i + " has no name");
            } else {
                useCaseName = Conversion.replaceAllString(useCase.getString("name").trim(), " ", "");
                useCases.add(new AvatarUseCase(useCaseName, null));
            }
        }


        return errors;
    }


    public void addActor(AvatarActor aa) {
        actors.add(aa);
    }

    public void addUseCase(AvatarUseCase auc) {
        useCases.add(auc);
    }

    public void addConnection(AvatarUCDConnection aucdc) {
        connections.add(aucdc);
    }




    public ArrayList<AvatarActor> getActors() {
        return actors;
    }

    public ArrayList<AvatarUseCase> getUseCases() {
        return useCases;
    }

    public ArrayList<AvatarUCDConnection> getConnections() {
        return connections;
    }

    public ArrayList<String> makeConnectionsFromJson(String _json) {
        ArrayList<String> errors = new ArrayList<>();

        if (_json == null) {
            errors.add("No \"usecases\" array in json");
            return errors;
        }
        int indexStart = _json.indexOf('{');
        int indexStop = _json.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            errors.add("Invalid JSON object (start or stop)");
            return errors;
        }

        _json = _json.substring(indexStart, indexStop + 1);

        //TraceManager.addDev("Cut spec: " + _spec);

        JSONObject mainObject = new JSONObject(_json);
        JSONArray useCasesJ = mainObject.getJSONArray("connections");

        if (useCasesJ == null) {
            TraceManager.addDev("No \"connections\" array in json");
            errors.add("No \"connections\" array in json");
            return errors;
        }

        for (int i = 0; i < useCasesJ.length(); i++) {
            JSONObject useCase = useCasesJ.getJSONObject(i);
            String elt1 = useCase.getString("element1");
            if (elt1 == null) {
                errors.add("element1 #" + i + " does not exist");
            } else {
                elt1 = Conversion.replaceAllString(elt1.trim(), " ", "");
                String elt2 = useCase.getString("element2");
                if (elt2 == null) {
                    errors.add("element2 #" + i + " does not exist");
                } else {
                    elt2 = Conversion.replaceAllString(elt2.trim(), " ", "");
                    // We add the connection
                    try {
                        addConnection(elt1, elt2, true);
                    } catch (AvatarUseCaseDiagramException aucde) {
                        errors.add(aucde.getMessage());
                    }
                }
            }
        }

        // Check that all actors are connected to at least one use case
        for(AvatarActor aa: actors) {
            boolean found = false;
            for(AvatarUCDConnection conn: connections) {
                if (conn.has(aa)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                errors.add("Actor \"" + aa.getName() + "\" must be connected to at least one use case");
            }
        }

        // Only one connection between the same elements
        AvatarUCDConnection c1, c2;
        for(int i=0; i<connections.size(); i++) {
            c1 = connections.get(i);
            for(int j=i+1; j<connections.size(); j++) {
                c2 = connections.get(j);
                if (c1.hasSameElementsAs(c2)) {
                    errors.add(c1.getName() + " and " + c2.getName() + " are connected with multiple connections: use only one connection between " +
                            "the" +
                            " same elements");
                }
            }
        }

        return errors;
    }

    public void addAllActors(String _actors) {
        for(String s: _actors.split(" ")) {
            actors.add(new AvatarActor(s, null));
        }
    }

    public void addAllUseCases(String _usecases) {
        for(String s: _usecases.split(" ")) {
            useCases.add(new AvatarUseCase(s, null));
        }
    }

    public String getActorNames() {
        StringBuffer sb = new StringBuffer();
        for(AvatarActor actor: actors) {
            if (sb.length() == 0) {
                sb.append(actor.getName());
            } else {
                sb.append(" " + actor.getName());
            }
        }



        return sb.toString();
    }

    public String getUseCaseNames() {
        StringBuffer sb = new StringBuffer();
        for(AvatarUseCase useCase: useCases) {
            if (sb.length() == 0) {
                sb.append(useCase.getName());
            } else {
                sb.append(" " + useCase.getName());
            }
        }

        return sb.toString();
    }

    public String getConnectionsNames() {
        StringBuffer sb = new StringBuffer();
        for(AvatarUCDConnection conn: connections) {
            if (sb.length() != 0) {
                sb.append(" ");
            }
            sb.append(conn.type.label + "(" + conn.origin.getName() + "," + conn.destination.getName() + ")");

        }

        return sb.toString();
    }



    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("actors: " + getActorNames() + "\n");
        sb.append("Use cases: " + getUseCaseNames() + "\n");
        sb.append("Connections: " + getConnectionsNames() + "\n");

        return sb.toString();
    }

    public AvatarActor getActorByName(String _name) {
        for(AvatarActor aa: actors) {
            if (aa.getName().equals(_name)) {
                return aa;
            }
        }
        return null;
    }

    public AvatarUseCase getUseCaseByName(String _name) {
        for(AvatarUseCase auc: useCases) {
            if (auc.getName().equals(_name)) {
                return auc;
            }
        }
        return null;
    }


    public AvatarUCDConnection addConnection(String nameOrigin, String nameDestination, boolean force) throws AvatarUseCaseDiagramException {
        AvatarActor actor1 = getActorByName(nameOrigin);
        AvatarActor actor2 = getActorByName(nameDestination);

        if ((actor1 != null) && (actor2 != null)) {
            throw new AvatarUseCaseDiagramException("Connection between two actors is not supported");
        }

        if ((actor1 == null) && (actor2 == null)) { // between use cases
            AvatarUseCase uc1 = getUseCaseByName(nameOrigin);
            AvatarUseCase uc2 = getUseCaseByName(nameDestination);

            if (!force) {
                if (uc1 == null) {
                    throw new AvatarUseCaseDiagramException("Use case \"" + nameOrigin + "\" is invalid");
                }
                if (uc2 == null) {
                    throw new AvatarUseCaseDiagramException("Use case \"" + nameDestination + "\" is invalid");
                }
            }

            if (uc1 == null) {
                uc1 = new AvatarUseCase(nameOrigin, null);
                useCases.add(uc1);
            }

            if (uc2 == null) {
                uc2 = new AvatarUseCase(nameDestination, null);
                useCases.add(uc2);
            }

            AvatarUCDConnection connection = new AvatarUCDConnection("con " + uc1.getName() + " - " + uc2.getName(), null);
            connection.origin = uc1;
            connection.destination = uc2;
            connection.type = AvatarUCDConnection.CONNECTION.INCLUDE;
            connections.add(connection);
            return connection;


        } else { // Between one actor and one use case
            AvatarUseCase useCase;
            if (actor1 == null) {
                useCase = getUseCaseByName(nameOrigin);
            } else {
                useCase = getUseCaseByName(nameDestination);
            }

            if (useCase == null) {
                if (!force) {
                    throw new AvatarUseCaseDiagramException("Connection between one actor and an unknown use case is not supported");
                }
                if (actor1 == null) {
                    useCase = new AvatarUseCase(nameOrigin, null);
                } else {
                    useCase = new AvatarUseCase(nameDestination, null);
                }
                useCases.add(useCase);
            }

            AvatarActor aa = actor1;
            if (actor1 == null) {
                aa = actor2;
            }

            AvatarUCDConnection connection = new AvatarUCDConnection("con " + aa.getName() + " - " + useCase.getName(), null);
            connection.origin = aa;
            connection.destination = useCase;
            connection.type = AvatarUCDConnection.CONNECTION.WITH_ACTOR;
            connections.add(connection);
            return connection;

        }

    }





}
