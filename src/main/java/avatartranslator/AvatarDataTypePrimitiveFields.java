/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package avatartranslator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class AvatarDataTypePrimitiveFields: sequence of primitive fields that characterizes a datatype.
 *
 * Such datatype extension can only be produced through the buildMap method that takes a list
 * of datatype as input and unroll the nested ones to build the lists of primitive fields.
 * The result is a map that links each datatype with its extension.
 *
 * Creation: 20/06/2024
 * @version 1.0 20/06/2024
 * @author Sophie Coudert
 */
public class AvatarDataTypePrimitiveFields {
    private List<AvatarAttribute> fields;

    private AvatarDataTypePrimitiveFields() {
        fields = new ArrayList<AvatarAttribute>();
    }

    public int size() { return fields.size(); }

    private boolean addField (String name, AvatarType type) {
        if(name == null || type == null) return false;
        for(AvatarAttribute f : fields) if (f.getName().equals(name)) return false;
        fields.add(new AvatarAttribute(name, type, null, null));
        return true;
    }

    public List<AvatarAttribute> getFields() { return fields; }

    public String getName(int i) { return fields.get(i).getName(); }

    public AvatarType getType(int i) { return fields.get(i).getType(); }


    /**  unroll the nested datatype among the one provided as input to build the lists of primitive fields associated
     * to each datatype.
     * The returned result is a map that links each datatype with its primitive fields extension.
     */
    public static LinkedHashMap<AvatarDataType, AvatarDataTypePrimitiveFields> buildMap(List<AvatarDataType> dtList, List<String> err) {
        LinkedHashMap<AvatarDataType, AvatarDataTypePrimitiveFields> map = new LinkedHashMap<AvatarDataType, AvatarDataTypePrimitiveFields>();
        for(AvatarDataType dt : dtList) buildPrimitiveFields(dt, map, new ArrayList<AvatarAttribute>(), err);
        return map;
    }

    // IMPLEMENTATION

    /**
     * unroll the fields of datatype dt to build its associated list of primitive fields.
     * @param dt the datatype to unroll
     * @param map the mapping of datatypes to their unrolled version, i.e. primitive fields
     * @param l the list of containing datatype fields (ancestors when unrolling). Technical: to detect recursive datatypes.
     * @param err to put encountered errors.
     */
    private static void buildPrimitiveFields (AvatarDataType dt, LinkedHashMap<AvatarDataType, AvatarDataTypePrimitiveFields> map,
                                       List<AvatarAttribute> l, List<String> err) {

        if (map.get(dt) != null) return; // the job is already done (function may be called directly or recursively for nested datatype)

        AvatarDataTypePrimitiveFields primitiveFields = new AvatarDataTypePrimitiveFields(); // create primitive-field-list structure

        AvatarAttribute aa; // to iterate on datatype original fields
        int size = dt.attributeNb();
        for(int i = 0; i < size; i++) { // iterate: keep primitive-typed and extend datatype-typed (nested -> recursive call)
            aa = dt.getAttributes().get(i);
            if (aa.getType() == AvatarType.INTEGER) // atomic primitive : keep unchanged
                primitiveFields.addField(aa.getName(), AvatarType.INTEGER);
            else if (aa.getType() == AvatarType.BOOLEAN)  // atomic primitive : keep unchanged
                primitiveFields.addField(aa.getName(), AvatarType.BOOLEAN);
            else if (aa.getType() == AvatarType.TIMER) { // forbidden
                if (err != null) err.add("type timer unauthorized for field " + aa.getName() + " in datatype " + dt.getName() +
                        "(set to integer)");
                // recover
                primitiveFields.addField(aa.getName(), AvatarType.INTEGER);
            }
            else { // datatype
                AvatarDataType adt = aa.getDataType();
                // verify that datatype is well-defined
                if (adt == null) {
                    if (err != null) err.add("type of field " + aa.getName() + " in datatype " + dt.getName() + " is undefined (set to integer)");
                    primitiveFields.addField(aa.getName(), AvatarType.INTEGER);
                    continue;
                }
                // detect recursive datatype
                int bug;
                int len = l.size();
                for(bug = 0; bug < len; bug++) { // search datatype in ancestor fields types
                    if (l.get(bug).getDataType() == adt) break;
                }
                if (bug != len) { // datatype found => error: recursive datatypes not allowed

                    // builds the textual representation of the detected cycle to provide a precise error message
                    StringBuffer path = new StringBuffer();
                    bug++;
                    int endpath = l.size();
                    while(bug < endpath)  { path.append(l.get(bug).getName() + "."); bug++; }
                    path.append(aa.getName());

                    // register error message
                    if (err != null) err.add("datatype " + adt.getName() + " is recursive through field " + path.toString() + " (set to integer)");

                    // recover by breaking the cycle
                    primitiveFields.addField(aa.getName(), AvatarType.INTEGER);
                    continue;
                }

                // unrolling nested datatype

                l.add(aa); // update ancestor list before a nested call
                buildPrimitiveFields(adt, map, l, err); // nested call to build adt's primitive fields (if not already done).
                l.remove(l.size() - 1); // restore ancestor list after a nested call

                // extend aa w.r.t adt's primitive fields
                AvatarDataTypePrimitiveFields fields = map.get(adt);
                for (int j = 0; j < fields.size(); j++) {
                    primitiveFields.addField(aa.getName() + "__" + fields.getName(j), fields.getType(j));
                }
            }
        }
        // register the new "datatype -> primitive-fields" link.
        map.put(dt, primitiveFields);
    }
}

