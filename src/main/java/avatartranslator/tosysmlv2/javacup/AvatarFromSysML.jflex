/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 package avatartranslator.tosysmlv2;
import java_cup.runtime.*;
import java_cup.runtime.ComplexSymbolFactory.*;
import java.util.HashSet;
import avatartranslator.tosysmlv2.AvatarFromSysMLSyntax.*;

%%

%class LexerClassName
%public
%unicode
%cup
%line
%column
%eofval{
   return makeSymbol(#Symb.EOF);
%eofval}

%yylexthrow Exception

%{  /** create a symbol provided with current lexeme and location */
    ComplexSymbol makeSymbol(int id, Object val) {
        // System.out.print(" ##" + #Symb.terminalNames[id] + "-" + val + " " + "(" + "l:" + yyline + ",c:" + yycolumn + ")");
        return new ComplexSymbol(
            #Symb.terminalNames[id],
            id,
            new Location(yyline, yycolumn),
            new Location(yyline, yycolumn + yytext().length()),
            val);
    }
    /** create a symbol provided with current lexeme and location */
    ComplexSymbol makeSymbol(int id) {
        // System.out.print(" ##" + #Symb.terminalNames[id] + "(" + "l:" + yyline + ",c:" + yycolumn + ")");
        return new ComplexSymbol(
            #Symb.terminalNames[id],
            id,
            new Location(yyline, yycolumn),
            new Location(yyline, yycolumn + yytext().length()));
    }

    /** to memorize error location before throwing an exception */
    Location errorLocation;

    /** memorize error location */
    public void setErrorLocation(Location _errorLocation) { errorLocation = _errorLocation; }

    /** get memorized error location */
    public Location getErrorLocation() { return errorLocation; }

    /** get current location. To allow parser to know location without lexeme (i.e. through empty grammar rule) */
    public Location getLocation() { return new Location(yyline, yycolumn); }
%}
%init{
  // there is initially no error
    errorLocation = null;
%init}
Space = [\ \n\r\t\f]
Natural = 0 | [1-9][0-9]*
Real = {Natural} "." 0* {Natural}

AvIdent = ("_"[a-zA-Z0-9] | [a-zA-Z])   ("_"[a-zA-Z0-9] | [a-zA-Z0-9])*    ("_")?
dataTypeName = "'@dt:" {AvIdent} "'"
blockName = "'@blk:" {AvIdent} "'"
attributeName = "'$" {AvIdent} "'"
fieldName = "'" {AvIdent} "'"
relationName = ("'@BF" | "'@NBF" | "'@SYN") {Natural} ":" {AvIdent} "-" {AvIdent} "'"
channelPreName = {AvIdent} "." {AvIdent} (">"|"<") {AvIdent} "." {AvIdent}
signalName = "'@sig:" {AvIdent} "'"
channelName = ("'@syn" | "'@bf" | "'@nbf") ":" {channelPreName} "'"
messageName = "'@MSG:" {AvIdent} "." {AvIdent} "'"
startStateName = "'@st:start'"
stopStateName = "'@st:stop'"
standardStateName = "'@st:standard." {AvIdent} "'"
choiceStateName = "'@st:standard.choice__" [0-9]* "'"
randomStateName = "'@st:random." {Natural} "'"
countStateName = "'@st:count." {AvIdent} "." {Natural} "'"
sendStateName = "'@st:send." {AvIdent} "." {Natural} "'"
receiveStateName = "'@st:receive." {AvIdent} "." {Natural} "'"
presendStateName = "'@st:presend." {AvIdent} "." {Natural} "'"
prereceiveStateName = "'@st:prereceive." {AvIdent} "." {Natural} "'"
setTimerStateName = "'@st:set." {AvIdent} "." {Natural} "'"
resetTimerStateName = "'@st:reset." {AvIdent} "." {Natural} "'"
expireTimerStateName = "'@st:expire." {AvIdent} "." {Natural} "'"
presetTimerStateName = "'@st:preset." {AvIdent} "." {Natural} "'"
preresetTimerStateName = "'@st:prereset." {AvIdent} "." {Natural} "'"
preexpireTimerStateName = "'@st:preexpire." {AvIdent} "." {Natural} "'"
timerBlockName = "'@tmr:" {AvIdent} "'"


%state COMMENTLINE
%state IMPORTITEM

%%

<COMMENTLINE> {
 [^\n\r]+                   { break; }
 "\n"                       { yybegin(YYINITIAL); break; }
}
<IMPORTITEM> {
 [^;]+                   { break; }
 "\;"                       { yybegin(YYINITIAL); break; }
}

<YYINITIAL> {
 {Space}        { break; }
 "//"           { yybegin(COMMENTLINE); break; }
 {Natural}      { try { return makeSymbol(#Symb.INT, Integer.parseInt(yytext())); }
                  catch (NumberFormatException nfe) {
                    setErrorLocation(new Location(yyline, yycolumn));
                    throw new Exception ("lexing : bad format for number : " + yytext()); }
                }
 {Real}         { try { return makeSymbol(#Symb.REAL, Double.parseDouble(yytext())); }
                  catch (NumberFormatException nfe) {
                    setErrorLocation(new Location(yyline, yycolumn));
                    throw new Exception ("lexing : bad format for number : " + yytext()); }
                }

  "+"            { return makeSymbol(#Symb.PLUS); }
  "-"            { return makeSymbol(#Symb.MINUS); }
  "*"            { return makeSymbol(#Symb.MULT); }
  "/"            { return makeSymbol(#Symb.DIV); }
  "%"            { return makeSymbol(#Symb.MOD); }
  "&"            { return makeSymbol(#Symb.AND); }
  "|"            { return makeSymbol(#Symb.OR); }
  "!="           { return makeSymbol(#Symb.DIF); }
  "<"            { return makeSymbol(#Symb.LT); }
  ">"            { return makeSymbol(#Symb.GT); }
  "<="           { return makeSymbol(#Symb.LEQ); }
  ">="           { return makeSymbol(#Symb.GEQ); }

  "?"            { return makeSymbol(#Symb.QUEST); }

 "=="            { return makeSymbol(#Symb.EQ); }
 "true"          { return makeSymbol(#Symb.BOOL, Boolean.TRUE); }
 "false"         { return makeSymbol(#Symb.BOOL, Boolean.FALSE); }
  "("            { return makeSymbol(#Symb.LPAR); }
  ")"            { return makeSymbol(#Symb.RPAR); }
  "{"            { return makeSymbol(#Symb.LBRAC); }
  "}"            { return makeSymbol(#Symb.RBRAC); }
  ","            { return makeSymbol(#Symb.COMMA); }
  ";"            { return makeSymbol(#Symb.SEMICOLON); }
  ":"            { return makeSymbol(#Symb.COLON); }
  "."            { return makeSymbol(#Symb.POINT); }
  "::"           { return makeSymbol(#Symb.FOURPOINTS); }
  "="            { return makeSymbol(#Symb.EQUAL); }
  ":="           { return makeSymbol(#Symb.GETS); }
  [\"][^\"]*[\"]          { String s = yytext();
                      return makeSymbol(#Symb.STRING, s.substring(1, s.length() - 1)); }
  "'@max_size'"     { return makeSymbol(#Symb.MAXSIZE_F); }
  "'@private'"      { return makeSymbol(#Symb.PRIVATE_F); }
  "'@relation'"     { return makeSymbol(#Symb.RELATION_F); }
  "'@block1'"       { return makeSymbol(#Symb.BLOCK1_F); }
  "'@block2'"       { return makeSymbol(#Symb.BLOCK2_F); }
  "'@block'"       { return makeSymbol(#Symb.BLOCK_F); }
  "'@pool'"         { return makeSymbol(#Symb.POOL_F); }
  "'@request'"      { return makeSymbol(#Symb.REQUEST_F); }
  "'@state_action'" { return makeSymbol(#Symb.STATEACTION_F); }
  "'@index'"        { return makeSymbol(#Symb.INDEX_F); }
  "'@delay'"        { return makeSymbol(#Symb.DELAY_F); }
  "'@payload'"      { return makeSymbol(#Symb.PAYLOAD_F); }
  "'@channel'"      { return makeSymbol(#Symb.CHANNEL_F); }
  "'@value'"        { return makeSymbol(#Symb.VALUE_F); }
  "'@statemachine'" { return makeSymbol(#Symb.STATEMACHINE); }
  "'@set'"          { return makeSymbol(#Symb.SET_TM); }
  "'@reset'"        { return makeSymbol(#Symb.RESET_TM); }
  "'@expire'"       { return makeSymbol(#Symb.EXPIRE_TM); }
  "'@distributionLaw'" { return makeSymbol(#Symb.DISTRIBUTION_A); }
  "'@weight'"      { return makeSymbol(#Symb.WEIGHT_A); }

  "'#Relation'"    { return makeSymbol(#Symb.RELATION_T); }
  "'#Sync_Rel'"    { return makeSymbol(#Symb.SYNCREL_T); }
  "'#Bfifo_Rel'"   { return makeSymbol(#Symb.BFIFOREL_T); }
  "'#NBfifo_Rel'"  { return makeSymbol(#Symb.NBFIFOREL_T); }
  "'#InMessage'"   { return makeSymbol(#Symb.INMESSAGE_T); }
  "'#OutMessage'"  { return makeSymbol(#Symb.OUTMESSAGE_T); }
  "'#Channel'"     { return makeSymbol(#Symb.CHANNEL_T); }
  "'#Fifo'"        { return makeSymbol(#Symb.FIFO_T); }
  "'#Bfifo'"       { return makeSymbol(#Symb.BFIFO_T); }
  "'#NBfifo'"      { return makeSymbol(#Symb.NBFIFO_T); }
  "'#Sync'"        { return makeSymbol(#Symb.SYNC_T); }
  "'#InSignalBinding'"  { return makeSymbol(#Symb.INSIGNALBINDING_T); }
  "'#OutSignalBinding'" { return makeSymbol(#Symb.OUTSIGNALBINDING_T); }
  "'#InJunkSignal'"     { return makeSymbol(#Symb.INJSIGNAL_T); }
  "'#OutJunkSignal'"    { return makeSymbol(#Symb.OUTJSIGNAL_T); }

  "'start'"               { return makeSymbol(#Symb.START_U); }
  "'done'"                { return makeSymbol(#Symb.DONE_U); }
  "start"                 { return makeSymbol(#Symb.START_U); }
  "done"                  { return makeSymbol(#Symb.DONE_U); }
  "'@amount'"             { return makeSymbol(#Symb.AMOUNT_U); }
  "'#NOKrequest'"         { return makeSymbol(#Symb.NOKREQUEST_T); }
  "'#TrivialRequest'"     { return makeSymbol(#Symb.TRIVIALREQUEST_T); }
  "'#immediate_request'"  { return makeSymbol(#Symb.IMMEDIATEREQUEST_U); }
  "'#SendRequest'"        { return makeSymbol(#Symb.SENDREQUEST_T); }
  "'#ReceiveRequest'"     { return makeSymbol(#Symb.RECEIVEREQUEST_T); }
  "'@msg'"                { return makeSymbol(#Symb.MSG_U); }
  "'#else'"               { return makeSymbol(#Symb.ELSEGUARD); }

  "'#AvatarDataType'"  { return makeSymbol(#Symb.AVDATATYPE_T); }
  "'#AvatarBlock'"  { return makeSymbol(#Symb.AVBLOCK_T); }
  "'#AvatarVoidMethod'"  { return makeSymbol(#Symb.AVVOIDMETHOD_T); }
  "'#AvatarCalcMethod'"  { return makeSymbol(#Symb.AVCALCMETHOD_T); }
  "'#AvatarStandardState'"  { return makeSymbol(#Symb.AVSTANDARDSTATE_T); }
  "'#AvatarStopState'"  { return makeSymbol(#Symb.AVSTOPSTATE_T); }
  "'#AvatarStartState'"  { return makeSymbol(#Symb.AVSTARTSTATE_T); }
  "'#AvatarSendState'"  { return makeSymbol(#Symb.AVSENDSTATE_T); }
  "'#AvatarReceiveState'"  { return makeSymbol(#Symb.AVRECEIVESTATE_T); }
  "'#AvatarRandomState'"  { return makeSymbol(#Symb.AVRANDOMSTATE_T); }
  "'#AvatarCountState'"  { return makeSymbol(#Symb.AVCOUNTSTATE_T); }
  "'#AvatarPreSendState'"  { return makeSymbol(#Symb.AVPRESENDSTATE_T); }
  "'#AvatarPreReceiveState'"  { return makeSymbol(#Symb.AVPRERECEIVESTATE_T); }
  "'#AvatarTransition'"  { return makeSymbol(#Symb.AVTRANSITION_T); }
  "'#AvatarStateMachine'"  { return makeSymbol(#Symb.AVSTATEMACHINE_T); }
  "'#ReceiveAction'"  { return makeSymbol(#Symb.RECEIVEACTION_T); }
  "'#TransitionAction'"  { return makeSymbol(#Symb.TRANSITIONACTION_T); }

  "'#AvatarSetTimerState'"  { return makeSymbol(#Symb.AVSETTIMERSTATE_T); }
  "'#AvatarResetTimerState'"  { return makeSymbol(#Symb.AVRESETTIMERSTATE_T); }
  "'#AvatarExpireTimerState'"  { return makeSymbol(#Symb.AVEXPIRETIMERSTATE_T); }
  "'#AvatarSetTimerRequest'"  { return makeSymbol(#Symb.SETTIMERREQUEST_T); }
  "'#AvatarResetTimerRequest'"  { return makeSymbol(#Symb.RESETTIMERREQUEST_T); }
  "'#AvatarExpireTimerRequest'"  { return makeSymbol(#Symb.EXPIRETIMERREQUEST_T); }
  "'#TimerSetMsg'"  { return makeSymbol(#Symb.TIMERSETMSG_T); }
  "'#TimerResetMsg'"  { return makeSymbol(#Symb.TIMERRESETMSG_T); }
  "'#TimerExpireMsg'"  { return makeSymbol(#Symb.TIMEREXPIREMSG_T); }
  "'#AvatarTimer'"  { return makeSymbol(#Symb.AVTIMER_T); }

  "'#Assignment'"  { return makeSymbol(#Symb.ASSIGNMENT_T); }
  "'#bound_random'"  { return makeSymbol(#Symb.BOUNDRANDOM_U); }
  "Integer"          { return makeSymbol(#Symb.INTEGER_T); }
  "Boolean"          { return makeSymbol(#Symb.BOOLEAN_T); }
  "String"           { return makeSymbol(#Symb.STRING_T); }
  "Real"             { return makeSymbol(#Symb.REAL_T); }

  "about"            { return makeSymbol(#Symb.ABOUT); }
  "abstract"         { return makeSymbol(#Symb.ABSTRACT); }
  "accept"           { return makeSymbol(#Symb.ACCEPT); }
  "action"           { return makeSymbol(#Symb.ACTION); }
  "actor"            { return makeSymbol(#Symb.ACTOR); }
  "after"            { return makeSymbol(#Symb.AFTER); }
  "alias"            { return makeSymbol(#Symb.ALIAS); }
  "all"              { return makeSymbol(#Symb.ALL); }
  "allocate"         { return makeSymbol(#Symb.ALLOCATE); }
  "allocation"       { return makeSymbol(#Symb.ALLOCATION); }
  "analysis"         { return makeSymbol(#Symb.ANALYSIS); }
  "and"              { return makeSymbol(#Symb.AND); }
  "as"               { return makeSymbol(#Symb.AS); }
  "assert"           { return makeSymbol(#Symb.ASSERT); }
  "assign"           { return makeSymbol(#Symb.ASSIGN); }
  "assoc"            { return makeSymbol(#Symb.ASSOC); }
  "assume"           { return makeSymbol(#Symb.ASSUME); }
  "at"               { return makeSymbol(#Symb.AT); }
  "attribute"        { return makeSymbol(#Symb.ATTRIBUTE); }
  "bind"             { return makeSymbol(#Symb.BIND); }
  "binding"          { return makeSymbol(#Symb.BINDING); }
  "block"            { return makeSymbol(#Symb.BLOCK); }
  "by"               { return makeSymbol(#Symb.BY); }
  "calc"             { return makeSymbol(#Symb.CALC); }
  "case"             { return makeSymbol(#Symb.CASE); }
  "comment"          { return makeSymbol(#Symb.COMMENT); }
  "concern"          { return makeSymbol(#Symb.CONCERN); }
  "connect"          { return makeSymbol(#Symb.CONNECT); }
  "connection"       { return makeSymbol(#Symb.CONNECTION); }
  "constraint"       { return makeSymbol(#Symb.CONSTRAINT); }
  "decide"           { return makeSymbol(#Symb.DECIDE); }
  "def"              { return makeSymbol(#Symb.DEF); }
  "default"          { return makeSymbol(#Symb.DEFAULT); }
  "defined"          { return makeSymbol(#Symb.DEFINED); }
  "dependency"       { return makeSymbol(#Symb.DEPENDENCY); }
  "derived"          { return makeSymbol(#Symb.DERIVED); }
  "do"               { return makeSymbol(#Symb.DO); }
  "doc"              { return makeSymbol(#Symb.DOC); }
  "else"             { return makeSymbol(#Symb.ELSE); }
  "end"              { return makeSymbol(#Symb.END); }
  "entry"            { return makeSymbol(#Symb.ENTRY); }
  "enum"             { return makeSymbol(#Symb.ENUM); }
  "event"            { return makeSymbol(#Symb.EVENT); }
  "exhibit"          { return makeSymbol(#Symb.EXHIBIT); }
  "exit"             { return makeSymbol(#Symb.EXIT); }
  "expose"           { return makeSymbol(#Symb.EXPOSE); }
  "filter"           { return makeSymbol(#Symb.FILTER); }
  "first"            { return makeSymbol(#Symb.FIRST); }
  "flow"             { return makeSymbol(#Symb.FLOW); }
  "for"              { return makeSymbol(#Symb.FOR); }
  "fork"             { return makeSymbol(#Symb.FORK); }
  "frame"            { return makeSymbol(#Symb.FRAME); }
  "from"             { return makeSymbol(#Symb.FROM); }
  "hastype"          { return makeSymbol(#Symb.HASTYPE); }
  "if"               { return makeSymbol(#Symb.IF); }
  "implies"          { return makeSymbol(#Symb.IMPLIES); }
  "import"           { yybegin(IMPORTITEM); return makeSymbol(#Symb.IMPORT); }
  "in"               { return makeSymbol(#Symb.IN); }
  "include"          { return makeSymbol(#Symb.INCLUDE); }
  "individual"       { return makeSymbol(#Symb.INDIVIDUAL); }
  "inout"            { return makeSymbol(#Symb.INOUT); }
  "interface"        { return makeSymbol(#Symb.INTERFACE); }
  "istype"           { return makeSymbol(#Symb.ISTYPE); }
  "item"             { return makeSymbol(#Symb.ITEM); }
  "join"             { return makeSymbol(#Symb.JOIN); }
  "language"         { return makeSymbol(#Symb.LANGUAGE); }
  "loop"             { return makeSymbol(#Symb.LOOP); }
  "merge"            { return makeSymbol(#Symb.MERGE); }
  "message"          { return makeSymbol(#Symb.MESSAGE); }
  "metadata"         { return makeSymbol(#Symb.METADATA); }
  "nonunique"        { return makeSymbol(#Symb.NONUNIQUE); }
  "not"              { return makeSymbol(#Symb.NOT); }
  "objective"        { return makeSymbol(#Symb.OBJECTIVE); }
  "occurrence"       { return makeSymbol(#Symb.OCCURRENCE); }
  "of"               { return makeSymbol(#Symb.OF); }
  "or"               { return makeSymbol(#Symb.OR); }
  "ordered"          { return makeSymbol(#Symb.ORDERED); }
  "out"              { return makeSymbol(#Symb.OUT); }
  "package"          { return makeSymbol(#Symb.PACKAGE); }
  "parallel"         { return makeSymbol(#Symb.PARALLEL); }
  "part"             { return makeSymbol(#Symb.PART); }
  "perform"          { return makeSymbol(#Symb.PERFORM); }
  "port"             { return makeSymbol(#Symb.PORT); }
  "private"          { return makeSymbol(#Symb.PRIVATE); }
  "protected"        { return makeSymbol(#Symb.PROTECTED); }
  "public"           { return makeSymbol(#Symb.PUBLIC); }
  "readonly"         { return makeSymbol(#Symb.READONLY); }
  "redefines"        { return makeSymbol(#Symb.REDEFINES); }
  "ref"              { return makeSymbol(#Symb.REF); }
  "references"       { return makeSymbol(#Symb.REFERENCES); }
  "render"           { return makeSymbol(#Symb.RENDER); }
  "rendering"        { return makeSymbol(#Symb.RENDERING); }
  "rep"              { return makeSymbol(#Symb.REP); }
  "require"          { return makeSymbol(#Symb.REQUIRE); }
  "requirement"      { return makeSymbol(#Symb.REQUIREMENT); }
  "return"           { return makeSymbol(#Symb.RETURN); }
  "satisfy"          { return makeSymbol(#Symb.SATISFY); }
  "self"             { return makeSymbol(#Symb.SELF); }
  "send"             { return makeSymbol(#Symb.SEND); }
  "snapshot"         { return makeSymbol(#Symb.SNAPSHOT); }
  "specializes"      { return makeSymbol(#Symb.SPECIALIZES); }
  ":>"               { return makeSymbol(#Symb.REFINES); }
  "stakeholder"      { return makeSymbol(#Symb.STAKEHOLDER); }
  "state"            { return makeSymbol(#Symb.STATE); }
  "subject"          { return makeSymbol(#Symb.SUBJECT); }
  "subsets"          { return makeSymbol(#Symb.SUBSETS); }
  "succession"       { return makeSymbol(#Symb.SUCCESSION); }
  "then"             { return makeSymbol(#Symb.THEN); }
  "timeslice"        { return makeSymbol(#Symb.TIMESLICE); }
  "to"               { return makeSymbol(#Symb.TO); }
  "transition"       { return makeSymbol(#Symb.TRANSITION); }
  "until"            { return makeSymbol(#Symb.UNTIL); }
  "use"              { return makeSymbol(#Symb.USE); }
  "variant"          { return makeSymbol(#Symb.VARIANT); }
  "variation"        { return makeSymbol(#Symb.VARIATION); }
  "verification"     { return makeSymbol(#Symb.VERIFICATION); }
  "verify"           { return makeSymbol(#Symb.VERIFY); }
  "via"              { return makeSymbol(#Symb.VIA); }
  "view"             { return makeSymbol(#Symb.VIEW); }
  "viewpoint"        { return makeSymbol(#Symb.VIEWPOINT); }
  "when"             { return makeSymbol(#Symb.WHEN); }
  "while"            { return makeSymbol(#Symb.WHILE); }
  "xor"              { return makeSymbol(#Symb.XOR); }
  "null"             { return makeSymbol(#Symb.NULL); }

  {dataTypeName}              { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.DATATYPENAME,yytext())); }
  {blockName}                 { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.BLOCKNAME,yytext())); }
  {attributeName}             { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.ATTRIBUTENAME,yytext())); }
  {relationName}              { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.RELATIONNAME,yytext())); }
  {channelPreName}            { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.CHANNELPRENAME,yytext())); }
  {signalName}                { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.SIGNALNAME,yytext())); }
  {channelName}               { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.CHANNELNAME,yytext())); }
  {messageName}               { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.MESSAGENAME,yytext())); }
  {startStateName}            { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.CHANNELPRENAME,yytext())); }
  {stopStateName}             { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.STARTSTATENAME,yytext())); }
  {choiceStateName}           { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.STANDARDSTATENAME,yytext())); }
  {standardStateName}         { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.STANDARDSTATENAME,yytext())); }
  {randomStateName}           { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.RANDOMSTATENAME,yytext())); }
  {countStateName}            { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.COUNTSTATENAME,yytext())); }
  {sendStateName}             { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.SENDSTATENAME,yytext())); }
  {receiveStateName}          { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.RECEIVESTATENAME,yytext())); }
  {presendStateName}          { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.PRESENDSTATENAME,yytext())); }
  {prereceiveStateName}       { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.PRERECEIVESTATENAME,yytext())); }
  {setTimerStateName}         { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.SETTIMERSTATENAME,yytext())); }
  {resetTimerStateName}       { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.RESETTIMERSTATENAME,yytext())); }
  {expireTimerStateName}      { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.EXPIRETIMERSTATENAME,yytext())); }
  {presetTimerStateName}      { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.PRESETTIMERSTATENAME,yytext())); }
  {preresetTimerStateName}    { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.PRERESETTIMERSTATENAME,yytext())); }
  {preexpireTimerStateName}   { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.PREEXPIRETIMERSTATENAME,yytext())); }
  {timerBlockName}            { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.TIMERBLOCKNAME,yytext())); }
  {fieldName}                 { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.QUOTEDNAME,yytext())); }
  {AvIdent}                   { return makeSymbol(#Symb.IDENT, new Ident(AvatarFromSysMLSyntax.AVATARNAME,yytext())); }
  "'"[^']+"'"                  { String s = yytext();
                                return makeSymbol(#Symb.ANYNAME, s.substring(1, s.length() - 1)); }
 . { setErrorLocation(new Location(yyline, yycolumn));
     throw new Exception ("unexpected character in  expression"); }
}


