/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package avatartranslator.tosysmlv2;
import java_cup.runtime.*;
import static avatartranslator.tosysmlv2.Avatar2SysMLNames.*;

parser code {:
:}


/* Terminals (tokens returned by the scanner). */
terminal            PLUS, MINUS, MULT, DIV, MOD, AND, OR, EQ, DIF, LT, GT, LEQ, GEQ;
terminal            NOT, LPAR, RPAR, UMINUS, COMMA;
terminal String     IDENT, CONSTANT;


/* Non terminals */
non terminal String expr, exprlist;

/* Precedences (probably useless for the current grammar) */
precedence left OR;
precedence left AND;
precedence left NOT;
precedence left EQ, DIF;
precedence nonassoc GT, LT, LEQ, GEQ;
precedence left PLUS, MINUS;
precedence left MULT, DIV, MOD;
precedence left UMINUS;


/* The grammar */
expr    ::=
     expr:l PLUS expr:r {: RESULT = l + " + " + r; :}
   | expr:l MINUS expr:r {: RESULT = l + " - " + r; :}
   | expr:l MULT expr:r {: RESULT = l + " * " + r; :}
   | expr:l DIV expr:r {: RESULT = l + " / " + r; :}
   | expr:l MOD expr:r {: RESULT = l + " % " + r; :}
   | MINUS expr:e {: RESULT = "-" + e; :} %prec UMINUS
   | expr:l OR expr:r {: RESULT = l + " or " + r; :}
   | expr:l AND expr:r {: RESULT = l + " and " + r; :}
   | NOT expr:e {: RESULT = "not " + e; :}
   | expr:l LT expr:r {: RESULT = l + " < " + r; :}
   | expr:l GT expr:r {: RESULT = l + " > " + r; :}
   | expr:l LEQ expr:r {: RESULT = l + " <= " + r; :}
   | expr:l GEQ expr:r {: RESULT = l + " >= " + r; :}
   | expr:l EQ expr:r {: RESULT = l + " == " + r; :}
   | expr:l DIF expr:r {: RESULT = l + " != " + r; :}
   | LPAR expr:e RPAR {: RESULT = "(" + e + ")"; :}
   | IDENT:i LPAR RPAR {: RESULT = methodSysMLname(i) + "()"; :}
   | IDENT:i LPAR exprlist:l RPAR {: RESULT = methodSysMLname(i) + "(" + l + ")"; :}
   | CONSTANT:e {: RESULT = e; :}
   | IDENT:e {: RESULT = attributeSysMLname(e); :}
;
exprlist ::=
    expr:e  {: RESULT = e; :}
   | expr:e COMMA exprlist:l {: RESULT = e + ", " + l; :}
;
