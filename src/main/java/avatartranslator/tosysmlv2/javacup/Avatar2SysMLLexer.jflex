/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package avatartranslator.tosysmlv2;
import java_cup.runtime.*;

%%
%class LexerClassName
%public
%unicode
%cup
%eofval{
   return new java_cup.runtime.Symbol(#Symb.EOF);
%eofval}

%yylexthrow Exception

%{

%}

Space = [\ \n\r\t\f]
Natural = 0 | [1-9][0-9]*
Identifier = [a-zA-Z_][a-zA-Z0-9_\.]*

%%

<YYINITIAL> {
 {Space}        { break; }
 "true"         { return new Symbol(#Symb.CONSTANT, "true"); }
 "false"        { return new Symbol(#Symb.CONSTANT, "false"); }
 {Natural}      { try { return new Symbol(#Symb.CONSTANT, "" + Integer.parseInt(yytext()));}
                  catch (NumberFormatException nfe) { throw new Exception ("Lexer : Integer Format : " + yytext()); }
                }
 "+"            { return new Symbol(#Symb.PLUS); }
 "-"            { return new Symbol(#Symb.MINUS); }
 "*"            { return new Symbol(#Symb.MULT); }
 "/"            { return new Symbol(#Symb.DIV); }
 "%"            { return new Symbol(#Symb.MOD); }
 "&&"           { return new Symbol(#Symb.AND); }
 "and"          { return new Symbol(#Symb.AND); }
 "||"           { return new Symbol(#Symb.OR); }
 "or"           { return new Symbol(#Symb.OR); }
 "!"            { return new Symbol(#Symb.NOT); }
 "not"          { return new Symbol(#Symb.NOT); }
 "=="           { return new Symbol(#Symb.EQ); }
 "!="           { return new Symbol(#Symb.DIF); }
 "<"            { return new Symbol(#Symb.LT); }
 ">"            { return new Symbol(#Symb.GT); }
 "<="           { return new Symbol(#Symb.LEQ); }
 ">="           { return new Symbol(#Symb.GEQ); }
 "("            { return new Symbol(#Symb.LPAR); }
 ")"            { return new Symbol(#Symb.RPAR); }
 ","            { return new Symbol(#Symb.COMMA); }
 {Identifier}   { return new Symbol(#Symb.IDENT, yytext()); }
 . { throw new Exception ("Unknown character in  expression"); }
}
