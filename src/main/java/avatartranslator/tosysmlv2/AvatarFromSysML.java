/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * Class AvatarFromSysML
 * Creation: 20/06/2024
 *
 * @author Sophie Coudert
 * @version 0.1 20/06/2024
 */
package avatartranslator.tosysmlv2;

import java.io.FileReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.BiConsumer;

import avatartranslator.*;
import avatartranslator.tosysmlv2.AvatarFromSysMLSyntax.*;
import static java_cup.runtime.ComplexSymbolFactory.*;
import java_cup.runtime.ComplexSymbolFactory;
import myutil.TraceManager;

//import static avatartranslator.AvatarTransition.*;

/** Class AvatarFromSysML
 * Creation: 20/06/2024
 *
 * @author Sophie Coudert
 * @version 0.1 20/06/2024
 *
 * Building Avatar Models from Their SysML V2 description. This Class exports the main function that does this. Technically,
 * the class implements the last step of this process, i.e. it builds Avatar models from abstract syntax trees build by AvatarSysMLParser.
 */
public class AvatarFromSysML {
    /** the build Avatar specification, returned at the end of the building process */
    private AvatarSpecification avSpec;
    /** the source abstract syntax tree, obtained through a call to the parser */
    private StxModel stxSpec;
    /** maps signal syntactic elements to their associated avatar signal by the building process */
    private LinkedHashMap<StxSignal, AvatarSignal> signalMap;
    /** maps junk signal syntactic elements to their associated avatar signal by the building process */
    private LinkedHashMap<StxJunkSignal, AvatarSignal> junkSignalMap;
    /** maps block syntactic elements to their associated avatar block by the building process */
    private LinkedHashMap<StxBlock, AvatarBlock> blockMap;
    /** maps state syntactic elements to their associated avatar state-machine element by the building process (when exists) */
    private LinkedHashMap<StxState, AvatarStateMachineElement> stateMap;
    /** maps Avatar state-machine elements to their associated set of incoming transitions and their block.
     * <p>filled while handling blocks. Used thereafter to check some well-formedness constraints such as reachability... </p> */
    private LinkedHashMap<AvatarStateMachineElement, StateTransitions> transitionMap;
    /** maps Avatar datatypes extracted from the SysML sourse to their sequence of primitive fields */
    private LinkedHashMap<AvatarDataType, AvatarDataTypePrimitiveFields> primitiveFieldsMap;
    /** all errors encountered while parsing and building model from parsing-returned abstract syntax tree */
    private List<AvatarFromSysMLError> errors;

    public AvatarFromSysML(){
        avSpec = null;
        stxSpec = null;
        signalMap = new LinkedHashMap<StxSignal, AvatarSignal>();
        junkSignalMap = new LinkedHashMap<StxJunkSignal, AvatarSignal>();
        blockMap = new LinkedHashMap<StxBlock, AvatarBlock>();
        stateMap = new LinkedHashMap<StxState, AvatarStateMachineElement>();
        transitionMap = new LinkedHashMap<AvatarStateMachineElement, StateTransitions>();
        errors = null;
    }

    /** register an error encountered while parsing and building model from parsing-returned abstract syntax tree */
    private void addError(AvatarFromSysMLError _err) { errors.add(_err); }
    /** get the lis of errors encountered while parsing and building a model */
    public List<AvatarFromSysMLError> getErrors() { return errors; }
    
    /** to memorize the set of transitions of a state and memorize its block with them */
    private class StateTransitions {
        private AvatarBlock block;
        private List<AvatarTransition> transitions;
        public StateTransitions (AvatarBlock _block, List<AvatarTransition> _transitions) {
            block = _block;
            transitions = _transitions;
        }
        public AvatarBlock getBlock() { return block; }
        public List<AvatarTransition> getTransitions() { return transitions; }
    }

    /** Builds an Avatar Specification from an Avatar SysML V2 description provided in a file.
     *  If errors are found, they are printed using TraceManager.addError before returning
     */
    public AvatarSpecification sysMLFiletoSpec(String _fileName) {
        errors = new ArrayList<AvatarFromSysMLError>();
        // Initialize parser
        AvatarFromSysMLParser parser;
        try { parser =
                new AvatarFromSysMLParser(new AvatarFromSysMLLexer(new FileReader(_fileName)),
                        new ComplexSymbolFactory()); }
        catch (java.lang.Exception e) {
            //e.printStackTrace(out);
            String errorMsg = "cannot initialize parser from file " + _fileName;
            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, errorMsg));
            TraceManager.addError(errorMsg);
            return null;
        }
        return sysMLtoSpec(parser);
    }
    /** Builds an Avatar Specification from an Avatar SysML V2 description provided in a string.
     *  If errors are found, they are printed using TraceManager.addError before returning
     */
    public AvatarSpecification sysMLStringtoSpec(String _string) {
        errors = new ArrayList<AvatarFromSysMLError>();
        // Initialize parser
        AvatarFromSysMLParser parser;
        try {
            parser =
                    new AvatarFromSysMLParser(new AvatarFromSysMLLexer(new StringReader(_string)),
                            new ComplexSymbolFactory());
        } catch (java.lang.Exception e) {
            //e.printStackTrace(out);
            String errorMsg = "cannot initialize parser from string ";
            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, errorMsg));
            TraceManager.addError(errorMsg);
            return null;
        }
        return sysMLtoSpec(parser);
    }
    private AvatarSpecification sysMLtoSpec(AvatarFromSysMLParser parser) {
        // Run parser and get parser errors
        TraceManager.addDev("Parsing Model");
        stxSpec = parser.parseModel();
        if (stxSpec == null) {
            for(AvatarFromSysMLError e : parser.getErrors())
                TraceManager.addError(e.toString());
            errors = parser.getErrors();
            return null;
        }
        errors = stxSpec.getErrors();

        // Build Specification from parser-returned abstract syntax tree.
        try {
            TraceManager.addDev("Building Specification");
            avSpec = new AvatarSpecification("FromSysMLV2_SPECIFICATION", null);
            signalMap.clear();
            junkSignalMap.clear();
            blockMap.clear();
            stateMap.clear();
            TraceManager.addDev("Building DataTypes");
            buildDataTypes();
            TraceManager.addDev("Building Blocks");
            buildBlocks();
            TraceManager.addDev("Building Relations");
            buildRelations();
        }
        catch (Exception ex) {
            for(AvatarFromSysMLError e : errors)
                TraceManager.addError(e.toString());
            ex.printStackTrace();
            TraceManager.addError("Building failed with exception " + ex.toString());
            return null;
        }
        // TODO: move error handling
        boolean hasError = false;
        for(AvatarFromSysMLError e : errors){
            if (e.getLevel() > AvatarFromSysMLError.WARNING)
                hasError = true;
            TraceManager.addError(e.toString());
        }
        if (hasError) return null;
        else return avSpec;
    }

    /** initialize a state entry in the map that associates outgoing transitions to state-machine elements */
    private void addState(AvatarStateMachineElement e, AvatarBlock b) {
        if (transitionMap.get(e) == null){
            ArrayList<AvatarTransition> l = new ArrayList<AvatarTransition>();
            transitionMap.put(e, new StateTransitions(b, l));
        }
    }

    /** add an outgoing transition to a state in the map that associates outgoing transitions to state-machine elements */
    private void addTransition(AvatarTransition t, AvatarStateMachineElement e) {
        transitionMap.get(e).getTransitions().add(t);
    }

    /** get the Avatar block associated to a block syntactic element. Create it if necessary (and create entry in the map) */
    private AvatarBlock getBlock(StxBlock _b) {
        AvatarBlock b = blockMap.get(_b);
        if (b == null) {
            b = new AvatarBlock(_b.getName(), avSpec, null);
            blockMap.put(_b, b);
        }
        return b;
    }

    /** get the Avatar signal associated to a signal syntactic element. Create it if necessary (and create entry in the map) */
    private AvatarSignal getSignal(StxSignal _b) {
        AvatarSignal b = signalMap.get(_b);
        if (b == null) {
            if (_b.isInput())
                b = new AvatarSignal(_b.getName(), AvatarSignal.IN, null);
            else
                b = new AvatarSignal(_b.getName(), AvatarSignal.OUT, null);
            signalMap.put(_b, b);
        }
        return b;
    }

    /** get the Avatar signal associated to a signal syntactic element. Create it if necessary (and create entry in the map) */
    private AvatarSignal getJunkSignal(StxJunkSignal _b) {
        AvatarSignal b = junkSignalMap.get(_b);
        if (b == null) {
            if (_b.isin())
                b = new AvatarSignal(_b.getName(), AvatarSignal.IN, null);
            else
                b = new AvatarSignal(_b.getName(), AvatarSignal.OUT, null);
            junkSignalMap.put(_b, b);
        }
        return b;
    }

    /** get the basic attribute associated to a name (possibly with "__") in a block */
    private AvatarAttribute getAttributeByName(String _name, AvatarBlock _block) {
        List<AvatarAttribute> l = _block.getAttributes();
        int size = l.size();
        for (int i = 0; i < size; i++)
            if(l.get(i).getName().equals(_name)) return  l.get(i);
        return null;
    }

    /** get the basic attribute associated to a timer in a block */
    private AvatarAttribute getTimerByName(String _name, AvatarBlock _block) {
        AvatarAttribute res =  getAttributeByName(_name, _block);
        if (res != null && res.getType() == AvatarType.TIMER) return res;
        return null;
    }

    /** get the Avatar state-machine element associated to a state syntactic element. Create it if necessary (and create entry in the map)
     *
     * <p> The creation process may use different state-machine element constructors (sub-classes) depending on the type of
     * the syntactic state.</p>
     * <p> States that have been technically added (STXPRESENDSTATEs and STXPRERECEIVESTATEs) are not handled as they have no correspondant
     * state-machine element in the Avatar Model.</p>
     * :*/
    private AvatarStateMachineElement getState(StxState _s, AvatarBlock _b) {
        AvatarStateMachineElement s = stateMap.get(_s);
        if (s == null) {
            switch(_s.getType()) {
                case AvatarFromSysMLSyntax.STXSTARTSTATE :
                    s = new AvatarStartState("StartState", null, _b);
                    break;
                case AvatarFromSysMLSyntax.STXSTANDARDSTATE :
                    s = new AvatarState(_s.getName(), null, _b);
                    break;
                case AvatarFromSysMLSyntax.STXRANDOMSTATE :
                    s = new AvatarRandom(_s.getName(), null, _b);
                    break;
                case AvatarFromSysMLSyntax.STXCOUNTSTATE :
                    s = new AvatarQueryOnSignal(_s.getName(), getSignal(_s.getSignal()),
                            getAttributeByName(_s.getVariable().replaceAll("\\.", "__"),_b), null, _b);
                    break;
                case AvatarFromSysMLSyntax.STXSTOPSTATE :
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.BUG, null,
                            "adding stop state (AvatarFromSysML.getState)", null));
                    break;
                case AvatarFromSysMLSyntax.STXSENDSTATE :
                case AvatarFromSysMLSyntax.STXRECEIVESTATE :
                    s = new AvatarActionOnSignal(_s.getName(), null, null, _b);
                    break;
                case AvatarFromSysMLSyntax.STXPRESENDSTATE :
                case AvatarFromSysMLSyntax.STXPRERECEIVESTATE :
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.BUG, null,
                            "adding precom state (AvatarFromSysML.getState)", null));
                    break;
                case AvatarFromSysMLSyntax.STXSETTIMERSTATE :
                    s = new AvatarSetTimer(_s.getName(), null, _b);
                    break;
                case AvatarFromSysMLSyntax.STXRESETTIMERSTATE :
                    s = new AvatarResetTimer(_s.getName(), null, _b);
                    break;
                case AvatarFromSysMLSyntax.STXEXPIRETIMERSTATE :
                    s = new AvatarExpireTimer(_s.getName(), null, _b);
            }
            if (s != null) stateMap.put(_s, s);
        }
        return s;
    }

    // BUILDING DATATYPES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    /** To be run on the (filled by parsing) maps that associates syntactic datatypes to their names
     *
     *  <p> This creates-and-adds-to-avSpec the AvatarDataType corresponding to the syntactic datatype. </p>
     *  <p> datatypes associated to datatype-typed fields cannot be set as here, the complete list
     *  of datatypes (which is required for this) is not
     *  known. Thus this setting is delayed.</p>
     *  <p> To make this delayed setting easier, two informations are collected while iterating on entries:</p>
     *  <ul>
     *      <li> a mapping of the attribute to be later completed to the name of their expected datatype </li>
     *      <li> a mapping of the datatype names to the build AvatarDataTypes </li>
     *  </ul>
     * */
    private class BuildDataType implements BiConsumer<String, StxDataType> {
        private LinkedHashMap<String,AvatarDataType> dataTypeMap;
        private LinkedHashMap<AvatarAttribute,String> attributeMap;

        /** parameters of the constructor are the maps to fill while iterating on entries */
        public BuildDataType(LinkedHashMap<String,AvatarDataType> _dataTypeMap, LinkedHashMap<AvatarAttribute,String> _attributeMap) {
            dataTypeMap = _dataTypeMap;
            attributeMap = _attributeMap;
        }

        /** create and add the datatype to avSpec. updates dataTypeMap, and attributeMap for each encountered datatype-typed field. */
        public void accept(String n, StxDataType d) {
            AvatarDataType dataType = new AvatarDataType(d.getName(),null);
            avSpec.addDataType(dataType);
            dataTypeMap.put(d.getName(), dataType);
            int nbFields = d.getSize();
            for (int i = 0; i < nbFields; i++) {
                StxField f = d.getField(i);
                String type =  f.getType();
                AvatarAttribute aa;
                if(type.equals("Integer"))
                    aa = new AvatarAttribute(f.getName(),AvatarType.INTEGER,null,null);
                else if(type.equals("Boolean"))
                    aa = new AvatarAttribute(f.getName(),AvatarType.BOOLEAN,null,null);
                else {
                    aa = new AvatarAttribute(f.getName(), AvatarType.UNDEFINED, null, null);
                    attributeMap.put(aa, type);
                }
                String initVal = f.getInitialValue().trim();
                if(f.getDefaultValue() && initVal.equals(aa.getType().getDefaultInitialValue()))
                    aa.setInitialValue("");
                else aa.setInitialValue(initVal);

                dataType.addAttribute(aa);
            }
        }
    }

    /** to be run on the map that maps datatype-typed datatype fields to the name of their typing datatype. Using the (to-constructor)
     * provided map from names to Avatar datatypes, accept find the associated datatype and update the field with it. */
    private class CompleteDataTypeAttribute implements BiConsumer<AvatarAttribute,String> {
        LinkedHashMap<String,AvatarDataType> dataTypeMap;
        public CompleteDataTypeAttribute(LinkedHashMap<String,AvatarDataType> _dataTypeMap) { dataTypeMap = _dataTypeMap;}

        public void accept(AvatarAttribute aa, String dt) {
            AvatarDataType adt = dataTypeMap.get(dt); // find the associated datatype
            if (adt == null) {
                errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, "datatype " + dt + " in datatype field " +
                        aa.getName() + " is undefined"));
                aa.setType(AvatarType.INTEGER); // should not be used...
            }
            else aa.setDataType(adt);} // update the field with it
    }

    /** fills avSpec's list of Avatar datatypes from stxSpec's list of syntactic datatypes. Then, computes and updates primitiveFieldsMap.
     *
     * <p> Process with two steps:</p>
     * <ul>
     *     <li> invoke BuildDataType to build and add the datatypes, but not set the datatype attribute of datatype-typed fields. Memorize
     *     information to do this later </li>
     *     <li> invoke CompleteDataTypeAttribute to do this delayed job with provided information: a mapping of these fields to the name of their
     *     datatype type, and a mapping of these datatype names to their associated AvatarDataType structure.
     *     </li>
     * </ul>
     * <p> A mapping of all datatypes to their extended list of basic fields is then computed and assigned to primitiveFieldsMap </p>
     * <p> During this process, errors may be collected </p>
     */
    private void buildDataTypes(){
        List<AvatarDataType> dtList = avSpec.getDataTypes(); // the list to fill
        LinkedHashMap<String,AvatarDataType> dataTypeMap = new LinkedHashMap<String,AvatarDataType>();
        LinkedHashMap<AvatarAttribute,String> attributeMap = new LinkedHashMap<AvatarAttribute,String>();
        stxSpec.getDataTypeMap().forEach(new BuildDataType(dataTypeMap, attributeMap));
        attributeMap.forEach(new CompleteDataTypeAttribute(dataTypeMap));

        List<String> err = new ArrayList<String>();
        primitiveFieldsMap = AvatarDataTypePrimitiveFields.buildMap(dtList, err);
        for(String e : err) errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR,e));
    }


    // BUILDING BLOCKS (but not statemachines) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    /** To be run on the (filled by parsing) maps that associates block names to syntactic blocks.
     *
     * <p> this create-and-add-to-avSpec all the corresponding AvatarBlocks and fills their static declarative part (attributes, signals, methods)
     * but not their state-machine. Building state-machines requires the static declarations of all hierarchically containing blocks to be up to date
     * (to find used signals) and thus is delayed after the iteration of BuildBlock.
     * </p>
     */
    private class BuildBlock implements BiConsumer<String, StxBlock> {
        public BuildBlock(){}
        public void accept(String n, StxBlock blk) {
            if (!blk.isDeclared()) {
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR,
                        "block " + blk.getName() +" has been used but has not been declared"));
                return;
            }

            // create and add the block...................................... !!! TO ADAPT FOR LIBRARIES
            AvatarBlock theBlock = getBlock(blk); // create
            avSpec.addBlock(theBlock); // add

            // set father...................................... !!! TO suppress FOR LIBRARIES
            StxBlock fth = blk.getFather();
            if (fth != null) theBlock.setFather(getBlock(fth));

            // add Attributes......................................
            int size = blk.getNbAttributes();
            for (int i = 0; i < size; i++) {

                StxAttribute a = blk.getAttribute(i);
                String initVal = a.getInit().trim();
                AvatarAttribute aa; // for original attributes
                AvatarAttribute ax; // for extended basic attributes

                if (a.getType().equals("Integer")) {
                    aa = new AvatarAttribute(a.getName(), AvatarType.INTEGER, theBlock, null);
                    ax = new AvatarAttribute(a.getName(), AvatarType.INTEGER, theBlock, null);
                    if(a.getDefaultValue() && initVal.equals(AvatarType.INTEGER.getDefaultInitialValue()))
                        initVal = "";
                    aa.setInitialValue(initVal);
                    ax.setInitialValue(initVal);
                    theBlock.addAttribute(ax);
                    ax.setAsConstant(false);
                }
                else if (a.getType().equals("Boolean")) {
                    aa = new AvatarAttribute(a.getName(), AvatarType.BOOLEAN, theBlock, null);
                    ax = new AvatarAttribute(a.getName(), AvatarType.BOOLEAN, theBlock, null);
                    if(a.getDefaultValue() && initVal.equals(AvatarType.BOOLEAN.getDefaultInitialValue()))
                        initVal = "";
                    aa.setInitialValue(initVal);
                    ax.setInitialValue(initVal);
                    theBlock.addAttribute(ax);
                    ax.setAsConstant(false);
                }
                else {
                    aa = new AvatarAttribute(a.getName(), AvatarType.UNDEFINED, theBlock, null);
                    AvatarDataType adt = avSpec.getDataTypeByName(a.getType());
                    if(adt == null) {
                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, a.getLeft(),
                                "datatype " + a.getType() + " of attribute " + a.getName() + " does not exist"));
                    }
                    else {
                        aa.setDataType(adt);
                        String aaName = a.getName() + "__";
                        AvatarDataTypePrimitiveFields primitiveFields = primitiveFieldsMap.get(adt);
                        // add basic attributes corresponding of the extension of the datatype
                        if (primitiveFields == null) {
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.BUG, a.getLeft(),
                                    "datatype " + a.getType() + " of attribute " + a.getName() + "cannot be extended"));
                        } else {
                            int dtsize = primitiveFields.size();
                            for (int j = 0; j < dtsize; j++) {
                                ax = new AvatarAttribute(aaName + primitiveFields.getName(j),
                                        primitiveFields.getType(j), theBlock, null);
                                theBlock.addAttribute(ax);
                                ax.setAsConstant(false);
                            }
                        }
                    }
                }
                theBlock.addOriginalAttribute(aa);
                aa.setAsConstant(false);
            }

            // add Constants......................................
            size = blk.getNbConstants();
            for (int i = 0; i < size; i++) {
                StxAttribute a = blk.getConstant(i);
                String initVal = a.getInit().trim();
                AvatarAttribute aa = null;
                AvatarAttribute ax;
                if (a.getType().equals("Integer")) {
                    aa = new AvatarAttribute(a.getName(), AvatarType.INTEGER, theBlock, null);
                    theBlock.addOriginalAttribute(aa);
                    aa.setAsConstant(true);
                    ax = new AvatarAttribute(a.getName(), AvatarType.INTEGER, theBlock, null);
                    theBlock.addAttribute(ax);
                    ax.setAsConstant(true);
                    if(a.getDefaultValue() && initVal.equals(AvatarType.INTEGER.getDefaultInitialValue()))
                        initVal = "";
                    aa.setInitialValue(initVal);
                    ax.setInitialValue(initVal);
                }
                else if (a.getType().equals("Boolean")) {
                    aa = new AvatarAttribute(a.getName(), AvatarType.BOOLEAN, theBlock, null);
                    theBlock.addOriginalAttribute(aa);
                    aa.setAsConstant(true);
                    ax = new AvatarAttribute(a.getName(), AvatarType.BOOLEAN, theBlock, null);
                    theBlock.addAttribute(ax);
                    ax.setAsConstant(true);
                    if(a.getDefaultValue() && initVal.equals(AvatarType.BOOLEAN.getDefaultInitialValue()))
                        initVal = "";
                    aa.setInitialValue(initVal);
                    ax.setInitialValue(initVal);
                }
                else
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR,
                            a.getLeft(), "datatype for constants is not supported", null));
            }

            // add Timers......................................
            size = blk.getNbTimers();
            for (int i = 0; i < size; i++) {
                AvatarAttribute aa = new AvatarAttribute(blk.getTimer(i).getName(), AvatarType.TIMER, theBlock, null);
                theBlock.addOriginalAttribute(aa);
                aa.setAsConstant(false);
                aa = new AvatarAttribute(blk.getTimer(i).getName(), AvatarType.TIMER, theBlock, null);
                theBlock.addAttribute(aa);
                aa.setAsConstant(false);
           }

            // add Methods......................................
            size = blk.getNbMethods();
            for (int i = 0; i < size; i++) {

                StxMethod sm = blk.getMethod(i);
                 // create method
                AvatarMethod am = new AvatarMethod(sm.getName(),null);
                 // add method
                theBlock.addMethod(am);
                // add input parameters to method
                buildProfile(sm, am, theBlock);

                // handle return type
                String returnType = sm.getReturnType();

                if (returnType == null || returnType.length() == 0) continue; // no return type, void method

                if (returnType.equals("Integer")) { // return integer
                    am.addReturnParameter(new AvatarAttribute("return__0", AvatarType.INTEGER, theBlock, null));
                    am.addOriginalReturnParameter(new AvatarAttribute("return__", AvatarType.INTEGER, theBlock, null));
               }
                else if (returnType.equals("Boolean")) { // return boolean
                    am.addReturnParameter(new AvatarAttribute("return__0", AvatarType.BOOLEAN, theBlock, null));
                    am.addOriginalReturnParameter(new AvatarAttribute("return__", AvatarType.BOOLEAN, theBlock, null));
                }
               else { // return datatype
                    // find datatype
                    AvatarDataType adt = avSpec.getDataTypeByName(returnType);
                    if (adt == null) {
                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, sm.getLeft(),
                                "unknown datatype " + returnType + "for method return parameter", null));
                        AvatarAttribute ret = new AvatarAttribute("return__", AvatarType.INTEGER, theBlock, null);
                        am.addOriginalReturnParameter(ret);
                        am.addReturnParameter(ret);
                    }
                    else {
                        AvatarAttribute ret = new AvatarAttribute("return__", AvatarType.UNDEFINED, theBlock, null);
                        // set datatype
                        ret.setDataType(adt);
                        am.addOriginalReturnParameter(ret);
                        // extend datatype to a list of primitive basic fields
                        AvatarDataTypePrimitiveFields primitiveFields = primitiveFieldsMap.get(adt);
                        if (primitiveFields == null)
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.BUG, sm.getLeft(),
                                "datatype " + returnType + "for method return parameter cannot be extended", null));
                        else {
                            int dtsize = primitiveFields.size();
                            for (int j = 0; j < dtsize; j++) {
                                ret = new AvatarAttribute("return__" + j,
                                        primitiveFields.getType(j), theBlock, null);
                                am.addReturnParameter(ret);
                            }
                        }
                    }
                }
            }

            // add Signals ..............................
            size = blk.getNbSignals();
            for (int i = 0; i < size; i++) {
                StxSignal ss = blk.getSignal(i);
                AvatarSignal as = getSignal(ss); // create signal
                if (!ss.isDeclared()) {
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, ss.getLeft(),
                            "signal " + theBlock.getName() + "." + ss.getName() + " has not been declared"));
                    continue;
                }
                theBlock.addSignal(as); // add signal
                StxChannel ch = ss.getChannel();
                if (ch == null) {
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, ss.getLeft(),
                            "channel of signal " + ss.getName() + " has not been set"));
                    continue;
                }
                if (!ch.isDeclared()) {
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, ss.getLeft(),
                            "channel of signal " + theBlock.getName() + "." + ss.getName() + " has not been declared"));
                    continue;
                }
                if (ss.isInput()) {
                    StxInMessage im = ss.getChannel().getInProfile(); // profile is channel's in profile
                    if (im == null) {
                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, ss.getLeft(),
                                "input profile of signal " + ss.getName() + " has not been set"));
                        continue;
                    }
                    buildProfile(im, as, theBlock); // add input signal parameters
                }
                else {
                    StxOutMessage om = ss.getChannel().getOutProfile(); // profile is channel's out profile
                    if (om == null) {
                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, ss.getLeft(),
                                "output profile of signal " + ss.getName() + " has not been set"));
                        continue;
                    }
                    buildProfile(om, as, theBlock); // add output signal parameters
                }
            }
            // add Junk Signals ..............................
            size = blk.getNbJunkSignals();
            for (int i = 0; i < size; i++) {
                StxJunkSignal ss = blk.getJunkSignal(i);
                AvatarSignal as = getJunkSignal(ss); // create signal
                if (!ss.isDeclared()) {
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, ss.getLeft(),
                            "junk signal " + theBlock.getName() + "." + ss.getName() + " has not been declared"));
                    continue;
                }
                theBlock.addSignal(as); // add signal
                buildProfile(ss, as, theBlock);
            }
        }
    }
    /**
     * <p></p> */

    /** To be run on the maps that associates block names to syntactic blocks, after the iteration of BuildBlock (which creates AvatarBlocks
     * and fills their static part: attributes, methods, timers, signals).
     *
     * <p> accept updates the AvatarBlock associated to the handled syntactic block by computing and setting its state-machine. </p>
     */
    private class BuildStateMachine implements BiConsumer<String, StxBlock> {
        public BuildStateMachine(){}

        /** updates the AvatarBlock associated to the handled StxBlock by computing and setting its state-machine */
        public void accept(String n, StxBlock blk) {

            AvatarBlock theBlock = getBlock(blk); // get relevant block
            AvatarStateMachine asm = theBlock.getStateMachine(); // get state-machine to fill
            // initialize map that maps states to their sequence of incomming transitions, and block
            // filled while handling blocks. Used thereafter to check some well-formedness constraints
            transitionMap.clear();

            AvatarStateMachineElement theSourceState; // technical variable
            AvatarStartState startState = null;
            Location startStateLocation = null;

            // iterate on syntactic states of the syntactic block
            // - build statemachine
            // - fills transitionMap for a final check.
            StxState[] states = blk.getStates();
            int size = (states == null ? 0 : states.length);
            for (int i = 0; i < size; i++) {

                // identify current state type
                byte stateType = states[i].getType();
                boolean undeclaredState = false;
                if (! states[i].isDeclared()) { // in this case, stateType may be null
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR,
                            "state " + theBlock.getName() + "." + states[i].getName() + " is used but not declared"));
                    stateType = AvatarFromSysMLSyntax.STXSTANDARDSTATE;
                    undeclaredState = true;
                }

                // handle all states that have a unique corresponding avatar state-machine element (others are handled indirectly)
                if (stateType != AvatarFromSysMLSyntax.STXPRESENDSTATE && // STXPRESENDSTATEs disappear in avatar final model
                        stateType != AvatarFromSysMLSyntax.STXPRERECEIVESTATE && // STXPRERECEIVESTATEs disappear in avatar final model
                        stateType != AvatarFromSysMLSyntax.STXSTOPSTATE ) { // STXSTOPSTATE: special handling: several associated avatar elements

                    if (undeclaredState) {  // try to recover and continue
                        states[i].setType(AvatarFromSysMLSyntax.STXSTANDARDSTATE);
                    }

                    // create the handled state, which is the source state of outgoing transitions that will be handled
                    theSourceState = getState(states[i], theBlock);

                    // two state types that require some specific transition-independent handling
                    if (stateType == AvatarFromSysMLSyntax.STXRANDOMSTATE) {
                        ((AvatarRandom) theSourceState).setOriginalValues(states[i].getMinValue(), states[i].getMaxValue());
                        ((AvatarRandom) theSourceState).setValues(
                                states[i].getMinValue().replaceAll("\\.", "__"),
                                states[i].getMaxValue().replaceAll("\\.", "__"));
                        ((AvatarRandom) theSourceState).setVariable(states[i].getVariable().replaceAll("\\.", "__"));
                        randSetDistributionLaw((AvatarRandom) theSourceState,
                                states[i].getDistributionLaw(),states[i].getDistributionExtra(), states[i]);
                    } else if (stateType == AvatarFromSysMLSyntax.STXSTARTSTATE)
                        if (startState == null) {
                            startState = (AvatarStartState) theSourceState;
                            startStateLocation = states[i].getLeft();
                            asm.setStartState(startState);
                        } else {
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, states[i].getLeft(),
                                    "several start-states declared, only one is kept", startStateLocation));
                            continue;
                        }

                    // add the handled state.................
                    addState(theSourceState, theBlock);
                    asm.addElement(theSourceState);

                    // build and add transitions.........................................

                    // get outgoing transitions list
                    List<StxTransition> transitions = states[i].getTransitions();
                    int tsize = transitions.size();
                    if (tsize > 1 && stateType != AvatarFromSysMLSyntax.STXSTANDARDSTATE) { // Avatar well-formedness constraint broken
                        tsize = 1; // recover to continue
                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, states[i].getTransitions().get(1).getLeft(),
                                "state " + states[i].getName() + " only accept one single outgoing transition",
                                states[i].getTransitions().get(1).getRleft()));
                    }

                    // iterate on outgoing transitions
                    for (int j = 0; j < tsize; j++) {
                        StxTransition transition = transitions.get(j);

                        // skip some transition that have not been fully defined during parsing
                        if (transition == null) {
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, states[i].getLeft(),
                                    "something is wrong in state " + states[i].getName() + " : null transition found"));
                            continue;
                        }
                        if (! transition.isDeclared()) {
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                    "transition request in state " + states[i].getName() + " of block " + theBlock.getName() +
                                            " has no associated declaration" ));
                            continue;
                        }
                        if (! transition.isRequested()) {
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                    "transition has no request in state " + states[i].getName()));
                            continue;
                        }

                        // create and fills the transition (only added at the end if no critical error occurs)
                        AvatarTransition theTransition = new AvatarTransition(theBlock,"", null);

                        // handle guard
                        if (transition.getGuard() != null) {
                            theTransition.setGuard(
                                    "[" + transition.getGuard().replaceAll("\\.", "__") + "]",
                                    "[" + transition.getGuard() + "]");
                        }

                        // handle delay
                        String minDelay = transition.getMinDelay().trim();
                        String maxDelay = transition.getMaxDelay().trim();
                        if (minDelay.equals(maxDelay)) maxDelay = "";
                        theTransition.setDelays(
                                minDelay.replaceAll("\\.", "__"),
                                maxDelay.replaceAll("\\.", "__"),
                                minDelay,
                                maxDelay);

                        // handle distribution law and priority
                        transSetDistributionLaw(theTransition, transition.getDelayDistributionLaw(),transition.getDelayExtra(), transition);
                        theTransition.setProbability(transition.getProbability());

                        // handle actions: iterate on them and add them
                        List<StxAction> actions = transition.getActions();
                        int asize = actions.size();
                        for (int k = 0; k < asize; k++) { // iterate on transition's actions

                            StxAction action = actions.get(k);

                            // Assignment action ..............
                            if(action.getType() == AvatarFromSysMLSyntax.STXASSIGNACTION){

                                // compute left hand
                                String originalLeftHandStr = action.getTarget(); // get assigned variable
                                // compute tuple string "(x__field1, ..., x__fieldN)" if variable has a datatype type
                                extendLocation = action.getLeft();
                                String leftHandStr = extendIdent(action.getTarget().replaceAll("\\.", "__"), blk);

                                // compute right hand
                                String originalRightHandStr;
                                String rightHandStr; //
                                StxTerm value = action.getValue();
                                if (value instanceof StxId) { // value is a variable (potentially field) ident
                                    originalRightHandStr = ((StxId)value).toString();
                                    rightHandStr = ((StxId)value).toString().replaceAll("\\.", "__");
                                }
                                else if (value instanceof StxExpr){ // value is an int/bool expression
                                    originalRightHandStr = ((StxExpr)value).toString();
                                    rightHandStr = ((StxExpr)value).toString().replaceAll("\\.", "__");
                                }
                                else { // value is a method call
                                    extendLocation = value.getLeft();
                                    originalRightHandStr = extendOriginalCall((StxCall)value,blk); // rebuilds original string
                                    rightHandStr = extendCall((StxCall)value,blk); // rebuilds while extending datatype variables
                                }
                                // add the action
                                theTransition.addAction(leftHandStr + "=" + rightHandStr,
                                        originalLeftHandStr + "=" + originalRightHandStr);
                            }
                            // method call action ...................
                            else { // STXMETHODACTION
                                if (action.getValue() instanceof StxCall) { // else ERROR, action ignored
                                    // rebuilds original string
                                    String originalTermStr = extendOriginalCall((StxCall)action.getValue(),blk);
                                    // rebuilds while extending datatype variables
                                    extendLocation = action.getValue().getLeft();
                                    String termStr = extendCall((StxCall)action.getValue(),blk);
                                    // add the action
                                    theTransition.addAction(termStr, originalTermStr);
                                }
                            }
                        }

                        // Handles transition target.
                        // Depends on target type.
                        // Also checks compatibility constraints between transition and target types.

                        if (transition.getTarget() == null) { // unrecoverable, transition skiped
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                    "something is wrong in transition : no target defined", transition.getLeft()));
                            continue;
                        }

                        // handle transition to an added state as target that disappear in avatar
                        // produced transition must then lead to the target of the single outgoing transition of the removed state

                        if (transition.getTarget().getType() == AvatarFromSysMLSyntax.STXPRESENDSTATE ||
                                transition.getTarget().getType() == AvatarFromSysMLSyntax.STXPRERECEIVESTATE) { // target must be removed

                            if (transition.getTarget().getTransitions().size() == 0 ||
                                    transition.getTarget().getTransition(0) == null) { // target has no outgoing transition, unrecoverable
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getTarget().getLeft(),
                                        "something is wrong in precommunication state " + transition.getTarget().getName() +
                                                " : no outgoing transition"));
                                continue;
                            }

                            byte type = transition.getTarget().getType(); // type of the state to remove
                            // get second transition (outgoing from removed target) to complete the single produced avatar transition
                            transition = transition.getTarget().getTransition(0);
                            // TODO: check errors: this transition must not have actions, delays,... not verified. if exist, these data are skipped

                            // check type compatibility between this transition and its source (the removed state). error -> unrecoverable
                            if (type == AvatarFromSysMLSyntax.STXPRESENDSTATE & // expects some sending state
                                    ! ( ( transition.getTarget().getType() == AvatarFromSysMLSyntax.STXSENDSTATE &&
                                            transition.getType() == AvatarFromSysMLSyntax.STXSENDTRANSITION ) ||
                                            ( transition.getTarget().getType() == AvatarFromSysMLSyntax.STXSETTIMERSTATE &&
                                                    transition.getType() == AvatarFromSysMLSyntax.STXSETTIMERTRANSITION ) ||
                                            ( transition.getTarget().getType() == AvatarFromSysMLSyntax.STXRESETTIMERSTATE &&
                                                    transition.getType() == AvatarFromSysMLSyntax.STXRESETTIMERTRANSITION ) )) {
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getTarget().getLeft(),
                                        "outgoing transitions from pre-send state must be sending transitions leading to some sending state " +
                                                "(send/set timer /reset timer)", transition.getLeft()));
                                continue;
                            }
                            else if (type == AvatarFromSysMLSyntax.STXPRERECEIVESTATE & // expects some receiving state
                                    ! ( ( transition.getTarget().getType() == AvatarFromSysMLSyntax.STXRECEIVESTATE &&
                                            transition.getType() == AvatarFromSysMLSyntax.STXRECEIVETRANSITION ) ||
                                            ( transition.getTarget().getType() == AvatarFromSysMLSyntax.STXEXPIRETIMERSTATE &&
                                                    transition.getType() == AvatarFromSysMLSyntax.STXEXPIRETIMERTRANSITION ) )) {
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getTarget().getLeft(),
                                        "outgoing transitions from pre-reveive state must be sending transitions leading to some " +
                                                "receivinging state (receive/expire timer)", transition.getLeft()));
                                continue;
                            }
                        }

                        // from here, the target of avatar transition under building  is the state-machine element associated
                        // to the target of variable transition

                        // handle transition target.......
                        AvatarStateMachineElement tgtState; // target of Avatar Transition

                        if (transition.getTarget().getType() == AvatarFromSysMLSyntax.STXSTOPSTATE) {
                            // special case: a different Avatar stopstate target for each transition to the unique syntactic stopstate
                            if (transition.getTarget().getTransitions().size() != 0) {
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.WARNING, transition.getTarget().getLeft(),
                                        "stopstate should not have outgoing transitions"));
                            }
                            tgtState = new AvatarStopState("StopState", null, theBlock); // create the stopstate
                            asm.addElement(tgtState);
                        }
                        else // standard case
                            tgtState = getState(transition.getTarget(), theBlock);

                        addState(tgtState, theBlock); // initialize the map-to-incomming-transitions for the target state

                        // type dependent transition handling (transition and target types) + error detections
                        byte tgtType = transition.getTarget().getType();

                        // sending transitions =====================
                        if (transition.getType() == AvatarFromSysMLSyntax.STXSENDTRANSITION) {
                            if(tgtState instanceof AvatarActionOnSignal) { // required
                                AvatarActionOnSignal state = (AvatarActionOnSignal)tgtState;

                                // some unrecovered errors (transition skiped)
                                if (transition.getSignal() == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                            "sending transition should have an associated signal", transition.getLeft()));
                                    continue;
                                }
                                state.setSignal( getSignal(transition.getSignal()));
                                if (state.getSignal().isIn()) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                            "signal of sending transition must be an output signal", transition.getSignal().getLeft()));
                                    continue;
                                }
                                if (! transition.getSignal().isDeclared()) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                            "transition uses a signal " + transition.getSignal().getName() + " that has not been declared"));
                                    continue;
                                }

                                ArrayList<StxTerm> sendPayload = transition.getSendPayload();
                                if (sendPayload != null) { // add payload info to target state (renamed "state")
                                    int nbParam = sendPayload.size();
                                    for(int u = 0; u < nbParam; u++) { // iterate on payload parameter list
                                        StxTerm p = sendPayload.get(u);

                                        if (p instanceof StxId) { // parameter is an attribute (field) identifier
                                            state.addOriginalValue(p.toString());
                                            String[] ids;
                                            // extends parameter if datatpe: "x__field1, ..., x__fieldN" (if well typed)
                                            extendLocation = p.getLeft();
                                            String extendedPath = extendPath(p.toString().replaceAll("\\.", "__"), blk);
                                            if (extendedPath == null) { // parameter cannot be typed
                                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, p.getLeft(), "attribute \"" +
                                                        p.toString() + "\" cannot be typed"));
                                                // recover w.r.t. expected type to continue, while providing information
                                                AvatarDataType adt = state.getSignal().getListOfOriginalAttributes().get(u).getDataType();
                                                if (adt == null) { // basic type, keep parameter unchanged
                                                    ids = new String[1];
                                                    ids[0] = p.toString();
                                                } else { // datatype, extend parameter with fields (although not well typed)
                                                    ids = extendName(p.toString().replaceAll("\\.", "__"),adt).split(",");
                                                }
                                            }
                                            else // parameter has been typed (not verified that it is the expected type)
                                                ids = extendedPath.split(",");

                                            // add parameter to ActionOnSignal state
                                            int isize = ids.length;
                                            for (int w = 0; w < isize; w++) state.addValue(ids[w]);
                                        }

                                        else if (p instanceof StxExpr) { // parameter is an int/bool expression, basic type
                                            state.addOriginalValue( p.toString() );
                                            state.addValue( p.toString().replaceAll("\\.", "__"));
                                        }
                                        else { // parameter is a method call
                                            extendLocation = p.getLeft();
                                            state.addOriginalValue(extendOriginalCall((StxCall)p,blk)); // rebuild original string
                                            state.addValue(extendCall((StxCall)p,blk)); // rebuild while extending datatype-typed variable
                                        }
                                    }
                                }
                            }
                            else  { // unrecovered error, skip transition
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                        "sending transition should lead to a send state", transition.getRleft()));
                                continue;
                            }
                        }

                        // receiving transitions =====================
                        else if (transition.getType() == AvatarFromSysMLSyntax.STXRECEIVETRANSITION) {
                            if(tgtState instanceof AvatarActionOnSignal) { // expected
                                AvatarActionOnSignal state = (AvatarActionOnSignal)tgtState;

                                // some unrecovered errors (transition skiped)
                                if (transition.getSignal() == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                            "receiving transition should have an associated signal", transition.getLeft()));
                                    continue;
                                }
                                state.setSignal(getSignal(transition.getSignal()));

                                if (! transition.getSignal().isDeclared()) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                            "transition uses a signal " + transition.getSignal().getName() + " that has not been declared"));
                                    continue;
                                }
                                if (state.getSignal().isOut()) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                            "signal of receiving transition must be an input signal", transition.getSignal().getLeft()));
                                    continue;
                                }
                                if (transition.getSignal().getChannel() == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getRleft(),
                                            "signal of receiving transition doesn't have an associated channel", transition.getSignal().getLeft()));
                                    continue;
                                }

                                // update target state (renamed "state") with assigned attributes (fields)

                                List<AvatarAttribute> profile = state.getSignal().getListOfOriginalAttributes();
                                if (profile != null && !profile.isEmpty()) { // signal conveys values (otherwise, nothing to do)

                                    // map that maps parameter names to string denoting the attributes (fields) to assign
                                    HashMap<String, String> receivePayload = transition.getReceivePayload();
                                    if (receivePayload == null) {
                                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                                    "receive payload of transition has not been set"));
                                        continue;
                                    }

                                    int nbParams = profile.size();
                                    for (int k = 0; k < nbParams; k++) { // iterate on receiving profile parameters
                                        AvatarAttribute aa = profile.get(k); // parameter name and type
                                        String value; // to receive name of attribute (field) to assign with parameter

                                        if (aa == null) { // parameter has not been correctly defined while parsing
                                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                                    "parameter " + k + " of signal " + state.getSignal().getName() +
                                                            " is not well defined"));
                                            value = ""; // default, because no way to identify target variable
                                        } else { // parameter well defined
                                            value = receivePayload.get(aa.getName());
                                            if (value == null) { // target variable undefined
                                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                                        "receiving transition has a missing field"));
                                                value = "";
                                            }
                                        }
                                        state.addOriginalValue(value);

                                        // add basic primitive attributesextend value w.r.t

                                        String[] ids;
                                        // extend value w.r.t its datatype if relevant
                                        extendLocation = transition.getLeft();
                                        String extendedPath = extendPath(value.replaceAll("\\.", "__"), blk);
                                        if (extendedPath == null) { // value cannot be typed
                                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(), "attribute \"" +
                                                    value + "\" cannot be typed"));
                                            // recover w.r.t expected type to continue while providing info
                                            AvatarDataType adt = profile.get(k).getDataType();
                                            if (adt == null) { // primitive type, keep target variable name as it is
                                                ids = new String[1];
                                                ids[0] = value;
                                            } else { // datatype, extend target variable w.r.t datatype fields (although not well typed)
                                                ids = extendName(value.replaceAll("\\.", "__"),adt).split(",");
                                            }
                                        }
                                        else // value has a type (not verified: is it the expected type?)
                                            ids = extendedPath.split(",");

                                        // update state with (extended) value as target attribute (fields)
                                        int isize = ids.length;
                                        for (int w = 0; w < isize; w++) state.addValue(ids[w]);
                                    }
                                }
                            }
                            else  { // unrecoved error, skip transition
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                        "receiving transition should lead to a receive state", transition.getRleft()));
                                continue;
                            }
                        }

                        // set-timer transitions =====================
                        else if (transition.getType() == AvatarFromSysMLSyntax.STXSETTIMERTRANSITION) {
                            if(tgtState instanceof AvatarSetTimer) {
                                AvatarSetTimer state = (AvatarSetTimer) tgtState;

                                // some unrecovered errors (transition skipped)
                                if (transition.getTimer() == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                            "set-timer transition has no associated timer", transition.getRleft()));
                                    continue;
                                }
                                AvatarAttribute tmr = getTimerByName(transition.getTimer(), theBlock);
                                if (tmr == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getTarget().getLeft(),
                                            "timer of set-timer state does not exist", transition.getRleft()));
                                    continue;
                                }
                                state.setTimer(tmr);
                                if (transition.getSendPayload() == null || transition.getSendPayload().size() != 1
                                        || !(transition.getSendPayload().get(0) instanceof StxExpr)) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                            "payload of set-timer transition is not well formed", transition.getRleft()));
                                    continue;
                                }

                                // update target state with its specific parameters
                                state.setTimerValue(((StxExpr)transition.getSendPayload().get(0)).toString().replaceAll(".","__"));
                                state.setTimerOriginalValue(((StxExpr)transition.getSendPayload().get(0)).toString());
                            }
                            else  { // unrecoved error, skip transition
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                        "set-timer transition should lead to a set-timer state", transition.getRleft()));
                                continue;
                            }
                        }

                        // reset-timer transitions =====================
                        else if (transition.getType() == AvatarFromSysMLSyntax.STXRESETTIMERTRANSITION) {
                            if(tgtState instanceof AvatarResetTimer) {
                                AvatarResetTimer state = (AvatarResetTimer) tgtState;

                                // an unrecovered error (transition skipped)
                                if (transition.getTimer() == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                            "reset-timer transition has no associated timer", transition.getRleft()));
                                    continue;
                                }

                                // update target state with its specific parameter
                                AvatarAttribute tmr = getTimerByName(transition.getTimer(), theBlock);
                                if (tmr == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getTarget().getLeft(),
                                            "timer of reset-timer state does not exist", transition.getRleft()));
                                    continue;
                                }
                                state.setTimer(tmr);
                            }
                            else  { // unrecoved error, skip transition
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                        "reset-timer transition should lead to a reset-timer state", transition.getRleft()));
                                continue;
                            }
                        }
                        // expire-timer transitions =====================
                        else if (transition.getType() == AvatarFromSysMLSyntax.STXEXPIRETIMERTRANSITION) {
                            if(tgtState instanceof AvatarExpireTimer) {
                                AvatarExpireTimer state = (AvatarExpireTimer) tgtState;

                                // an unrecovered error (transition skipped)
                                if (transition.getTimer() == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                            "expire-timer transition has no associated timer", transition.getRleft()));
                                    continue;
                                }

                                // update target state with its specific parameter
                                AvatarAttribute tmr = getTimerByName(transition.getTimer(), theBlock);
                                if (tmr == null) {
                                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getTarget().getLeft(),
                                            "timer of expire-timer state does not exist", transition.getRleft()));
                                    continue;
                                }
                                state.setTimer(tmr);
                                state.setTimer(getTimerByName(transition.getTimer(), theBlock));
                            }
                            else  { // unrecoved error, skip transition
                                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                        "expire-timer transition should lead to a expire-timer state", transition.getRleft()));
                                continue;
                            }
                        }

                        // standard transitions to non-communicating state =====================
                        else if (! (tgtType == AvatarFromSysMLSyntax.STXSTOPSTATE ||
                                tgtType == AvatarFromSysMLSyntax.STXSTANDARDSTATE ||
                                tgtType == AvatarFromSysMLSyntax.STXRANDOMSTATE ||
                                tgtType == AvatarFromSysMLSyntax.STXCOUNTSTATE ||
                                tgtType == AvatarFromSysMLSyntax.STXSTARTSTATE) ) {
                            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, transition.getLeft(),
                                    "transition type is not consistant with the type of its target state", transition.getTarget().getLeft()));
                            continue;
                        }

                        // All unrecovered errors have been avoided, the transition can be added
                        asm.addElement(theTransition);
                        theSourceState.addNext(theTransition);
                        theTransition.addNext(tgtState);
                        addTransition(theTransition, tgtState); // memorize incoming transition for post-processing
                    }
                }
            }
            // post-processing: verify some constraints on incoming transitions
            transitionMap.forEach(new CheckIncommings());
        }
    }

    /** makes some verifications relative to incomming edges, not all. In particular, not all unreachable states are removed, but this must ensure at
     * least that keeped states are sufficiently well defined to be handled by the graphic representation building process (avoiding crashes) */
    private class CheckIncommings implements BiConsumer<AvatarStateMachineElement, StateTransitions> {
        List<AvatarDataType> dataTypeList;
        public CheckIncommings(){}
        public void accept(AvatarStateMachineElement e, StateTransitions st) {
            int s = st.getTransitions().size();
            if (s == 0 && ! (e instanceof AvatarStartState)) { // unreachable state that may be not well defined => to remove
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, "state " + e.getName() +
                        " of block " + st.getBlock().getName() + " is unreachable --> removed"));
                st.getBlock().getStateMachine().removeElement(e);
                for(AvatarStateMachineElement x : e.getNexts())
                    st.getBlock().getStateMachine().removeElement(x);
            }
            if (s > 0 && (e instanceof AvatarStartState)) { // start state has input edges => redirected to error stopstate
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, "start state in bloc " +
                        st.getBlock().getName() + " has some input transition"));
                for(int i = 0; i < s; i++) {
                    st.getTransitions().get(i).getNexts().clear();
                    AvatarStopState nw = new AvatarStopState("errorstate",null, st.getBlock());
                    st.getTransitions().get(i).addNext(nw);
                    st.getBlock().getStateMachine().addElement(nw);
                }
            }
            if (s > 1 && !(e instanceof AvatarStartState || e instanceof AvatarStateElement)) { // too many input edges => redirected to error
                // stopstate
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, "state " + e.getName() + " in bloc " +
                        st.getBlock().getName() + " has more than one incomming transition"));

                for(int i = 1; i < s; i++) {
                    st.getTransitions().get(i).getNexts().clear();
                    AvatarStopState nw = new AvatarStopState("errorstate",null, st.getBlock());
                    st.getTransitions().get(i).addNext(nw);
                    st.getBlock().getStateMachine().addElement(nw);
                }
            }
        }
    }
    /** create-and-add AvatarBlocks. successive calls of BuildBlock and BuildStateMachine for each syntactic block */
    private void buildBlocks(){
        stxSpec.getBlockMap().forEach(new BuildBlock());
        stxSpec.getBlockMap().forEach(new BuildStateMachine());
    }

    /** complete the parameter transition with distribution law information provided in other parameters.
     *
     * @param _t the transition to provide with distribution law information
     * @param distributionLaw the distribution law name
     * @param delayExtra a map of distribution law parameters to values
     * @param _st the syntactic transition (provides locations for error messages)
     */
    private void transSetDistributionLaw(AvatarTransition _t, String distributionLaw, HashMap<String,String> delayExtra, StxTransition _st) {
        int law = -1;
        int size;

        // identify distribution law......... set default DELAY_UNIFORM_LAW when a problem is encountered
        if (distributionLaw == null || distributionLaw.equals("")) law = AvatarTransition.DELAY_UNIFORM_LAW;
        else {
            size = AvatarTransition.DISTRIBUTION_LAWS.length;
            for (int i = 0; i < size; i++) { // search in defined laws
                if (AvatarTransition.DISTRIBUTION_LAWS[i].equals(distributionLaw)) {
                    law = i;
                    break;
                }
            }
            if (law == -1) { // not found
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, _st.getLeft(),
                 "transition with unknown transition law : " + distributionLaw + " (set to uniform)"));
                law = AvatarTransition.DELAY_UNIFORM_LAW;
            }
        }
        // try to get law parameters............
        // get law parameter names
        String extra1 = AvatarTransition.LABELS_OF_EXTRA_ATTRIBUTES_1[law];
        String extra2 = AvatarTransition.LABELS_OF_EXTRA_ATTRIBUTES_2[law];

        size = AvatarTransition.NB_OF_EXTRA_ATTRIBUTES[law];
        int max = size;
        String tmpString;
        if (size > 0) {
            tmpString = extra1;
            extra1 = delayExtra.get(extra1); // try to get value of first parameter through its name
            if (extra1 == null) { // not found
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, _st.getLeft(),
                 "transition law : " + distributionLaw + " requires a " + tmpString + " parameter"));
                max--;
            }
            if (size > 1) {
                tmpString = extra2;
                extra2 = delayExtra.get(extra2); // try to get value of second parameter through its name
                if (extra2 == null) { // not found
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, _st.getLeft(),
                     "transition law : " + distributionLaw + " requires a " + tmpString + " parameter"));
                    max--;
                }
            }
        }
        if (delayExtra != null && delayExtra.size() > max) { // there is some unexpected parameter value
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.WARNING, _st.getLeft(),
                     "transition law : " + distributionLaw + " has irrelevant parameters"));

        }
        // complete transition with identified data
        _t.setDistributionLaw(law, extra1, extra2);
    }

    /** complete the parameter random state with distribution law information provided in other parameters.
     *
     * @param _s the random state to provide with distribution law information
     * @param distributionLaw the distribution law name
     * @param distrExtra a map of distribution law parameters to values
     * @param _ss the syntactic random state (provides locations for error messages)
     */
    private void randSetDistributionLaw(AvatarRandom _s, String distributionLaw, HashMap<String,String> distrExtra, StxState _ss) {
        int law = -1;
        int size;

        // identify distribution law......... set default DELAY_UNIFORM_LAW when a problem is encountered
        if (distributionLaw == null || distributionLaw.equals("")) law = AvatarRandom.RANDOM_UNIFORM_LAW;
        else {
            size = AvatarRandom.DISTRIBUTION_LAWS.length;
            for (int i = 0; i < size; i++) { // search in defined laws
                if (AvatarRandom.DISTRIBUTION_LAWS[i].equals(distributionLaw)) {
                    law = i;
                    break;
                }
            }
            if (law == -1) { // not found
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, _ss.getLeft(),
                        "random state with unknown transition law : " + distributionLaw + " (set to uniform)"));
                law = AvatarRandom.RANDOM_UNIFORM_LAW;
            }
        }
        // try to get law parameters............
        // get law parameter names
        String extra1 = AvatarRandom.LABELS_OF_EXTRA_ATTRIBUTES_1[law];
        String extra2 = AvatarRandom.LABELS_OF_EXTRA_ATTRIBUTES_2[law];

        size = AvatarRandom.NB_OF_EXTRA_ATTRIBUTES[law];
        int max = size;
        String tmpString;
        if (size > 0) {
            tmpString = extra1;
            extra1 = distrExtra.get(extra1); // try to get value of first parameter through its name
            if (extra1 == null) { // not found
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, _ss.getLeft(),
                        "transition law : " + distributionLaw + " requires a " + tmpString + " parameter"));
                max--;
            }
            if (size > 1) {
                tmpString = extra2;
                extra2 = distrExtra.get(extra2); // try to get value of second parameter through its name
                if (extra2 == null) { // not found
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, _ss.getLeft(),
                            "transition law : " + distributionLaw + " requires a " + tmpString + " parameter"));
                    max--;
                }
            }
        }
        if (distrExtra != null && distrExtra.size() > max) { // there is some unexpected parameter value
            addError(new AvatarFromSysMLError(AvatarFromSysMLError.WARNING, _ss.getLeft(),
                    "transition law : " + distributionLaw + " has irrelevant parameters"));

        }
        // complete random state with identified data
        _s.setFunctionId(law);
        _s.setExtraAttribute1(extra1);
        _s.setExtraAttribute2(extra2);
    }

    /** fills the parameters of a method (or signal) w.r.t. the data of a syntactic structure
     *
     * @param ss the syntactic structure containing field information
     * @param am the method to update by adding the fields characterized in ss
     * @param b the block of the method
     */
    private void buildProfile(StxStructure ss, AvatarMethod am, AvatarBlock b) {
        AvatarAttribute aa;
        int size = ss.getSize();
        for (int i = 0; i < size; i++) { // iterates on the fields of the structure ss
            String type = ss.getFieldType(i);
            if (type.equals("Integer")){
                aa = new AvatarAttribute(ss.getFieldName(i), AvatarType.INTEGER, b, null);
                am.addOriginalParameter(aa);
                aa.setAsConstant(false);
                aa = new AvatarAttribute(ss.getFieldName(i), AvatarType.INTEGER, b, null);
                am.addParameter(aa);
                aa.setAsConstant(false);
           } else if (type.equals("Boolean")) {
                aa = new AvatarAttribute(ss.getFieldName(i), AvatarType.BOOLEAN, b, null);
                am.addOriginalParameter(aa);
                aa.setAsConstant(false);
                aa = new AvatarAttribute(ss.getFieldName(i), AvatarType.BOOLEAN, b, null);
                am.addParameter(aa);
                aa.setAsConstant(false);
            } else { // the field is datatype-typed
                AvatarDataType adt = avSpec.getDataTypeByName(type);
                if (adt == null) {
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, ss.getLeft(),
                     "message profile uses undefined datatype " + type));
                }
                // add the original version of the field to am
                String aaName = ss.getFieldName(i);
                aa = new AvatarAttribute(aaName, AvatarType.UNDEFINED, b, null);
                aa.setDataType(adt);
                am.addOriginalParameter(aa);
                aa.setAsConstant(false);
                // add the primitive basic attributes corresponding to the datatype's field
                if (adt != null) {
                    aaName = aaName + "__";
                    AvatarDataTypePrimitiveFields primitiveFields = primitiveFieldsMap.get(adt);
                    int dtsize = primitiveFields.size();
                    for (int j = 0; j < dtsize; j++) {
                        aa = new AvatarAttribute(aaName + primitiveFields.getName(j),
                                primitiveFields.getType(j), b, null);
                        am.addParameter(aa);
                        aa.setAsConstant(false);
                    }
                } else { // error, try to continue with the non coherent attribute alone
                    aa = new AvatarAttribute(aaName, AvatarType.UNDEFINED, b, null);
                    am.addParameter(aa);
                    aa.setAsConstant(false);
                }
           }
        }
    }

    /** get the type of an attribute (not an attribute field) from a syntactic block */
    private String getStxAttributeType(String name, StxBlock blk){
        int size = blk.getNbAttributes();
        for (int i = 0; i < size; i++) {
            if (blk.getAttribute(i).getName().equals(name)) return blk.getAttribute(i).getType();
        }
        return null;
    }

    /** get the type of an attribute (or an attribute field, with "__" as field delimiter) from a syntactic block */
    private String getStxPathType(String s, StxBlock b) {
        String[] path = s.split("__");
        String type = getStxAttributeType(path[0], b);
        int size = path.length;
        for (int i = 1; i < size; i++) {
            AvatarDataType adt = avSpec.getDataTypeByName(type);
            if (adt == null) return null;
            int nbFields = adt.attributeNb();
            int j;
            for (j = 0; j < nbFields; j++)
                if (adt.getAttribute(j).getName().equals(path[i])) break;
            if (j == nbFields) return null;
            AvatarType adtType = adt.getAttribute(j).getType();
            if (adtType == AvatarType.INTEGER)
                type = "Integer";
            else if (adtType == AvatarType.BOOLEAN)
                type = "Boolean";
            else
                type = adt.getAttribute(j).getDataType().getName();
        }
        return type;
    }
    /** (string) Type found for ident extended by by extendPath (and thus extendIdent) */
    private String extendTypeString;
    /** DataType found for ident extended by extendPath (and thus extendIdent) if not primitive type */
    private AvatarDataType extendDataType;
    /** location for error messages while extending paths */
    private Location extendLocation;
    /** Primitive type found for ident extended by extendPath (and thus extendIdent) if not datatype type */
    private AvatarType extendPrimitiveType;

    /** extends a parameter attribute or field name x in a list "x__field1, ..., x__fieldN" respecting the fields of its associated type.
     * If type is primitive, returns x unchanged. Updates extendTypeString, extendDataType and extendPrimitiveType w.r.t. the found type.
     * If the path is irrelevant or leads to some undeclared datatype, returns "__undefined__path" or "__path__undefined_datatype_datatype"*/
    private String extendPath(String path, StxBlock b) {
        extendTypeString = getStxPathType(path, b);
        if (extendTypeString == null) { // path cannot be typed, return null to notify the error
            extendDataType = null;
            extendPrimitiveType = null;
            addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, extendLocation,
                    "ident " + path + " is used but not declared in block " + b.getName()));
            return "__undefined__" + path;
        }
        if (extendTypeString.equals("Integer")) {
            extendDataType = null;
            extendPrimitiveType = AvatarType.INTEGER;
            return path;
        }
        if (extendTypeString.equals("Boolean")) {
            extendDataType = null;
            extendPrimitiveType = AvatarType.BOOLEAN;
            return path;
        }
        extendPrimitiveType = null;
        extendDataType = avSpec.getDataTypeByName(extendTypeString);
        if (extendDataType != null)
            return extendName(path, extendDataType);
        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, extendLocation,
                "ident " + path + " in block " + b.getName() + " has an unknown data type " + extendTypeString));
        return "__" + path + "__undefined_datatype__" + extendTypeString;

    }

    /** extends a parameter name x in a list "x__field1, ..., x__fieldN" respecting the fields of parameter datatype */
    private String extendName(String name, AvatarDataType adt) {
        if (adt != null) {
            AvatarDataTypePrimitiveFields primitiveFields = primitiveFieldsMap.get(adt);
            int size = primitiveFields.size() -1;
            StringBuffer res = new StringBuffer();
            int i;
            for (i = 0; i < size; i++)
                res.append(name + "__" + primitiveFields.getName(i) + ", ");
            res.append(name + "__" + primitiveFields.getName(i));
            return res.toString();
        }
        else return null;
    }
    /** extends a parameter attribute or field name x in a list in brackets "(x__field1, ..., x__fieldN)" respecting the fields of its associated
     * type.  If this type is primitive, returns x unchanged. Updates extendTypeString, extendDataType and extendPrimitiveType w.r.t. the found type.
     * extends a parameter name x in a list "x__field1, ..., x__fieldN" respecting the fields of parameter datatype
     */
    private String extendIdent (String path, StxBlock b) {
        String result = extendPath(path, b);
        return (result.indexOf(',') == -1 ? result : "(" + result + ")");
    }

    /** builds the string associated to a method call while extending the ones among the idents it uses that are datatype-typed */
     private String extendCall (StxCall call, StxBlock b) {
        StringBuffer result = new StringBuffer();
        int size = call.getNbIdents();
        int i;
        for (i =0 ; i < size; i++) {
            result.append(call.getSegment(i));
            result.append(extendPath(call.getIdent(i).replaceAll("\\.", "__"), b));
        }
        result.append(call.getSegment(i));
        return result.toString().replaceAll("\\.", "__");
     }
    /** rebuilds the original string associated to a method call, without ident extension */
     private String extendOriginalCall (StxCall call, StxBlock b) {
        StringBuffer result = new StringBuffer();
        int size = call.getNbIdents();
        int i;
        for (i =0 ; i < size; i++) {
            result.append(call.getSegment(i));
            result.append(call.getIdent(i));
        }
        result.append(call.getSegment(i));
        return result.toString();
     }

     // BUILD RELATIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    /** create-and-add-to-avSpec the relations identified in stxSpec */
    private void buildRelations(){
        stxSpec.getRelationMap().forEach(new BuildRelation());
    }

    /** to be run on relations identified while parsing and memorized in stxSpec's relation map. Create, fill and adds-to-avSpec a relation. */
    private class BuildRelation implements BiConsumer<String, StxRelation> {
        public BuildRelation() {
        }
        public void accept(String n, StxRelation r) {
            if (!r.isDeclared()) { // declaration part is missing in source
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR,
                        "relation " + r.getName() + " has been used but has not been declared"));
            }
            // check size
            int size = r.getSize();
            if (size == 0) { // empty relation is droped
                addError(new AvatarFromSysMLError(AvatarFromSysMLError.WARNING, r.getLeft(),
                 "relation is empty"));
            }
            int i = 0;
            if (r.getBlock1() == null || r.getBlock2() == null) { // AvatarBlock structures not found
                // try to recover
                while (i < size && (r.getChannel(i).getBlockA() == null || r.getChannel(i).getBlockB() == null)) i++;
                if (i == size){
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR,
                            "blocs of relation " + r.getName() + " unspecified, recovery from channels failed"));
                    return;
                }
                else
                    addError(new AvatarFromSysMLError(AvatarFromSysMLError.WARNING,
             "blocs of relation " + r.getName() + " unspecified, recovered from channel " + r.getChannel(i).getName()));
                r.setBlock1(stxSpec.getBlockMap().get(r.getChannel(i).getBlockA()));
                r.setBlock2(stxSpec.getBlockMap().get(r.getChannel(i).getBlockB()));
            }

            // from here, relation has blocks (from declaration or recovered)

            AvatarBlock blk1 = getBlock(r.getBlock1());
            AvatarBlock blk2 = getBlock(r.getBlock2());

            // create-and-add-to-avSpec the Avatar relation
            AvatarRelation theRelation = new AvatarRelation(n, blk1, blk2, null);
            avSpec.addRelation(theRelation);

            // set relation parameters
            theRelation.setAsynchronous(r.getAsynchronous());
            theRelation.setPrivate(r.getPrivate());
            theRelation.setLossy(r.getLossy());
            theRelation.setBlocking(r.getBlocking());
            theRelation.setSizeOfFIFO(r.getFifoSize());

            for (i = 0; i < size; i++) { // iterate on channels
                StxChannel c = r.getChannel(i); // get channel
                c.commuteSignals(r.getBlock1().getName()); // make channel respect block order in relation
                if (!blk1.getName().equals(c.getBlockA()) || !blk2.getName().equals(c.getBlockB())) { // channel doesn't respect relation's blocks
                    if(c.getBlockA() == null || c.getBlockA() == null)
                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, c.getLeft(),
                                "missing associated block to channel " + c.getName()));
                    else
                        addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, c.getLeft(),
                        "inconsistency between blocs of channel " + c.getName() + " and its relation " + r.getName()));
                    continue;
                }
                // try to add channel to relation
                if (c.getSignalA() != null && c.getSignalB() != null)
                    theRelation.addSignals(getSignal(c.getSignalA()),getSignal(c.getSignalB()));
                else
                 addError(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, c.getLeft(),
                 "missing signal binding for channel" + c.getName()));
           }
        }
    }
}
