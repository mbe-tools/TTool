/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package avatartranslator.tosysmlv2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.BiConsumer;

import avatartranslator.AvatarTransition;
import java_cup.runtime.ComplexSymbolFactory.Location;

/** Class AvatarFromSysMLSyntax
 * Creation: 20/06/2024
 *
 * @author Sophie Coudert
 * @version 0.1 20/06/2024
 *
 * Abstract Syntax "trees" (in fact models) and their components.
 * Associated locations (left, right) locate the component with
 * line and column in the source file
 */
public class AvatarFromSysMLSyntax {

     /** type of idents generated for datatype names */
    public static final byte DATATYPENAME = 0;
     /** type of idents generated for block names */
    public static final byte BLOCKNAME = 1;
     /** type of idents generated for attribute or method names */
    public static final byte ATTRIBUTENAME = 2;
     /** type of idents generated for relation names */
    public static final byte RELATIONNAME = 3;
     /** type of idents generated for some part of channel names */
    public static final byte CHANNELPRENAME = 4;
     /** type of idents generated for signal names */
    public static final byte SIGNALNAME = 5;
     /** type of idents generated for channel names */
    public static final byte CHANNELNAME = 6;
     /** type of idents generated for signal profile names */
    public static final byte MESSAGENAME = 7;
     /** type of idents generated for Avatar state names */
    public static final byte STANDARDSTATENAME = 8;
     /** type of idents generated for start state names */
    public static final byte STARTSTATENAME = 9;
     /** type of idents generated for stop state names */
    public static final byte STOPSTATENAME = 10;
     /** type of idents generated for random operation names */
    public static final byte RANDOMSTATENAME = 11;
     /** type of idents generated for signal count operation names */
    public static final byte COUNTSTATENAME = 12;
     /** type of idents generated for signal sending action names */
    public static final byte SENDSTATENAME = 13;
     /** type of idents generated for signal receiving operation names */
    public static final byte RECEIVESTATENAME = 14;
     /** type of idents generated for technically added state before sending action names */
    public static final byte PRESENDSTATENAME = 15;
     /** type of idents generated for technically added state before receiving action names */
    public static final byte PRERECEIVESTATENAME = 16;
     /** type of idents generated for setting timer action names */
    public static final byte SETTIMERSTATENAME = 17;
     /** type of idents generated for resetting timer action names */
    public static final byte RESETTIMERSTATENAME = 18;
     /** type of idents generated for expire timer action names */
    public static final byte EXPIRETIMERSTATENAME = 19;
     /** type of idents generated for technically added state before setting timer action names */
    public static final byte PRESETTIMERSTATENAME = 20;
     /** type of idents generated for technically added state before resetting timer action names */
    public static final byte PRERESETTIMERSTATENAME = 21;
     /** type of idents generated for technically added state before expire timer action names */
    public static final byte PREEXPIRETIMERSTATENAME = 22;
     /** type of idents generated for timer names */
    public static final byte TIMERBLOCKNAME = 23;
     /** type of idents generated for any name between quotes that is not of another type */
    public static final byte QUOTEDNAME = 24;
     /** type of idents generated for Avatar-well-formed ident names */
    public static final byte AVATARNAME = 25;
     /** type of start states */
    public static final byte STXSTARTSTATE = 0;
     /** type of stop states */
    public static final byte STXSTOPSTATE = 1;
     /** type of Avatar states */
    public static final byte STXSTANDARDSTATE = 2;
     /** type of Random actions */
    public static final byte STXRANDOMSTATE = 3;
     /** type of count signal actions */
    public static final byte STXCOUNTSTATE = 4;
     /** type of send actions */
    public static final byte STXSENDSTATE = 5;
     /** type of receive actions */
    public static final byte STXRECEIVESTATE = 6;
     /** type of state added before send actions */
    public static final byte STXPRESENDSTATE = 7;
     /** type of state added before receive actions */
    public static final byte STXPRERECEIVESTATE = 8;
     /** type of set timer actions */
    public static final byte STXSETTIMERSTATE = 9;
     /** type of reset timer actions */
    public static final byte STXRESETTIMERSTATE = 10;
     /** type of expire timer actions */
    public static final byte STXEXPIRETIMERSTATE = 11;
     /** type of transitions without communication */
    public static final byte STXTRIVIALTRANSITION = 0;
     /** type of transitions requiring a send action to complete */
    public static final byte STXSENDTRANSITION = 1;
     /** type of transitions requiring a receive action to complete */
    public static final byte STXRECEIVETRANSITION = 2;
     /** type of transitions requiring a set timer action to complete */
    public static final byte STXSETTIMERTRANSITION = 3;
     /** type of transitions requiring a reset timer action to complete */
    public static final byte STXRESETTIMERTRANSITION = 4;
     /** type of transitions requiring an expire timer action to complete */
    public static final byte STXEXPIRETIMERTRANSITION = 5;
     /** type of assignment transitions action */
    public static final byte STXASSIGNACTION = 0;
     /** type of method-call transitions action */
    public static final byte STXMETHODACTION = 1;

    /** (technical) Arrays allowing to set a value at any index.
     *
     *  Missing values are set to null.
     */
    public static class MyArray<E> extends ArrayList<E> {
        @Override
        public E set(int index, E element){
            int max = size();
            for(int i = max; i <= index; i++)
                add(null);
            return super.set(index, element);
        }
        @Override

        public E get(int index){
            if (index < 0 || index >= size()) return null;
            else return super.get(index);
        }
    }

    /** idents collected while parsing an Avatar SysML Model.
     *
     * when generated, sysML names differ from original Avatar names.
     * All these names are without quotes (removed when found while parsing)
     */
    public static class Ident  extends StxElement {
        /** type of the ident */
        private byte type;
        /** sysML ident as found in the source file */
        private String sysMLName; // yet unused. perhaps to remove...
        /** Avatar name
         *
         * different from sysMLName for generated idents: original Avatar names are
         * restored
         */
        private String avatarName;
        /** builds an ident from a SysML string
         * @param _type the ident type
         * @param _smlName the SysML string
         */

        public Ident(byte _type, String _smlName) {
            super(_smlName.trim());
            setTypeName(_type, _smlName.trim());
        }

        /** builds an ident from a SysML string
         * @param _left
         * @param _right
         * @param _type the ident type
         * @param _smlName the SysML string
         */
        public Ident(Location _left, Location _right, byte _type, String _smlName) {
            super(_smlName.trim(), _left, _right);
            setTypeName(_type, _smlName.trim());
        };

        /** generally the Avatar name
         * @return the associated string
         */
        @Override
        public String toString() { return getAvatarName(); }

        /** Sets the names.
         *
         * Extract the Avatar original names from the generated names if relevant.
         * If generated, _smlName must be consistent with _type
         *
         * @param _type known type
         * @param _smlName SysML String.
         */
        private void setTypeName(byte _type, String _smlName) {
            type = _type;
            if (type == AVATARNAME) {
                 sysMLName = _smlName;
                avatarName = _smlName;
            } else {
                sysMLName = _smlName.substring(1, _smlName.length() - 1);
                if (type == DATATYPENAME) {
                    avatarName = _smlName.substring(5, _smlName.length() - 1);
                } else if (type == ATTRIBUTENAME) {
                    avatarName = _smlName.substring(2, _smlName.length() - 1);
                } else if (type == SIGNALNAME) {
                    avatarName = _smlName.substring(6, _smlName.length() - 1);
                } else if (type == STANDARDSTATENAME) {
                    avatarName = _smlName.substring(14, _smlName.length() - 1);
                } else if (type == BLOCKNAME) {
                    avatarName = _smlName.substring(6, _smlName.length() - 1);
                } else if (type == TIMERBLOCKNAME) {
                    avatarName = _smlName.substring(6, _smlName.length() - 1);
                } else {
                    avatarName = sysMLName;
                    if (type == QUOTEDNAME) type = AVATARNAME;
                }

            }
        }

        /**  @return type of the ident */
        public byte getType() { return type; }

        /**  @return the SysML name */
        public String getSysMLName() { return sysMLName; } //unused
        /**  @return the Avatar name */
        public String getAvatarName() { return avatarName; }
        /** Check ident type */
        public boolean is_DATATYPENAME() { return type == DATATYPENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_BLOCKNAME() { return type == BLOCKNAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_ATTRIBUTENAME() { return type == ATTRIBUTENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_RELATIONNAME() { return type == RELATIONNAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_CHANNELPRENAME() { return type == CHANNELPRENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_SIGNALNAME() { return type == SIGNALNAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_CHANNELNAME() { return type == CHANNELNAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_MESSAGENAME() { return type == MESSAGENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_STANDARDSTATENAME() { return type == STANDARDSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_STARTSTATENAME() { return type == STARTSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_STOPSTATENAME() { return type == STOPSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_RANDOMSTATENAME() { return type == RANDOMSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_COUNTSTATENAME() { return type == COUNTSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_SENDSTATENAME() { return type == SENDSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_RECEIVESTATENAME() { return type == RECEIVESTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_PRESENDSTATENAME() { return type == PRESENDSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_PRERECEIVESTATENAME() { return type == PRERECEIVESTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_SETTIMERSTATENAME() { return type == SETTIMERSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_RESETTIMERSTATENAME() { return type == RESETTIMERSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_EXPIRETIMERSTATENAME() { return type == EXPIRETIMERSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_PRESETTIMERSTATENAME() { return type == PRESETTIMERSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_PRERESETTIMERSTATENAME() { return type == PRERESETTIMERSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_PREEXPIRETIMERSTATENAME() { return type == PREEXPIRETIMERSTATENAME || type == AVATARNAME; }
        /** Check ident type */
        public boolean is_TIMERBLOCKNAME() { return type == TIMERBLOCKNAME || type == AVATARNAME; }
    }

    /** Class of classified expression terms */
    public abstract static class StxTerm extends StxElement {
        StxTerm(String _name, Location _left, Location _right) { super(_name, _left, _right); }
        StxTerm(String _name) { super(_name); }
        public String toString() { return getName(); }
    }

    /** Terms that are int/bool expressions.
     *
     *  They may be identifiers but not identified as such.*/
    public static class StxExpr extends StxTerm {
        public StxExpr(String v) { super(v); }
    }

    /** Terms that are Identifiers */
    public static class StxId extends StxTerm {
        private String value ;
        public StxId(String v) { super(v); }
    }

    /** Terms that are method call.
     *
     * (technical) a call string is split into a sequence
     * "segment ident segment ... ident segment"
     * allowing to easily replace the idents */
    public static class StxCall extends StxTerm {
        private ArrayList<String> segments;
        private ArrayList<String> idents;
        public StxCall() {
            super(null);
            segments = new ArrayList<String>();
            idents = new ArrayList<String>();
        }
        public String getName() { super.setName(toString()); return(super.getName()); }

        public void addSegment(String s) { segments.add(s); }

        public void addIdent(String s) { idents.add(s); }

        public int getNbIdents() { return idents.size(); }

        public String getSegment(int i) { return segments.get(i); }

        public String getIdent(int i) { return idents.get(i); }

        /** rebuilds the original string by concatenating segments and idents */
        public String toString() {
            StringBuffer result = new StringBuffer();
            int size = getNbIdents();
            int i;
            for (i =0 ; i < size; i++) {
                result.append(getSegment(i));
                result.append(getIdent(i));
            }
            result.append(getSegment(i));
            return result.toString();
        }
    }

    /** Syntactic elements with a name and locations in the source code.
     *
     *  When possible, usually the left is the beginning location and the right is the ending one.
     */
    public abstract static class StxElement {
        private String name;
        protected Location defaultLocation = new Location(-1,-1);
        private Location left;
        private Location right;
        private boolean declared;

        public StxElement(String _name, Location _left, Location _right){ name = _name; left = _left; right = _right; declared = false; }

        public StxElement(String _name){ name = _name; left = defaultLocation; right = defaultLocation; declared = false; }

        public void setLeft(Location _left) { left = _left; }

        public void setRight(Location _right) { right = _right; }

        public String getName() { return name; }

        public void setName(String _name) { name = _name; }

        public Location  getLeft() { return left; }

        public Location  getRight() { return right; }

        /** sets the element as declared
         *
         * @return true iff the element was not already declared
         */
        public boolean declare() { boolean res = !declared; declared = true; return res; }

        public boolean isDeclared() { return declared; }
    }

    /** Typed fields for syntactic elements associated to structures. */
    public static class StxField extends StxElement {
       private String type;
        private String initialValue;
        /** defaultValue = true means that the value is not explicitly set in Avatar */
        private boolean defaultValue;

        public StxField(String _name, String _type) {
            super(_name.trim());
            type = _type.trim();
            initialValue = "";
            defaultValue = false;
        }

        public StxField(Location _left, Location _right, String _name, String _type) {
            super(_name.trim(), _left, _right);
            type = _type.trim();
            initialValue = "";
            defaultValue = false;
        }

        /** Primitive types are "Integer" and "Boolean".
         * Other string are interpreted as datatype names.
         */
        public String getType() { return type; }

        public void setType(String _type) { type = _type; }

        public void setInitialValue(String _initialValue) { initialValue = _initialValue; }

        public String getInitialValue() { return initialValue; }

        /** defaultValue = true means that the value is not explicitly set in Avatar.
         *
         * Relevant only if initial value is equal to Avatar'initial value for the field's type
         */
        public void setDefaultValue(boolean b) { defaultValue = b; }

        /** defaultValue = true means that the value is not explicitly set in Avatar.
         *
         * Relevant only if initial value is equal to Avatar'initial value for the field's type
         */
        public boolean getDefaultValue() { return defaultValue; }
    }

    /** Syntactiv Elements associated Structures
     *
     * Structures have an ordered list of fields
     */
    public static class StxStructure extends StxElement {

        protected MyArray<StxField> fields;

        public StxStructure(String _name, Location _left, Location _right) {
            super(_name.trim(), _left, _right);
            fields = new MyArray<StxField>();
        }

        public StxStructure(String _name) {
            super(_name.trim());
            fields = new MyArray<StxField>();
        }

        /** add a field at the end of the list of fields.
         *
         * @param _field the field to add
         * @return null if success. Otherwise, the field found with same name.
         */
        public StxField addField(StxField _field){
            StxField result = null;
            for (StxField fld : fields) {
                if (fld.getName().equals(_field.getName())) {
                    result = fld;
                    break;
                }
            }
            if (result == null) fields.add(_field);
            return result;
        }

        /** amount of fields */
        public int getSize() { return fields.size(); }

        public StxField getField(int i) { return fields.get(i);}

        public String getFieldName(int i) { return fields.get(i).getName();}

        public String getFieldType(int i) { return fields.get(i).getType();}
    }

    /** Syntactic elements associated to signals that are not connected. */
    public static class StxJunkSignal extends StxStructure {
        boolean in;

        public StxJunkSignal(Location _left, Location _right, String _name) { super(_name, _left, _right); }

        public StxJunkSignal(String _name) { super(_name); }

        public void setin(boolean _in) {in = _in;}
        public boolean isin() {return in;}
    }

    /** Syntactic elements associated to Avatar datatype types. */
    public static class StxDataType extends StxStructure {

        public StxDataType(Location _left, Location _right, String _name) { super(_name, _left, _right); }

        public StxDataType(String _name) { super(_name); }
    }

    /** Syntactic elements associated to Avatar Relations.
     *
     * They are typed communication connections between blocks
     * that contain channels linking input and output signals */
    public static class StxRelation extends StxElement {
        ArrayList<StxChannel> channels;
        StxBlock block1;
        StxBlock block2;
        boolean blocking;
        boolean lossy;
        boolean isprivate;
        int sizeOfFIFO;
        boolean asynchronous;

        public StxRelation(String _name, Location _left, Location _right) {
            super(_name.trim(), _left, _right);
            channels = new ArrayList<StxChannel>();
            isprivate = false;
            lossy = false;
            sizeOfFIFO = 1;
            asynchronous = true;
            String block1 = null;
            String block2 = null;
        }

        public StxRelation(String _name) {
            super(_name.trim());
            channels = new ArrayList<StxChannel>();
            isprivate = false;
            lossy = false;
            sizeOfFIFO = 1;
            asynchronous = true;
            String block1 = null;
            String block2 = null;
        }

        public void setBlock1(StxBlock _block) { block1 = _block; }

        public void setBlock2(StxBlock _block) { block2 = _block; }

        public void setBlocking(Boolean _b) { blocking = _b; }

        public void setPrivate(Boolean _b) { isprivate = _b; }

        public void setLossy(Boolean _b) { lossy = _b; }

        public void setFifoSize(int _size) { sizeOfFIFO = _size; }

        public void setAsynchronous(boolean _b) { asynchronous = _b; }

        public void addChannel(StxChannel ch) { channels.add(ch); }

        public StxBlock getBlock1() { return block1; }

        public StxBlock getBlock2() { return block2; }

        public Boolean getBlocking() { return blocking; }

        public Boolean getPrivate() { return isprivate; }

        public Boolean getLossy() { return lossy; }

        public int getFifoSize() { return sizeOfFIFO; }

        public boolean getAsynchronous() { return asynchronous; }

        public int getSize() { return channels.size(); }

        public StxChannel getChannel(int i) { return channels.get(i); }
    }

    /** Syntactic elements associated to Avatar methods.
     *
     * Fields of structure correspond to the method's input profile.  */
    public static class StxMethod extends StxStructure {

        private String returnType;

        public StxMethod(Location _left, Location _right, String _name) { super(_name, _left, _right); returnType = null; }

        public StxMethod(String _name) { super(_name); returnType = null; }

        public String getReturnType() { return returnType; }

        public void setReturnType(String _s) { returnType = _s; }
    }

    /** Syntactic elements associated to Avatar attributes of blocks.
     *
     * defaultValue = true means that the initial value is not explicitly set.
     * It is ignored if the initial value is not the Avatar default one.
     */
    public static class StxAttribute extends StxElement {
        private String type;
        private String init;
        private boolean defaultValue;

        public StxAttribute(String _name, Location _left, Location _right, String _type){
            super(_name.trim(), _left, _right);
            type = _type;
            init = "";
            defaultValue = false;
        }

        public StxAttribute(String _name, String _type){
            super(_name.trim());
            type = _type;
            init = "";
            defaultValue = false;
        }

        public String getType() { return type; }

        public void setInit(String _s) { init = _s; }

        /** defaultValue = true means that the initial value is not explicitly set.
        * It is ignored if the initial value is not the Avatar default one.
        */
        public void setDefaultValue(boolean b) { defaultValue = b; }

        public String getInit() { return init; }

        /** defaultValue = true means that the initial value is not explicitly set.
        * It is ignored if the initial value is not the Avatar default one.
        */
        public boolean getDefaultValue() { return defaultValue; }
   }

    /** Syntactic elements associated to Avatar Timers, among attributes */
    public static class StxTimer extends StxAttribute {

        public StxTimer( String _name, Location _left, Location _right){ super(_name, _left, _right, ""); }

        public StxTimer(String _name){ super(_name, ""); }
    }

    /** Syntactic elements associated to Avatar  Blocks.
     *
     * (technical) Transitions are contained in their source state.
     * Names of constants, attributes, methods, signals and timers
     * must be different. */
    public static class StxBlock extends StxElement {
        StxBlock father = null;
        ArrayList<StxAttribute> attributes;
        ArrayList<StxAttribute> constants;
        ArrayList<StxMethod> methods;
        ArrayList<StxSignal> signals;
        ArrayList<StxJunkSignal> junksignals;
        ArrayList<StxTimer> timers;
        StxState[] states;

        public StxBlock(String _name, Location _left, Location _right) {
            super(_name, _left, _right);
            father = null;
            attributes = new ArrayList<StxAttribute>();
            constants = new ArrayList<StxAttribute>();
            methods = new ArrayList<StxMethod>();
            signals = new ArrayList<StxSignal>();
            junksignals = new ArrayList<StxJunkSignal>();
            timers = new ArrayList<StxTimer>();
        }

        public StxBlock(String _name) {
            super(_name);
            father = null;
            attributes = new ArrayList<StxAttribute>();
            constants = new ArrayList<StxAttribute>();
            methods = new ArrayList<StxMethod>();
            signals = new ArrayList<StxSignal>();
            junksignals = new ArrayList<StxJunkSignal>();
            timers = new ArrayList<StxTimer>();
        }

        public void setFather(StxBlock _father) { father = _father; }

        public StxState[] getStates() { return states; }

        public void setStates(StxState[] l) { states = l; }

        /** Add an attribute (at the end: order respected)
         * @param a the attribute to add
         * @return null in case of success. Otherwise, the found element with same name.
         */
        public StxElement addAttribute(StxAttribute a){
            for (StxSignal sig : signals) if (sig.getName().equals(a.getName())) return sig;
            for (StxJunkSignal sig : junksignals) if (sig.getName().equals(a.getName())) return sig;
            for (StxAttribute att : constants) if (att.getName().equals(a.getName())) return att;
            for (StxAttribute att : attributes) if (att.getName().equals(a.getName())) return att;
            for (StxAttribute att : timers) if (att.getName().equals(a.getName())) return att;
            for (StxMethod mth : methods) if (mth.getName().equals(a.getName())) return mth;
            attributes.add(a);
            return null;
        }

         /** Add a constant (at the end: order respected)
         * @param a the constant to add
         * @return null in case of success. Otherwise, the found element with same name.
         */
        public StxElement addConstant(StxAttribute a){
            for (StxSignal sig : signals) if (sig.getName().equals(a.getName())) return sig;
            for (StxJunkSignal sig : junksignals) if (sig.getName().equals(a.getName())) return sig;
            for (StxAttribute att : constants) if (att.getName().equals(a.getName())) return att;
            for (StxAttribute att : attributes) if (att.getName().equals(a.getName())) return att;
            for (StxAttribute att : timers) if (att.getName().equals(a.getName())) return att;
            for (StxMethod mth : methods) if (mth.getName().equals(a.getName())) return mth;
            constants.add(a);
            return null;
        }

        /** Add a method (at the end)
        * @param m the method to add
        * @return null in case of success. Otherwise, the found element with same name.
        */
        public StxElement addMethod(StxMethod m){
            for (StxSignal sig : signals) if (sig.getName().equals(m.getName())) return sig;
            for (StxJunkSignal sig : junksignals) if (sig.getName().equals(m.getName())) return sig;
            for (StxAttribute att : constants) if (att.getName().equals(m.getName())) return att;
            for (StxAttribute att : attributes) if (att.getName().equals(m.getName())) return att;
            for (StxAttribute att : timers) if (att.getName().equals(m.getName())) return att;
            for (StxMethod mth : methods) if (mth.getName().equals(m.getName())) return mth;
            methods.add(m);
            return null;
        }

        /** Add a signal (at the end)
         * @param s the signal to add
         * @return null in case of success. Otherwise, the found element with same name.
         */
        public StxElement addSignal(StxSignal s){
            for (StxSignal sig : signals) if (sig.getName().equals(s.getName())) return sig;
            for (StxJunkSignal sig : junksignals) if (sig.getName().equals(s.getName())) return sig;
            for (StxAttribute att : constants) if (att.getName().equals(s.getName())) return att;
            for (StxAttribute att : attributes) if (att.getName().equals(s.getName())) return att;
            for (StxAttribute att : timers) if (att.getName().equals(s.getName())) return att;
            for (StxMethod mth : methods) if (mth.getName().equals(s.getName())) return mth;
            signals.add(s);
            return null;
        }

        /** Add a signal (at the end)
         * @param s the signal to add
         * @return null in case of success. Otherwise, the found element with same name.
         */
        public StxElement addJunkSignal(StxJunkSignal s){
            for (StxSignal sig : signals) if (sig.getName().equals(s.getName())) return sig;
            for (StxJunkSignal sig : junksignals) if (sig.getName().equals(s.getName())) return sig;
            for (StxAttribute att : constants) if (att.getName().equals(s.getName())) return att;
            for (StxAttribute att : attributes) if (att.getName().equals(s.getName())) return att;
            for (StxAttribute att : timers) if (att.getName().equals(s.getName())) return att;
            for (StxMethod mth : methods) if (mth.getName().equals(s.getName())) return mth;
            junksignals.add(s);
            return null;
        }
        /** Add a timer (at the end)
        * @param t the timer to add
        * @return null in case of success. Otherwise, the found element with same name.
        */
        public StxElement addTimer(StxTimer t){
            for (StxSignal sig : signals) if (sig.getName().equals(t.getName())) return sig;
            for (StxJunkSignal sig : junksignals) if (sig.getName().equals(t.getName())) return sig;
            for (StxAttribute att : constants) if (att.getName().equals(t.getName())) return att;
            for (StxAttribute att : attributes) if (att.getName().equals(t.getName())) return att;
            for (StxAttribute att : timers) if (att.getName().equals(t.getName())) return att;
            for (StxMethod mth : methods) if (mth.getName().equals(t.getName())) return mth;
            timers.add(t);
            return null;
        }

        /** @return the containing block (null if none)
         */
        public StxBlock getFather() { return father; }

        public int getNbAttributes() { return attributes.size(); }

        public int getNbConstants() { return constants.size(); }

        public int getNbMethods() { return methods.size(); }

        public int getNbSignals() { return signals.size(); }
        public int getNbJunkSignals() { return junksignals.size(); }

        public int getNbTimers() { return timers.size(); }

        public StxAttribute getAttribute(int i) { return attributes.get(i); }

        public StxAttribute getConstant(int i) { return constants.get(i); }

        public StxMethod getMethod(int i) { return methods.get(i); }

        public StxSignal getSignal(int i) { return signals.get(i); }
        public StxJunkSignal getJunkSignal(int i) { return junksignals.get(i); }
        public StxTimer getTimer(int i) { return timers.get(i); }
    }

    /** Syntactic elements associated to Avatar signal connections (in Relations) **/
    public static class StxChannel extends StxElement {

        private StxInMessage inProfile; // typing the connected  input signal
        private StxOutMessage outProfile; // typing the connected  output signal
        private String blockA; // block of signalA
        private StxSignal signalA; // one of the connected signals
        private String blockB; // block of signalB
        private StxSignal signalB; // the other connected signals

        public StxChannel(String _name, Location _left, Location _right){
            super(_name, _left, _right);
            inProfile = null;
            outProfile = null;
            blockA = null;
            blockB = null;
            signalA = null;
            signalB = null;
        }

        public StxChannel(String _name){
            super(_name);
            inProfile = null;
            outProfile = null;
            blockA = null;
            blockB = null;
            signalA = null;
            signalB = null;
        }

        public void setInProfile(StxInMessage m) { inProfile = m; }

        public void setOutProfile(StxOutMessage m) { outProfile = m; }

        /** Set one of the connected signals.
         *
         * To be used twice: does nothing if both signals are set.
         * @param _block the bloc of the signal to set
         * @param s the signal to set
         */
        public void setSignal(String _block, StxSignal s) {
            if (signalA == null) { blockA = _block; signalA = s; }
            else if (signalB == null) { blockB = _block; signalB = s; }
        }

        /** commute signals so that signalA is the signal of the parameter block
         *
         * If possible... Usefull to respect block order in relations.
         * @param _block1 the block to which signalA is expected to belong
         */
        public void commuteSignals(String _block1) {
            boolean permut =
                    (blockB != null && blockB.equals(_block1)) || (blockA != null && ! blockA.equals(_block1));
            if (permut) {
                String auxStr = blockA;
                StxSignal auxSig = signalA;
                blockA = blockB;
                signalA = signalB;
                blockB = auxStr;
                signalB = auxSig;
            }
        }

        public StxInMessage getInProfile() { return inProfile; }

        public StxOutMessage getOutProfile() { return outProfile; }

        public StxSignal getSignalA() { return signalA; }

        public StxSignal getSignalB() { return signalB; }

        public String getBlockA() { return blockA; }

        public String getBlockB() { return blockB; }
    }

    /** Syntactic elements associated to Avatar Signals */
    public static class StxSignal extends StxElement {

        private boolean input;
        private StxChannel channel;

        public StxSignal(String _name, Location _left, Location _right){
            super(_name, _left, _right);
        }

        public StxSignal(String _name){
            super(_name);
        }

        /** Set signal direction
         * @param _b true for an input signal
         */
        public void setInput(boolean _b) { input = _b; }

        /** set the channel that connects the signal
         *
         * @param _c the channel (connection belonging to a relation)
         */
        public void setChannel(StxChannel _c) { channel = _c; }

        /** get the channel of the signal **/
        public StxChannel getChannel() { return channel; }

        /** signal direction
         *
         * @return true for receiving signals, false for sending
         */
       public boolean isInput() { return input; }
    }

    /** Syntactic elements associated to receiving signal profiles.
     *
     *  A profile is associated to one single signal (through an associated channel) */
    public static class StxInMessage extends StxStructure {

        protected StxChannel channel;

        public StxInMessage(String _name, Location _left, Location _right) { super(_name, _left, _right); }


        public StxInMessage(String _name) { super(_name); }

        /** Set the channel connecting the associated signal */
        public void setChannel(StxChannel c) {channel = c; }

        /** get the index of a field/parameter
         *
         * @return the index, or -1 if not found */
        public int indexOf(String s) {
            int i = 0;
            for (StxField f : fields) {
                if (f.getName().equals(s)) break;
                i++;
            }
            if (i == fields.size()) return -1;
            return i;
        }
    }

    /** Syntactic elements associated to sending signal profiles */
    public static class StxOutMessage extends StxInMessage {

        private StxInMessage inMessage; // associated input profile
        private LinkedHashMap<StxField,String> fieldMap; // maps parameters/fields to associated names in input profile

        public StxOutMessage(String _name, Location _left, Location _right, StxInMessage _inMessage) {
            super(_name, _left, _right);
            inMessage = _inMessage;
            fieldMap = new LinkedHashMap<StxField,String>();
        }

        public StxOutMessage(String _name, StxInMessage _inMessage) {
            super(_name);
            inMessage = _inMessage;
            fieldMap = new LinkedHashMap<StxField,String>();
        }

        /** Associate an input field name to an output field */
        public boolean addFieldLink(StxField outField, String inField){
            String test = fieldMap.get(outField);
            if (test == null) {
                fieldMap.put(outField, inField);
                return true;
            }
            return false;
        }


        /** to complete the output profile using infos from the associated input profile
         *
         * Applied to fieldMap which links both profiles. Used to set the type and the order
         *  (thus the relevant order is the one in the declaration of the input profile)
         */

        private class Complete implements BiConsumer<StxField, String> {
            List<AvatarFromSysMLError> errors; // to put encountered errors
            private boolean[] inProfile; // to verify that all input fields have been handled at the end

            public boolean[] getInProfile() { return inProfile; }

            public Complete(List<AvatarFromSysMLError> l){
                errors = l;
                inProfile = new boolean[inMessage.getSize()];
                for (int i = 0; i < inMessage.getSize(); i++) { inProfile[i] = false; }
            };

            public void accept(StxField o, String i) {
                int index = inMessage.indexOf(i);
                if ( index == -1) {
                    errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, o.getLeft(),"Field " +
                            o.getName() + " of out-message is " +
                            "associated to a field (" + i + ") in in-message that does not exist", inMessage.getLeft()));
                }
                else {
                    inProfile[index] = true; // input field handled
                    o.setType(inMessage.fields.get(index).getType()); // set type
                    fields.set(index, o); // respects input order
                }
            }
        }

        /** to complete the output profile (and some other data) using infos from the associated input profile.
         *
         *  Builds field list. Get types and fields order from associated input profile.
         *  (thus the relevant order is the one in the declaration of the input profile)
         *
         *  Also updates some other data in associated components (channel,...)
         *  To be run after Parsing for a consistent abstract syntax tree/model
         */
        public void complete(List<AvatarFromSysMLError> errors) {
            if (inMessage == null)
                if (errors != null) {
                    errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, getLeft(),
                            "in-message associated to out-message has not been set",null));
                    return;
                }
            // error case or default: missing fields -> copy input fields.
            if (fieldMap.isEmpty()) for (StxField f: inMessage.fields) fields.add(f);
            else {
                Complete cmp = new Complete(errors);
                fieldMap.forEach(cmp); // handle fields
                boolean[] inProfile = cmp.getInProfile();
                for(int i =0; i < inProfile.length; i++) { // verify that no input field is missing
                    if (!inProfile[i]) {
                        //errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.WARNING, getLeft(),
                        //        "out-message has no field associated to in-message field " + inMessage.getFieldName(i) + " --> field added",
                        //        inMessage.getLeft()));
                        // for consistency, to avoid some redoundant error messages later.
                        fields.set(i, new StxField(inMessage.getFieldName(i), inMessage.getFieldType(i)));
                    }
                }
            }

            channel = inMessage.channel; // set Channel
            if (channel == null)
                if (errors != null) {
                    errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, getLeft(),
                            "Channel of in-message associated to out-message has not been set",
                            inMessage.getLeft()));
                return;
            }
            channel.setOutProfile(this); //update channel
            if (channel.getSignalA() == null)
                if (errors != null) {
                    errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, getLeft(),
                            "Channel " + channel.getName() +
                                    " of in-message associated to out-message has no associated signal (missing bindings?)",
                            channel.getLeft()));
                    return;
            }
            channel.getSignalA().setChannel(channel); // complete one signal
            if (channel.getSignalB() == null)
                if (errors != null) {
                    errors.add(new AvatarFromSysMLError(AvatarFromSysMLError.ERROR, getLeft(),
                            "Channel " + channel.getName() +
                                    " of in-message associated to out-message has only one associated signal (missing binding?)",
                            channel.getLeft()));
                    return;
            }
            channel.getSignalB().setChannel(channel); // complete the other signal
        }
    }

    /** Syntactic elements associated to "boxes" of state-machines (states,communications,...) */
    public static class StxState extends StxElement {
        private byte type; // box kind
        private StxSignal signal; // for count (Avatar QueryOnSignal=
        private String variable; // for count and random
        private String minValue; // for random
        private String maxValue; // for random
        private String distributionLaw; // for random
        private HashMap<String,String> distributionExtra; // for random
        private MyArray<StxTransition>  transitions; // outgoing transitions

        public StxState(String _name) {
            super(_name);
            type = -1;
            signal = null;
            variable = null;
            minValue = null;
            maxValue = null;
            distributionLaw = null;
            distributionExtra = null;
            transitions = new MyArray<StxTransition>();
        }

        public byte getType() { return type; };

        public void setType(byte _b) { type = _b; };

        public StxSignal getSignal() { return signal; };

        public void setSignal(StxSignal _s) { signal = _s; };

        public String getVariable() { return variable; };

        public void setVariable(String _s) { variable = _s; };

        public String getMinValue() { return minValue; };

        public void setMinValue(String _s) { minValue = _s; };

        public String getMaxValue() { return maxValue; };

        public void setMaxValue(String _s) { maxValue = _s; };

        public String getDistributionLaw() { return distributionLaw; }

        public void setDistributionLaw(String _s) { distributionLaw = _s; }

        public HashMap<String,String> getDistributionExtra() { return distributionExtra; }

        public void setDistributionExtra(HashMap<String,String> _m) { distributionExtra = _m; }

        public List<StxTransition> getTransitions() { return transitions; }

        public StxTransition getTransition(int i) {
            if (i<0) return null;
            StxTransition result = transitions.get(i);
            if (result == null) {
                result = new StxTransition(i);
                transitions.set(i,result);
            }
            return result;
        }
    }

    /** Syntactic elements associated to transitions of state-machines.
     *
     *  some information associated to these transition is finally associated
     *  to their target state-machine "boxes" in Avatar model, in particular
     *  signal and timer information.
     */
    public static class StxTransition extends StxElement {

        private byte type; // kind : ordinary, sending, ...
        private final int index; // from sysML Model, yet useless for Avatar ...?
        private String guard; // for all transitions
        private String signalName; // for sending and receiveving transitions
        private String blockName;  // block of the containing state-machine (technical)
        private StxSignal signal; // for sending and receiveving transitions, set from signalName, once parsing is completed.
        private String timer; // for timer operation transitions
        private ArrayList<StxTerm> sendPayload;  // for sending transitions
        private HashMap<String,String> receivePayload; // for receiving transitions
        private String minDelay; // for all transitions
        private String maxDelay; // for all transitions
        private double probability; // for all transitions
        private String delayDistributionLaw; // for all transitions
        private HashMap<String,String> delayExtra; // for all transitions
        private StxState target; // for all transitions
        private ArrayList<StxAction> actions; // for standard transitions (no communication & no timer)
        private Location rleft; // location of associated request in source state
        private Location rright; // location of associated request in source state
        private boolean requested; // set when associated request is parsed;

        public StxTransition(int _index){
            super("transition_" + _index);
            type = -1;
            index = _index;
            guard = null;
            signal = null;
            sendPayload = null;
            receivePayload = null;
            minDelay = "";
            maxDelay = "";
            delayDistributionLaw = null;
            delayExtra = null;
            target = null;
            actions = new ArrayList<StxAction>();
            rleft = defaultLocation;
            rright = defaultLocation;
            requested = false;
            probability = AvatarTransition.DEFAULT_PROBABILITY;
        }

        public void setType(byte _t) { type = _t; };

        public void setGuard(String _s) { guard = _s; }

        public void setSignal(StxSignal _s) { signal = _s; }

        public void setSignalName(String _blockname, String _signalName) {
            signalName  = _signalName; blockName = _blockname;
        }

        public void setSendPayload(ArrayList<StxTerm> _p) { sendPayload = _p; }

        public void setReceivePayload(HashMap<String,String> _p) { receivePayload = _p; }

        public void setTimer(String _s) { timer = _s; }

        public void setMinDelay(String _s) { minDelay = _s; }

        public void setMaxDelay(String _s) { maxDelay = _s; }

        public void setProbability(double _p) { probability = _p; }

        public void setDelayDistributionLaw(String _s) { delayDistributionLaw = _s; }

        public void setDelayExtra(HashMap<String,String> _m) { delayExtra = _m; }

        public void setTarget(StxState _s) { target = _s; }

        public void setActions(ArrayList<StxAction> _a) { actions = _a; }

        /** location of associated request in source state */
        public void setRleft(Location _l) { rleft = _l; }

        /** location of associated request in source state */
        public void setRright(Location _l) { rright = _l; }

        /** to set when associated request is parsed */
        public void setRequested(boolean _b) { requested = _b; }

        public byte getType() { return type; }

        public int getIndex() { return index; }

        public String getGuard() { return guard; }

        public String getTimer() { return timer; }

        public StxSignal getSignal() { return signal; }

        public String getSignalName() { return signalName; }

        public String getBlockName() { return blockName; }

        public ArrayList<StxTerm> getSendPayload() { return sendPayload; }

        public HashMap<String, String> getReceivePayload() { return receivePayload; }

        public String getMinDelay() { return minDelay; }

        public String getMaxDelay() { return maxDelay; }

        public double getProbability() { return probability; }

        public String getDelayDistributionLaw() { return delayDistributionLaw; }


        public HashMap<String,String> getDelayExtra() { return delayExtra; }

        public StxState getTarget() { return target; }

        public ArrayList<StxAction> getActions() { return actions; }

        /** location of associated request in source state */
        public Location getRleft() { return rleft; }

        /** location of associated request in source state */

        public Location getRright() { return rright; }

        /** test if associated request is parsed */
        public boolean isRequested() { return requested; }
    }

    /** Syntactic elements associated to transitions of state-machines.
     *
     *  some information associated to these transition is finally associated
     *  to their target state-machine "boxes" in Avatar model, in particular
     *  signal and timer information.
     */
    public static class StxAction extends StxElement {

        private byte type; // assignment or method call
        private StxTerm value; // method call or assigned value
        private String target; // assignad variable

        public StxAction (StxTerm _value) {
            super("methodCall_" + _value.toString());
            type = STXMETHODACTION;
            value = _value;
        }

        public StxAction (Location _left, Location _right, StxTerm _value) {
            super("methodCall_" + _value.toString(), _left, _right);
            type = STXMETHODACTION;
            value = _value;
        }

        public StxAction (String _target, StxTerm _value) {
            super("assignment_" + _target + "<-" + _value.toString() );
            type = STXASSIGNACTION;
            value = _value;
            target = _target;
        }

         public StxAction (Location _left, Location _right, String _target, StxTerm _value) {
            super("assignment_" + _target + "<-" + _value.toString(), _left, _right);
            type = STXASSIGNACTION;
            value = _value;
            target = _target;
        }

        public byte getType() { return type; }

        public StxTerm getValue() { return value; }

        public String getTarget() { return target; }
    }

    /** syntactic element corresponding to a complete model -- complex Abstract Syntax Tree */
    public static class StxModel{

        private ArrayList<AvatarFromSysMLError> errors; // error encountered while parsing and finalizing the syntactic model
        private LinkedHashMap<String,StxDataType> dataTypeMap;
        private LinkedHashMap<String,StxRelation> relationMap;
        private LinkedHashMap<String,StxChannel> channelMap;
        private LinkedHashMap<String,StxBlock> blockMap;
        private LinkedHashMap<String,StxSignal> signalMap;

        /** parameters are the ones filled by the associated parser.
         *
         * all parameters are kept as internal attribute except the two last ones that are used for finalization (called by constructor)
         * @param outMessageMap out messages that require finalization
         * @param commTransitions transitions that require finalization
         */
        public StxModel(
                ArrayList<AvatarFromSysMLError> _errors,
                LinkedHashMap<String,StxDataType> _dataTypeMap,
                LinkedHashMap<String,StxRelation> _relationMap,
                LinkedHashMap<String,StxChannel> _channelMap,
                LinkedHashMap<String,StxBlock> _blockMap,
                LinkedHashMap<String,StxSignal> _signalMap,
                LinkedHashMap<String, StxOutMessage> outMessageMap,
                List<StxTransition> commTransitions
        ) {
            errors = _errors;
            dataTypeMap = _dataTypeMap;
            relationMap = _relationMap;
            channelMap = _channelMap;
            blockMap = _blockMap;
            signalMap = _signalMap;
            this.finalize(outMessageMap, commTransitions);
        }

        public ArrayList<AvatarFromSysMLError> getErrors() { return errors; }

        public LinkedHashMap<String,StxDataType> getDataTypeMap() { return dataTypeMap; }

        public LinkedHashMap<String,StxRelation> getRelationMap() { return relationMap; }

        public LinkedHashMap<String,StxChannel> getChannelMap() { return channelMap; }

        public LinkedHashMap<String,StxBlock> getBlockMap() { return blockMap; }

        public LinkedHashMap<String,StxSignal> getSignalMap() { return signalMap; }

        /** complete Signal information
         *
         * Find signals associated to signal names in transitions */
        private void updateSignal(StxTransition st) {
            String name = st.getSignalName();
            StxBlock blk = blockMap.get(st.getBlockName());
            while (blk != null) {
                int size = blk.getNbSignals();
                int i;
                for (i = 0; i < size; i++) {
                    StxSignal s = blk.getSignal(i);
                    if (s.getName().equals(name)) {
                        st.setSignal(s);
                        break;
                    }
                }
                if (i < size) blk = null;
                else blk = blk.getFather();
            }
        }

        private class CompleteOutMessage implements BiConsumer<String, StxOutMessage> {
            public CompleteOutMessage(){}
            public void accept(String s, StxOutMessage o) { o.complete(errors); }
        }

        /** complete OutMessage related information.
         *
         * Run completion algorithm provided by OutMessage class: set missing links between channels, signals and profiles.
         */
        public void finalize(LinkedHashMap<String, StxOutMessage> outMessageMap, List<StxTransition> commTransitions) {
            outMessageMap.forEach(new CompleteOutMessage());
            for(StxTransition st: commTransitions) updateSignal(st);
        }
    }
}
