/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * Class Avatar2SysMLNames
 * Creation: 20/06/2024
 *
 * @author Sophie Coudert
 * @version 0.1 20/06/2024
 */
package avatartranslator.tosysmlv2;
import java_cup.runtime.ComplexSymbolFactory;

import common.CDElement;

import java.io.StringReader;
import java.util.List;

/**
 * Class Avatar2SysMLNames
 * Creation: 20/06/2024
 *
 * Generation of SysML idents from Avatar names.
 *
 * <p> This ensures that the identifiers used in the Avatar Model don't clash
 * with the SysML keywords or some fieldName in the used SysML libraries.
 * The parsing of AvatarFromSysML recovers the original Avatar names from these identifiers.
 * "name" parameters for these name generators are intended to be the original Avatar names.
 * Except for attributes, all the parameter names are short names (Avatar names without ".").
 * </p>
 * <p> Some name generators require an integer as last parameter. This parameter is used to be
 * able to produce different identifiers for the same values of the other parameters</p>
 *
 * @author Sophie Coudert
 * @version 0.1 20/06/2024
 */
public class Avatar2SysMLNames {
    /** type of synchroneous communications */
    public final static int SYNC = 0;
    /** type of blocking-fifo communications */
    public final static int BFIFO = 1;
    /** type of non-blocking-fifo communications */
    public final static int NBFIFO = 2;


    public static String dataTypeSysMLname(String name){ return "'@dt:" + name.trim() + "'"; }

    public static String blockSysMLname(String name){ return "'@blk:" + name.trim() + "'"; }

    /** attribute names may be chained using "." as separator */
    public static String attributeSysMLname(String _name){
        if (_name == null) return null;
        String name = _name.trim();
        if (_name.equals("") || _name.equals(".")) return null;
        String[] list = name.split("\\.");
        int length = list.length;
        if (length == 0)  return null;
        StringBuffer result = new StringBuffer("'$" + list[0] + "'");
        for(int i = 1; i < length; i++)
            result.append(".'" + list[i] + "'");
        return result.toString();
    }

    /** specific... as SysML requires a "::" as first separator (instead of ".") for variable assignment */
    public static String leftHandSysMLname(String _name){
        if (_name == null) return null;
        String name = _name.trim();
        if (_name.equals("") || _name.equals(".")) return null;
        String[] list = name.split("\\.");
        int length = list.length;
        if (length == 0)  return null;
        StringBuffer result = new StringBuffer("'$" + list[0] + "'");
        for(int i = 1; i < length; i++)
            result.append("::'" + list[i] + "'");
        return result.toString();
    }
    /** Note: method name are similar to attribute name (do not change, code rely on this) */
    public static String methodSysMLname(String name){ return "'$" + name.trim() + "'"; }

    public static String fieldSysMLname(String name){ return "'" + name.trim() + "'"; }

    /** Relation names contain information about the kind of communication.
     *
     * @param b1 bloc name
     * @param b2 bloc name
     * @param type among SYNC, FIFO and NBFIFO
     */
    public static String relationSysMLname(String b1, String b2, int type, int n){
        if (type == NBFIFO) return "'@NBF" + n + ":" + b1.trim() + "-" + b2.trim() + "'";
        if (type == BFIFO) return "'@BF" + n + ":" + b1.trim() + "-" + b2.trim() + "'";
        if (type == SYNC) return "'@SYN" + n + ":" + b1.trim() + "-" + b2.trim() + "'";
        return "";
    }

    /** channelNames are informative substrings of channelSysMLNames.
     *
     * a channel connects two signals s1 and s2 belonging respectively to blocs b1 and b2
     *
     * @param out2in must be true if s1 is an output signal
     */
    public static String channelName(String b1, String b2, String s1, String s2, boolean out2in){
        if(out2in) return b1.trim() + "." + s1.trim() + ">" + b2.trim() + "." + s2.trim();
        else return b1.trim() + "." + s1.trim() + "<" + b2.trim() + "." + s2.trim();
    }

    public static String signalSysMLname(String _name){ return "'@sig:" + _name + "'"; }

    /** Channel SysML names contain information about the kind of communication.
     *
     * @param type among SYNC, FIFO and NBFIFO
     */
    public static String channelSysMLname(String _signalname, int type){
        if (type == NBFIFO) return "'@nbf:" + _signalname.trim() + "'";
        if (type == BFIFO) return "'@bf:" + _signalname.trim() + "'";
        if (type == SYNC) return "'@syn:" + _signalname.trim() + "'";
        return "";
    }

    /** messages denote structures that are signal profiles */
    public static String messageSysMLname(String _blockname, String _signalsname){
        return "'@MSG:" + _blockname.trim() + "." + _signalsname.trim() + "'";
    }

    public static String startStateSysMLname(){ return "'@st:start'"; }

    public static String stopStateSysMLname(){ return "'@st:stop'"; }

    /** associated to AvatarStates */
    public static String standardStateSysMLname(String name){ return "'@st:standard." + name.trim() + "'"; }

    /** associated to AvatarRandoms */
    public static String randomStateSysMLname(int number){ return "'@st:random." + number + "'"; }

    /** associated to QueriesOnSignal */
    public static String countStateSysMLname(String sigName, int number){ return "'@st:count." + sigName.trim() + "." + number + "'"; }

    /** associated to sending AvatarActionOnSignal */
    public static String sendStateSysMLname(String sigName, int number){ return "'@st:send." + sigName.trim() + "." + number + "'"; }

    /** associated to receiving AvatarActionOnSignal */
    public static String receiveStateSysMLname(String sigName, int number){ return "'@st:receive." + sigName.trim() + "." + number + "'"; }

    /** (technical) added states before some sending AvatarActionOnSignal */
    public static String presendStateSysMLname(String sigName, int number){ return "'@st:presend." + sigName.trim() + "." + number + "'"; }

    /** (technical) added states before some receiving AvatarActionOnSignal */
    public static String prereceiveStateSysMLname(String sigName, int number){ return "'@st:prereceive." + sigName.trim() + "." + number + "'"; }

    /** associated to AvatarSetTimer */
    public static String setTimerStateSysMLname(String timerName, int number){ return "'@st:set." + timerName.trim() + "." + number + "'"; }

    /** associated to AvatarResetTimer */
    public static String resetTimerStateSysMLname(String timerName, int number){ return "'@st:reset." + timerName.trim() + "." + number + "'"; }

    /** associated to AvatarExpireTimer */
    public static String expireTimerStateSysMLname(String timerName, int number){ return "'@st:expire." + timerName.trim() + "." + number + "'"; }

    /** (technical) added states before some AvatarSetTimer */
    public static String presetTimerStateSysMLname(String timerName, int number){ return "'@st:preset." + timerName.trim() + "." + number + "'"; }

    /** (technical) added states before some AvatarResetTimer */
    public static String preresetTimerStateSysMLname(String timerName, int number){ return "'@st:prereset." + timerName.trim() + "." + number + "'"; }

    /** (technical) added states before some AvatarExpireTimer */
    public static String preexpireTimerStateSysMLname(String timerName, int number){ return "'@st:preexpire." + timerName.trim() + "." + number + "'"; }

    /** associated to Avatar timers (represented by blocks) */
    public static String timerBlockSysMLname(String timerName){ return "'@tmr:" + timerName.trim() + "'"; }

    /** convert an Avatar int/bool expression into a SysML expression */
    public static String expr2SysML(String _expr) {
        Avatar2SysMLParser parser = new Avatar2SysMLParser(new Avatar2SysMLLexer(new StringReader (_expr)));
        try {
            String res = (String)parser.parse().value;
            return res;
        }
        catch (java.lang.Exception e) {
            e.printStackTrace();
            return ""; }
    }
}

