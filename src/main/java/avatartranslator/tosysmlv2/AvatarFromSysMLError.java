/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package avatartranslator.tosysmlv2;
import java_cup.runtime.ComplexSymbolFactory.Location;

/**
 * Class AvatarFromSysMLError
 * Creation: 20/06/2024
 *
 * Errors that are reported when parsing an Avatar SysML model and building the corresponding Avatar Model.
 * These errors are (potentially) provided with two locations. Idea: the location where the error occurs + a reference location for conflicting
 * declarations, for example.
 * @author Sophie Coudert
 * @version 0.1 20/06/2024
 */
public class AvatarFromSysMLError {
    public static final byte WARNING = 0;
    public static final byte ERROR = 1;
    public static final byte BUG = 2;
    /** location (sometime approximative) where the error has been detected */
    private Location location;
    /** reference location concerned by the error, in case of conflict for example */
    private Location reference;
    /** message describing the error */
    private String message;
    /** seriousness of the error */
    private byte level;

    public AvatarFromSysMLError(Byte lv, Location loc, String msg, Location ref){
        location = loc;
        reference = ref;
        message = msg;
        level = lv;
    }
    public AvatarFromSysMLError(Byte lv, Location loc, String msg){
        location = loc;
        reference = null;
        message = msg;
        level = lv;
    }
    public AvatarFromSysMLError(Byte lv, String msg){
        location = null;
        reference = null;
        message = msg;
        level = lv;
    }
    public byte getLevel() { return level; }
    @Override
    public String toString() {
        StringBuffer res = new StringBuffer();
        if (level > BUG || level < WARNING)
            res.append("PROBLEM ");
        else if (level == WARNING)
            res.append("WARNING ");
        else if (level == ERROR)
            res.append("ERROR ");
        else
            res.append("BUG ");
        if (location != null)
            res.append("(l" + (location.getLine() + 1) + ",c" + (location.getColumn() + 1) + ")");
        res.append(": " + message);
        if (reference != null)
            res.append(" (c.f. l" + (reference.getLine() + 1) + ",c" + (reference.getColumn() + 1) + ")");
        //res.append("\n");
        return res.toString();
    }

}
