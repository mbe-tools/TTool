package common;

public interface ICDElementVisitor {
	
	boolean visit( CDElement element );
}
