package common;

public abstract class AbstractCDElement implements CDElement {
	
	private boolean enabled;
    protected int elementID = -1; // -1 means NEVER_SET
	
	protected AbstractCDElement() {
		enabled = true;
	}

    @Override
    public int getHeight() {
        return 0;
    }
    @Override
    public void setElementID(int id) {
        elementID = id;
    }
    @Override
    public int getElementID() {
        return elementID;
    }

    /**
     * Issue #69
     * @param _enabled  :   boolean data type
     */
    @Override
    public void setEnabled( final boolean _enabled ) {
    	doSetEnabled( _enabled );
    }
    
    /**
     * Issue #69
     * @param _enabled  :   boolean data type
     */
    @Override
    public void doSetEnabled( final boolean _enabled ) {
    	enabled = _enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean isEnabled( boolean checkBranch ) {
        return isEnabled();
    }

    /**
     * Issue #69
     * @return  :   Always False
     */
    @Override
    public boolean canBeDisabled() {
    	return false;
    }

    /**
     * Issue #69
     * @param label :   Label
     * @return      :   Always False
     */

    public boolean canLabelBeDisabled( LabelElement label ) {
    	return false;
    }
    
    /* Issue #69
     * (non-Javadoc)
     * @see common.CDElement#acceptForward(common.ICDElementVisitor)
     */
    @Override
	public void acceptForward( final ICDElementVisitor visitor ) {
		visitor.visit( this );
    }
    
    /* Issue #69
     * (non-Javadoc)
     * @see common.CDElement#acceptBackward(common.ICDElementVisitor)
     */
    @Override
	public void acceptBackward( final ICDElementVisitor visitor ) {
		visitor.visit( this );
    }
}
