package ai;

import common.ConfigurationTTool;
import launcher.RshClient;
import launcher.RshClientReader;
import myutil.TraceManager;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class AIUseCaseDiagram
 * <p>
 * Creation: 18/03/2024
 *
 * @author Alan Birchler De Allende
 */
public class CAPECTracer extends AIInteract {
    private final String QUESTIONTRACECAPECS = "From the provided system specifications, identify all of the possible " +
            "attack patterns that an attacker could use to exploit the system.";
    private final String QUESTIONMITIGATIONS = "From the provided attack, identify possible " +
            "countermeasures that could help offset an attack.";

    private final Path projectPath;
    private String results;

    public CAPECTracer(AIChatData _chatData) {
        super(_chatData);
        projectPath = Paths.get("../capectracer").normalize().toAbsolutePath();
        results = "";
    }

    public String getResults() {
        return this.results;
    }

    @Override
    public void internalRequest() {
        String command = getCommand(projectPath.toString(), false);

        chatData.feedback.addToChat(QUESTIONTRACECAPECS, true);
        String systemSpec = chatData.lastQuestion.trim();

        writeToFile(projectPath + "/system_specs.txt", systemSpec);

        String results = runCapecTracer(projectPath.toString(), command);
        chatData.feedback.addToChat(results, false);
        this.results = results;
    }

    public void getMitigations(String attack) {
        String command = getCommand(projectPath.toString(), true);
        String question = QUESTIONMITIGATIONS + "\nThe attack is the following: " + attack + "\n";

        chatData.feedback.addToChat(question, true);

        writeToFile(projectPath + "/attack.txt", attack);

        String results = runCapecTracer(projectPath.toString(), command);
        chatData.feedback.addToChat(results, false);
        this.results = results;
    }

    public String getInfo(String command) {
        return "Running command: " + command;
    }

    private void writeToFile(String filePath, String content) {
        try {
            // Create a FileWriter object
            FileWriter fileWriter = new FileWriter(filePath, false);

            // Wrap the FileWriter in a BufferedWriter for efficient writing
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            // Write the content to the file
            bufferedWriter.write(content);

            // Close the BufferedWriter to flush the buffer and close the file
            bufferedWriter.close();

        } catch (IOException e) {
            TraceManager.addDev(e.getMessage());
        }
    }

    private String getCommand(String capecTracerFolder, boolean getMitigations) {
        String python = "python3";
        String command;

        if (!ConfigurationTTool.PythonPathForCapec.isEmpty()) {
            python = ConfigurationTTool.PythonPathForCapec;
        }

        command = python + " " + capecTracerFolder + "/capec_tracer.py";

        if (getMitigations) {
            command += " --mitigations";
        }

        return command;
    }

    private String runCapecTracer(String capecTracerFolder, String command) {
        String traces = "";
        String output = "";

        try {
            RshClient rshc = new RshClient("localhost");
            rshc.setCmd(command);
            rshc.sendExecuteCommandRequest();
            RshClientReader data = rshc.getDataReaderFromProcess();
            int characterInt = data.read();
            StringBuilder outputBuilder = new StringBuilder();

            while (characterInt != -1) {
                outputBuilder.append((char) characterInt);
                characterInt = data.read();
            }

            output = outputBuilder.toString();
            TraceManager.addDev(output);

            byte[] bytes = Files.readAllBytes(Path.of(capecTracerFolder + "/traces.txt"));
            traces = new String(bytes);
        } catch (Exception e) {
            TraceManager.addDev(e.getMessage());
        }

        if (!traces.contains("Error") &&
                !output.contains("Error") &&
                !traces.contains("Failed to download the list of CAPECs from MITRE.")) {
            return traces;
        }
        else {
            return "The tracer failed to run successfully.";
        }
    }

    @Override
    public Object applyAnswer(Object input) {
        return null;
    }
}
