package ai;

import attacktrees.Attack;
import attacktrees.AttackNode;
import attacktrees.AttackTree;
import myutil.TraceManager;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AIAPTMitigations extends AIInteract {
    private static final String KNOWLEDGE_ON_JSON_FOR_MITIGATIONS = "When you are asked to identify " +
            "mitigations for a provided list of attack steps, " +
            "return them as a JSON specification formatted as follows: " +
            "{\"mitigations\": [{\"name\":  \"NameOfMitigation\", \"description\": " +
            "\"The description of the mitigation and how it prevents the attack.\"}, " +
            "\"attacksteps\": [\"TheNameOfAProvidedAttackStep\" ...]} ...]} " +
            "# Respect: All words in \"name\" must be conjoined together. " +
            "# Respect: There must be no more than forty characters in \"name\". " +
            "# Respect: For each word in \"name\", its first letter must be capitalized. " +
            "# Respect: All words in \"description\" must be separated with spaces. " +
            "# Respect: \"attacksteps\" is the list of attack steps that are associated with the " +
            "mitigation. It must contain only the names of the associated attack steps that are " +
            "from the provided list of attack steps. " +
            "# Respect: If there are no mitigations that are able to be identified, have \"mitigations\" be " +
            "an empty array.";

    private static final String[] KNOWLEDGE_STAGES = {
            KNOWLEDGE_ON_JSON_FOR_MITIGATIONS
    };

    private final String[] QUESTION_IDENTIFY_ATD = {
            "Using the specified JSON format, " +
                    "identify a list of possible mitigations that would prevent an attacker " +
                    "from using most, if not all, of the provided attack steps to further advance " +
                    "the attacker's attack scenario. Each mitigation should be associated with at least " +
                    "one attack step from the provided attack step list. If applicable, use the " +
                    "provided list of possible countermeasures as support for identifying mitigations. " +
                    "Do respect the JSON format, and provide only JSON (no explanation before or after).\n"
    };

    private final APTAuditor auditor;
    private int maxRetries;
    private final CAPECTracer tracer;
    private AttackTree at;

    public AIAPTMitigations(AIChatData _chatData) {
        super(_chatData);
        maxRetries = 10;
        auditor = new APTAuditor();
        tracer = new CAPECTracer(chatData);
    }

    public void setReferenceAT(AttackTree at) {
        this.at = at;
    }

    public void setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
    }

    public Object applyAnswer(Object input) {
        return at;
    }

    private void initKnowledge() {
        chatData.aiinterface.clearKnowledge();
    }

    private void makeKnowledge(String[][] attackSteps, String[] counters) {
        String[] know = KNOWLEDGE_STAGES[0].split("#");

        for (String s : know) {
            TraceManager.addDev("\nKnowledge added: " + s);
            chatData.aiinterface.addKnowledge(s, "ok");
        }

        if (attackSteps != null && attackSteps.length > 0) {
            String attackStepsString = buildDelimStringFromMatrix(attackSteps);
            TraceManager.addDev("\nKnowledge added: " + attackStepsString);
            chatData.aiinterface.addKnowledge("The attack steps are: " + attackStepsString, "ok");
        }

        if (counters != null && counters.length > 0) {
            String countersString = buildDelimStringFromArray(counters);
            TraceManager.addDev("\nKnowledge added: " + countersString);
            chatData.aiinterface.addKnowledge("The possible counters are: " + countersString, "ok");
        }
    }

    public void internalRequest() {
        ArrayList<Attack> attackNodes = at.getAttacks();
        ArrayList<String> attackSteps = new ArrayList<>();
        Set<String> countersSet = new HashSet<>();

        for (Attack attackNode : attackNodes) {
            if (!attackNode.isRoot() && attackNode.isLeaf()) {
                String attackStepName = attackNode.getName();
                String attackStepDesc = attackNode.getDescription();
                String attackStep = attackStepName + ". " + attackStepDesc;
                attackSteps.add(attackStep);

                tracer.getMitigations(attackStepDesc);
                String traces = tracer.getResults();

                if (!traces.equals("The tracer failed to run successfully.")) {
                    String[] possibleCounters = tracer.getResults().split("\n\n");
                    countersSet.addAll(Arrays.asList(possibleCounters));
                }
                else {
                    return;
                }
            }
        }

        String[] counters = countersSet.toArray(new String[0]);
        String[] attSteps = attackSteps.toArray(new String[0]);
        makeRequest(attSteps, counters);

        waitIfConditionTrue(true);
        TraceManager.addDev("Reached end of AIAPTMitigations internal request.");
    }

    private void makeRequest(String[] attackSteps, String[] counters) {
        int numRetries = 0;
        boolean done = false;
        String json = "";
        String questionT = QUESTION_IDENTIFY_ATD[0];
        String[][] attackStepData = extractAttStepData(attackSteps);

        while (numRetries < maxRetries && !done) {
            initKnowledge();
            makeKnowledge(attackStepData, counters);
            boolean ok = makeQuestion(questionT);

            if (!ok) {
                TraceManager.addDev("Make question failed");
            }

            ArrayList<String> errors = new ArrayList<>();

            try {
                TraceManager.addDev("\n\nMaking specification from " + chatData.lastAnswer + "\n\n");
                json = extractJSON();
                JSONObject mitigationsJSON = auditor.checkMitigations(at, json, attackStepData, errors);

                if (mitigationsJSON.isEmpty() && !errors.isEmpty()) {
                    errors.add("You must provide a list of possible mitigations that would prevent an attacker " +
                            "from using most, if not all, of the provided attack steps to further advance " +
                            "the attacker's attack scenario. Do respect the JSON format, and " +
                            "provide only JSON (no explanation before or after).");
                }

                if (!mitigationsJSON.isEmpty()) {
                    TraceManager.addDev("Identified mitigations: " + mitigationsJSON);
                }
            }
            catch (org.json.JSONException e) {
                TraceManager.addDev("Invalid JSON spec: " + extractJSON() + " because " + e.getMessage() + ": INJECTING ERROR");
                errors = new ArrayList<>();
                errors.add("There is an error in your JSON: " + e.getMessage() + ". Probably the JSON spec was incomplete. " +
                        "Do correct it. I need the full specification at once.");
            }

            if (!errors.isEmpty()) {
                questionT = "Your answer was as follows: " + json + "\n\nYet, it was not correct because of the following errors:";
                // Updating knowledge
                for (String s : errors) {
                    questionT += "\n- " + s;
                }

                numRetries++;
            }
            else {
                done = true;
            }
        }
    }

    private String buildDelimStringFromArray(String[] list) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i <= list.length - 1; i++) {
            String listItemCleaned = list[i].trim();
            builder.append(listItemCleaned);

            if (i < list.length - 1) {
                builder.append(" | ");
            }
        }

        return builder.toString();
    }

    private String buildDelimStringFromMatrix(String[][] list) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i <= list.length - 1; i++) {
            String attStepName = list[i][0].trim();
            String attStepDesc = list[i][1].trim();
            builder.append("Name: ").append(attStepName).append(" Description: ").append(attStepDesc);

            if (i < list.length - 1) {
                builder.append(" | ");
            }
        }

        return builder.toString();
    }

    private String[][] extractAttStepData(String[] attackSteps) {
        String[][] attStepData = new String[attackSteps.length][2];

        for (int i = 0; i < attackSteps.length; i++) {
            int periodIndex = attackSteps[i].indexOf('.');
            String attackStepName = attackSteps[i].substring(0, periodIndex);
            String attackStepDesc = attackSteps[i].substring(periodIndex + 2);
            String[] data = {attackStepName, attackStepDesc};
            attStepData[i] = data;
        }

        return attStepData;
    }
}
