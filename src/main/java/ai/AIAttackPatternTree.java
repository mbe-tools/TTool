package ai;

import attacktrees.*;
import myutil.TraceManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * Class AIUseCaseDiagram
 * <p>
 * Creation: 19/03/2024
 *
 * @author Alan Birchler De Allende
 */
public class AIAttackPatternTree extends AIInteract {
    private final String KNOWLEDGE_ON_JSON_FOR_ROOT = "When you are asked to identify a motive, " +
            "return the motive formatted as JSON like so: " +
            "{\"rootattack\": {\"name\":  \"NameOfMotive\", \"description\": \"" +
            "The description of the motive.\"}} " +
            "# Respect: All words in \"name\" must be conjoined together. " +
            "# Respect: There must be no more than forty characters in \"name\". " +
            "# Respect: For each word in \"name\", its first letter must be capitalized. " +
            "# Respect: All words in \"description\" must be separated with spaces.";

    private final String KNOWLEDGE_ON_JSON_FOR_ATTACK_SCEN = "When you are asked to identify all the possible " +
            "attack scenarios that an attacker would perform to successfully achieve an " +
            "attack, return them as a JSON specification formatted as follows: " +
            "{\"attack\": \"NameOfAttack\", \"attackscenarios\": [{\"name\":  \"NameOfAttackScenario\", \"description\": \"" +
            "The description of the attack scenario and how it brings an attacker closer to the attack.\"} ...]} " +
            "# Respect: All words in \"name\" must be conjoined together. " +
            "# Respect: \"attack\" must be the name of the provided attack. " +
            "# Respect: There must be no more than forty characters in \"name\". " +
            "# Respect: For each word in \"name\", its first letter must be capitalized. " +
            "# Respect: All words in \"description\" must be separated with spaces. " +
            "# Respect: If there are no attack scenarios that are able to be identified, have \"attackscenarios\" be " +
            "an empty array.";

    private final String KNOWLEDGE_ON_JSON_FOR_ATTACK_GROUPS = "When you are asked to group all of the provided " +
            "attack scenarios, return them as a JSON specification formatted as follows: " +
            "{\"attscengroups\": [{\"name\":  \"NameOfAttackScenario\", \"operator\": \"OR or AND\", " +
            "\"groupnumber\": integer} ...]} " +
            "# Respect: \"name\" must be the name of a provided attack scenario. " +
            "# Respect: a group of attack scenarios is denoted as attack scenarios that have the same \"groupnumber\" " +
            "and \"operator\"." +
            "# Respect: \"groupnumber\" must be an integer greater than zero." +
            "# Respect: \"operator\" must be only \"AND\" or \"OR\". Use \"AND\" to denote that an attacker must " +
            "complete all of the attack scenarios with the same \"groupnumber\" and with an AND \"operator\" simultaneously " +
            "to achieve the attack. Use \"OR\" to denote that an attacker only needs to complete one of the attack " +
            "scenarios amongst all of the attack scenarios with the same \"groupnumber\" and with an OR \"operator\" to achieve " +
            "the attack. " +
            "# Respect: The attack scenario groups must contain more than one attack scenario. " +
            "# Respect: An attack scenario can only belong to one group.";

    private final String KNOWLEDGE_ON_JSON_FOR_ATT_STEPS = "When you are asked to identify all of the steps that " +
            "that an attacker needs to complete to successfully perform an attack scenario, " +
            "return them as a JSON specification formatted as follows: " +
            "{\"attackscenario\": \"NameOfAttackScenario\", \"attacksteps\": [{\"name\":  \"NameOfAttackStep\", \"description\": \"" +
            "The description of the attack step and how it is needed to achieve the attack scenario.\"} ...]} " +
            "# Respect: All words in \"name\" must be conjoined together. " +
            "# Respect: \"attackscenario\" must be the name of the provided attack scenario. " +
            "# Respect: There must be no more than forty characters in \"name\". " +
            "# Respect: For each word in \"name\", its first letter must be capitalized. " +
            "# Respect: All words in \"description\" must be separated with spaces. " +
            "# Respect: The elements of \"attacksteps\" need to be ordered sequentially. That is, the first indexed element " +
            "in \"attacksteps\" is the step that an attacker needs to complete first while the last indexed element is the step " +
            "that an attacker needs to complete last. " +
            "# Respect: There must be at least two attack steps in \"attacksteps\".";

    private final String[] KNOWLEDGE_STAGES = {
            KNOWLEDGE_ON_JSON_FOR_ROOT,
            KNOWLEDGE_ON_JSON_FOR_ATTACK_SCEN,
            KNOWLEDGE_ON_JSON_FOR_ATTACK_GROUPS,
            KNOWLEDGE_ON_JSON_FOR_ATT_STEPS
    };

    private final String[] QUESTION_IDENTIFY_ATD = {
            "From the provided system specification " +
            "and using the specified JSON format, identify a motive as to why an attacker would " +
            "want to exploit the system. Use at least one attack pattern to identify a possible motive. " +
            "Do respect the JSON format, and provide only JSON (no explanation before or after). \n",

            "Using the specified JSON format, " +
                    "identify all of the attack scenarios that an attacker can conduct to achieve the provided attack. " +
                    "If provided with a system specification, make sure to associate the specification with the attack " +
                    "scenarios. In addition, if provided with a list of attack patterns, use at least one attack pattern " +
                    "from this list to identify the attack scenarios. Do respect the JSON format, and provide " +
                    "only JSON (no explanation before or after).\n",

            "Using the specified JSON format, " +
                    "group all of the provided attack scenarios to denote which ones an attacker can conduct individually or " +
                    "needs to perform simultaneously with other attack scenarios in the list to achieve the provided attack. " +
                    "Do respect the JSON format, and provide only JSON (no explanation before or after).\n",

            "Identify all of the attack steps that an attacker needs to conduct to " +
                    "achieve the provided attack scenario. Do respect the JSON format, and " +
                    "provide only JSON (no explanation before or after).\n",
    };

    private JSONObject rootAttack;
    private ArrayList<JSONObject> attackScenarios;
    private ArrayList<JSONArray> attackSteps;
    private AttackTree at;
    private int maxNumATs;
    private int maxLevels;
    private int maxRetries;
    private int topN;
    private final ArrayList<AttackTree> atList;
    private final APTAuditor auditor;
    private final CAPECTracer tracer;

    public AIAttackPatternTree(AIChatData _chatData) {
        super(_chatData);
        atList = new ArrayList<>();
        maxRetries = 10;
        maxLevels = 1;
        maxNumATs = 1;
        topN = 20;
        auditor = new APTAuditor();
        tracer = new CAPECTracer(chatData);
    }

    public void setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
    }

    public void setMaxLevels(int maxLevels) {
        this.maxLevels = maxLevels;
    }

    public void setMaxNumAts(int maxNumATs) { this.maxNumATs = maxNumATs; }

    public void setTopN(int topN) { this.topN = topN; }

    public Object applyAnswer(Object input) {
        return atList;
    }

    private void initKnowledge() {
        chatData.aiinterface.clearKnowledge();
    }

    private void makeKnowledge(int stage, String _spec, String[] _attackPatterns, String previousRootAtts) {
        String[] know = KNOWLEDGE_STAGES[stage].split("#");

        for (String s : know) {
            TraceManager.addDev("\nKnowledge added: " + s);
            chatData.aiinterface.addKnowledge(s, "ok");
        }

        if (_spec != null) {
            TraceManager.addDev("\nKnowledge added: " + _spec);
            chatData.aiinterface.addKnowledge("The system specification is: " + _spec, "ok");
        }

        if (_attackPatterns != null && _attackPatterns.length > 0) {
            String attackPatternsString = buildAttackPatternsString(_attackPatterns);
            TraceManager.addDev("\nKnowledge added: " + attackPatternsString);
            chatData.aiinterface.addKnowledge("The attack patterns are: " + attackPatternsString,
                    "ok");
        }

        if (previousRootAtts != null && !previousRootAtts.isEmpty()) {
            TraceManager.addDev("\nKnowledge added: " + previousRootAtts);
            chatData.aiinterface.addKnowledge(
                    "Identify a motive other than the motives in the following JSONs: " + previousRootAtts,
                    "ok");

        }
    }

    public void internalRequest() {
        tracer.internalRequest();
        String traces = tracer.getResults();

        if (!traces.equals("The tracer failed to run successfully.")) {
            String previousRootAttacks = "";

            for (int z = 0; z < maxNumATs; z++) {
                at = new AttackTree("", null);
                int stage = 0;
                int numLevels = 0;
                int numTries = 0;
                boolean done = false;

                String systemSpec = chatData.lastQuestion.trim();
                String[] allAttackPatterns = traces.split("\n\n");
                String[] attackPatterns = Arrays.copyOfRange(allAttackPatterns, 0, topN);
//                String[] attackPatterns = null;

                while (!done) {
                    if (stage % 3 == 1) {
                        attackScenarios = new ArrayList<>();
                    } else if (stage % 3 == 0 && stage != 0) {
                        attackSteps = new ArrayList<>();
                    }

                    if (stage == 0) {
                        numTries = makeRequest(stage, systemSpec, attackPatterns, null, null, null,
                                previousRootAttacks);
                    }
                    else if (stage == 1) {
                        numTries = makeRequest(stage, systemSpec, attackPatterns, null, null, rootAttack,
                                null);
                    }
                    else if (stage % 3 == 1) {
                        for (JSONArray attackStepArray : attackSteps) {
                            for (int k = 0; k < attackStepArray.length(); k++) {
                                JSONObject attackStep = (JSONObject) attackStepArray.get(k);
                                numTries = makeRequest(stage, systemSpec, attackPatterns, null, null,
                                        attackStep, null);

                                if (numTries >= maxRetries) {
                                    break;
                                }
                            }

                            if (numTries >= maxRetries) {
                                break;
                            }
                        }
                    }
                    else if (stage % 3 == 2) {
                        for (JSONObject attackScenariosJSON : attackScenarios) {
                            JSONObject attack;
                            JSONArray attackScenariosArray = attackScenariosJSON.getJSONArray("attackscenarios");

                            if (stage == 2) {
                                attack = rootAttack;
                            }
                            else {
                                String attackName = attackScenariosJSON.getString("attack");
                                attack = findAttackStep(attackSteps, attackName);
                            }

                            numTries = makeRequest(stage, systemSpec, attackPatterns, null,
                                    attackScenariosArray, attack, null);

                            if (numTries >= maxRetries) {
                                break;
                            }
                        }
                    }
                    else {
                        for (JSONObject attackScenariosJSON : attackScenarios) {
                            JSONArray attackScenariosArray = attackScenariosJSON.getJSONArray("attackscenarios");

                            for (int k = 0; k < attackScenariosArray.length(); k++) {
                                JSONObject attackScenarioString = (JSONObject) attackScenariosArray.get(k);
                                numTries = makeRequest(stage, systemSpec, attackPatterns, attackScenarioString,
                                        null, null, null);

                                if (numTries >= maxRetries) {
                                    break;
                                }
                            }

                            if (numTries >= maxRetries) {
                                break;
                            }
                        }
                    }

                    if (stage % 3 == 0 && stage != 0) {
                        numLevels++;
                    }

                    if (numTries >= maxRetries || numLevels >= maxLevels) {
                        done = true;
                    }

                    stage++;
                    waitIfConditionTrue(!done);
                }

                atList.add(at);
                previousRootAttacks = previousRootAttacks + rootAttack + " ";
            }
        }

        TraceManager.addDev("Reached end of AIAttackPatternTree internal request.");
    }

    private int makeRequest(int stage, String systemSpec, String[] attackPatterns, JSONObject attSc,
                            Iterable<Object> attScens, JSONObject att, String previousRootAtts) {
        int numRetries = 0;
        boolean done = false;
        String json = "";
        String questionT;

        if (stage == 0) {
            questionT = QUESTION_IDENTIFY_ATD[stage];
        }
        else if (stage % 3 == 1) {
            questionT = QUESTION_IDENTIFY_ATD[1];
        }
        else if (stage % 3 == 2) {
            questionT = QUESTION_IDENTIFY_ATD[2];
        }
        else {
            questionT = QUESTION_IDENTIFY_ATD[3];
        }

        if (stage == 1) {
            questionT += "\nThe attack data is in the following JSON:\n" + rootAttack + "\n";
        }
        else {
            if (attSc != null) {
                questionT += "\nThe attack scenario data is in the following JSON:\n" + attSc + "\n";
            }

            if (attScens != null) {
                questionT += "\nThe provided attack scenarios are in the following JSON array:\n" + attScens + "\n";
            }

            if (att != null) {
                questionT += "\nThe attack data is in the following JSON:\n" + att + "\n";
            }
        }

        while (numRetries < maxRetries && !done) {
            initKnowledge();

            if (stage == 0) {
                makeKnowledge(stage, systemSpec, attackPatterns, previousRootAtts);
            }
            else if (stage == 1) {
                makeKnowledge(stage, systemSpec, attackPatterns, null);
            }
            else {
                int actualStage = stage % 3;

                if (actualStage == 0) {
                    actualStage = 3;
                }

                makeKnowledge(actualStage, null, null, null);
            }

            boolean ok = makeQuestion(questionT);

            if (!ok) {
                TraceManager.addDev("Make question failed");
                return maxRetries;
            }

            ArrayList<String> errors = new ArrayList<>();

            try {
                TraceManager.addDev("\n\nMaking specification from " + chatData.lastAnswer + "\n\n");
                json = extractJSON();

                if (stage == 0) {
                    rootAttack = auditor.checkRootAttack(atList, at, json, errors);

                    if (rootAttack.isEmpty()) {
                        errors.add("You must provide a motive that an attacker would " +
                                "have as to why they would want to exploit the provided system specification. " +
                                "Do respect the JSON " +
                                "format, and provide only JSON (no explanation before or after).");
                    }
                    else {
                        TraceManager.addDev("Identified root attack - " + rootAttack);
                    }
                }
                else if (stage % 3 == 1) {
                    assert att != null;
                    JSONObject attackScenarioJSON = auditor.checkAttackScenarios(at, json, att, errors, stage);

                    if (attackScenarioJSON.isEmpty() && !errors.isEmpty()) {
                        errors.add("You must provide all possible attack scenarios showing how an " +
                                "attacker uses them to achieve the provided attack. " +
                                "Do respect the JSON format, and " +
                                "provide only JSON (no explanation before or after).");
                    }

                    if (!attackScenarioJSON.isEmpty()) {
                        JSONArray attackScenariosArray = attackScenarioJSON.getJSONArray("attackscenarios");
                        TraceManager.addDev("Identified attack scenarios: " + attackScenariosArray);
                        attackScenarios.add(attackScenarioJSON);
                    }
                }
                else if (stage % 3 == 2) {
                    assert att != null;
                    JSONArray attackScenGroups = auditor.checkAttScenGroups(at, json, attScens, att, errors);

                    if (attackScenGroups.isEmpty()) {
                        errors.add("You must group all of the provided attack scenarios to show which ones can be " +
                                "performed individually or need to be performed simultaneously with other attack scenarios " +
                                "in the list for an attacker to achieve the provided attack. Do respect the JSON format, " +
                                "and provide only JSON (no explanation before or after).");
                    }
                    else {
                        TraceManager.addDev("Identified attack scenario groups: " + attackScenGroups);
                    }
                }
                else {
                    assert attSc != null;
                    JSONArray attackStepJSON = auditor.checkAttackSteps(at, json, attSc, errors);

                    if (attackStepJSON.isEmpty()) {
                        errors.add("You must provide all of the attack steps needed for an attacker " +
                                "to achieve the provided attack scenario. " +
                                "Do respect the JSON format, and " +
                                "provide only JSON (no explanation before or after).");
                    }
                    else {
                        TraceManager.addDev("Identified attack steps: " + attackStepJSON);
                        attackSteps.add(attackStepJSON);
                    }
                }
            }
            catch (org.json.JSONException e) {
                TraceManager.addDev("Invalid JSON spec: " + extractJSON() + " because " + e.getMessage() + ": INJECTING ERROR");
                errors = new ArrayList<>();
                errors.add("There is an error in your JSON: " + e.getMessage() + ". Probably the JSON spec was incomplete. " +
                        "Do correct it. I need the full specification at once.");
            }

            if (!errors.isEmpty()) {
                questionT = "Your answer was as follows: " + json + "\n\nYet, it was not correct because of the following errors:";
                // Updating knowledge
                for (String s : errors) {
                    questionT += "\n- " + s;
                }

                numRetries++;
            }
            else {
                done = true;
            }
        }

        return numRetries;
    }

    private JSONObject findAttackStep(Iterable<JSONArray> attackSteps, String attackStepName) {
        for (JSONArray attackStepsArray : attackSteps) {
            for (Object attackStepObj : attackStepsArray) {
                JSONObject attackStepJSON = (JSONObject) attackStepObj;
                String name = attackStepJSON.getString("name");

                if (name.equals(attackStepName)) {
                    return attackStepJSON;
                }
            }
        }

        return new JSONObject();
    }

    private String buildAttackPatternsString(String[] _attackPatterns) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i <= _attackPatterns.length - 1; i++) {
            String attackPatternCleaned = _attackPatterns[i].
                    replaceAll("Confidence score: [0-9]+%", "").
                    replace("\n", " ").
                    trim();
            builder.append(attackPatternCleaned);

            if (i < _attackPatterns.length - 1) {
                builder.append(" | ");
            }
        }

        return builder.toString();
    }
}