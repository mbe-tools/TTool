/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 * 
 * ludovic.apvrille AT enst.fr
 * 
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


package ai;


import avatartranslator.AvatarSpecification;
import myutil.TraceManager;

/**
 * Class AIDiagramCoherencyWithFormalRules
 *
 * Creation: 22/03/2024
 * @version 1.0 22/03/2024
 * @author Ludovic APVRILLE
 */


public class AIDiagramCoherencyWithFormalRules extends AIInteract {

    private static String KNOWLEDGE_ON_JSON_FOR_INCOHERENCIES = "When you are asked to identify all the relevant incoherencies between two " +
            "diagrams," +
            " return them as a JSON specification formatted as follows:" +
            "{incoherencies: [{ \"diagram\" : \"diagram1 or diagram2\", \"description\": \"description of the incoherency\"}..." +
            "]}" +
            "#Respect: In a block diagram, the blocks representing actors as defined in the use-case diagram must bear identical names to their " +
            "corresponding use cases.\n" +
            "#Respect: In a block diagram, blocks representing actors from the use case diagram must not be connected together.\n" +
            "#Repect: In a block diagram, a block representing an actor from the use case diagram must be interconnected with at least one block " +
            "that doesn't represent an actor." +
            "#Respect: Give any incoherency you can identify concerning the two provided diagrams"
           ;


    public static String[] KNOWLEDGE_STAGES = {KNOWLEDGE_ON_JSON_FOR_INCOHERENCIES};

    private String[] QUESTION_IDENTIFY_INCOHERENCIES = {"From the provided specification and from the two SysML diagram given in textual format," +
            "identify the incoherencies between the two diagrams. Do respect the JSON format, and\n" +
            "provide only JSON (no explanation before or after).\n"};

    public AIDiagramCoherencyWithFormalRules(AIChatData _chatData) {
        super(_chatData);
    }

    public void internalRequest() {
        boolean done = false;

        int stage = 0;
        String systemSpec = chatData.lastQuestion.trim();
        String json = "";
        initKnowledge();
        makeKnowledge(stage, null);
        String questionT = QUESTION_IDENTIFY_INCOHERENCIES[stage] + "\n" + systemSpec + "\n";


        int cpt = 0;
        while (!done && cpt < 20) {
            cpt++;
            boolean ok = makeQuestion(questionT) ;
            if (!ok) {
                done = true;
                TraceManager.addDev("Make question failed");
            }
            json = extractJSON();
            TraceManager.addDev("Received json:" + json);
            done = true;

        }
    }

    private void initKnowledge() {
        chatData.aiinterface.clearKnowledge();
    }



    public void makeKnowledge(int stage, String _spec) {
        //TraceManager.addDev("makeKnowledge. stage: " + stage + " chatData.knowledgeOnBlockJSON: " + chatData.knowledgeOnBlockJSON);
        //chatData.aiinterface.clearKnowledge();

        String [] know = KNOWLEDGE_STAGES[stage].split("#");
        for(String s: know) {
            TraceManager.addDev("\nKnowledge added: " + s);
            chatData.aiinterface.addKnowledge(s, "ok");
        }

        if (_spec != null) {
            TraceManager.addDev("\nKnowledge added: " + _spec);
            chatData.aiinterface.addKnowledge("The system specification is: " + _spec, "ok");

        }


    }
    public Object applyAnswer(Object input) {
        return null;
    }

	
    
}
