package ai;

import attacktrees.*;
import myutil.TraceManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class APTAuditor {
    public JSONObject checkRootAttack(Iterable<AttackTree> atList, AttackTree _at,
                                             String _spec, Collection<String> _errors)
            throws org.json.JSONException {
        if (_spec == null) {
            _errors.add("No \"rootattack\" object in json");

            return new JSONObject();
        }

        int indexStart = _spec.indexOf('{');
        int indexStop = _spec.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            _errors.add("Invalid JSON object (start or stop)");

            return new JSONObject();
        }

        String json = _spec.substring(indexStart, indexStop + 1);

        JSONObject mainObject = new JSONObject(json);

        if (mainObject.length() != 1) {
            TraceManager.addDev("Extra key/value mappings added in the outer json object");
            _errors.add("\"rootattack\" should be the only key/value mapping in the outer json object");

            return new JSONObject();
        }

        JSONObject rootAttackJSON = mainObject.getJSONObject("rootattack");

        if (rootAttackJSON == null) {
            TraceManager.addDev("No \"rootattack\" object in json");
            _errors.add("No \"rootattack\" object in json");

            return new JSONObject();
        }
        else {
            _errors.addAll(checkAttackSyntax(_at, rootAttackJSON));
        }

        String name = rootAttackJSON.getString("name");

        for (AttackTree attackTree : atList) {
            for (Attack attack : attackTree.getAttacks()) {
                if (attack.isRoot() && attack.getName().equals(name)) {
                    TraceManager.addDev("Duplicate motive identified");
                    _errors.add("The motive that you provided has already been identified. " +
                            "Please provide another motive.");

                    return new JSONObject();
                }
            }
        }

        if (_errors.isEmpty()) {
            Attack rootAttack = new Attack("", null);
            rootAttack.setRoot(true);
            rootAttack.setName(rootAttackJSON.getString("name"));
            rootAttack.setDescription(rootAttackJSON.getString("description"));
            _at.addAttack(rootAttack);

            return rootAttackJSON;
        }
        else {
            return new JSONObject();
        }
    }

    public JSONObject checkAttackScenarios(AttackTree _at, String _spec, JSONObject attack,
                                            Collection<String> _errors, int stage) throws org.json.JSONException {
        String attackName = attack.getString("name");

        if (_spec == null) {
            _errors.add("No \"attackscenarios\" object in json");

            return new JSONObject();
        }

        int indexStart = _spec.indexOf('{');
        int indexStop = _spec.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            _errors.add("Invalid JSON object (start or stop)");

            return new JSONObject();
        }

        String json = _spec.substring(indexStart, indexStop + 1);

        JSONObject mainObject = new JSONObject(json);

        if (mainObject.length() != 2) {
            TraceManager.addDev("Extra key/value mappings added in outer json object");
            _errors.add("\"attack\" and \"attackscenarios\" should be the only key/value mappings in " +
                    "the outer json object");

            return new JSONObject();
        }

        String jsonAttackName = mainObject.getString("attack");
        JSONArray attackScenariosJSON = mainObject.getJSONArray("attackscenarios");

        if (jsonAttackName == null) {
            TraceManager.addDev("No \"attack\" in json");
            _errors.add("No \"attack\" in json");

            return new JSONObject();
        }
        else if (!attackName.equals(jsonAttackName)) {
            TraceManager.addDev("The value of \"attack\" should be set to " + attackName);
            _errors.add("The value of \"attack\" should be set to " + attackName);

            return new JSONObject();
        }
        else if (attackScenariosJSON == null) {
            TraceManager.addDev("No \"attackscenarios\" array in json");
            _errors.add("No \"attackscenarios\" array in json");

            return new JSONObject();
        }
        else if (attackScenariosJSON.isEmpty()) {
            if (stage == 1) {
                TraceManager.addDev("\"attackscenarios\" json array is empty.");
                _errors.add("\"attackscenarios\" json array is empty. Please make sure to " +
                        "include at least two attack scenarios.");
            }

            return new JSONObject();
        }
        else if (attackScenariosJSON.length() == 1) {
            TraceManager.addDev("\"attackscenarios\" json array only contains one attack scenario.");

            if (stage == 1) {
                _errors.add("\"attackscenarios\" json array only contains one attack scenario. Please make sure to " +
                        "include at least two attack scenarios.");
            }
            else {
                _errors.add("\"attackscenarios\" json array only contains one attack scenario. Please remove it " +
                        "and return an empty array instead.");
            }

            return new JSONObject();
        }

        int i = 0;

        while (i < attackScenariosJSON.length() && _errors.isEmpty())
        {
            _errors.addAll(checkAttackScenSyntax(_at, attackScenariosJSON.getJSONObject(i)));
            i++;
        }

        if (_errors.isEmpty()) {
            createAttackScenarios(_at, attackScenariosJSON);

            return mainObject;
        }
        else {
            return new JSONObject();
        }
    }

    public JSONArray checkAttScenGroups(AttackTree _at, String _spec, Iterable<Object> attScens, JSONObject attack,
                                               Collection<String> _errors) throws org.json.JSONException {
        String attackName = attack.getString("name");

        if (_spec == null) {
            _errors.add("No \"attscengroups\" object in json");

            return new JSONArray();
        }

        int indexStart = _spec.indexOf('{');
        int indexStop = _spec.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            _errors.add("Invalid JSON object (start or stop)");

            return new JSONArray();
        }

        String json = _spec.substring(indexStart, indexStop + 1);

        JSONObject mainObject = new JSONObject(json);

        if (mainObject.length() != 1) {
            TraceManager.addDev("Extra key/value mappings added in outer json object");
            _errors.add("\"attscengroups\" should be the only key/value mapping in " +
                    "the outer json object");

            return new JSONArray();
        }

        JSONArray jsonGroups = mainObject.getJSONArray("attscengroups");

        if (jsonGroups == null) {
            TraceManager.addDev("No \"attscengroups\" in json");
            _errors.add("No \"attscengroups\" in json");

            return new JSONArray();
        }
        else if (jsonGroups.isEmpty()) {
            TraceManager.addDev("\"attscengroups\" json array is empty.");
            _errors.add("\"attscengroups\" json array is empty. Please make sure to " +
                        "group all provided attack scenarios.");

            return new JSONArray();
        }

        int i = 0;

        while (i < jsonGroups.length() && _errors.isEmpty())
        {
            _errors.addAll(checkAttScenGroupsSyntax(jsonGroups.getJSONObject(i), attScens));
            i++;
        }

        if (_errors.isEmpty()) {
            _errors.addAll(checkAttScenGroupLogic(jsonGroups, attScens));
        }

        if (_errors.isEmpty()) {
            createAttackScenarioGroups(_at, jsonGroups, attackName);

            return jsonGroups;
        }
        else {
            return new JSONArray();
        }
    }

    public JSONArray checkAttackSteps(AttackTree _at, String _spec, JSONObject attackScenario, Collection<String> _errors)
            throws org.json.JSONException {
        String attackScenarioName = attackScenario.getString("name");

        if (_spec == null) {
            _errors.add("No \"attacksteps\" object in json");

            return new JSONArray();
        }

        int indexStart = _spec.indexOf('{');
        int indexStop = _spec.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            _errors.add("Invalid JSON object (start or stop)");

            return new JSONArray();
        }

        String json = _spec.substring(indexStart, indexStop + 1);

        JSONObject mainObject = new JSONObject(json);

        if (mainObject.length() != 2) {
            TraceManager.addDev("Extra key/value mappings added in outer json object");
            _errors.add("\"attackscenario\" and \"attacksteps\" should be the only key/value mappings in " +
                    "the outer json object");

            return new JSONArray();
        }

        String jsonAttScenName = mainObject.getString("attackscenario");
        JSONArray attackStepsJSON = mainObject.getJSONArray("attacksteps");

        if (jsonAttScenName == null) {
            TraceManager.addDev("No \"attackscenario\" in json");
            _errors.add("No \"attackscenario\" in json");

            return new JSONArray();
        }
        else if (!attackScenarioName.equals(jsonAttScenName)) {
            TraceManager.addDev("The value of \"attackscenario\" should be set to " + attackScenarioName);
            _errors.add("The value of \"attackscenario\" should be set to " + attackScenarioName);

            return new JSONArray();
        }
        else if (attackStepsJSON == null) {
            TraceManager.addDev("No \"attacksteps\" array in json");
            _errors.add("No \"attacksteps\" array in json");

            return new JSONArray();
        }
        else if (attackStepsJSON.length() <= 1) {
            TraceManager.addDev("Only one provided attack step");
            _errors.add("Please provide at least two attack steps in \"attacksteps\"");

            return new JSONArray();
        }
        else {
            int i = 0;

            while (i < attackStepsJSON.length() && _errors.isEmpty())
            {
                _errors.addAll(checkAttackSyntax(_at, attackStepsJSON.getJSONObject(i)));
                i++;
            }
        }

        if (_errors.isEmpty()) {
            createAttackSteps(_at, attackStepsJSON, jsonAttScenName);

            return attackStepsJSON;
        }
        else {
            return new JSONArray();
        }
    }

    public JSONObject checkMitigations(AttackTree at, String _spec, String[][] attackStepNames, Collection<String> _errors)
            throws org.json.JSONException {
        if (_spec == null) {
            _errors.add("No \"mitigations\" object in json");

            return new JSONObject();
        }

        int indexStart = _spec.indexOf('{');
        int indexStop = _spec.lastIndexOf('}');

        if ((indexStart == -1) || (indexStop == -1) || (indexStart > indexStop)) {
            _errors.add("Invalid JSON object (start or stop)");

            return new JSONObject();
        }

        String json = _spec.substring(indexStart, indexStop + 1);

        JSONObject mainObject = new JSONObject(json);

        if (mainObject.length() != 1) {
            TraceManager.addDev("Extra key/value mappings added in outer json object");
            _errors.add("\"mitigations\" should be the only key/value mapping in " +
                    "the outer json object");

            return new JSONObject();
        }

        JSONArray mitigationsJSON = mainObject.getJSONArray("mitigations");

        if (mitigationsJSON == null) {
            TraceManager.addDev("No \"mitigations\" array in json");
            _errors.add("No \"mitigations\" array in json");

            return new JSONObject();
        }
        else if (mitigationsJSON.isEmpty()) {
            return new JSONObject();
        }

        int i = 0;

        while (i < mitigationsJSON.length() && _errors.isEmpty())
        {
            _errors.addAll(checkMitiSyntax(mitigationsJSON.getJSONObject(i), attackStepNames));
            i++;
        }

        if (_errors.isEmpty()) {
            createMitigations(at, mitigationsJSON);

            return mainObject;
        }
        else {
            return new JSONObject();
        }
    }

    private ArrayList<String> checkAttackScenSyntax(AttackTree at, JSONObject attackScen) {
        ArrayList<String> errors = new ArrayList<>();

        if (attackScen.length() != 2) {
            errors.add("The \"attackscenarios\" object should only contain \"name\", and \"description\".");

            return errors;
        }

        String name = attackScen.getString("name");
        String description = attackScen.getString("description");

        if (name == null) {
            errors.add("Attack scenario has no name");
            return errors;
        }

        Attack alreadyPresentAtt = findAttack(at, name);

        if (alreadyPresentAtt != null) {
            errors.add(name + " is already the name of an attack scenario or step that you created. " +
                    "Please remove it from \"attackscenarios\".");
            return errors;
        }
        else if (!name.matches("[a-zA-Z0-9]+")) {
            errors.add(name + " must only contain alphanumeric characters");
            return errors;
        }
        else if (name.length() > 40) {
            errors.add(name + " must only contain forty characters max");
            return errors;
        }
        else if (description == null) {
            errors.add("Attack scenario has no description");
            return errors;
        }
        else if (!description.contains(" ")) {
            errors.add("The words in \"description\" must be separated with spaces");
            return errors;
        }

        return errors;
    }

    private void createAttackScenarios(AttackTree _at, JSONArray attackScenarios) {
        for (int i = 0; i < attackScenarios.length(); i++) {
            JSONObject attackScen = attackScenarios.getJSONObject(i);
            Attack attack = new Attack("", null);
            attack.setName(attackScen.getString("name"));
            attack.setDescription(attackScen.getString("description"));
            _at.addAttack(attack);
        }
    }

    private ArrayList<String> checkAttScenGroupsSyntax(JSONObject attScenGrouping,
                                                       Iterable<Object> attScenarios) {
        ArrayList<String> errors = new ArrayList<>();

        if (attScenGrouping.length() != 3) {
            errors.add("The \"attscengroups\" object should only contain \"name\", \"operator\", " +
                    "and \"groupnumber\".");

            return errors;
        }

        String name = attScenGrouping.getString("name");
        String operator = attScenGrouping.getString("operator");
        int groupNumber = attScenGrouping.getInt("groupnumber");

        if (name == null) {
            errors.add("Attack scenario grouping has no name");
            return errors;
        }
        else if (operator == null) {
            errors.add("Attack scenario grouping has no operator");
            return errors;
        }
        else if (!operator.equals("OR") && !operator.equals("AND")) {
            errors.add("The value of \"operator\" must be either OR or AND");
            return errors;
        }
        else if (groupNumber <= 0) {
            errors.add("Attack scenario grouping must have a \"groupnumber\" that must be an integer greater than zero");
            return errors;
        }

        boolean nameIsAttScen = false;

        for (Object attackScenario : attScenarios) {
            JSONObject attackScenarioJSON = (JSONObject) attackScenario;
            String attackScenName = attackScenarioJSON.getString("name");

            if (name.equals(attackScenName)) {
                nameIsAttScen = true;
                break;
            }
        }

        if (!nameIsAttScen) {
            errors.add(name + " is not the name of a provided attack scenario. " +
                    "Please make sure to only group the attack scenarios in the following JSON array: " +
                    attScenarios);
        }

        return errors;
    }

    private ArrayList<String> checkAttScenGroupLogic(Iterable<Object> attScenGroups,
                                                            Iterable<Object> attScenarios) {
        ArrayList<String> errors = new ArrayList<>();
        Map<String, Integer> counts = getAttScenGroupMap(attScenGroups);

        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();

            if (value == 1) {
                String[] soleGroupID = key.split("-");

                TraceManager.addDev("Group " + soleGroupID[0] + " " + soleGroupID[1] + " is a group with only one " +
                        "attack scenario.");
                errors.add("Group " + soleGroupID[0] + " " + soleGroupID[1] + " is a group with only one " +
                        "attack scenario. Please make sure all groups contain at least two attack scenarios.");

                return errors;
            }
        }

        String missingAttScenName = "";
        Set<String> attScenInGroups;

        try {
            attScenInGroups = getAttackScenNames(attScenGroups);
        }
        catch (DuplicateEntryException exception) {
            TraceManager.addDev(exception.toString());
            errors.add(exception.toString());

            return errors;
        }

        for (Object attScenObject : attScenarios) {
            JSONObject attScen = (JSONObject) attScenObject;
            String attScenName = attScen.getString("name");

            if (!attScenInGroups.contains(attScenName)) {
                missingAttScenName = attScenName;
            }
        }

        if (!missingAttScenName.isEmpty()) {
            TraceManager.addDev(missingAttScenName + "is a provided attack scenario that has not been grouped.");
            errors.add(missingAttScenName + " is a provided attack scenario that has not been grouped. Please group it " +
                    "with other attack scenarios.");

            return errors;
        }

        return errors;
    }

    private Map<String, Integer> getAttScenGroupMap(Iterable<Object> attScenGroups) {
        Map<String, Integer> counts = new HashMap<>();

        for (Object object : attScenGroups) {
            JSONObject json = (JSONObject) object;
            String operator = json.getString("operator");
            int group = json.getInt("groupnumber");
            String key = group + "-" + operator;

            if (!counts.containsKey(key)) {
                counts.put(key, 0);
            }

            counts.put(key, counts.get(key) + 1);
        }

        return counts;
    }

    private Set<String> getAttackScenNames(Iterable<Object> attScenarios) throws DuplicateEntryException {
        Set<String> attScenNames = new HashSet<>();

        for (Object attScenObject : attScenarios) {
            JSONObject attScen = (JSONObject) attScenObject;
            String attScenName = attScen.getString("name");

            if (attScenNames.contains(attScenName)) {
                throw new DuplicateEntryException(attScenNames + " has been grouped twice. Please make sure that " +
                        "all attack scenarios belong to only one group.");
            }
            else {
                attScenNames.add(attScenName);
            }

        }

        return attScenNames;
    }

    private void createAttackScenarioGroups(AttackTree _at, JSONArray attackScenarioGroups, String parentAttName) {
        Map<String, AttackNode> attNodeDict = new HashMap<>();
        Attack parentAttack = findAttack(_at, parentAttName);

        for (int i = 0; i < attackScenarioGroups.length(); i++) {
            JSONObject attackScenGroup = attackScenarioGroups.getJSONObject(i);
            Attack childAttack = findAttack(_at, attackScenGroup.getString("name"));

            String operator = attackScenGroup.getString("operator");
            int group = attackScenGroup.getInt("groupnumber");
            String key = group + "-" + operator;

            AttackNode connection;

            if (!attNodeDict.containsKey(key)) {
                if (operator.equals("OR")) {
                    connection = new ORNode("", null);
                }
                else {
                    connection = new ANDNode("", null);
                }

                connection.setResultingAttack(parentAttack);
                _at.addNode(connection);
                attNodeDict.put(key, connection);
            }
            else {
                connection = attNodeDict.get(key);
            }

            connection.addInputAttack(childAttack, i + 1);
        }
    }

    private ArrayList<String> checkAttackSyntax(AttackTree _at, JSONObject attack) {
        ArrayList<String> errors = new ArrayList<>();

        if (attack.length() != 2) {
            errors.add("The attack should only contain \"name\" and \"description\"");

            return errors;
        }

        String name = attack.getString("name");
        String description = attack.getString("description");

        if (name == null) {
            errors.add("Attack has no name");
            return errors;
        }

        Attack alreadyPresentAtt = findAttack(_at, name);

        if (alreadyPresentAtt != null) {
            errors.add(name + " is already the name of an attack scenario or step that you created. " +
                    "Please remove it from the list.");
            return errors;
        }
        else if (!name.matches("[a-zA-Z0-9]+")) {
            errors.add(name + " must only contain alphanumeric characters");
            return errors;
        }
        else if (name.length() > 40) {
            errors.add(name + " must only contain forty characters max");
            return errors;
        }
        else if (description == null) {
            errors.add("The JSON object has no description");
            return errors;
        }
        else if (!description.contains(" ")) {
            errors.add("The words in \"description\" must be separated with spaces");
            return errors;
        }

        return errors;
    }

    private ArrayList<String> checkMitiSyntax(JSONObject mitigation, String[][] attackStepNames) {
        ArrayList<String> errors = new ArrayList<>();

        if (mitigation.length() != 3) {
            errors.add("The \"mitigation\" object should only contain \"name\", \"description\", " +
                    "and \"attacksteps\".");

            return errors;
        }

        String name = mitigation.getString("name");
        String description = mitigation.getString("description");
        JSONArray relatedAttSteps = mitigation.getJSONArray("attacksteps");

        if (name == null) {
            errors.add("Mitigation has no name");
            return errors;
        }
        else if (!name.matches("[a-zA-Z0-9]+")) {
            errors.add(name + " must only contain alphanumeric characters");
            return errors;
        }
        else if (name.length() > 40) {
            errors.add(name + " must only contain forty characters max");
            return errors;
        }
        else if (description == null) {
            errors.add("Mitigation has no description");
            return errors;
        }
        else if (!description.contains(" ")) {
            errors.add("The words in \"description\" must be separated with spaces");
            return errors;
        }

        for (Object object : relatedAttSteps) {
            String relatedAttStep = (String) object;

            if (!foundNameInList(relatedAttStep, attackStepNames)) {
                errors.add(relatedAttStep + " is not the name of a provided attack step. Please only associate " +
                        "mitigations with attack steps from the provided list.");
                return errors;
            }
        }

        return errors;
    }

    private void createAttackSteps(AttackTree _at, JSONArray attackSteps, String parentAttackName) {
        AttackNode seqConnection = new SequenceNode("", null);
        Attack parentAttack = findAttack(_at, parentAttackName);
        seqConnection.setResultingAttack(parentAttack);
        _at.addNode(seqConnection);

        for (int i = 0; i < attackSteps.length(); i++) {
            JSONObject attackStep = attackSteps.getJSONObject(i);
            Attack attack = new Attack("", null);
            attack.setName(attackStep.getString("name"));
            attack.setDescription(attackStep.getString("description"));
            seqConnection.addInputAttack(attack, i + 1);

            _at.addAttack(attack);
        }
    }

    private void createMitigations(AttackTree _at, JSONArray mitigations) {
        for (int i = 0; i < mitigations.length(); i++) {
            JSONObject mitigation = mitigations.getJSONObject(i);
            JSONArray relations = mitigation.getJSONArray("attacksteps");

            Defense defense = new Defense("", null);
            defense.setName(mitigation.getString("name"));
            defense.setDescription(mitigation.getString("description"));

            for (int j = 0; j < relations.length(); j++) {
                String attackNodeName = relations.getString(j);
                Attack attack = findAttack(_at, attackNodeName);
                defense.addRelatedAttack(attack);
            }

            _at.addDefense(defense);
        }
    }

    private Attack findAttack(AttackTree at, String attackNodeName) {
        String attackNNLower = attackNodeName.toLowerCase();
        ArrayList<Attack> attackNodeList = at.getAttacks();
        Attack attack;
        int i = 0;

        while (i < attackNodeList.size()) {
            attack = attackNodeList.get(i);

            if (attackNNLower.equals(attack.getName().toLowerCase())) {
                return attack;
            }

            i++;
        }

        return null;
    }

    private boolean foundNameInList(String target, String[][] list) {
        for (String[] s : list) {
            if (s[0].equals(target)) {
                return true;
            }
        }

        return false;
    }

    private static class DuplicateEntryException extends Exception {
        public DuplicateEntryException(String message) {
            super(message);
        }
    }
}
