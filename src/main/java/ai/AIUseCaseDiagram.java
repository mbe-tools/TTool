/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


package ai;


import avatartranslator.AvatarSpecification;
import avatartranslator.avatarucd.AvatarUseCaseDiagram;
import myutil.TraceManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Class AIUseCaseDiagram
 * <p>
 * Creation: 01/03/2024
 *
 * @author Ludovic APVRILLE
 */


public class AIUseCaseDiagram extends AIInteract {


    public static String KNOWLEDGE_ON_JSON_FOR_ACTORS = "When you are asked to identify actors, " +
            "return them as a JSON specification " +
            "formatted as follows:" +
            "{actors: [{ \"name\": \"Name of actor\"...]} " +
            "#Respect: Any actor identifier must no contain any space. Use \"_\" instead if necessary. " +
            "#Respect: The name of an actor must start with a noun. " +
            "#Respect: Avoid using \"User\" or \"Environment\" for actor names, be more precise";



    public static String KNOWLEDGE_ON_JSON_FOR_USECASES = "When you are asked to identify the use cases from a specification, " +
            "return them as a JSON specification " +
            "formatted as follows:" +
            "{usecases: [{ \"name\": \"Name of use case\" ...]}" +
            "# Respect: each use case must start with a verb describing the point of view of the system. " +
            "# Respect: Any identifier of use cases must not contain any space. Use \"_\" instead.";


    public static String KNOWLEDGE_ON_JSON_FOR_CONNECTIONS = "When you are asked to identify connections between use cases and actors, " +
            "return them as a JSON specification " +
            "formatted as follows:" +
            "{connections: [{ \"element1\": \"usecase or actor name\", \"element2\": \"usecase or actor name\" ...]} " +
            "# Respect: two actors cannot be connected together. " +
            "# Respect: only include relations are supported between use cases" +
            "# Respect: in an include relation, the use case including the other one must be \"element1\" in the json" +
            "# Respect: All actors must be connected to at least one use case";



    public static String[] KNOWLEDGE_STAGES = {KNOWLEDGE_ON_JSON_FOR_ACTORS, KNOWLEDGE_ON_JSON_FOR_USECASES, KNOWLEDGE_ON_JSON_FOR_CONNECTIONS};
    AvatarSpecification specification, specification0;

    private String[] QUESTION_IDENTIFY_UCD = {"From the provided system specification, using the specified JSON format, identify the " +
            "typical actors, that is elements not part of the system but directly interacting with it. Do respect the JSON format, and " +
            "provide only JSON (no explanation before or after).\n",

            "From the provided system specification, using the specified JSON format, identify the " +
            "typical use cases of the system. Do respect the JSON format, and provide only JSON (no explanation before or after).\n",
            
            "From the previous JSON and system specification, find the typical connections between actors and use case, " +
                    "and between use cases. Do give them respecting the specified JSON format"};

    private String namesOfActors = "";
    private String namesOfUseCases = "";
    private String connections = "";

    private AvatarUseCaseDiagram ad;

    public AIUseCaseDiagram(AIChatData _chatData) {
        super(_chatData);
    }

    public AvatarUseCaseDiagram getUseCaseDiagram() {
        return ad;
    }

    public void internalRequest() {

        int stage = 0;
        String systemSpec = chatData.lastQuestion.trim();
        String json = "";
        String questionT = QUESTION_IDENTIFY_UCD[stage] ;
        initKnowledge();
        makeKnowledge(stage, systemSpec);

        boolean done = false;
        int cpt = 0;

        // actors, use cases and connections
        while (!done && cpt < 20) {
            cpt++;
            boolean ok = makeQuestion(questionT);
            if (!ok) {
                done = true;
                TraceManager.addDev("Make question failed");
            }
            ArrayList<String> errors = null;
            try {
                TraceManager.addDev("\n\nMaking specification from " + chatData.lastAnswer + "\n\n");
                json = extractJSON();
                if (stage == 0) {
                    namesOfActors = "";
                    errors = new ArrayList<>();
                    namesOfActors = getActorNames(json, errors);
                    TraceManager.addDev("Names of actors: " + namesOfActors);
                    if (namesOfActors.length() == 0) {
                        errors.add("You must give the name of at least one actor");
                    }
                } else if (stage == 1) {
                    namesOfUseCases = "";
                    errors = new ArrayList<>();
                    namesOfUseCases = getUseCaseNames(json, errors);
                    TraceManager.addDev("Names of uses cases: " + namesOfUseCases);
                    if (namesOfUseCases.length() == 0) {
                        errors.add("You must give the name of at least one use case");
                    }
                    //specification0 = AvatarSpecification.fromJSONConnection(json, "design", null, true);
                    //errors = AvatarSpecification.getJSONErrors();
                } else if (stage == 2) {
                    connections = "";
                    errors = new ArrayList<>();
                    ad = getConnections(json, errors);

                    TraceManager.addDev("Obtained use case diagram: " + ad.toString());
                    TraceManager.addDev("" + errors.size() + " errors found");
                    /*specification = AvatarSpecification.fromJSON(json, "design", null, true);
                    if (specification != null) {
                        specification.addSignalsAndConnection(specification0);
                        specification.makeMinimalStateMachines();
                        specification.improveNames();
                    }
                    //TraceManager.addDev("Full spec: " + specification);
                    errors = AvatarSpecification.getJSONErrors();*/

                }

            } catch (org.json.JSONException e) {
                TraceManager.addDev("Invalid JSON spec: " + extractJSON() + " because " + e.getMessage() + ": INJECTING ERROR");
                errors = new ArrayList<>();
                errors.add("There is an error in your JSON: " + e.getMessage() + ". probably the JSON spec was incomplete. Do correct it. I need " +
                        "the full specification at once.");
            }

            if ((errors != null) && (errors.size() > 0)) {
                questionT = "Your answer was as follows: " + json + "\n\nYet, it was not correct because of the following errors:";
                // Updating knowledge
                for (String s : errors) {
                    questionT += "\n- " + s;
                }
                initKnowledge();
                makeKnowledge(stage, systemSpec);

            } else {
                //TraceManager.addDev(" Avatar spec=" + specification);
                stage++;
                if (stage == KNOWLEDGE_STAGES.length) {
                    done = true;
                } else {
                    initKnowledge();
                    makeKnowledge(stage, systemSpec);
                    questionT = QUESTION_IDENTIFY_UCD[stage]; // + chatData.lastQuestion.trim();
                    if (namesOfActors.length() > 0) {
                        questionT += "\nThe actors to be used are: " + namesOfActors.trim();
                    }
                    if (namesOfUseCases.length() > 0) {
                        questionT += "\nThe use cases to be used are: " + namesOfUseCases.trim();
                    }
                }
            }

            waitIfConditionTrue(!done && cpt < 20);

            cpt++;
        }
        TraceManager.addDev("Reached end of AIUseCaseDiagram internal request cpt=" + cpt);

    }

    public Object applyAnswer(Object input) {
        if (input == null) {
            return ad;
        }

        return ad;
    }


    private void initKnowledge() {
        chatData.aiinterface.clearKnowledge();
    }

    public void makeKnowledge(int stage) {
        makeKnowledge(stage, null);
    }
    public void makeKnowledge(int stage, String _spec) {
        //TraceManager.addDev("makeKnowledge. stage: " + stage + " chatData.knowledgeOnBlockJSON: " + chatData.knowledgeOnBlockJSON);
        //chatData.aiinterface.clearKnowledge();

        String [] know = KNOWLEDGE_STAGES[stage].split("#");
        for(String s: know) {
            TraceManager.addDev("\nKnowledge added: " + s);
            chatData.aiinterface.addKnowledge(s, "ok");
        }

        if (_spec != null) {
            TraceManager.addDev("\nKnowledge added: " + _spec);
            chatData.aiinterface.addKnowledge("The system specification is: " + _spec, "ok");

        }


    }

    public void makeKnowledgeError(int stage) {
        //TraceManager.addDev("makeKnowledge. stage: " + stage + " chatData.knowledgeOnBlockJSON: " + chatData.knowledgeOnBlockJSON);
        chatData.aiinterface.clearKnowledge();

        String [] know = KNOWLEDGE_STAGES[stage].split("#");
        for(String s: know) {
            TraceManager.addDev("\nKnowledge added: " + s);
            chatData.aiinterface.addKnowledge(s, "ok");
        }

    }

    private String getActorNames(String _spec, ArrayList<String> _errors) throws org.json.JSONException {
        AvatarUseCaseDiagram ad = new AvatarUseCaseDiagram("", null);
        _errors.addAll(ad.makeActorsFromJson(_spec));

        return ad.getActorNames();
    }

    private String getUseCaseNames(String _spec, ArrayList<String> _errors) throws org.json.JSONException {
        AvatarUseCaseDiagram ad = new AvatarUseCaseDiagram("", null);
        _errors.addAll(ad.makeUseCasesFromJson(_spec));

        return ad.getUseCaseNames();
    }

    private AvatarUseCaseDiagram getConnections(String _spec, ArrayList<String> _errors) throws org.json.JSONException {
        AvatarUseCaseDiagram ad = new AvatarUseCaseDiagram("", null);
        ad.addAllActors(namesOfActors);
        ad.addAllUseCases(namesOfUseCases);
        _errors.addAll(ad.makeConnectionsFromJson(_spec));


        return ad;
    }








}
