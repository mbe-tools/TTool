/* ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


package tmltranslator.toavatarsec;

import avatartranslator.*;
import myutil.TraceManager;
import proverifspec.ProVerifQueryResult;
import tmltranslator.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class AVATAR2ProVerif
 * Creation: 29/02/2016
 *
 * @author Ludovic APVRILLE, Letitia LI
 * @version 1.1 29/02/2016
 */
public class TML2Avatar {
    private final static Integer channelPublic = 0;
    private final static Integer channelPrivate = 1;
    private final static Integer channelUnreachable = 2;
    //private AvatarAttribute pKey;
    private final Map<TMLTask, AvatarBlock> taskBlockMap = new HashMap<>();
    private final Map<String, Integer> originDestMap = new HashMap<>();
    private final Map<String, Object> stateObjectMap = new HashMap<>();
    private final Map<TMLTask, List<SecurityPattern>> accessKeys = new HashMap<>();
    private final List<SecurityPattern> secPatterns = new ArrayList<>();
    private int loopLimit = 1;
    private final HashMap<TMLActivityElement, Set<String>> channelsSecAttributes = new HashMap<>();
    private final HashMap<TMLChannel, Set<AvatarPragma>> secChannelMap = new HashMap<>();
    private final Map<TMLWriteChannel, Set<AvatarPragma>> confPragmaMap = new HashMap<>();
    private final Map<TMLActivityElement, Set<AvatarPragma>> authPragmaMap = new HashMap<>();
    private final HashMap<String, List<AvatarAttributeState>> signalAuthOriginMap = new HashMap<>();
    private final HashMap<String, List<AvatarAttributeState>> signalAuthDestMap = new HashMap<>();
    private final List<AvatarSignal> signals = new ArrayList<>();
    private AvatarSpecification avspec;
    private final List<String> allStates;
    private final boolean mc;
    private final boolean security;
    private final TMLMapping<?> tmlmap;
    private final TMLModeling<?> tmlmodel;
    private final Set<SecurityPattern> keysPublicBus = new HashSet<>();
    private final Map<SecurityPattern, List<AvatarAttribute>> symKeys = new HashMap<>();
    private final Map<SecurityPattern, List<AvatarAttribute>> pubKeys = new HashMap<>();

    private final Map<SecurityPattern, AvatarAttribute> secPatternEncAttribute = new HashMap<>();
    private final Map<SecurityPattern, AvatarAttribute> secPatternDecAttribute = new HashMap<>();
    private final Map<SecurityPattern, AvatarPragmaSecret> secPatternPragmaMap = new HashMap<>();
    private final Map<String, String> nameMap = new HashMap<>();
    private final Map<String, AvatarSignal> signalInMap = new HashMap<>();
    private final Map<String, AvatarSignal> signalOutMap = new HashMap<>();

    private final Object referenceObject;

    public TML2Avatar(TMLMapping<?> tmlmap, boolean modelcheck, boolean sec, Object _referenceObject) {
        this.tmlmap = tmlmap;

        this.tmlmodel = tmlmap.getTMLModeling();

        allStates = new ArrayList<>();
        mc = modelcheck;
        security = sec;

        referenceObject = _referenceObject;
    }

    private void checkConnections() {
        List<HwLink> links = tmlmap.getTMLArchitecture().getHwLinks();
        for (TMLTask t1 : tmlmodel.getTasks()) {
            List<SecurityPattern> keys = new ArrayList<>();
            accessKeys.put(t1, keys);
            for (HwLink link : links) {
                if (link.bus.privacy == HwBus.BUS_PUBLIC &&  link.hwnode instanceof HwMemory) {
                    List<SecurityPattern> patterns = tmlmap.getMappedPatterns((HwMemory) link.hwnode);
                    keysPublicBus.addAll(patterns);
                }
            }

            HwExecutionNode node1 = tmlmap.getHwNodeOf(t1);
            //Try to find memory using only private buses from origin
            List<HwNode> toVisit = new ArrayList<>();
            //List<HwNode> toMemory = new ArrayList<HwNode>();
            List<HwNode> complete = new ArrayList<>();
            for (HwLink link : links) {
                if (link.hwnode == node1) {
                    if (link.bus.privacy == 1) {
                        toVisit.add(link.bus);
                    }
                }
            }
            boolean memory = false;
            //memloop:
            while (!toVisit.isEmpty()) {
                HwNode curr = toVisit.remove(0);
                for (HwLink link : links) {
                    if (curr == link.bus) {
                        if (link.hwnode instanceof HwMemory) {
                            memory = true;
                            List<SecurityPattern> patterns = tmlmap.getMappedPatterns((HwMemory) link.hwnode);
                            accessKeys.get(t1).addAll(patterns);
                            //  break memloop;
                        }
                        if (!complete.contains(link.hwnode) && !toVisit.contains(link.hwnode) && link.hwnode instanceof HwBridge) {
                            toVisit.add(link.hwnode);
                        }
                    } else if (curr == link.hwnode) {
                        if (!complete.contains(link.bus) && !toVisit.contains(link.bus) && link.bus.privacy == 1) {
                            toVisit.add(link.bus);
                        }
                    }
                }
                complete.add(curr);
            }

            // Find path to secure memory from destination node

            for (TMLTask t2 : tmlmodel.getTasks()) {
                HwExecutionNode node2 = tmlmap.getHwNodeOf(t2);
                if (!memory) {
                    // There is no path to a private memory
                    originDestMap.put(t1.getName() + "__" + t2.getName(), channelPublic);

                } else if (node1 == node2) {
                    originDestMap.put(t1.getName() + "__" + t2.getName(), channelPrivate);

                } else {
                    //Navigate architecture for node

                    //HwNode last = node1;
                    List<HwNode> found = new ArrayList<>();
                    List<HwNode> done = new ArrayList<>();
                    List<HwNode> path = new ArrayList<>();
                    Map<HwNode, List<HwNode>> pathMap = new HashMap<>();
                    for (HwLink link : links) {
                        if (link.hwnode == node1) {
                            found.add(link.bus);
                            List<HwNode> tmp = new ArrayList<>();
                            tmp.add(link.bus);
                            pathMap.put(link.bus, tmp);
                        }
                    }
                    outerloop:
                    while (!found.isEmpty()) {
                        HwNode curr = found.remove(0);
                        for (HwLink link : links) {
                            if (curr == link.bus) {
                                if (link.hwnode == node2) {
                                    path = pathMap.get(curr);
                                    break outerloop;
                                }
                                if (!done.contains(link.hwnode) && !found.contains(link.hwnode) && link.hwnode instanceof HwBridge) {
                                    found.add(link.hwnode);
                                    List<HwNode> tmp = new ArrayList<>(pathMap.get(curr));
                                    tmp.add(link.hwnode);
                                    pathMap.put(link.hwnode, tmp);
                                }
                            } else if (curr == link.hwnode) {
                                if (!done.contains(link.bus) && !found.contains(link.bus)) {
                                    found.add(link.bus);
                                    List<HwNode> tmp = new ArrayList<>(pathMap.get(curr));
                                    tmp.add(link.bus);
                                    pathMap.put(link.bus, tmp);
                                }
                            }
                        }
                        done.add(curr);
                    }
                    if (path.isEmpty()) {
                        originDestMap.put(t1.getName() + "__" + t2.getName(), channelUnreachable);
                    } else {
                        int priv = 1;
                        HwBus bus;
                        //Check if all buses and bridges are private
                        for (HwNode n : path) {
                            if (n instanceof HwBus) {
                                bus = (HwBus) n;
                                if (bus.privacy == 0) {
                                    priv = 0;
                                    break;
                                }
                            }
                        }
                        originDestMap.put(t1.getName() + "__" + t2.getName(), priv);
                    }
                }
            }
        }

    }


	/*	public void checkChannels(){
            List<TMLChannel> channels = tmlmodel.getChannels();
			List<TMLTask> destinations = new ArrayList<TMLTask>();
			TMLTask a; 
			for (TMLChannel channel: channels){	
				destinations.clear();
				if (channel.isBasicChannel()){
					a = channel.getOriginTask();
					destinations.add(channel.getDestinationTask());
				}
				else {
					a=channel.getOriginTasks().get(0);
					destinations.addAll(channel.getDestinationTasks());
				}  
				HwExecutionNode node1 = tmlmap.getHwNodeOf(a);
				for (TMLTask t: destinations){
					//List<HwBus> buses = new ArrayList<HwBus>();
					HwNode node2 = tmlmap.getHwNodeOf(t);

					//Check if each node has a secure path to memory
					
					
					

					if (node1==node2){
						channelMap.put(channel, channelPrivate);
					}

					if (node1!=node2){
						//Navigate architecture for node
						List<HwLink> links = tmlmap.getTMLArchitecture().getHwLinks();
						//HwNode last = node1;
						List<HwNode> found = new ArrayList<HwNode>();	
						List<HwNode> done = new ArrayList<HwNode>();
						List<HwNode> path = new ArrayList<HwNode>();
						Map<HwNode, List<HwNode>> pathMap = new HashMap<HwNode, List<HwNode>>();

						for (HwLink link: links){
							if (link.hwnode == node1){
								found.add(link.bus);
								List<HwNode> tmp = new ArrayList<HwNode>();
								tmp.add(link.bus);
								pathMap.put(link.bus, tmp);
							}
						}
						outerloop:
						while (found.size()>0){
							HwNode curr = found.remove(0);
							for (HwLink link: links){
								if (curr == link.bus){
									if (link.hwnode == node2){
										path = pathMap.get(curr);
										break outerloop;
									}
									if (!done.contains(link.hwnode) && !found.contains(link.hwnode) && link.hwnode instanceof HwBridge){
										found.add(link.hwnode);
										List<HwNode> tmp = new ArrayList<HwNode>(pathMap.get(curr));
										tmp.add(link.hwnode);
										pathMap.put(link.hwnode, tmp);
									}
								}
								else if (curr == link.hwnode){
									if (!done.contains(link.bus) && !found.contains(link.bus)){
										found.add(link.bus);
										List<HwNode> tmp = new ArrayList<HwNode>(pathMap.get(curr));
										tmp.add(link.bus);
										pathMap.put(link.bus, tmp);
									}
								}
							}
							done.add(curr);
						}

						if (path.size() ==0){
				
							channelMap.put(channel, channelUnreachable);
						}
						else {
							int priv=1;
							HwBus bus;
							//Check if all buses and bridges are private
							for (HwNode n: path){
								if (n instanceof HwBus){
									bus = (HwBus) n;
									if (bus.privacy ==0){
										priv=0;
										break;
									}
								}
							}

							channelMap.put(channel, priv);
							//TraceManager.addDev("Channel "+channel.getName() + " between Task "+ a.getTaskName() + " and Task " + t.getTaskName() + " is " + (priv==1 ? "confidential" : "not confidential"));
						}
					}
				}
			}  
			TraceManager.addDev(channelMap);
		}*/

    private List<AvatarStateMachineElement> translateState(TMLActivityElement ae, AvatarBlock block) {

        //		TMLActionState tmlaction;
        //		TMLChoice tmlchoice;
        //		TMLExecI tmlexeci;
        //		TMLExecIInterval tmlexecii;
        //	TMLExecC tmlexecc;
        //	TMLExecCInterval tmlexecci;
        //		TMLForLoop tmlforloop;
        //		TMLReadChannel tmlreadchannel;
        //		TMLSendEvent tmlsendevent;
        //		TMLSendRequest tmlsendrequest;
        //		TMLStopState tmlstopstate;
        //		TMLWaitEvent tmlwaitevent;
        //		TMLNotifiedEvent tmlnotifiedevent;
        //		TMLWriteChannel tmlwritechannel;
        //		TMLSequence tmlsequence;
        //		TMLRandomSequence tmlrsequence;
        //		TMLSelectEvt tmlselectevt;
        //		TMLDelay tmldelay;

        AvatarTransition tran = new AvatarTransition(block, "", null);
        List<AvatarStateMachineElement> elementList = new ArrayList<>();

        if (ae == null) {
            return elementList;
        }

        if (ae instanceof TMLStopState) {
            AvatarStopState stops = new AvatarStopState(ae.getName(), ae.getReferenceObject(), block);
            elementList.add(stops);
            return elementList;
        } else if (ae instanceof TMLStartState) {
            AvatarStartState ss = new AvatarStartState(ae.getName(), ae.getReferenceObject(), block);
            tran = new AvatarTransition(block, "__after_" + ae.getName(), ss.getReferenceObject());
            ss.addNext(tran);
            elementList.add(ss);
            elementList.add(tran);
        } else if (ae instanceof TMLRandom) {
            AvatarState signalState = new AvatarState("signalstate_" + reworkStringName(ae.getName()), ae.getReferenceObject(), block);
            AvatarTransition signalTran = new AvatarTransition(block, "__after_signalstate_" + ae.getName(), ae.getReferenceObject());
            AvatarRandom ar = new AvatarRandom(ae.getName(), ae.getReferenceObject(), block);
            TMLRandom tmlr = (TMLRandom) ae;
            ar.setVariable(tmlr.getVariable());
            ar.setValues(tmlr.getMinValue(), tmlr.getMaxValue());
            tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
            elementList.add(signalState);
            signalState.addNext(signalTran);
            elementList.add(signalTran);
            signalTran.addNext(ar);
            elementList.add(ar);
            ar.addNext(tran);
            elementList.add(tran);
        } else if (ae instanceof TMLSequence) {
            //Get all list of sequences and paste together
            List<AvatarStateMachineElement> seq = translateState(ae.getNextElement(0), block);
            List<AvatarStateMachineElement> tmp;
            // elementList.addAll(seq);
            //get rid of any stops in the middle of the sequence and replace with the start of the next sequence
            for (int i = 1; i < ae.getNbNext(); i++) {
                tmp = translateState(ae.getNextElement(i), block);
                for (AvatarStateMachineElement e : seq) {
                    if (e instanceof AvatarStopState) {
                        //ignore
                    } else if (e.getNexts().isEmpty()) {
                        e.addNext(tmp.get(0));
                        elementList.add(e);
                    } else if (e.getNext(0) instanceof AvatarStopState) {
                        //Remove the transition to AvatarStopState
                        e.removeNext(0);
                        e.addNext(tmp.get(0));
                        elementList.add(e);
                    } else {
                        elementList.add(e);
                    }
                }
                //elementList.addAll(tmp);
                seq = tmp;
            }
            //Put stop states on the end of the last in sequence

            for (AvatarStateMachineElement e : seq) {
                if (e.getNexts().isEmpty() && !(e instanceof AvatarStopState)) {
                    AvatarStopState stop = new AvatarStopState("stop", null, block);
                    e.addNext(stop);
                    elementList.add(stop);
                }
                elementList.add(e);
            }
            return elementList;

        } else if (ae instanceof TMLSendRequest) {
            TMLSendRequest sr = (TMLSendRequest) ae;
            TMLRequest req = sr.getRequest();
            AvatarSignal sig;

            boolean checkAcc = ae.hasCheckableAccessibility();
            boolean checked =  ae.hasCheckedAccessibility();
            AvatarState signalState = new AvatarState("signalstate_" + reworkStringName(ae.getName()) + "_" + reworkStringName(req.getName()),
                    ae.getReferenceObject(), block, checkAcc, checked);
            AvatarTransition signalTran = new AvatarTransition(block, "__after_signalstate_" + ae.getName() + "_" + req.getName(), ae.getReferenceObject());
            if (!signalOutMap.containsKey(req.getName())) {
                sig = new AvatarSignal(getNameReworked(req.getName(), 1), AvatarSignal.OUT, req.getReferenceObject());
                signals.add(sig);
                signalOutMap.put(req.getName(), sig);
                block.addSignal(sig);
                int cpt = 0;
                for (TMLType tmlt : req.getParams()) {
                    AvatarAttribute aa = new AvatarAttribute("p" + cpt, getAvatarType(tmlt), null, null);
                    sig.addParameter(aa);
                    cpt++;
                }
            } else {
                sig = signalOutMap.get(req.getName());
            }

            AvatarActionOnSignal as = new AvatarActionOnSignal(ae.getName(), sig, ae.getReferenceObject(), block);
            for (int i = 0; i < sr.getNbOfParams(); i++) {
                if (block.getAvatarAttributeWithName(sr.getParam(i)) == null) {
                    //Throw Error
                    AvatarType type;
                    if (sr.getParam(i).matches("-?\\d+")) {
                        type = AvatarType.INTEGER;
                    } else if (sr.getParam(i).matches("(?i)^(true|false)")) {
                        type = AvatarType.BOOLEAN;
                    } else {
                        type = AvatarType.UNDEFINED;
                    }
                    String nameNewAtt = getNameReworked(req.getName(), 1) + "_" + req.getID() + "_" + i + "_" + sr.getParam(i);
                    if (block.getAvatarAttributeWithName(nameNewAtt) == null) {
                        AvatarAttribute avattr = new AvatarAttribute(nameNewAtt, type, block, null);
                        avattr.setInitialValue(sr.getParam(i));
                        block.addAttribute(avattr);
                        as.addValue(avattr.getName());
                        TraceManager.addDev("Missing Attribute " + sr.getParam(i));
                    } else {
                        as.addValue(block.getAvatarAttributeWithName(nameNewAtt).getName());
                    }
                } else {
                    //	Add parameter to signal and actiononsignal
                    as.addValue(sr.getParam(i));
                }
            }
                /*
				if (req.checkAuth){
					AvatarAttributeState authOrig = new AvatarAttributeState(block.getName()+"."+signalState.getName()+"."+requestData.getName(),ae.getReferenceObject(),requestData, signalState);
					signalAuthOriginMap.put(req.getName(), authOrig);
				}*/
            tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
            elementList.add(signalState);
            signalState.addNext(signalTran);
            elementList.add(signalTran);
            signalTran.addNext(as);
            elementList.add(as);
            as.addNext(tran);
            elementList.add(tran);
        } else if (ae instanceof TMLRandomSequence) {
            //HashMap<Integer, List<AvatarStateMachineElement>> seqs = new HashMap<Integer, List<AvatarStateMachineElement>>();
            AvatarState choiceStateInit = new AvatarState("seqchoiceInit__" + reworkStringName(ae.getName()), ae.getReferenceObject(), block);
            AvatarState choiceState = new AvatarState("seqchoice__" + reworkStringName(ae.getName()), ae.getReferenceObject(), block);
            AvatarTransition tranChoiceStateInit = new AvatarTransition(block, "trans_seqchoiceInit__" + reworkStringName(ae.getName()), ae.getReferenceObject());
            elementList.add(choiceStateInit);
            elementList.add(tranChoiceStateInit);
            choiceStateInit.addNext(tranChoiceStateInit);
            tranChoiceStateInit.addNext(choiceState);
            elementList.add(choiceState);

            if (ae.getNbNext() == 1) {
                tran = new AvatarTransition(block, "__after_" + ae.getName() + "_0", ae.getReferenceObject());
                elementList.add(tran);
                choiceState.addNext(tran);
                List<AvatarStateMachineElement> set0 = translateState(ae.getNextElement(0), block);
                tran.addNext(set0.get(0));
                elementList.addAll(set0);
                return elementList;
            } else {
                block.addAttribute(new AvatarAttribute("seqNb_" + ae.getName() + ae.getID(), AvatarType.INTEGER, block, null));
                for (int i = 0; i < ae.getNbNext(); i++) {
                    tran = new AvatarTransition(block, "__after_" + ae.getName() + "_" + i, ae.getReferenceObject());
                    block.addAttribute(new AvatarAttribute("seq_" + ae.getName() + i + "_" + ae.getID(), AvatarType.INTEGER, block, null));
                    tran.addAction("seq_" + ae.getName() + i + "_" + ae.getID() + " = 1");
                    tran.addAction("seqNb_" + ae.getName() + ae.getID() +  " = " + "seqNb_" + ae.getName() + ae.getID() + " + 1");
                    tran.setGuard("seq_" + ae.getName() + i + "_" + ae.getID() + " == 0");
                    if (i == 0) {
                        tranChoiceStateInit.addAction("seqNb_" + ae.getName() + ae.getID() +  " = 0");
                    }
                    tranChoiceStateInit.addAction("seq_" + ae.getName() + i + "_" + ae.getID() + " = 0");
                    //tran.addAction(AvatarTerm.createActionFromString(block, "seq_"+ae.getName()+i+ae.getID()+" = 1"));
                    //tran.addAction(AvatarTerm.createActionFromString(block, "seqNb_"+ae.getName()+ae.getID()+" = 1 + " + "seqNb_"+ae.getName()+ae.getID()));
                    
                    choiceState.addNext(tran);
                    elementList.add(tran);
                    List<AvatarStateMachineElement> tmp = translateState(ae.getNextElement(i), block);
                    AvatarState choiceStateEnd = new AvatarState("seqchoiceend__" + i + "_" + reworkStringName(ae.getName()), ae.getReferenceObject(), block);
                    AvatarTransition tranChoiceStateEnd = new AvatarTransition(block, "trans_seqchoiceend__" + i + "_" + reworkStringName(ae.getName()), ae.getReferenceObject());
                    choiceStateEnd.addNext(tranChoiceStateEnd);
                    elementList.add(choiceStateEnd);
                    elementList.add(tranChoiceStateEnd);
                    //elementList.addAll(tmp);
                    for (AvatarStateMachineElement e : tmp) {
                        if (e instanceof AvatarStopState) {
                            //ignore
                        } else if (e.getNexts().isEmpty()) {
                            e.addNext(choiceStateEnd);
                            tranChoiceStateEnd.addNext(choiceState);
                            elementList.add(e);
                        } else if (e.getNext(0) instanceof AvatarStopState) {
                            e.removeNext(0);
                            e.addNext(choiceStateEnd);
                            tranChoiceStateEnd.addNext(choiceState);
                            elementList.add(e);
                        } else {
                            elementList.add(e);
                        }
                    }
                    tran.addNext(tmp.get(0));
                }
                tran = new AvatarTransition(block, "__after_" + ae.getName() + "_" + ae.getNbNext(), ae.getReferenceObject());
                tran.setGuard("seqNb_" + ae.getName() + ae.getID() + " == " + ae.getNbNext());
                choiceState.addNext(tran);
                elementList.add(tran);
                AvatarStopState stop = new AvatarStopState("stop", null, block);
                tran.addNext(stop);
                elementList.add(stop);
                
            }
            /*if (ae.getNbNext() == 2) {
                List<AvatarStateMachineElement> set0 = translateState(ae.getNextElement(0), block);
                List<AvatarStateMachineElement> set1 = translateState(ae.getNextElement(1), block);
                //		elementList.addAll(set0);

                //Remove stop states of sets and route their transitions to the first element of the following sequence
                for (AvatarStateMachineElement e : set0) {
                    if (e instanceof AvatarStopState) {
                        //ignore
                    } else if (e.getNexts().size() == 0) {
                        e.addNext(set1.get(0));
                        elementList.add(e);
                    } else if (e.getNext(0) instanceof AvatarStopState) {
                        //Remove the transition to AvatarStopState
                        e.removeNext(0);
                        e.addNext(set1.get(0));
                        elementList.add(e);
                    } else {
                        elementList.add(e);
                    }
                }


                //Build branch 0
                tran = new AvatarTransition(block, "__after_" + ae.getName() + "_0", ae.getReferenceObject());
                choiceState.addNext(tran);
                elementList.add(tran);
                tran.addNext(set0.get(0));
                //Put stop states at the end of set1 if they don't already exist
                AvatarStopState stop = new AvatarStopState("stop", null, block);
                for (AvatarStateMachineElement e : set1) {
                    if (e.getNexts().size() == 0 && (e instanceof AvatarTransition)) {
                        e.addNext(stop);
                    }
                    elementList.add(e);
                }
                elementList.add(stop);

                //Build branch 1
                List<AvatarStateMachineElement> set0_1 = translateState(ae.getNextElement(0), block);
                List<AvatarStateMachineElement> set1_1 = translateState(ae.getNextElement(1), block);
                for (AvatarStateMachineElement e : set1_1) {
                    if (e instanceof AvatarStopState) {
                        //ignore
                    } else if (e.getNexts().size() == 0) {
                        e.addNext(set0_1.get(0));
                        elementList.add(e);
                    } else if (e.getNext(0) instanceof AvatarStopState) {
                        //Remove the transition to AvatarStopState
                        e.removeNext(0);
                        e.addNext(set0_1.get(0));
                        elementList.add(e);
                    } else {
                        elementList.add(e);
                    }
                }
                tran = new AvatarTransition(block, "__after_" + ae.getName() + "_1", ae.getReferenceObject());
                elementList.add(tran);
                choiceState.addNext(tran);
                tran.addNext(set1_1.get(0));
                stop = new AvatarStopState("stop", null, block);
                for (AvatarStateMachineElement e : set0_1) {
                    if (e.getNexts().size() == 0 && (e instanceof AvatarTransition)) {
                        e.addNext(stop);
                    }
                    elementList.add(e);
                }
                elementList.add(stop);

            } else {
                //This gets really complicated in Avatar...
                for (int i = 0; i < ae.getNbNext(); i++) {
                    //For each of the possible state blocks, translate 1 and recurse on the remaining random sequence
                    tran = new AvatarTransition(block, "__after_" + ae.getName() + "_" + i, ae.getReferenceObject());
                    choiceState.addNext(tran);
                    List<AvatarStateMachineElement> tmp = translateState(ae.getNextElement(i), block);


                    AvatarState choiceStateEnd = new AvatarState("seqchoiceend__" + i + "_" +
                            reworkStringName(ae.getName()), ae.getReferenceObject(), block);
                    elementList.add(choiceStateEnd);


                    //Remove stop states from the first generated set
                    for (AvatarStateMachineElement e : tmp) {
                        if (e instanceof AvatarStopState) {
                            //ignore
                        } else if (e.getNexts().size() == 0) {
                            //e.addNext(set1.get(0));
                            e.addNext(choiceStateEnd);
                            elementList.add(e);
                        } else if (e.getNext(0) instanceof AvatarStopState) {
                            //Remove the transition to AvatarStopState
                            e.removeNext(0);
                            e.addNext(choiceStateEnd);
                            //e.addNext(set1.get(0));
                            elementList.add(e);
                        } else {
                            elementList.add(e);
                        }
                    }


                    tran.addNext(tmp.get(0));

                    TMLRandomSequence newSeq = new TMLRandomSequence("seqchoice__" + i + "_" + ae.getNbNext() + "_" + ae.getName(), ae.getReferenceObject());
                    for (int j = 0; j < ae.getNbNext(); j++) {
                        if (j != i) {
                            newSeq.addNext(ae.getNextElement(j));
                        }
                    }


                    tran = new AvatarTransition(block, "__after_" + ae.getNextElement(i).getName(), ae.getReferenceObject());
                    choiceStateEnd.addNext(tran);
                    elementList.add(tran);

                    List<AvatarStateMachineElement> nexts = translateState(newSeq, block);
                    elementList.addAll(nexts);
                    tran.addNext(nexts.get(0));

                }

            }*/
            return elementList;
        } else if (ae instanceof TMLActivityElementEvent) {
            TMLActivityElementEvent aee = (TMLActivityElementEvent) ae;
            TMLEvent evt = aee.getEvent();

            boolean checkAcc = ae.hasCheckableAccessibility();
            boolean checked =  ae.hasCheckedAccessibility();
            AvatarState signalState = new AvatarState("signalstate_" + reworkStringName(ae.getName()) + "_" + evt.getName(), ae.getReferenceObject(),
                    block, checkAcc, checked);
            AvatarTransition signalTran = new AvatarTransition(block, "__after_signalstate_" + ae.getName() + "_" + evt.getName(),
                    ae.getReferenceObject());

            if (ae instanceof TMLSendEvent) {
                AvatarSignal sig;
                if (!signalOutMap.containsKey(evt.getName())) {
                    sig = new AvatarSignal(getNameReworked(evt.getName(), 1), AvatarSignal.OUT, evt.getReferenceObject());
                    signals.add(sig);
                    block.addSignal(sig);
                    signalOutMap.put(evt.getName(), sig);
                    int cpt = 0;
                    for (TMLType tmlt : evt.getParams()) {
                        AvatarAttribute aa = new AvatarAttribute("p" + cpt, getAvatarType(tmlt), null, null);
                        sig.addParameter(aa);
                        cpt++;
                    }
                } else {
                    sig = signalOutMap.get(evt.getName());
                }
                AvatarActionOnSignal as = new AvatarActionOnSignal(ae.getName(), sig, ae.getReferenceObject(), block);
                for (int i = 0; i < aee.getNbOfParams(); i++) {
                    if (block.getAvatarAttributeWithName(aee.getParam(i)) == null) {
                        //Throw Error
                        AvatarType type;
                        if (aee.getParam(i).matches("-?\\d+")) {
                            type = AvatarType.INTEGER;
                        } else if (aee.getParam(i).matches("(?i)^(true|false)")) {
                            type = AvatarType.BOOLEAN;
                        } else {
                            type = AvatarType.UNDEFINED;
                        }
                        String nameNewAtt = getNameReworked(evt.getName(), 1) + "_" + evt.getID() + "_" + i + "_" + aee.getParam(i);
                        if (block.getAvatarAttributeWithName(nameNewAtt) == null) {
                            AvatarAttribute avattr = new AvatarAttribute(nameNewAtt, type, block, null);
                            avattr.setInitialValue(aee.getParam(i));
                            block.addAttribute(avattr);
                            as.addValue(avattr.getName());
                            TraceManager.addDev("Missing Attribute " + aee.getParam(i));
                        } else {
                            as.addValue(block.getAvatarAttributeWithName(nameNewAtt).getName());
                        }
                    } else {
                        //	Add parameter to signal and actiononsignal
                        as.addValue(aee.getParam(i));
                    }
                }

                tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                elementList.add(signalState);
                signalState.addNext(signalTran);
                elementList.add(signalTran);
                signalTran.addNext(as);
                elementList.add(as);
                as.addNext(tran);
                elementList.add(tran);


            } else if (ae instanceof TMLWaitEvent) {
                AvatarSignal sig;
                if (!signalInMap.containsKey(evt.getName())) {
                    sig = new AvatarSignal(getNameReworked(evt.getName(), 3), AvatarSignal.IN, evt.getReferenceObject());
                    signals.add(sig);
                    block.addSignal(sig);
                    signalInMap.put(evt.getName(), sig);
                    int cpt = 0;
                    for (TMLType tmlt : evt.getParams()) {
                        AvatarAttribute aa = new AvatarAttribute("p" + cpt, getAvatarType(tmlt), null, null);
                        sig.addParameter(aa);
                        cpt++;
                    }
                } else {
                    sig = signalInMap.get(evt.getName());
                }
                AvatarActionOnSignal as = new AvatarActionOnSignal(ae.getName(), sig, ae.getReferenceObject(), block);
                for (int i = 0; i < aee.getNbOfParams(); i++) {
                    if (block.getAvatarAttributeWithName(aee.getParam(i)) == null) {
                        //Throw Error
                        AvatarType type;
                        if (aee.getParam(i).matches("-?\\d+")) {
                            type = AvatarType.INTEGER;
                        } else if (aee.getParam(i).matches("(?i)^(true|false)")) {
                            type = AvatarType.BOOLEAN;
                        } else {
                            type = AvatarType.UNDEFINED;
                        }
                        String nameNewAtt = getNameReworked(evt.getName(), 3) + "_" + evt.getID() + "_" + i + "_" + aee.getParam(i);
                        if (block.getAvatarAttributeWithName(nameNewAtt) == null) {
                            AvatarAttribute avattr = new AvatarAttribute(nameNewAtt, type, block, null);
                            avattr.setInitialValue(aee.getParam(i));
                            block.addAttribute(avattr);
                            as.addValue(avattr.getName());
                            TraceManager.addDev("Missing Attribute " + aee.getParam(i));
                        } else {
                            as.addValue(block.getAvatarAttributeWithName(nameNewAtt).getName());
                        }
                    } else {
                        //	Add parameter to signal and actiononsignal
                        as.addValue(aee.getParam(i));
                    }
                }

                tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                elementList.add(signalState);
                signalState.addNext(signalTran);
                elementList.add(signalTran);
                signalTran.addNext(as);
                elementList.add(as);
                as.addNext(tran);
                elementList.add(tran);
            } else {
                //Notify Event, I don't know how to translate this
                TraceManager.addDev("Notify event");

                AvatarRandom ar = new AvatarRandom(ae.getName(), ae.getReferenceObject(), block);
                ar.setVariable(aee.getVariable());
                ar.setValues("0", "1");

                tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                elementList.add(signalState);
                signalState.addNext(signalTran);
                elementList.add(signalTran);
                signalTran.addNext(ar);
                elementList.add(ar);
                ar.addNext(tran);
                elementList.add(tran);
            }

        } else if (ae instanceof TMLActivityElementWithAction) {
            //Might be encrypt or decrypt
            AvatarState as = new AvatarState(reNameStateWithAction(ae.getValue()) + "_" + reNameStateWithAction(ae.getName()),
                    ae.getReferenceObject(), block);
            tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
            as.addNext(tran);
            elementList.add(as);
            elementList.add(tran);
            if (security && ae.getSecurityPattern() != null && ae instanceof TMLExecC) {
                //If encryption
                if (!((TMLExecC) ae).isDecryptionProcess()) {
                    secPatterns.add(ae.getSecurityPattern());
                    switch (ae.getSecurityPattern().getType()) {
                        case SecurityPattern.ADVANCED_PATTERN:
                            //Type Advanced
                            tran.addAction(ae.getSecurityPattern().getFormula());
                            break;
                        case SecurityPattern.SYMMETRIC_ENC_PATTERN:
                            //Type Symmetric Encryption
                            if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                //Create sencrypt method for key
                                AvatarAttribute keyToEncrypt = new AvatarAttribute(ae.getSecurityPattern().getKey(), AvatarType.INTEGER, block, null);
                                block.addAttribute(keyToEncrypt);
                                AvatarAttribute keySecPattern = new AvatarAttribute("key_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block,
                                        null);
                                block.addAttribute(keySecPattern);
                                if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                    //Concatenate nonce to data
                                    tran.addAction(keyToEncrypt.getName() + " = concat2(" + keyToEncrypt.getName() + "," + ae.getSecurityPattern().getNonce() + ")");
                                }
                                //Securing a key
                                tran.addAction(secPatternEncAttribute.get(ae.getSecurityPattern()).getName() + " = sencrypt(" + keyToEncrypt.getName() + ", " + keySecPattern.getName() + ")");
                            } else {
                                //Add sencrypt method for data
                                AvatarAttribute msgToEncrypt = new AvatarAttribute(ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(msgToEncrypt);
                                AvatarAttribute keySecPattern = new AvatarAttribute("key_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER,
                                        block, null);
                                block.addAttribute(keySecPattern);
                                if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                    //Concatenate nonce to data
                                    tran.addAction(msgToEncrypt.getName() + " = concat2(" + msgToEncrypt.getName() + "," + ae.getSecurityPattern().getNonce() + ")");
                                }
                                tran.addAction(secPatternEncAttribute.get(ae.getSecurityPattern()).getName() + " = sencrypt(" + msgToEncrypt.getName() + ", " + keySecPattern.getName() + ")");
                            }
                            //Set as origin for authenticity
                            //ae.getSecurityPattern().setOriginTask(block.getName());
                            ae.getSecurityPattern().setState1(as);
                            break;
                        case SecurityPattern.ASYMMETRIC_ENC_PATTERN:
                            //Securing a key instead of data
                            if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                //Add aencrypt method
                                AvatarAttribute keyToEncrypt = new AvatarAttribute("key_" + ae.getSecurityPattern().getKey(), AvatarType.INTEGER, block, null);
                                block.addAttribute(keyToEncrypt);
                                AvatarAttribute pubKey = new AvatarAttribute("pubKey_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block,
                                        null);
                                block.addAttribute(pubKey);
                                if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                    //Concatenate nonce to data
                                    tran.addAction(keyToEncrypt.getName() + " = concat2(" + keyToEncrypt.getName() + "," + ae.getSecurityPattern().getNonce() + ")");
                                }
                                tran.addAction(secPatternEncAttribute.get(ae.getSecurityPattern()).getName() + " = aencrypt(" + keyToEncrypt.getName() + ", " + pubKey.getName() + ")");
                            } else {
                                //Securing data

                                //Add aencrypt method
                                AvatarAttribute msgToEncrypt = new AvatarAttribute(ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(msgToEncrypt);
                                AvatarAttribute pubKey = new AvatarAttribute("pubKey_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER,
                                        block, null);
                                block.addAttribute(pubKey);
                                tran.addAction(secPatternEncAttribute.get(ae.getSecurityPattern()).getName() + " = aencrypt(" + msgToEncrypt.getName() + ", " + pubKey.getName() + ")");
                            }
                            //Set as origin state for authenticity
                            //ae.getSecurityPattern().setOriginTask(block.getName());
                            ae.getSecurityPattern().setState1(as);
                            break;
                        case SecurityPattern.HASH_PATTERN: {
                            AvatarAttribute msgToEncrypt = new AvatarAttribute(ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                            block.addAttribute(msgToEncrypt);
                            tran.addAction(secPatternEncAttribute.get(ae.getSecurityPattern()).getName() + " = hash(" + msgToEncrypt.getName() + ")");
                            break;
                        }
                        case SecurityPattern.MAC_PATTERN: {
                            AvatarAttribute msgToEncrypt = new AvatarAttribute(ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                            block.addAttribute(msgToEncrypt);
                            AvatarAttribute keySecPattern = new AvatarAttribute("key_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block,
                                    null);
                            block.addAttribute(keySecPattern);
                            AvatarAttribute msgMac = new AvatarAttribute(ae.getSecurityPattern().getName() + "_mac", AvatarType.INTEGER,
                                    block, null);
                            block.addAttribute(msgMac);
                            if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                //Add nonce

                                //Add concat2 method
                                tran.addAction(msgToEncrypt.getName() + " = concat2(" + msgToEncrypt.getName() + ", " + ae.getSecurityPattern().getNonce() + ")");
                            }

                            //Add MAC method
                            tran.addAction(msgMac.getName() + " = MAC(" + msgToEncrypt.getName() + ", " + keySecPattern.getName() + ")");

                            //Concatenate msg and mac(msg)
                            tran.addAction(secPatternEncAttribute.get(ae.getSecurityPattern()).getName() + " = concat2(" + msgToEncrypt.getName() + ", " + msgMac.getName() + ")");
                            //ae.getSecurityPattern().setOriginTask(block.getName());
                            ae.getSecurityPattern().setState1(as);
                            break;
                        }
                    }

                    // Set attribute state for authenticity
                    if (block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()) != null) {
                        AvatarPragmaSecret avPragmaSecret = new AvatarPragmaSecret("#Confidentiality " + block.getName() + "." + ae.getSecurityPattern().getName(),
                                ae.getReferenceObject(), block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()));
                        secPatternPragmaMap.put(ae.getSecurityPattern(), avPragmaSecret);
                        AvatarAttributeState authOrigin = new AvatarAttributeState(block.getName() + "." + as.getName() + "." +
                                ae.getSecurityPattern().getName(), ae.getReferenceObject(), block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()), as);
                        if (signalAuthOriginMap.containsKey(ae.getSecurityPattern().getName())) {
                            signalAuthOriginMap.get(ae.getSecurityPattern().getName()).add(authOrigin);
                        } else {
                            LinkedList<AvatarAttributeState> tmp = new LinkedList<>();
                            tmp.add(authOrigin);
                            signalAuthOriginMap.put(ae.getSecurityPattern().getName(), tmp);
                        }
                    }

                } else {
                    //Decryption action
                    switch (ae.getSecurityPattern().getType()) {
                        case SecurityPattern.SYMMETRIC_ENC_PATTERN: {
                            AvatarAttribute decryptedMsg;
                            if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                //Decrypting a key
                                //Add sdecrypt method
                                decryptedMsg = new AvatarAttribute("key_" + ae.getSecurityPattern().getKey(), AvatarType.INTEGER, block,
                                        null);
                                block.addAttribute(decryptedMsg);
                                AvatarAttribute key = new AvatarAttribute("key_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(key);
                                tran.addAction(decryptedMsg.getName() + " = sdecrypt(" + secPatternDecAttribute.get(ae.getSecurityPattern()).getName() + ", " + key.getName() + ")");
                            } else {
                                //Decrypting data
                                decryptedMsg = new AvatarAttribute(ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(decryptedMsg);
                                AvatarAttribute key = new AvatarAttribute("key_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(key);
                                tran.addAction(decryptedMsg.getName() + " = sdecrypt(" + secPatternDecAttribute.get(ae.getSecurityPattern()).getName() + ", " + key.getName() + ")");
                            }
                            if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                //Separate out the nonce
                                AvatarAttribute testNonce = new AvatarAttribute("testnonce_" + ae.getSecurityPattern().getNonce(), AvatarType.INTEGER,
                                        block, null);
                                block.addAttribute(testNonce);
                                //Add get2 method
                                tran.addAction("get2(" + decryptedMsg.getName() + ", " + decryptedMsg.getName() + ", " + testNonce.getName() + ")");

                                //Add state after get2 statement
                                AvatarState guardState = new AvatarState(reworkStringName(ae.getName()) + "_guarded", ae.getReferenceObject(), block);
                                tran.addNext(guardState);
                                tran = new AvatarTransition(block, "__guard_" + ae.getName(), ae.getReferenceObject());
                                guardState.addNext(tran);
                                elementList.add(guardState);
                                elementList.add(tran);

                                //Guard transition to determine if nonce matches
                                tran.setGuard(testNonce.getName() + "==" + ae.getSecurityPattern().getNonce());
                            }

                            // Add a dummy state afterwards for authenticity after decrypting the data
                            AvatarState dummy = new AvatarState(reworkStringName(ae.getName()) + "_dummy", ae.getReferenceObject(), block);
                            ae.getSecurityPattern().setState2(dummy);
                            tran.addNext(dummy);
                            tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                            dummy.addNext(tran);
                            elementList.add(dummy);
                            elementList.add(tran);
                            if (block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()) != null) {
                                AvatarAttributeState authDest = new AvatarAttributeState(block.getName() + "." + dummy.getName() + "." +
                                        ae.getSecurityPattern().getName(), ae.getReferenceObject(), block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()), dummy);
                                if (signalAuthDestMap.containsKey(ae.getSecurityPattern().getName())) {
                                    signalAuthDestMap.get(ae.getSecurityPattern().getName()).add(authDest);
                                } else {
                                    LinkedList<AvatarAttributeState> tmp = new LinkedList<>();
                                    tmp.add(authDest);
                                    signalAuthDestMap.put(ae.getSecurityPattern().getName(), tmp);
                                }
                            }
                            break;
                        }
                        case SecurityPattern.ASYMMETRIC_ENC_PATTERN: {
                            AvatarAttribute decryptedMsg;
                            if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                //Decrypting key
                                //Add adecrypt method
                                AvatarAttribute privKey = new AvatarAttribute("privKey_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block
                                        , null);
                                block.addAttribute(privKey);
                                decryptedMsg = new AvatarAttribute("key_" + ae.getSecurityPattern().getKey(), AvatarType.INTEGER, block,
                                        null);
                                block.addAttribute(decryptedMsg);
                                tran.addAction(decryptedMsg.getName() + " = adecrypt(" + secPatternDecAttribute.get(ae.getSecurityPattern()).getName() + ", " + privKey.getName() + ")");
                            } else {
                                //Decrypting data

                                //Add adecrypt method
                                decryptedMsg = new AvatarAttribute(ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(decryptedMsg);
                                AvatarAttribute privKey = new AvatarAttribute("privKey_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block
                                        , null);
                                block.addAttribute(privKey);

                                tran.addAction(decryptedMsg.getName() + " = adecrypt(" + secPatternDecAttribute.get(ae.getSecurityPattern()).getName() + ", " + privKey.getName() + ")");
                            }
                            if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                AvatarAttribute testNonce = new AvatarAttribute("testnonce_" + ae.getSecurityPattern().getNonce(), AvatarType.INTEGER,
                                        block, null);
                                block.addAttribute(testNonce);
                                tran.addAction("get2(" + decryptedMsg.getName() + "," + decryptedMsg.getName() + ", " + testNonce.getName() + ")");
                                AvatarState guardState = new AvatarState(reworkStringName(ae.getName()) + "_guarded", ae.getReferenceObject(), block);
                                tran.addNext(guardState);
                                tran = new AvatarTransition(block, "__guard_" + ae.getName(), ae.getReferenceObject());
                                elementList.add(guardState);
                                elementList.add(tran);
                                guardState.addNext(tran);
                                tran.setGuard(testNonce.getName() + "==" + ae.getSecurityPattern().getNonce());
                            }
                            AvatarState dummy = new AvatarState(reworkStringName(ae.getName()) + "_dummy", ae.getReferenceObject(), block);
                            tran.addNext(dummy);
                            tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                            dummy.addNext(tran);
                            elementList.add(dummy);
                            elementList.add(tran);
                            ae.getSecurityPattern().setState2(dummy);
                            if (block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()) != null) {
                                AvatarAttributeState authDest = new AvatarAttributeState(block.getName() + "." + dummy.getName() + "." + ae.getSecurityPattern().getName(), ae.getReferenceObject(), block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()), dummy);
                                if (signalAuthDestMap.containsKey(ae.getSecurityPattern().getName())) {
                                    signalAuthDestMap.get(ae.getSecurityPattern().getName()).add(authDest);
                                } else {
                                    LinkedList<AvatarAttributeState> tmp = new LinkedList<>();
                                    tmp.add(authDest);
                                    signalAuthDestMap.put(ae.getSecurityPattern().getName(), tmp);
                                }
                            }
                            break;
                        }
                        case SecurityPattern.MAC_PATTERN: {
                            //Separate MAC from MSG

                            //Add get2 method
                            AvatarAttribute mac = new AvatarAttribute(ae.getSecurityPattern().getName() + "_mac", AvatarType.INTEGER, block, null);
                            block.addAttribute(mac);
                            AvatarAttribute decryptedMsg = new AvatarAttribute(ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                            block.addAttribute(decryptedMsg);
                            AvatarAttribute key = new AvatarAttribute("key_" + ae.getSecurityPattern().getName(), AvatarType.INTEGER, block, null);
                            block.addAttribute(key);
                            tran.addAction("get2(" + secPatternDecAttribute.get(ae.getSecurityPattern()).getName() + ", " + decryptedMsg.getName() + ", " + mac.getName() + ")");
                            AvatarAttribute testNonceTest = new AvatarAttribute("testnonce_" + ae.getSecurityPattern().getName(), AvatarType.BOOLEAN,
                                    block, null);
                            block.addAttribute(testNonceTest);
                            tran.addAction(testNonceTest.getName() + " = verifyMAC(" + decryptedMsg.getName() + ", " + key.getName() + ", " + mac.getName() + ")");

                            AvatarAttribute testNonce = null;
                            if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                testNonce = new AvatarAttribute("testnonce_" + ae.getSecurityPattern().getNonce(), AvatarType.INTEGER,
                                        block, null);
                                block.addAttribute(testNonce);
                                tran.addAction("get2(" + decryptedMsg.getName() + "," + decryptedMsg.getName() + "," + testNonce.getName() + ")");
                            }

                            AvatarState guardState = new AvatarState(reworkStringName(ae.getName()) + "_guarded", ae.getReferenceObject(), block);
                            tran.addNext(guardState);
                            tran = new AvatarTransition(block, "__guard_" + ae.getName(), ae.getReferenceObject());
                            elementList.add(guardState);
                            elementList.add(tran);
                            guardState.addNext(tran);
                            tran.setGuard(testNonceTest.getName());

                            if (!ae.getSecurityPattern().getNonce().isEmpty()) {
                                //Add extra state and transition
                                AvatarState guardState2 = new AvatarState(reworkStringName(ae.getName()) + "_guarded2", ae.getReferenceObject(), block);
                                tran.addNext(guardState2);
                                tran = new AvatarTransition(block, "__guard_" + ae.getName(), ae.getReferenceObject());
                                tran.setGuard(testNonce.getName() + " == " + ae.getSecurityPattern().getNonce());
                                elementList.add(guardState2);
                                elementList.add(tran);

                                guardState2.addNext(tran);
                            }
                            AvatarState dummy = new AvatarState(reworkStringName(ae.getName()) + "_dummy", ae.getReferenceObject(), block);
                            ae.getSecurityPattern().setState2(dummy);
                            tran.addNext(dummy);
                            elementList.add(tran);
                            tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                            dummy.addNext(tran);
                            elementList.add(dummy);
                            elementList.add(tran);

                            if (block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()) != null) {
                                AvatarAttributeState authDest = new AvatarAttributeState(block.getName() + "." + dummy.getName() + "." + ae.getSecurityPattern().getName(), ae.getReferenceObject(), block.getAvatarAttributeWithName(ae.getSecurityPattern().getName()), dummy);
                                if (signalAuthDestMap.containsKey(ae.getSecurityPattern().getName())) {
                                    signalAuthDestMap.get(ae.getSecurityPattern().getName()).add(authDest);
                                } else {
                                    LinkedList<AvatarAttributeState> tmp = new LinkedList<>();
                                    tmp.add(authDest);
                                    signalAuthDestMap.put(ae.getSecurityPattern().getName(), tmp);
                                }
                            }
                            break;
                        }
                    }


                    //Can't decrypt hash or nonce
                }
            } else {
                //Translate state without security
                if (ae instanceof TMLActionState) {
                    TMLActionState actionsState = (TMLActionState) ae;
                    String[] actions = actionsState.getAction().split("\\$");
                    boolean isReqAction = false;
                    for (String action : actions) {
                        action = action.replaceAll(" ", "");
                        String[] terms = action.split("=");
                        if (terms.length == 2) {
                            if (terms[1].matches("arg" + "\\d+" + "__req")) {
                                isReqAction = true;
                            }
                        }
                    }
                    if (!isReqAction) {
                        String val = actionsState.getAction();
                        tran.addAction(reworkActionString(val));
                    }
                } else if (ae instanceof TMLExecI) {
                    tran.setDelays(reworkActionString(((TMLExecI) (ae)).getAction()), reworkActionString(((TMLExecI) (ae)).getAction()));
                } else if (ae instanceof TMLExecC) {
                    tran.setDelays(reworkActionString(((TMLExecC) (ae)).getAction()), reworkActionString(((TMLExecC) (ae)).getAction()));
                }
            }
        } else if (ae instanceof TMLActivityElementWithIntervalAction) {
            AvatarState as = new AvatarState(reworkStringName(ae.getName()), ae.getReferenceObject(), block);
            tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
            tran.setDelays(reworkActionString(((TMLActivityElementWithIntervalAction) (ae)).getMinDelay()),
                    reworkActionString(((TMLActivityElementWithIntervalAction) (ae)).getMaxDelay()));
            as.addNext(tran);
            elementList.add(as);
            elementList.add(tran);

            // Channels
        } else if (ae instanceof TMLActivityElementChannel) {
            TMLActivityElementChannel aec = (TMLActivityElementChannel) ae;
            TMLChannel ch = aec.getChannel(0);
            AvatarSignal sig;
            boolean checkAcc = ae.hasCheckableAccessibility();
            boolean checked =  ae.hasCheckedAccessibility();
            AvatarState signalState = new AvatarState("signalstate_" + reworkStringName(ae.getName()) + "_" +
                    ch.getName(), ae.getReferenceObject(), block, checkAcc, checked);
            AvatarTransition signalTran = new AvatarTransition(block, "__after_signalstate_" + ae.getName() + "_" + ch.getName(),
                    ae.getReferenceObject());

            if (ae instanceof TMLReadChannel) {
                // Read channel
                // Create signal if it does not already exist
                TraceManager.addDev("InMap  Looking for signal: " + ch.getName());
                if (!signalInMap.containsKey(ch.getName())) {
                    TraceManager.addDev("Not in InMap. Creating " + getName(ch.getName()));
                    sig = new AvatarSignal(getName(ch.getName()), AvatarSignal.IN, ch.getReferenceObject());
                    signals.add(sig);
                    block.addSignal(sig);
                    signalInMap.put(ch.getName(), sig);
                    AvatarAttribute channelData = new AvatarAttribute(getName(ch.getName()) + "_chData", AvatarType.INTEGER, block, null);
                    sig.addParameter(channelData);
                } else {
                    sig = signalInMap.get(ch.getName());
                }
                TraceManager.addDev("InMap sig= " + sig.getSignalName());
                AvatarActionOnSignal as = new AvatarActionOnSignal(ae.getName(), sig, ae.getReferenceObject(), block);
                AvatarAttribute channelData;
                String action = "";
                if (ae.getSecurityPattern() != null) {
                    //If nonce
                    if (ae.getSecurityPattern().getType().equals(SecurityPattern.NONCE_PATTERN)) {
                        String attributeName = ae.getSecurityPattern().getName();
                        channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                        block.addAttribute(channelData);
                    }
                    //Send the encrypted key
                    else if (!ae.getSecurityPattern().getKey().isEmpty()) {
                        if (secPatternDecAttribute.containsKey(ae.getSecurityPattern())) {
                            String attributeName = setNewAttributeName(secPatternDecAttribute.get(ae.getSecurityPattern()).getName(), block);
                            channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                            if (block.getAvatarAttributeWithName(secPatternDecAttribute.get(ae.getSecurityPattern()).getName()) == null) {
                                AvatarAttribute encryptedKey = new AvatarAttribute(secPatternDecAttribute.get(ae.getSecurityPattern()).getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(encryptedKey);
                            }
                            action =  secPatternDecAttribute.get(ae.getSecurityPattern()).getName() + " = " + channelData.getName();
                        } else {
                            String attributeName = setNewAttributeName(getName(ch.getName()) + "_chData", block);
                            channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                        }
                    } else {
                        //Send the encrypted data
                        if (secPatternDecAttribute.containsKey(ae.getSecurityPattern())) {
                            String attributeName = setNewAttributeName(secPatternDecAttribute.get(ae.getSecurityPattern()).getName(), block);
                            channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                            if (block.getAvatarAttributeWithName(secPatternDecAttribute.get(ae.getSecurityPattern()).getName()) == null) {
                                AvatarAttribute encrypted = new AvatarAttribute(secPatternDecAttribute.get(ae.getSecurityPattern()).getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(encrypted);
                            }
                            action = secPatternDecAttribute.get(ae.getSecurityPattern()).getName() + " = " + channelData.getName();
                        } else {
                            String attributeName = setNewAttributeName(getName(ch.getName()) + "_chData", block);
                            channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                        }
                    }
                } else {
                    String attributeName = setNewAttributeName(getName(ch.getName()) + "_chData", block);
                    channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                    block.addAttribute(channelData);
                }
                as.addValue(channelData.getName());

                tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                if (!action.isEmpty()) {
                    tran.addAction(action);
                }
                elementList.add(signalState);
                signalState.addNext(signalTran);
                elementList.add(signalTran);
                signalTran.addNext(as);
                as.addNext(tran);
                elementList.add(as);
                elementList.add(tran);
                if (ch.checkAuth) {
                    //Add aftersignal state
                    AvatarState afterSignalState = new AvatarState("aftersignalstate_" + reworkStringName(ae.getName()) +
                            "_" + ch.getName(), ae.getReferenceObject(), block);
                    tran.addNext(afterSignalState);
                    tran = new AvatarTransition(block, "__aftersignalstate_" + ae.getName(), ae.getReferenceObject());
                    afterSignalState.addNext(tran);
                    elementList.add(afterSignalState);
                    elementList.add(tran);
                    boolean foundDecrytionOp = false;
                    List<TMLActivityElement> allNextForAe = new ArrayList<>(ae.getNexts());
                    if (ae.getSecurityPattern() != null) {
                        while (!allNextForAe.isEmpty()) {
                            TMLActivityElement nextAe = allNextForAe.get(0);
                            allNextForAe.addAll(nextAe.getNexts());
                            if (security && nextAe.getSecurityPattern() != null) {
                                if (nextAe instanceof TMLExecC) {
                                    if (((TMLExecC) nextAe).isDecryptionProcess()) {
                                        if (nextAe.getSecurityPattern().equalsSpec(ae.getSecurityPattern())) {
                                            if (!channelsSecAttributes.containsKey(nextAe)) {
                                                Set<String> tmp = new HashSet<>();
                                                channelsSecAttributes.put(nextAe, tmp);
                                            }
                                            channelsSecAttributes.get(nextAe).add(channelData.getName());
                                            channelsSecAttributes.get(nextAe).add(ae.getSecurityPattern().getName());
                                            foundDecrytionOp = true;
                                        }
                                    }
                                }
                            }
                            allNextForAe.remove(nextAe);
                        }
                    }
                    if (!foundDecrytionOp) {
                        if (!channelsSecAttributes.containsKey(ae)) {
                            Set<String> tmp = new HashSet<>();
                            channelsSecAttributes.put(ae, tmp);
                        }
                        channelsSecAttributes.get(ae).add(channelData.getName());
                        if (ae.getSecurityPattern() != null) {
                            channelsSecAttributes.get(ae).add(ae.getSecurityPattern().getName());
                        }
                        /*if (block.getAvatarAttributeWithName(getName(ch.getName()) + "_chData") == null) {
                            AvatarAttribute channelData = new AvatarAttribute(getName(ch.getName()) + "_chData", AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                        }*/
                        AvatarAttributeState authDest = new AvatarAttributeState(block.getName() + "." + reworkStringName(afterSignalState.getName()) + "." +
                                channelData.getName(), ae.getReferenceObject(), block.getAvatarAttributeWithName(channelData.getName()), afterSignalState);
                        if (signalAuthDestMap.containsKey(ch.getName())) {
                            signalAuthDestMap.get(ch.getName()).add(authDest);
                        } else {
                            LinkedList<AvatarAttributeState> tmp = new LinkedList<>();
                            tmp.add(authDest);
                            signalAuthDestMap.put(ch.getName(), tmp);
                        }
                    }
                }

            } else {

                // Write Channel
                TraceManager.addDev("OutMap  Looking for signal: " + ch.getName());
                if (!signalOutMap.containsKey(ch.getName())) {
                    TraceManager.addDev("Not in OutMap. Creating " + getName(ch.getName()));
                    //Add signal if it does not exist
                    sig = new AvatarSignal(getName(ch.getName()), AvatarSignal.OUT, ch.getReferenceObject());
                    signals.add(sig);
                    block.addSignal(sig);
                    signalOutMap.put(ch.getName(), sig);
                    AvatarAttribute channelData = new AvatarAttribute(getName(ch.getName()) + "_chData", AvatarType.INTEGER, block, null);
                    sig.addParameter(channelData);
                } else {
                    sig = signalOutMap.get(ch.getName());
                }
                TraceManager.addDev("OutMap sig= " + sig.getSignalName());

                AvatarAttribute channelData;
                AvatarActionOnSignal as = new AvatarActionOnSignal(ae.getName(), sig, ae.getReferenceObject(), block);

                if (ae.getSecurityPattern() != null) {
                    //send nonce
                    if (ae.getSecurityPattern().getType().equals(SecurityPattern.NONCE_PATTERN)) {
                        String attributeName = ae.getSecurityPattern().getName();
                        channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                        block.addAttribute(channelData);
                    } else {
                        //send encrypted data
                        if (secPatternEncAttribute.containsKey(ae.getSecurityPattern())) {
                            String attributeName = setNewAttributeName(secPatternEncAttribute.get(ae.getSecurityPattern()).getName(), block);
                            channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                            if (block.getAvatarAttributeWithName(secPatternEncAttribute.get(ae.getSecurityPattern()).getName()) == null) {
                                AvatarAttribute encrypted = new AvatarAttribute(secPatternEncAttribute.get(ae.getSecurityPattern()).getName(), AvatarType.INTEGER, block, null);
                                block.addAttribute(encrypted);
                            }
                        } else {
                            String attributeName = setNewAttributeName(getName(ch.getName()) + "_chData", block);
                            channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                        }
                    }
                } else {
                    //No security pattern
                    //	TraceManager.addDev("no security pattern for " + ch.getName());
                    String attributeName = setNewAttributeName(getName(ch.getName()) + "_chData", block);
                    channelData = new AvatarAttribute(attributeName, AvatarType.INTEGER, block, null);
                    block.addAttribute(channelData);
                }
                as.addValue(channelData.getName());
                //Add the confidentiality pragma for this channel data
                if (ch.checkConf) {
                    if (!ch.originalOriginTasks.isEmpty() && ch.getOriginPort().getName().contains("PORTORIGIN")) {
                        //	System.out.println("Channel " + ch.getOriginPort().getName() + " block " + block.getName());
                        for (TMLTask origTask : ch.originalOriginTasks) {
                            AvatarBlock bl = avspec.getBlockWithName(origTask.getName().split("__")[origTask.getName().split("__").length - 1]);
                            if (bl != null) {
                                AvatarAttribute attr = bl.getAvatarAttributeWithName(channelData.getName());
                                if (attr != null) {
                                    AvatarPragmaSecret avPragmaSecret =
                                            new AvatarPragmaSecret("#Confidentiality " + bl.getName() + "." + channelData.getName(),
                                        ch.getReferenceObject(), attr);
                                    if (!secChannelMap.containsKey(ch)) {
                                        Set<AvatarPragma> tmp = new HashSet<>();
                                        secChannelMap.put(ch, tmp);
                                    }
                                    if (!confPragmaMap.containsKey((TMLWriteChannel) ae)) {
                                        Set<AvatarPragma> tmp = new HashSet<>();
                                        confPragmaMap.put((TMLWriteChannel) ae, tmp);
                                    }
                                    boolean isAvSecretInSet = false;
                                    for (AvatarPragma av : secChannelMap.get(ch)) {
                                        if (av.getName().equals(avPragmaSecret.getName())) {
                                            isAvSecretInSet = true;
                                            confPragmaMap.get((TMLWriteChannel) ae).add(av);
                                            break;
                                        }
                                    }
                                    if (!isAvSecretInSet) {
                                        secChannelMap.get(ch).add(avPragmaSecret);
                                        confPragmaMap.get((TMLWriteChannel) ae).add(avPragmaSecret);
                                    }
                                }
                            }
                        }
                    } else {
                        AvatarAttribute attr = block.getAvatarAttributeWithName(channelData.getName());
                        if (attr != null) {
                            AvatarPragmaSecret avPragmaSecret =
                                    new AvatarPragmaSecret("#Confidentiality " + block.getName() + "." + channelData.getName(),
                                            ch.getReferenceObject(), attr);
                            if (!secChannelMap.containsKey(ch)) {
                                Set<AvatarPragma> tmp = new HashSet<>();
                                secChannelMap.put(ch, tmp);
                            }
                            if (!confPragmaMap.containsKey((TMLWriteChannel) ae)) {
                                Set<AvatarPragma> tmp = new HashSet<>();
                                confPragmaMap.put((TMLWriteChannel) ae, tmp);
                            }
                            boolean isAvSecretInSet = false;
                            for (AvatarPragma av : secChannelMap.get(ch)) {
                                if (av.getName().equals(avPragmaSecret.getName())) {
                                    isAvSecretInSet = true;
                                    confPragmaMap.get((TMLWriteChannel) ae).add(av);
                                    break;
                                }
                            }
                            if (!isAvSecretInSet) {
                                secChannelMap.get(ch).add(avPragmaSecret);
                                confPragmaMap.get((TMLWriteChannel) ae).add(avPragmaSecret);
                            }
                        }
                    }
                }

                // Add the authenticity pragma for this channel data
                // To be removed in case another authenticity pragma is used on the channel
                // Also, to be duplicated for each send / receive
                boolean foundEncrytionOp = false;
                List<TMLActivityElement> allPrevOfAe = new ArrayList<>();
                TMLActivity actDiagram = ch.getOriginTask().getActivityDiagram();
                if (actDiagram.getPrevious(ae) != null) {
                    allPrevOfAe.add(actDiagram.getPrevious(ae));
                }
                if (ae.getSecurityPattern() != null) {
                    while (!allPrevOfAe.isEmpty()) {
                        TMLActivityElement prevAe = allPrevOfAe.get(0);
                        if (actDiagram.getPrevious(prevAe) != null) {
                            allPrevOfAe.add(actDiagram.getPrevious(prevAe));
                        }
                        if (security && prevAe.getSecurityPattern() != null) {
                            if (prevAe instanceof TMLExecC) {
                                if (!((TMLExecC) prevAe).isDecryptionProcess()) {
                                    if (prevAe.getSecurityPattern().equalsSpec(ae.getSecurityPattern())) {
                                        foundEncrytionOp = true;
                                    }
                                }
                            }
                        }
                        allPrevOfAe.remove(prevAe);
                    }
                }
                if (secPatternEncAttribute.containsKey(ae.getSecurityPattern())) {
                    signalTran.addAction(channelData.getName() + " = " + secPatternEncAttribute.get(ae.getSecurityPattern()).getName());
                }
                if (ch.checkAuth) {
                    if (!foundEncrytionOp) {
                        /*if (block.getAvatarAttributeWithName(getName(ch.getName()) + "_chData") == null) {
                            AvatarAttribute channelData = new AvatarAttribute(getName(ch.getName()) + "_chData", AvatarType.INTEGER, block, null);
                            block.addAttribute(channelData);
                        }*/
                        AvatarAttributeState authOrigin = new AvatarAttributeState(block.getName() + "." + reworkStringName(signalState.getName()) + "." +
                                channelData.getName(), ae.getReferenceObject(), block.getAvatarAttributeWithName(channelData.getName()), signalState);
                        if (signalAuthOriginMap.containsKey(ch.getName())) {
                            signalAuthOriginMap.get(ch.getName()).add(authOrigin);
                        } else {
                            LinkedList<AvatarAttributeState> tmp = new LinkedList<>();
                            tmp.add(authOrigin);
                            signalAuthOriginMap.put(ch.getName(), tmp);
                        }
                    }
                }



                tran = new AvatarTransition(block, "__after_" + ae.getName(), ae.getReferenceObject());
                elementList.add(signalState);
                signalState.addNext(signalTran);
                elementList.add(signalTran);
                signalTran.addNext(as);
                as.addNext(tran);
                elementList.add(as);
                elementList.add(tran);
            }
        } else if (ae instanceof TMLForLoop) {
            TMLForLoop loop = (TMLForLoop) ae;
            if (loop.isInfinite()) {
                //Make initializaton, then choice state with transitions
                List<AvatarStateMachineElement> elements = translateState(ae.getNextElement(0), block);
                /*List<AvatarStateMachineElement> afterloop =*/
                translateState(ae.getNextElement(1), block);
                AvatarState initState = new AvatarState(reworkStringName(ae.getName()) + "__init", ae.getReferenceObject(), block);
                elementList.add(initState);
                //Build transition to choice
                tran = new AvatarTransition(block, "loop_init__" + ae.getName(), ae.getReferenceObject());
                tran.addAction(AvatarTerm.createActionFromString(block, "loop_index=0"));
                elementList.add(tran);
                initState.addNext(tran);
                //Choice state
                AvatarState as = new AvatarState(reworkStringName(ae.getName()) + "__choice", ae.getReferenceObject(), block);
                elementList.add(as);
                tran.addNext(as);
                //transition to first element of loop
                tran = new AvatarTransition(block, "loop_increment__" + ae.getName(), ae.getReferenceObject());
                //Set default loop limit guard
                tran.setGuard(AvatarGuard.createFromString(block, "loop_index != " + loopLimit));
                tran.addAction(AvatarTerm.createActionFromString(block, "loop_index = loop_index + 1"));
                tran.addNext(elements.get(0));
                as.addNext(tran);
                elementList.add(tran);
                //Process elements in loop to remove stop states and empty transitions, and loop back to choice
                for (AvatarStateMachineElement e : elements) {
                    if (e instanceof AvatarStopState) {
                    } else if (e.getNexts().isEmpty()) {
                        if (e instanceof AvatarTransition) {
                            e.addNext(as);
                            elementList.add(e);
                        }
                    } else if (e.getNext(0) instanceof AvatarStopState) {
                        //Remove the transition to AvatarStopState
                        e.removeNext(0);
                        e.addNext(as);
                        elementList.add(e);
                    } else {
                        elementList.add(e);
                    }
                }

                //Transition if exiting loop
                tran = new AvatarTransition(block, "end_loop__" + ae.getName(), ae.getReferenceObject());
                tran.setGuard(new AvatarGuardElse());
                as.addNext(tran);
                AvatarStopState stop = new AvatarStopState("stop", null, block);
                tran.addNext(stop);
                elementList.add(tran);
                elementList.add(stop);
                return elementList;
            } else {
                //Make initializaton, then choice state with transitions
                List<AvatarStateMachineElement> elements = translateState(ae.getNextElement(0), block);
                List<AvatarStateMachineElement> afterloop = translateState(ae.getNextElement(1), block);
                AvatarState initState = new AvatarState(reworkStringName(ae.getName()) + "__init", ae.getReferenceObject(), block);
                elementList.add(initState);
                //Build transition to choice
                tran = new AvatarTransition(block, "loop_init__" + ae.getName(), ae.getReferenceObject());
                tran.addAction(AvatarTerm.createActionFromString(block, loop.getInit()));
                tran.addAction(AvatarTerm.createActionFromString(block, "loop_index=0"));
                elementList.add(tran);
                initState.addNext(tran);
                //Choice state
                AvatarState as = new AvatarState(reworkStringName(ae.getName()) + "__choice", ae.getReferenceObject(), block);
                elementList.add(as);
                tran.addNext(as);
                //transition to first element of loop
                tran = new AvatarTransition(block, "loop_increment__" + ae.getName(), ae.getReferenceObject());
                //Set default loop limit guard
                tran.setGuard(AvatarGuard.createFromString(block, "loop_index != " + loopLimit));
                /*AvatarGuard guard = */
                AvatarGuard.createFromString(block, loop.getCondition().replaceAll("<", "!="));
                int error = AvatarSyntaxChecker.isAValidGuard(avspec, block, loop.getCondition().replaceAll("<", "!="));
                if (error != 0) {
                    tran.addGuard(loop.getCondition().replaceAll("<", "!="), "");
                }
                tran.addAction(AvatarTerm.createActionFromString(block, loop.getIncrement()));
                tran.addAction(AvatarTerm.createActionFromString(block, "loop_index = loop_index + 1"));
                if (!elements.isEmpty()) {
                    tran.addNext(elements.get(0));
                    as.addNext(tran);
                    elementList.add(tran);
                }
                //Process elements in loop to remove stop states and empty transitions, and loop back to choice
                for (AvatarStateMachineElement e : elements) {
                    if (e instanceof AvatarStopState) {
                    } else if (e.getNexts().isEmpty()) {
                        e.addNext(as);
                        elementList.add(e);
                    } else if (e.getNext(0) instanceof AvatarStopState) {
                        //Remove the transition to AvatarStopState
                        e.removeNext(0);
                        e.addNext(as);
                        elementList.add(e);
                    } else {
                        elementList.add(e);
                    }
                }

                //Transition if exiting loop
                tran = new AvatarTransition(block, "end_loop__" + ae.getName(), ae.getReferenceObject());
                tran.setGuard(new AvatarGuardElse());
                as.addNext(tran);
                if (afterloop.isEmpty()) {
                    afterloop.add(new AvatarStopState("stop", null, block));
                }
                tran.addNext(afterloop.get(0));
                elementList.add(tran);
                elementList.addAll(afterloop);
                return elementList;
            }
        } else if (ae instanceof TMLChoice) {
            AvatarState as = new AvatarState(reworkStringName(ae.getName()), ae.getReferenceObject(), block);
            //Make many choices
            elementList.add(as);
            TMLChoice c = (TMLChoice) ae;
            for (int i = 0; i < c.getNbGuard(); i++) {
                tran = new AvatarTransition(block, "__after_" + ae.getName() + "_" + i, ae.getReferenceObject());
                //tran.setGuard(c.getGuard(i));
                as.addNext(tran);
                List<AvatarStateMachineElement> nexts = translateState(ae.getNextElement(i), block);
                if (!nexts.isEmpty()) {
                    tran.addNext(nexts.get(0));
                    elementList.add(tran);
                    elementList.addAll(nexts);
                }
            }
            return elementList;
        } else if (ae instanceof TMLSelectEvt) {
            AvatarState as = new AvatarState(reworkStringName(ae.getName()), ae.getReferenceObject(), block);
            elementList.add(as);
            //Make many choices
            //TMLSelectEvt c = (TMLSelectEvt) ae;
            for (int i = 0; i < ae.getNbNext(); i++) {
                tran = new AvatarTransition(block, "__after_" + ae.getName() + "_" + i, ae.getReferenceObject());
                as.addNext(tran);
                List<AvatarStateMachineElement> nexts = translateState(ae.getNextElement(i), block);
                tran.addNext(nexts.get(0));
                elementList.add(tran);
                elementList.addAll(nexts);
            }
            return elementList;
        } else {
            TraceManager.addDev("undefined tml element " + ae);
        }
        List<AvatarStateMachineElement> nexts = translateState(ae.getNextElement(0), block);
        if (nexts.isEmpty()) {
            //in an infinite loop i hope
            return elementList;
        }
        tran.addNext(nexts.get(0));
        elementList.addAll(nexts);
        return elementList;
    }

    private String processName(String name, int id) {
        name = reworkStringName(name).replaceAll("-", "_");
        if (allStates.contains(name)) {
            allStates.add(name + id);
            return name + id;

        } else {
            allStates.add(name);
            return name;
        }
    }
		/*	public AvatarPragma generatePragma(String[] s){

			}*/

    public String getName(String s) {
        //	System.out.println("String " + s);
        if (nameMap.containsKey(s)) {
            return nameMap.get(s);
        } else {
            if (!s.contains("__")) {
                nameMap.put(s, s);
                return s;
            } else if (s.split("__").length == 1 || s.split("__").length == 2 || s.split("__").length == 3 || s.split("__").length == 4) {
                nameMap.put(s, s.split("__")[s.split("__").length - 1]);
                return s.split("__")[s.split("__").length - 1];
            } else if (s.contains("JOIN") || s.contains("FORK")) {
                String t = "";
                t += s.split("__")[0];
                for (int i = 2; i < s.split("__").length; i++) {
                    t += "JOIN" + s.split("__")[i];
                }
                nameMap.put(s, t);
                return t;
            } else {
	       /*     String t = "";
                for (int i = 0; i < s.split("__").length; i++) {
                    t += s.split("__")[i];
                }*/
                nameMap.put(s, s);
                return s;
                // nameMap.put(s, s.split("__")[s.split("__").length - 1]);
                // return s.split("__")[s.split("__").length - 1];
            }
        }
    }

    public AvatarSpecification generateAvatarSpec(String _loopLimit) {

        TraceManager.addDev("security patterns " + tmlmodel.getSecPatterns());
        TraceManager.addDev("keys " + tmlmap.getMappedSecurity());


        //TODO: Make state names readable
        //TODO: Put back numeric guards
        //TODO: Calculate for temp variable

        // Reset the ID to ensure IDs are the same as those of the expected specifications
        AvatarElement.resetID();
        if (tmlmap.getTMLModeling() != null) {
            if (tmlmap.getTMLModeling().getReference() != null) {
                this.avspec = new AvatarSpecification("spec", referenceObject);
            } else {
                this.avspec = new AvatarSpecification("spec", null);
            }
        }
        tmlmodel.removeForksAndJoins();

//        System.out.println("MODIFIED model " + tmlmodel);

        for (TMLChannel chan : tmlmodel.getChannels()) {
            //System.out.println("chan " + chan);
            TMLTask task = chan.getOriginTask();

            TMLTask task2 = chan.getDestinationTask();
            HwExecutionNode node = tmlmap.getHwNodeOf(task);
            HwExecutionNode node2 = tmlmap.getHwNodeOf(task2);
            if (node == null) {
                tmlmap.addTaskToHwExecutionNode(task, node2);
            }

            if (node2 == null) {
                tmlmap.addTaskToHwExecutionNode(task2, node);
            }

            if (chan.getName().contains("fork__") || chan.getName().contains("FORKCHANNEL")) {
                chan.setName(chan.getName().replaceAll("__", ""));
            }

        }

        //Only set the loop limit if it's a number
        String pattern = "^[0-9]{1,2}$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(_loopLimit);
        if (m.find()) {
            loopLimit = Integer.parseInt(_loopLimit);
        }
        for (TMLChannel channel : tmlmodel.getChannels()) {
            TraceManager.addDev("Checking auth of channel " + channel.getName() + ": " + channel.isCheckAuthChannel());
            channel.checkAuth = channel.isCheckAuthChannel();
            for (TMLPortWithSecurityInformation p : channel.ports) {
                channel.checkConf = channel.checkConf || p.getCheckConf();
            }
        }

        AvatarBlock top = new AvatarBlock("TOP__TOP", avspec, null);
        if (mc) {
            avspec.addBlock(top);
            AvatarStateMachine topasm = top.getStateMachine();
            AvatarStartState topss = new AvatarStartState("start", null, topasm.getOwner());
            topasm.setStartState(topss);
            topasm.addElement(topss);
        }

        List<TMLTask> tasks = tmlmap.getTMLModeling().getTasks();

        for (TMLTask task : tasks) {
            AvatarBlock block = new AvatarBlock(task.getName().split("__")[task.getName().split("__").length - 1], avspec, task.getReferenceObject());
            if (mc) {
                block.setFather(top);
            }
            taskBlockMap.put(task, block);
            avspec.addBlock(block);
        }

        checkConnections();
        //	checkChannels();

        distributeKeys();
        createSecElements(tasks);

        TraceManager.addDev("ALL KEYS " + accessKeys);
			/*for (TMLTask t: accessKeys.keySet()){
				TraceManager.addDev("TASK " +t.getName());
				for (SecurityPattern sp: accessKeys.get(t)){
					TraceManager.addDev(sp.name);
				}
			}*/

        for (TMLTask task : tasks) {
            AvatarBlock block = taskBlockMap.get(task);
            // Add temp variable for unsendable signals

            // Add all channel signals
            for (TMLChannel chan : tmlmodel.getChannels(task)) {

                if (chan.hasOriginTask(task)) {
                    if (chan.getOriginPort() == null) {
                        TraceManager.addDev("NULL PORT in chan " + chan.getName());
                    } else if (chan.getOriginPort().getName() == null) {
                        TraceManager.addDev("NULL PORT NAME" + chan.getName());
                    }
                    AvatarAttribute channelData = new AvatarAttribute(getName(chan.getName()) + "_chData", AvatarType.INTEGER, block,
                            null);
                    AvatarSignal sig = new AvatarSignal(getName(chan.getName()), AvatarSignal.OUT, chan.getReferenceObject());

                    block.addSignal(sig);
                    signals.add(sig);

                    /*if (block.getAvatarAttributeWithName(getName(chan.getOriginPort().getName()) + "_chData") == null) {
                        block.addAttribute(channelData);
                    }*/
                    sig.addParameter(channelData);
                    signalOutMap.put(chan.getName(), sig);

                } else if (chan.hasDestinationTask(task)) {
                    AvatarSignal sig = new AvatarSignal(getName(chan.getName()), AvatarSignal.IN, chan.getReferenceObject());
                    block.addSignal(sig);
                    signals.add(sig);
                    signalInMap.put(chan.getName(), sig);
                    AvatarAttribute channelData = new AvatarAttribute(getName(chan.getName()) + "_chData", AvatarType.INTEGER,
                            block, null);
                    /*if (block.getAvatarAttributeWithName(getName(chan.getDestinationPort().getName()) + "_chData") == null) {
                        block.addAttribute(channelData);
                    }*/
                    sig.addParameter(channelData);
                }
            }

            // Add all event signals
            for (TMLEvent evt : tmlmodel.getEvents(task)) {
                //TraceManager.addDev("Handling evt: " + evt.getName() + " in task " + task.getName());
                if (evt.hasOriginTask(task)) {
                    String name = getNameReworked(evt.getName(), 1) + "_out";
                    TraceManager.addDev("Adding OUT evt:" + name);
                    AvatarSignal sig = new AvatarSignal(name, AvatarSignal.OUT, evt.getReferenceObject());
                    block.addSignal(sig);
                    signals.add(sig);
                    //Adding parameter
                    int cpt = 0;
                    for (TMLType tmlt : evt.getParams()) {
                        AvatarAttribute aa = new AvatarAttribute("p" + cpt, getAvatarType(tmlt), null, null);
                        sig.addParameter(aa);
                        cpt++;
                    }
                    signalOutMap.put(evt.getName(), sig);

                }

                if (evt.hasDestinationTask(task)) {
                    String name = getNameReworked(evt.getName(), 3) + "_in";
                    TraceManager.addDev("Adding IN evt:" + name);
                    AvatarSignal sig = new AvatarSignal(name, AvatarSignal.IN, evt.getReferenceObject());
                    block.addSignal(sig);
                    signals.add(sig);
                    signalInMap.put(evt.getName(), sig);

                    //Adding parameter
                    int cpt = 0;
                    for (TMLType tmlt : evt.getParams()) {
                        AvatarAttribute aa = new AvatarAttribute("p" + cpt, getAvatarType(tmlt), null, null);
                        sig.addParameter(aa);
                        cpt++;
                    }/*

                    name = getNameReworked(evt.getName(), 3) + NOTIFIED;
                    sig = block.addSignalIfApplicable(name, AvatarSignal.IN, evt.getReferenceObject());
                    signalInMap.put(evt.getName() + NOTIFIED, sig);

                    //Adding parameter
                    AvatarAttribute aa = new AvatarAttribute("p" + cpt, AvatarType.INTEGER, null, null);
                    sig.addParameter(aa); */

                }
            }

            AvatarAttribute tmp = new AvatarAttribute("tmp", AvatarType.INTEGER, block, null);
            block.addAttribute(tmp);

				/*   tmp = new AvatarAttribute("aliceandbob", AvatarType.INTEGER, block, null);
				     block.addAttribute(tmp);
				     tmp = new AvatarAttribute("aliceandbob_encrypted", AvatarType.INTEGER, block, null);
				     block.addAttribute(tmp);*/
            AvatarAttribute loop_index = new AvatarAttribute("loop_index", AvatarType.INTEGER, block, null);
            block.addAttribute(loop_index);
            for (TMLAttribute attr : task.getAttributes()) {
                if (!attr.getName().endsWith("__req")) {
                    AvatarType type;
                    if (attr.getType().getType() == TMLType.NATURAL) {
                        type = AvatarType.INTEGER;
                    } else if (attr.getType().getType() == TMLType.BOOLEAN) {
                        type = AvatarType.BOOLEAN;
                    } else {
                        type = AvatarType.UNDEFINED;
                    }
                    AvatarAttribute avattr = new AvatarAttribute(attr.getName(), type, block, null);
                    avattr.setInitialValue(attr.getInitialValue());
                    block.addAttribute(avattr);
                }
            }
            //AvatarTransition last;
            AvatarStateMachine asm = block.getStateMachine();
            //TODO: Create a fork with many requests. This looks terrible
            allStates.clear();
            if (tmlmodel.getRequestToMe(task) != null) {
                //Create iteration attribute
                AvatarAttribute req_loop_index = new AvatarAttribute("req_loop_index", AvatarType.INTEGER, block, null);
                block.addAttribute(req_loop_index);
                List<String> reqParams = new ArrayList<>();
                for (TMLRequest req : tmlmodel.getRequestsToMe(task)) {
                    for (TMLActivityElement  elements : task.getActivityDiagram().getElements()) {
                        if (elements instanceof TMLActionState) {
                            TMLActionState actionsState = (TMLActionState) elements;
                            String[] actions = actionsState.getAction().split("\\$");
                            for (String action : actions) {
                                action = action.replaceAll(" ", "");
                                String[] terms = action.split("=");
                                if (terms.length == 2) {
                                    if (terms[1].matches("arg" + "\\d+" + "__req")) {
                                        reqParams.add(terms[0]);
                                    }
                                }
                            }
                        }
                    }
                    for (int i = 0; i < req.getNbOfParams(); i++) {
                        if (block.getAvatarAttributeWithName(reqParams.get(i)) == null) {
                            AvatarType type;
                            if (req.getParam(i).matches("-?\\d+")) {
                                type = AvatarType.INTEGER;
                            } else if (req.getParam(i).matches("(?i)^(true|false)")) {
                                type = AvatarType.BOOLEAN;
                            } else {
                                type = AvatarType.UNDEFINED;
                            }
                            String nameNewAtt = "arg"+ (i+1) +"_req";
                            if (block.getAvatarAttributeWithName(nameNewAtt) == null) {
                                AvatarAttribute avattr = new AvatarAttribute(nameNewAtt, type, block, null);
                                avattr.setInitialValue(req.getParam(i));
                                block.addAttribute(avattr);
                            }
                        }
                    }
                }

                //TMLRequest request= tmlmodel.getRequestToMe(task);
                //Oh this is fun...let's restructure the state machine
                //Create own start state, and ignore the returned one
                List<AvatarStateMachineElement> elementList = translateState(task.getActivityDiagram().get(0), block);
                AvatarStartState ss = new AvatarStartState("start", task.getActivityDiagram().get(0).getReferenceObject(), block);
                asm.addElement(ss);
                AvatarTransition at = new AvatarTransition(block, "__after_start", task.getActivityDiagram().get(0).getReferenceObject());
                at.addAction(AvatarTerm.createActionFromString(block, "req_loop_index = 0"));
                ss.addNext(at);
                asm.addElement(at);

                AvatarState loopstart = new AvatarState("loopstart", task.getActivityDiagram().get(0).getReferenceObject(), block);
                at.addNext(loopstart);
                asm.addElement(loopstart);

                //Find the original start state, transition, and next element
                AvatarStateMachineElement start = elementList.get(0);
                AvatarStateMachineElement startTran = start.getNext(0);
                AvatarStateMachineElement newStart = startTran.getNext(0);
                elementList.remove(start);
                elementList.remove(startTran);
                //Find every stop state, remove them, reroute transitions to them
                //For now, route every transition to stop state to remove the loop on requests 

                for (AvatarStateMachineElement e : elementList) {
                    e.setName(processName(e.getName(), e.getID()));
                    stateObjectMap.put(task.getName().split("__")[task.getName().split("__").length-1] + "__" + e.getName(), e.getReferenceObject());

                    if (!(e instanceof AvatarStopState)) {
                        for (int i = 0; i < e.getNexts().size(); i++) {
                            if (e.getNext(i) instanceof AvatarStopState) {
                                e.removeNext(i);
                                //Route it back to the loop start
                                e.addNext(loopstart);
                            }
                        }
                        asm.addElement(e);
                    }
                }

                //Create exit after # of loop iterations is maxed out
                /*AvatarStopState stop =*/
                new AvatarStopState("stop", task.getActivityDiagram().get(0).getReferenceObject(), block);
                /*AvatarTransition exitTran = */
                new AvatarTransition(block, "to_stop", task.getActivityDiagram().get(0).getReferenceObject());


                //Add Requests, direct transition to start of state machine
                for (TMLRequest req : tmlmodel.getRequestsToMe(task)) {
                    AvatarTransition incrTran = new AvatarTransition(block, "__after_loopstart__" + req.getName(), task.getActivityDiagram().get(0).getReferenceObject());
                    incrTran.addAction(AvatarTerm.createActionFromString(block, "req_loop_index = req_loop_index + 1"));
                    incrTran.setGuard(AvatarGuard.createFromString(block, "req_loop_index != " + loopLimit));
                    asm.addElement(incrTran);
                    loopstart.addNext(incrTran);
                    AvatarSignal sig;
                    if (!signalInMap.containsKey(req.getName())) {
                        sig = new AvatarSignal(getName(req.getName()), AvatarSignal.IN, req.getReferenceObject());
                        block.addSignal(sig);
                        signals.add(sig);
                        signalInMap.put(req.getName(), sig);
                        int cpt = 0;
                        for (TMLType tmlt : req.getParams()) {
                            AvatarAttribute aa = new AvatarAttribute("p" + cpt, getAvatarType(tmlt), null, null);
                            sig.addParameter(aa);
                            cpt++;
                        }
                    } else {
                        sig = signalInMap.get(req.getName());
                    }
                    AvatarActionOnSignal as = new AvatarActionOnSignal("getRequest__" + req.getName(), sig, req.getReferenceObject(), block);
                    incrTran.addNext(as);
                    asm.addElement(as);
						/*as.addValue(req.getName()+"__reqData");
						AvatarAttribute requestData= new AvatarAttribute(req.getName()+"__reqData", AvatarType.INTEGER, block, null);
						block.addAttribute(requestData);*/
                    for (int i = 0; i < req.getNbOfParams(); i++) {
                        if (block.getAvatarAttributeWithName(reqParams.get(i)) == null) {
                            String nameNewAtt = "arg"+ (i+1) +"_req";
                            as.addValue(block.getAvatarAttributeWithName(nameNewAtt).getName());
                        } else {
                            //	Add parameter to signal and actiononsignal
                            as.addValue(reqParams.get(i));
                        }
                    }
                    AvatarTransition tran = new AvatarTransition(block, "__after_" + req.getName(), task.getActivityDiagram().get(0).getReferenceObject());
                    as.addNext(tran);
                    asm.addElement(tran);
                    tran.addNext(newStart);
						/*if (req.checkAuth){
							AvatarState afterSignalState = new AvatarState("aftersignalstate_"+req.getName().replaceAll(" ","")+"_"+req.getName().replaceAll(" ",""),req.getReferenceObject());
							AvatarTransition afterSignalTran = new AvatarTransition(block, "__aftersignalstate_"+req.getName(), req.getReferenceObject());
							tran.addNext(afterSignalState);
							afterSignalState.addNext(afterSignalTran);
							asm.addElement(afterSignalState);
							asm.addElement(afterSignalTran);
							afterSignalTran.addNext(newStart);
							AvatarAttributeState authDest = new AvatarAttributeState(block.getName()+"."+afterSignalState.getName()+"."+requestData.getName(),obj,requestData, afterSignalState);
							signalAuthDestMap.put(req.getName(), authDest);
						}  
						else {
							tran.addNext(newStart);
						}*/

                }


                asm.setStartState(ss);

            } else {
                //Not requested
                List<AvatarStateMachineElement> elementList = translateState(task.getActivityDiagram().get(0), block);
                for (AvatarStateMachineElement e : elementList) {
                    e.setName(processName(e.getName(), e.getID()));
                    asm.addElement(e);
                    stateObjectMap.put(task.getName().split("__")[task.getName().split("__").length-1] + "__" + e.getName(), e.getReferenceObject());
                }
                asm.setStartState((AvatarStartState) elementList.get(0));
            }

        }

        for (SecurityPattern secPattern : secPatternPragmaMap.keySet()) {
            for (TMLTask task : tmlmodel.getTasks()) {
                for (TMLChannel ch : tmlmodel.getChannels(task)) {
                    if (ch.hasOriginTask(task) && ch.isCheckConfChannel()) {
                        for (TMLActivityElement actElem : task.getActivityDiagram().getElements()) {
                            if (actElem instanceof TMLWriteChannel) {
                                TMLWriteChannel wc = (TMLWriteChannel) actElem;
                                if (wc.hasChannel(ch) && actElem.getSecurityPattern() != null
                                        && actElem.getSecurityPattern().getName().equals(secPattern.getName())) {
                                    if (!secChannelMap.containsKey(ch)) {
                                        Set<AvatarPragma> tmp0 = new HashSet<>();
                                        secChannelMap.put(ch, tmp0);
                                    }
                                    boolean isAvSecretInSet = false;
                                    for (AvatarPragma av : secChannelMap.get(ch)) {
                                        if (av.getName().equals(secPatternPragmaMap.get(secPattern).getName())) {
                                            isAvSecretInSet = true;
                                            break;
                                        }
                                    }
                                    if (!isAvSecretInSet) {
                                        secChannelMap.get(ch).add(secPatternPragmaMap.get(secPattern));
                                    }

                                    if (!confPragmaMap.containsKey(wc)) {
                                        Set<AvatarPragma> tmp = new HashSet<>();
                                        confPragmaMap.put(wc, tmp);
                                    }
                                    confPragmaMap.get(wc).add(secPatternPragmaMap.get(secPattern));
                                }
                            }
                        }
                    }
                }
            }
        }

        // Add authenticity pragmas
        for (String s : signalAuthOriginMap.keySet()) {
            for (AvatarAttributeState attributeStateOrigin : signalAuthOriginMap.get(s)) {
                if (signalAuthDestMap.containsKey(s)) {
                    for (AvatarAttributeState attributeStateDest : signalAuthDestMap.get(s)) {
                        AvatarPragmaAuthenticity pragma = new AvatarPragmaAuthenticity(
                                "#Authenticity " + attributeStateOrigin.getName() + " " + attributeStateDest.getName(),
                                attributeStateOrigin.getReferenceObject(), attributeStateOrigin, attributeStateDest);
                        TMLChannel ch = tmlmodel.getChannelByShortName(s);
                        if (ch != null && ch.checkAuth) {
                            if (!secChannelMap.containsKey(ch)) {
                                Set<AvatarPragma> tmp0 = new HashSet<>();
                                secChannelMap.put(ch, tmp0);
                            }
                            boolean isAvSecretInSet = false;
                            for (AvatarPragma av : secChannelMap.get(ch)) {
                                if (av.getName().equals(pragma.getName())) {
                                    isAvSecretInSet = true;
                                    break;
                                }
                            }
                            if (!isAvSecretInSet) {
                                secChannelMap.get(ch).add(pragma);
                            }
                            for (TMLTask task : tmlmodel.getTasks()) {
                                if (ch.hasDestinationTask(task) && ch.isCheckAuthChannel()) {
                                    for (TMLActivityElement actElem : task.getActivityDiagram().getElements()) {
                                        if (actElem instanceof TMLReadChannel) {
                                            TMLReadChannel rd = (TMLReadChannel) actElem;
                                            if (rd.hasChannel(ch) && channelsSecAttributes.containsKey(actElem) &&
                                                    channelsSecAttributes.get(actElem).contains(attributeStateDest.getAttribute().getName())) {
                                                if (!authPragmaMap.containsKey(rd)) {
                                                    Set<AvatarPragma> tmp = new HashSet<>();
                                                    authPragmaMap.put(rd, tmp);
                                                }
                                                authPragmaMap.get(rd).add(pragma);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        for (SecurityPattern sec : secPatterns) {
                            if (sec.getName().equals(s)) {
                                for (TMLTask task : tmlmodel.getTasks()) {
                                    for (TMLActivityElement actElem : task.getActivityDiagram().getElements()) {
                                        if (actElem.getSecurityPattern() != null && channelsSecAttributes.containsKey(actElem) &&
                                                channelsSecAttributes.get(actElem).contains(sec.getName())) {
                                            if (!authPragmaMap.containsKey(actElem)) {
                                                Set<AvatarPragma> tmp = new HashSet<>();
                                                authPragmaMap.put(actElem, tmp);
                                            }
                                            authPragmaMap.get(actElem).add(pragma);
                                        }
                                        if (actElem.getSecurityPattern() != null && actElem instanceof TMLActivityElementChannel &&
                                                actElem.getSecurityPattern().equalsSpec(sec)) {
                                            TMLActivityElementChannel actCh =  (TMLActivityElementChannel) actElem;
                                            for (int i = 0; i < actCh.getNbOfChannels(); i++) {
                                                TMLChannel chSec = actCh.getChannel(i);
                                                if (chSec.checkAuth) {
                                                    if (!secChannelMap.containsKey(chSec)) {
                                                        Set<AvatarPragma> tmp0 = new HashSet<>();
                                                        secChannelMap.put(chSec, tmp0);
                                                    }
                                                    boolean isAvSecretInSet = false;
                                                    for (AvatarPragma av : secChannelMap.get(chSec)) {
                                                        if (av.getName().equals(pragma.getName())) {
                                                            isAvSecretInSet = true;
                                                            break;
                                                        }
                                                    }
                                                    if (!isAvSecretInSet) {
                                                        secChannelMap.get(chSec).add(pragma);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        List<AvatarPragma> avPragmas = new ArrayList<>();
        for (TMLChannel ch : secChannelMap.keySet()) {
            for (AvatarPragma avPragma : secChannelMap.get(ch)) {
                boolean isAvSecretInSet = false;
                for (AvatarPragma av : avspec.getPragmas()) {
                    if (av.getName().equals(avPragma.getName())) {
                        isAvSecretInSet = true;
                        break;
                    }
                }
                if (!isAvSecretInSet) {
                    avPragmas.add(avPragma);
                }
            }
        }
        // Put AvatarPragmaAuthenticity pragmas at the end of the list.
        List<AvatarPragma> avPragmasToMove = new ArrayList<>();
        for (AvatarPragma avPragma : avPragmas) {
            if (avPragma instanceof AvatarPragmaAuthenticity) {
                avPragmasToMove.add(avPragma);
            }
        }
        avPragmas.removeAll(avPragmasToMove);
        avPragmas.addAll(avPragmasToMove);
        avspec.getPragmas().addAll(avPragmas);

        //Create relations
        //Channels are ?? to ??
        //Requests are n to 1
        //Events are ?? to ??
        AvatarBlock fifo = new AvatarBlock("FIFO", avspec, null);
        for (TMLChannel channel : tmlmodel.getChannels()) {
				/*if (channel.getName().contains("JOINCHANNEL")){
					//System.out.println("JOINCHANNEL");
					AvatarRelation ar= new AvatarRelation(channel.getName(), taskBlockMap.get(channel.getOriginTask()), taskBlockMap.get(channel.getDestinationTask()), channel.getReferenceObject());
					ar.setPrivate(false);
					if (channel.getType()==TMLChannel.BRBW){
						ar.setAsynchronous(true);		
						ar.setSizeOfFIFO(channel.getSize());
						ar.setBlocking(true);
					}
					else if (channel.getType()==TMLChannel.BRNBW){
						ar.setAsynchronous(true);
						ar.setSizeOfFIFO(channel.getSize());
						ar.setBlocking(false);
					}
					else {
						//Create new block, hope for best
						if (mc){
							fifo = createFifo(channel.getName());
							ar.setAsynchronous(false);
						}
					}
					//System.out.println(channel.getName() + " " +channel.getOriginTask().getName() + " " + channel.getDestinationTask().getName());
					//Find in signal
					//Sig1 contains IN Signals, Sig2 contains OUT signals
					List<AvatarSignal> sig1 = new ArrayList<AvatarSignal>();
					List<AvatarSignal> sig2 = new ArrayList<AvatarSignal>();
					for (AvatarSignal sig: taskBlockMap.get(channel.getDestinationTask()).getSignals()){
						if (sig.getInOut()==AvatarSignal.IN){
							String name = sig.getName();
							String tmp = getName(channel.getName());
							if (name.equals(tmp.split("JOIN")[tmp.split("JOIN").length-1]) || name.equals(tmp)){
								sig1.add(sig);
							}
						}
					}
					for (AvatarSignal sig: taskBlockMap.get(channel.getOriginTask()).getSignals()){
						if (sig.getInOut()==AvatarSignal.OUT){
							String name = sig.getName();
							String tmp = getName(channel.getName());
							if (name.equals(tmp.split("JOIN")[tmp.split("JOIN").length-1]) || name.equals(tmp)){
								sig2.add(sig);
							}
						}
					}

					if (sig1.size()==1 && sig2.size()==1){
						if (channel.getType()==TMLChannel.NBRNBW && mc){
							AvatarSignal read = fifo.getSignalByName("readSignal");

							ar.block2= fifo;
							//Set IN signal with read
							ar.addSignals(sig1.get(0), read);
							AvatarRelation ar2= new AvatarRelation(channel.getName()+"2", fifo, taskBlockMap.get(channel.getDestinationTask()), channel.getReferenceObject());
							AvatarSignal write = fifo.getSignalByName("writeSignal");
							//set OUT signal with write
							ar2.addSignals(write, sig2.get(0));
							ar2.setAsynchronous(false);
							avspec.addRelation(ar2);
						}
						else {
							ar.addSignals(sig2.get(0), sig1.get(0));
						}
					}		
					avspec.addRelation(ar);
				}
		
				else if (channel.getName().contains("FORKCHANNEL") || channel.getName().contains("fork__")){
					System.out.println("FORKCHANNEL " + channel.getName());
					AvatarRelation ar= new AvatarRelation(channel.getName(), taskBlockMap.get(channel.getOriginTask()), taskBlockMap.get(channel.getDestinationTask()), channel.getReferenceObject());
					ar.setPrivate(false);

					//System.out.println(channel.getName() + " " +channel.getOriginTask().getName() + " " + channel.getDestinationTask().getName());
					//Find in signal
					//Sig1 contains IN Signals, Sig2 contains OUT signals
					List<AvatarSignal> sig1 = new ArrayList<AvatarSignal>();
					List<AvatarSignal> sig2 = new ArrayList<AvatarSignal>();
					for (AvatarSignal sig: taskBlockMap.get(channel.getDestinationTask()).getSignals()){
						if (sig.getInOut()==AvatarSignal.IN){
							String name = sig.getName();
							String tmp = getName(channel.getName());
							if (name.equals(tmp.split("FORK")[tmp.split("FORK").length-1]) || name.equals(tmp)){
								sig1.add(sig);
							}
						}
					}
					for (AvatarSignal sig: taskBlockMap.get(channel.getOriginTask()).getSignals()){
						if (sig.getInOut()==AvatarSignal.OUT){
							String name = sig.getName();
							String tmp = getName(channel.getName());
							if (name.equals(tmp.split("FORK")[tmp.split("FORK").length-1]) || name.equals(tmp)){
								sig2.add(sig);
							}
						}
					}

					if (sig1.size()==1 && sig2.size()==1){
						if (channel.getType()==TMLChannel.NBRNBW && mc){
							AvatarSignal read = fifo.getSignalByName("readSignal");

							ar.block2= fifo;
							//Set IN signal with read
							ar.addSignals(sig1.get(0), read);
							AvatarRelation ar2= new AvatarRelation(channel.getName()+"2", fifo, taskBlockMap.get(channel.getDestinationTask()), channel.getReferenceObject());
							AvatarSignal write = fifo.getSignalByName("writeSignal");
							//set OUT signal with write
							ar2.addSignals(write, sig2.get(0));
							ar2.setAsynchronous(false);
							avspec.addRelation(ar2);
						}
						else {
							ar.addSignals(sig2.get(0), sig1.get(0));
						}
					}		
					avspec.addRelation(ar);
				}*/

            if (channel.isBasicChannel()) {
                //TraceManager.addDev("CHAN checking basic channel " + channel.getName());
                AvatarRelation ar = new AvatarRelation(channel.getName(), taskBlockMap.get(channel.getOriginTask()),
                        taskBlockMap.get(channel.getDestinationTask()), channel.getReferenceObject());
                LinkedList<HwCommunicationNode> path = tmlmap.findNodesForElement(channel);
                //TraceManager.addDev("CHAN checking basic channel " + channel.getName() + " path size: " + path.size());
                if (!path.isEmpty()) {
                    ar.setPrivate(true);
                    for (HwCommunicationNode node : path) {
                        //TraceManager.addDev("CHAN\t Element of path: " + node.getName());
                        if (node instanceof HwBus) {
                            if (node.privacy == HwCommunicationNode.BUS_PUBLIC) {
                                ar.setPrivate(false);
                                //TraceManager.addDev("CHAN\t Set as public: " + channel.getName() + "because of " + node.getName());
                                break;
                            }
                        }
                    }
                } else {
                    if (channel.originalOriginTasks.isEmpty()) {
                        ar.setPrivate(originDestMap.get(channel.getOriginTask().getName() + "__" + channel.getDestinationTask().getName()) == 1);
                    } else {
                        //System.out.println("complex channel " + channel.getName());
                        //Find privacy of original tasks
                        boolean priv = true;
                        for (TMLTask task1 : channel.originalOriginTasks) {
                            for (TMLTask task2 : channel.originalDestinationTasks) {
                                if (originDestMap.get(task1.getName() + "__" + task2.getName()) != 1) {
                                    priv = false;
                                    break;
                                }

                            }

                        }
                        ar.setPrivate(priv);
                    }
                }
                if (channel.getType() == TMLChannel.BRBW) {
                    ar.setAsynchronous(true);
                    ar.setSizeOfFIFO(channel.getSize());
                    ar.setBlocking(true);
                } else if (channel.getType() == TMLChannel.BRNBW) {
                    ar.setAsynchronous(true);
                    ar.setSizeOfFIFO(channel.getSize());
                    ar.setBlocking(false);
                } else {
                    //Create new block, hope for best
                    if (mc) {
                        fifo = createFifo(channel.getName());
                        ar.setAsynchronous(false);
                    }
                }
                //Find in signal

                List<AvatarSignal> sig1 = new ArrayList<>();
                //Sig1 contains IN Signals, Sig2 contains OUT signals
                sig1.add(signalInMap.get(channel.getName()));
                List<AvatarSignal> sig2 = new ArrayList<>();
                sig2.add(signalOutMap.get(channel.getName()));
                //Find out signal
                if (channel.getType() == TMLChannel.NBRNBW && mc) {
                    AvatarSignal read = fifo.getSignalByName("readSignal");

                    ar.block2 = fifo;
                    //Set IN signal with read
                    ar.addSignals(sig1.get(0), read);
                    AvatarRelation ar2 = new AvatarRelation(channel.getName() + "2", fifo, taskBlockMap.get(channel.getDestinationTask()), channel.getReferenceObject());
                    AvatarSignal write = fifo.getSignalByName("writeSignal");
                    //set OUT signal with write
                    ar2.addSignals(write, sig2.get(0));
                    //		System.out.println("Set " + sig2.get(0) + " and write");
                    ar2.setAsynchronous(false);
                    avspec.addRelation(ar2);
                } else {
                    ar.addSignals(sig2.get(0), sig1.get(0));
                }
                avspec.addRelation(ar);
            } else {
                //System.out.println("WTF Found non-basic channel");
                //If not a basic channel, create a relation between TOP block and itself
                AvatarRelation relation = new AvatarRelation(channel.getName(), top, top, channel.getReferenceObject());
                AvatarSignal s1 = new AvatarSignal(getName(channel.getName()) + "in", AvatarSignal.IN, null);
                AvatarSignal s2 = new AvatarSignal(getName(channel.getName()) + "out", AvatarSignal.OUT, null);
                top.addSignal(s1);
                top.addSignal(s2);
                relation.addSignals(s2, s1);
                avspec.addRelation(relation);
                for (TMLTask t1 : channel.getOriginTasks()) {
                    for (TMLTask t2 : channel.getDestinationTasks()) {
                        AvatarRelation ar = new AvatarRelation(channel.getName(), taskBlockMap.get(t1), taskBlockMap.get(t2), channel.getReferenceObject());
                        ar.setPrivate(originDestMap.get(t1.getName() + "__" + t2.getName()) == 1);
                        //Find in signal
                        List<AvatarSignal> sig1 = new ArrayList<>();
                        List<AvatarSignal> sig2 = new ArrayList<>();
                        for (AvatarSignal sig : signals) {
                            if (sig.getInOut() == AvatarSignal.IN) {
                                String name = sig.getName();
                                if (name.equals(getName(channel.getName()))) {
                                    sig1.add(sig);
                                }
                            }
                        }
                        //Find out signal
                        for (AvatarSignal sig : signals) {
                            if (sig.getInOut() == AvatarSignal.OUT) {
                                String name = sig.getName();
                                if (name.equals(getName(channel.getName()))) {
                                    sig2.add(sig);
                                }
                            }
                        }
                        if (sig1.isEmpty()) {
                            sig1.add(new AvatarSignal(getName(channel.getName()), AvatarSignal.IN, null));
                        }
                        if (sig2.isEmpty()) {
                            sig2.add(new AvatarSignal(getName(channel.getName()), AvatarSignal.OUT, null));
                        }
                        if (sig1.size() == 1 && sig2.size() == 1) {
                            ar.addSignals(sig2.get(0), sig1.get(0));
                        } else {
                            TraceManager.addDev("Failure to match signals for TMLChannel " + channel.getName() + " between " + t1.getName() + " and " + t2.getName());
                        }
                        avspec.addRelation(ar);
                    }
                }
            }
        }
        for (TMLRequest request : tmlmodel.getRequests()) {
            for (TMLTask t1 : request.getOriginTasks()) {
                AvatarRelation ar = new AvatarRelation(request.getName(), taskBlockMap.get(t1), taskBlockMap.get(request.getDestinationTask()), request.getReferenceObject());
                ar.setPrivate(originDestMap.get(t1.getName() + "__" + request.getDestinationTask().getName()) == 1);
                List<AvatarSignal> sig1 = new ArrayList<>();
                List<AvatarSignal> sig2 = new ArrayList<>();
                for (AvatarSignal sig : signals) {
                    if (sig.getInOut() == AvatarSignal.IN) {
                        String name = sig.getName();

                        if (name.equals(getName(request.getName()))) {
                            sig1.add(sig);
                        }
                    }
                }
                //Find out signal
                for (AvatarSignal sig : signals) {
                    if (sig.getInOut() == AvatarSignal.OUT) {
                        String name = sig.getName();

                        if (name.equals(getName(request.getName()))) {
                            sig2.add(sig);
                        }
                    }
                }
                if (sig1.isEmpty()) {
                    sig1.add(new AvatarSignal(getName(request.getName()), AvatarSignal.IN, null));
                }
                if (sig2.isEmpty()) {
                    sig2.add(new AvatarSignal(getName(request.getName()), AvatarSignal.OUT, null));
                }
                if (sig1.size() == 1 && sig2.size() == 1) {
                    ar.addSignals(sig2.get(0), sig1.get(0));
                } else {
                    //Throw error
                    TraceManager.addDev("Could not match for " + request.getName());
                }

                ar.setAsynchronous(false);
                avspec.addRelation(ar);
            }
        }
        for (TMLEvent event : tmlmodel.getEvents()) {

            AvatarRelation ar = new AvatarRelation(event.getName(), taskBlockMap.get(event.getOriginTask()), taskBlockMap.get(event.getDestinationTask()),
                    event.getReferenceObject());
            ar.setAsynchronous(true);
            ar.setPrivate(true);


            AvatarSignal sigOut = signalOutMap.get(event.getName());
            AvatarSignal sigIn = signalInMap.get(event.getName());
            ar.addSignals(sigOut, sigIn);

            if (event.isBlocking()) {
                ar.setAsynchronous(true);
                ar.setBlocking(true);
                ar.setSizeOfFIFO(event.getMaxSize());
            } else {
                ar.setAsynchronous(true);
                ar.setBlocking(false);
                ar.setSizeOfFIFO(event.getMaxSize());

            }
            avspec.addRelation(ar);
        }

        //	System.out.println("Avatar relations " + avspec.getRelations());

        for (AvatarSignal sig : signals) {
            //	System.out.println("signal " + sig.getName());
            //check that all signals are put in relations
            AvatarRelation ar = avspec.getAvatarRelationWithSignal(sig);
            if (ar == null) {
                TraceManager.addDev("missing relation for " + sig.getName());
            }
        }
        //Check if we matched up all signals
        for (SecurityPattern sp : symKeys.keySet()) {
            if (symKeys.get(sp).size() > 1) {
                String keys = "";
                for (AvatarAttribute key : symKeys.get(sp)) {
                    keys = keys + " " + key.getBlock().getName() + "." + key.getName();
                }
                avspec.addPragma(new AvatarPragmaInitialKnowledge("#InitialSystemKnowledge " + keys, null, symKeys.get(sp),
                        true), sp);
            }
        }
        
        for (SecurityPattern sp : keysPublicBus) {
            if (tmlmodel.getSecurityTaskMap().get(sp) != null) {
                for (TMLTask taskPattern : tmlmodel.getSecurityTaskMap().get(sp)) {
                    AvatarBlock b = taskBlockMap.get(taskPattern);
                    AvatarAttribute attrib = b.getAvatarAttributeWithName("key_" + sp.getName());
                    if (attrib != null) {
                        LinkedList<AvatarAttribute> arguments = new LinkedList<>();
                        arguments.add(attrib);
                        avspec.addPragma(new AvatarPragmaPublic("#Public " + b.getName() + "." + attrib.getName(), null,
                                arguments), sp);
                    }
                }
            }
        }
        for (SecurityPattern sp : pubKeys.keySet()) {
            if (!pubKeys.get(sp).isEmpty()) {
                String keys = "";
                List<String> pubKeyNames = new ArrayList<>();
                for (AvatarAttribute key : pubKeys.get(sp)) {
                    if (!pubKeyNames.contains(key.getBlock().getName() + "." + key.getName())) {
                        keys = keys + " " + key.getBlock().getName() + "." + key.getName();
                        pubKeyNames.add(key.getBlock().getName() + "." + key.getName());
                    }
                }
                //	avspec.addPragma(new AvatarPragmaInitialKnowledge("#InitialSessionKnowledge "+keys, null, pubKeys.get(sp),true));
                //System.out.println("pragma " + keys);
            }
        }

        tmlmap.getTMLModeling().setSecChannelMap(secChannelMap);
        tmlmap.getTMLModeling().setConfPragmaMap(confPragmaMap);
        tmlmap.getTMLModeling().setAuthPragmaMap(authPragmaMap);

//			System.out.println("avatar spec\n" +avspec);
        return avspec;
    }

/*    public AvatarSpecification generateAvatarSpec(String _loopLimit) {
        return generateAvatarSpec(_loopLimit, false);
    }*/

    public void backtraceReachability(Map<AvatarPragmaReachability, ProVerifQueryResult> reachabilityResults) {
        for (AvatarPragmaReachability pragma : reachabilityResults.keySet()) {
            ProVerifQueryResult result = reachabilityResults.get(pragma);
            if (!result.isProved())
                continue;

            int r = result.isSatisfied() ? 1 : 2;

            String s = pragma.getBlock().getName() + "__" + pragma.getState().getName();

            if (stateObjectMap.containsKey(s)) {
                Object obj = stateObjectMap.get(s);
                if (obj instanceof SecurityCheckable) {
                    SecurityCheckable wc = (SecurityCheckable) obj;
                    wc.setReachabilityInformation(r);
                }
            }
        }
    }

    private void distributeKeys() {
        List<TMLTask> tasks = tmlmap.getTMLModeling().getTasks();
        for (TMLTask t : accessKeys.keySet()) {
            AvatarBlock b = taskBlockMap.get(t);
            for (SecurityPattern sp : accessKeys.get(t)) {
                if (sp.getType().equals(SecurityPattern.SYMMETRIC_ENC_PATTERN) || sp.getType().equals(SecurityPattern.MAC_PATTERN)) {
                    AvatarAttribute key = new AvatarAttribute("key_" + sp.getName(), AvatarType.INTEGER, b, null);
                    if (symKeys.containsKey(sp)) {
                        symKeys.get(sp).add(key);
                    } else {
                        LinkedList<AvatarAttribute> tmp = new LinkedList<>();
                        tmp.add(key);
                        symKeys.put(sp, tmp);
                    }
                    b.addAttribute(key);
                } else if (sp.getType().equals(SecurityPattern.ASYMMETRIC_ENC_PATTERN)) {
                    AvatarAttribute pubkey = new AvatarAttribute("pubKey_" + sp.getName(), AvatarType.INTEGER, b, null);
                    b.addAttribute(pubkey);

                    AvatarAttribute privkey = new AvatarAttribute("privKey_" + sp.getName(), AvatarType.INTEGER, b, null);
                    b.addAttribute(privkey);
                    avspec.addPragma(new AvatarPragmaPrivatePublicKey("#PrivatePublicKeys " + b.getName() + " " + privkey.getName() + " " +
                            pubkey.getName(), null, privkey, pubkey), sp);
                    if (pubKeys.containsKey(sp)) {
                        pubKeys.get(sp).add(pubkey);
                    } else {
                        LinkedList<AvatarAttribute> tmp = new LinkedList<>();
                        tmp.add(pubkey);
                        pubKeys.put(sp, tmp);
                    }
                    //Distribute public key everywhere
                    for (TMLTask task2 : tasks) {
                        AvatarBlock b2 = taskBlockMap.get(task2);
                        pubkey = new AvatarAttribute("pubKey_" + sp.getName(), AvatarType.INTEGER, b2, null);
                        b2.addAttribute(pubkey);
                        if (pubKeys.containsKey(sp)) {
                            pubKeys.get(sp).add(pubkey);
                        }
                    }
                }
            }
        }

    }

    private AvatarBlock createFifo(String name) {
        AvatarBlock fifo = new AvatarBlock("FIFO__FIFO" + name, avspec, null);
        AvatarState root = new AvatarState("root", null, fifo, false, false);
        AvatarSignal read = new AvatarSignal("readSignal", AvatarSignal.IN, null);
        AvatarAttribute data = new AvatarAttribute("data", AvatarType.INTEGER, fifo, null);
        fifo.addAttribute(data);
        read.addParameter(data);
        AvatarSignal write = new AvatarSignal("writeSignal", AvatarSignal.OUT, null);
        write.addParameter(data);
        AvatarStartState start = new AvatarStartState("start", null, fifo);
        AvatarTransition afterStart = new AvatarTransition(fifo, "afterStart", null);
        fifo.addSignal(read);
        fifo.addSignal(write);
        AvatarTransition toRead = new AvatarTransition(fifo, "toReadSignal", null);
        AvatarTransition toWrite = new AvatarTransition(fifo, "toWriteSignal", null);
        AvatarTransition afterRead = new AvatarTransition(fifo, "afterReadSignal", null);
        AvatarTransition afterWrite = new AvatarTransition(fifo, "afterWriteSignal", null);
        AvatarActionOnSignal readAction = new AvatarActionOnSignal("read", read, null, fifo);
        AvatarActionOnSignal writeAction = new AvatarActionOnSignal("write", write, null, fifo);

        AvatarStateMachine asm = fifo.getStateMachine();
        asm.addElement(start);
        asm.setStartState(start);
        asm.addElement(afterStart);
        asm.addElement(root);
        asm.addElement(toRead);
        asm.addElement(toWrite);
        asm.addElement(afterRead);
        asm.addElement(afterWrite);
        asm.addElement(readAction);
        asm.addElement(writeAction);

        start.addNext(afterStart);
        afterStart.addNext(root);
        root.addNext(toRead);
        root.addNext(toWrite);
        toRead.addNext(readAction);
        toWrite.addNext(writeAction);
        readAction.addNext(afterRead);
        writeAction.addNext(afterWrite);
        afterRead.addNext(root);
        afterWrite.addNext(root);

        avspec.addBlock(fifo);
        return fifo;
    }

    private AvatarType getAvatarType(TMLType p) {
        switch (p.getType()) {
            case TMLType.NATURAL:
                return AvatarType.INTEGER;
            case TMLType.BOOLEAN:
                return AvatarType.BOOLEAN;
        }
        return AvatarType.UNDEFINED;
    }

    private String getNameReworked(String name, int index) {
        String[] split = name.split("__");
        if (split.length > index) {
            return split[index];
        }
        return name;
    }

    private String reworkStringName(String _name) {
        String ret = _name.replaceAll(" ", "");
        ret = ret.replaceAll("__", "_");
        ret = ret.replaceAll("-", "");
        return ret;
    }

    private String reworkActionString(String _name) {
        String ret = _name.replaceAll(" ", "");
        ret = ret.replaceAll("__", "_");
        return ret;
    }

    private String reNameStateWithAction(String _name) {
        Set<String> symbols = new HashSet<>(Arrays.asList(" ", "\\*", "\\+", "\\-", "\\/", "=", "\\)", "\\("));
        for (String symb : symbols) {
            _name = _name.replaceAll(symb, "");
        }
        return _name;
    }

    private String setNewAttributeName(String _attributeName, AvatarBlock _block) {
        String newAttributeName = _attributeName + "0";
        int indexAttribute = 0;
        while (_block.getAvatarAttributeWithName(newAttributeName) != null) {
            newAttributeName = _attributeName + indexAttribute;
            indexAttribute += 1;
        }
        return newAttributeName;
    }

    private void createSecElements(List<TMLTask> _tasks) {
        for (TMLTask task : _tasks) {
            AvatarBlock block = taskBlockMap.get(task);
            for (TMLActivityElement ae : task.getActivityDiagram().getElements()) {
                if (ae instanceof TMLActivityElementWithAction) {
                    if (ae.getSecurityPattern() != null && ae instanceof TMLExecC) {
                        //If encryption
                        if (!((TMLExecC) ae).isDecryptionProcess()) {
                            if ((!ae.getSecurityPattern().getNonce().isEmpty() || ae.getSecurityPattern().getType().equals(SecurityPattern.MAC_PATTERN)) &&
                                    block.getAvatarMethodWithName("concat2") == null) {
                                //Create concat2 method
                                AvatarAttribute msg1 = new AvatarAttribute("msg1", AvatarType.INTEGER, block, null);
                                AvatarAttribute msg2 = new AvatarAttribute("msg2", AvatarType.INTEGER, block, null);
                                AvatarAttribute ConcatedMsg = new AvatarAttribute("ConcatenatedMsg", AvatarType.INTEGER, block, null);
                                AvatarMethod concat2 = new AvatarMethod("concat2", ae);
                                concat2.addParameter(msg1);
                                concat2.addParameter(msg2);
                                concat2.addReturnParameter(ConcatedMsg);
                                block.addMethod(concat2);
                            }
                            switch (ae.getSecurityPattern().getType()) {
                                case SecurityPattern.SYMMETRIC_ENC_PATTERN:
                                    //Type Symmetric Encryption
                                    if (block.getAvatarMethodWithName("sencrypt") == null) {
                                        AvatarMethod sencrypt = new AvatarMethod("sencrypt", ae);
                                        AvatarAttribute msg = new AvatarAttribute("msg", AvatarType.INTEGER, block, null);
                                        AvatarAttribute key = new AvatarAttribute("key", AvatarType.INTEGER, block, null);
                                        AvatarAttribute encryptedMsg = new AvatarAttribute("encryptedMsg", AvatarType.INTEGER, block, null);
                                        sencrypt.addParameter(msg);
                                        sencrypt.addParameter(key);
                                        sencrypt.addReturnParameter(encryptedMsg);
                                        block.addMethod(sencrypt);
                                    }
                                    if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                        AvatarAttribute encryptedKey = new AvatarAttribute("encryptedKey_" + ae.getSecurityPattern().getKey(),
                                                AvatarType.INTEGER, block, null);
                                        block.addAttribute(encryptedKey);
                                        secPatternEncAttribute.put(ae.getSecurityPattern(), encryptedKey);
                                    } else {
                                        AvatarAttribute msgEncrypted = new AvatarAttribute(ae.getSecurityPattern().getName() + "_encrypted", AvatarType.INTEGER
                                                , block, null);
                                        block.addAttribute(msgEncrypted);
                                        secPatternEncAttribute.put(ae.getSecurityPattern(), msgEncrypted);
                                    }
                                    break;
                                case SecurityPattern.ASYMMETRIC_ENC_PATTERN:
                                    if (block.getAvatarMethodWithName("aencrypt") == null) {
                                        AvatarMethod aencrypt = new AvatarMethod("aencrypt", ae);
                                        AvatarAttribute msg = new AvatarAttribute("msg", AvatarType.INTEGER, block, null);
                                        AvatarAttribute pubKey = new AvatarAttribute("pubKey", AvatarType.INTEGER, block, null);
                                        AvatarAttribute encryptedMsg = new AvatarAttribute("encryptedMsg", AvatarType.INTEGER, block, null);
                                        aencrypt.addParameter(msg);
                                        aencrypt.addParameter(pubKey);
                                        aencrypt.addReturnParameter(encryptedMsg);
                                        block.addMethod(aencrypt);
                                    }
                                    if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                        AvatarAttribute encryptedKey = new AvatarAttribute("encryptedKey_" + ae.getSecurityPattern().getKey(),
                                                AvatarType.INTEGER, block, null);
                                        block.addAttribute(encryptedKey);
                                        secPatternEncAttribute.put(ae.getSecurityPattern(), encryptedKey);
                                    } else {
                                        AvatarAttribute msgEncrypted = new AvatarAttribute(ae.getSecurityPattern().getName() + "_encrypted", AvatarType.INTEGER
                                                , block, null);
                                        block.addAttribute(msgEncrypted);
                                        secPatternEncAttribute.put(ae.getSecurityPattern(), msgEncrypted);
                                    }
                                    break;
                                case SecurityPattern.HASH_PATTERN: {
                                    if (block.getAvatarMethodWithName("hash") == null) {
                                        AvatarMethod hash = new AvatarMethod("hash", ae);
                                        AvatarAttribute msg = new AvatarAttribute("msg", AvatarType.INTEGER, block, null);
                                        AvatarAttribute res = new AvatarAttribute("res", AvatarType.INTEGER, block, null);
                                        hash.addParameter(msg);
                                        hash.addReturnParameter(res);
                                        block.addMethod(hash);
                                    }
                                    AvatarAttribute msgEncrypted = new AvatarAttribute(ae.getSecurityPattern().getName() + "_encrypted", AvatarType.INTEGER
                                            , block, null);
                                    block.addAttribute(msgEncrypted);
                                    secPatternEncAttribute.put(ae.getSecurityPattern(), msgEncrypted);
                                    break;
                                }
                                case SecurityPattern.MAC_PATTERN: {
                                    if (block.getAvatarMethodWithName("MAC") == null) {
                                        AvatarMethod mac = new AvatarMethod("MAC", ae);
                                        AvatarAttribute msg = new AvatarAttribute("msg", AvatarType.INTEGER, block, null);
                                        AvatarAttribute key = new AvatarAttribute("key", AvatarType.INTEGER, block, null);
                                        AvatarAttribute encryptedMsg = new AvatarAttribute("encryptedMsg", AvatarType.INTEGER, block, null);
                                        mac.addParameter(msg);
                                        mac.addParameter(key);
                                        mac.addReturnParameter(encryptedMsg);
                                        block.addMethod(mac);
                                    }
                                    AvatarAttribute msgEncrypted = new AvatarAttribute(ae.getSecurityPattern().getName() + "_encrypted", AvatarType.INTEGER,
                                            block, null);
                                    block.addAttribute(msgEncrypted);
                                    secPatternEncAttribute.put(ae.getSecurityPattern(), msgEncrypted);
                                    break;
                                }
                            }
                        } else {
                            //Decryption action
                            if ((!ae.getSecurityPattern().getNonce().isEmpty() || ae.getSecurityPattern().getType().equals(SecurityPattern.MAC_PATTERN))
                                    && block.getAvatarMethodWithName("get2") == null) {
                                //Add get2 method
                                AvatarMethod get2 = new AvatarMethod("get2", ae);
                                AvatarAttribute msg = new AvatarAttribute("msg", AvatarType.INTEGER, block, null);
                                AvatarAttribute msg1 = new AvatarAttribute("msg1", AvatarType.INTEGER, block, null);
                                AvatarAttribute msg2 = new AvatarAttribute("msg2", AvatarType.INTEGER, block, null);
                                get2.addParameter(msg);
                                get2.addParameter(msg1);
                                get2.addParameter(msg2);
                                block.addMethod(get2);
                            }
                            switch (ae.getSecurityPattern().getType()) {
                                case SecurityPattern.SYMMETRIC_ENC_PATTERN: {
                                    if (block.getAvatarMethodWithName("sdecrypt") == null) {
                                        AvatarMethod sdecrypt = new AvatarMethod("sdecrypt", ae);
                                        AvatarAttribute msgToDecrypt = new AvatarAttribute("msgToDecrypt", AvatarType.INTEGER, block, null);
                                        AvatarAttribute key = new AvatarAttribute("key", AvatarType.INTEGER, block, null);
                                        AvatarAttribute msgDecrypted = new AvatarAttribute("msgDecrypted", AvatarType.INTEGER, block, null);
                                        sdecrypt.addParameter(msgToDecrypt);
                                        sdecrypt.addParameter(key);
                                        sdecrypt.addReturnParameter(msgDecrypted);
                                        block.addMethod(sdecrypt);
                                    }
                                    if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                        AvatarAttribute keyToDecrypt = new AvatarAttribute("encryptedKey_" + ae.getSecurityPattern().getKey(), AvatarType.INTEGER, block, null);
                                        block.addAttribute(keyToDecrypt);
                                        secPatternDecAttribute.put(ae.getSecurityPattern(), keyToDecrypt);
                                    } else {
                                        AvatarAttribute msgToDecrypt = new AvatarAttribute(ae.getSecurityPattern().getName() + "_encrypted", AvatarType.INTEGER, block, null);
                                        block.addAttribute(msgToDecrypt);
                                        secPatternDecAttribute.put(ae.getSecurityPattern(), msgToDecrypt);
                                    }
                                    break;
                                }
                                case SecurityPattern.ASYMMETRIC_ENC_PATTERN: {
                                    if (block.getAvatarMethodWithName("adecrypt") == null) {
                                        AvatarMethod adecrypt = new AvatarMethod("adecrypt", ae);
                                        AvatarAttribute msgToDecrypt = new AvatarAttribute("msgToDecrypt", AvatarType.INTEGER, block, null);
                                        AvatarAttribute privKey = new AvatarAttribute("privKey", AvatarType.INTEGER, block, null);
                                        AvatarAttribute msgDecrypted = new AvatarAttribute("msgDecrypted", AvatarType.INTEGER, block, null);
                                        adecrypt.addParameter(msgToDecrypt);
                                        adecrypt.addParameter(privKey);
                                        adecrypt.addReturnParameter(msgDecrypted);
                                        block.addMethod(adecrypt);
                                    }
                                    if (!ae.getSecurityPattern().getKey().isEmpty()) {
                                        AvatarAttribute keyToDecrypt = new AvatarAttribute("encryptedKey_" + ae.getSecurityPattern().getKey(),
                                                AvatarType.INTEGER, block, null);
                                        block.addAttribute(keyToDecrypt);
                                        secPatternDecAttribute.put(ae.getSecurityPattern(), keyToDecrypt);
                                    } else {
                                        AvatarAttribute msgToDecrypt = new AvatarAttribute(ae.getSecurityPattern().getName() + "_encrypted", AvatarType.INTEGER,
                                                block, null);
                                        block.addAttribute(msgToDecrypt);
                                        secPatternDecAttribute.put(ae.getSecurityPattern(), msgToDecrypt);
                                    }
                                    break;
                                }
                                case SecurityPattern.MAC_PATTERN: {
                                    AvatarAttribute msgToDecrypt = new AvatarAttribute(ae.getSecurityPattern().getName() + "_encrypted", AvatarType.INTEGER,
                                            block, null);
                                    block.addAttribute(msgToDecrypt);
                                    secPatternDecAttribute.put(ae.getSecurityPattern(), msgToDecrypt);
                                    //Add verifymac method
                                    if (block.getAvatarMethodWithName("verifyMAC") == null) {
                                        AvatarMethod verifymac = new AvatarMethod("verifyMAC", ae);
                                        AvatarAttribute msg = new AvatarAttribute("msg", AvatarType.INTEGER, block, null);
                                        verifymac.addParameter(msg);
                                        AvatarAttribute keyMAC = new AvatarAttribute("key", AvatarType.INTEGER, block, null);
                                        verifymac.addParameter(keyMAC);
                                        AvatarAttribute macMsg = new AvatarAttribute("mac", AvatarType.INTEGER, block, null);
                                        verifymac.addParameter(macMsg);
                                        AvatarAttribute testNonce = new AvatarAttribute("testNonce", AvatarType.BOOLEAN, block, null);
                                        verifymac.addReturnParameter(testNonce);
                                        block.addMethod(verifymac);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

   /*  public static boolean isInteger(String string) {
        if(string == null || string.equals("")) {
            return false;
        }

        try {
            int intValue = Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e) {

        }
        return false;
    }*/

}



