/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */



package tmltranslator;

import avatartranslator.AvatarState;
import myutil.TraceManager;

import java.util.Objects;


public class SecurityPattern {

    public final static String SYMMETRIC_ENC_PATTERN = "Symmetric Encryption";
    public final static String ASYMMETRIC_ENC_PATTERN = "Asymmetric Encryption";
    public final static String MAC_PATTERN = "MAC";
    public final static String HASH_PATTERN = "Hash";
    public final static String NONCE_PATTERN = "Nonce";
    public final static String ADVANCED_PATTERN = "Advanced";

    private String name = "";
    private String type = "";
    private int overhead = 0;
    private int size = 0;
    private int encTime = 0;
    private int decTime = 0;
    private AvatarState state1;
    private AvatarState state2;

    private String nonce;
    private String formula;
    private String key;
    private String algorithm = "";

    public SecurityPattern(String _name, String _type, String _overhead, String _size, String _enctime, String _dectime, String _nonce,
                           String _formula, String _key) {
        this.name = _name;
        this.type = _type;

        if (_nonce != null) {
            if (_nonce.compareTo("-") == 0) {
                this.nonce = "";
            } else {
                this.nonce = _nonce;
            }
        } else {
            this.nonce = "";
        }

        this.formula = _formula;
        this.key = _key;


        try {
            TraceManager.addDev("overhead=" + _overhead);
            this.overhead = Integer.parseInt(_overhead);
        } catch (NumberFormatException e) {}
        try {
            TraceManager.addDev("size=" + _size);
            this.size = Integer.parseInt(_size);
        } catch (NumberFormatException e) {}
        try {
            TraceManager.addDev("decTime=" + _dectime);
            this.decTime = Integer.parseInt(_dectime);
        } catch (NumberFormatException e) {}
        try {
            TraceManager.addDev("enctime=" + _enctime);
            this.encTime = Integer.parseInt(_enctime);
        } catch (NumberFormatException e) {}
    }

    public String toXML() {
        String s = "<SECURITYPATTERN ";

        s += " name=\"" + name;
        s += "\" type=\"" + type;
        s += "\" overhead=\"" + overhead;
        s += "\" size=\"" + size;
        s += "\" encTime=\"" + encTime;
        s += "\" decTime=\"" + decTime;
        if (state1 != null) {
            s += "\" state1=\"" + state1.getName();
        }
        if (state2 != null) {
            s += "\" state2=\"" + state2.getName();
        }
        s += "\" nonce=\"" + nonce;
        s += "\" formula=\"" + formula;
        s += "\" key=\"" + key;
        s += "\" />\n";

        return s;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOverhead() {
        return overhead;
    }

    public void setOverhead(int overhead) {
        this.overhead = overhead;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getEncTime() {
        return encTime;
    }

    public void setEncTime(int encTime) {
        this.encTime = encTime;
    }

    public int getDecTime() {
        return decTime;
    }

    public void setDecTime(int decTime) {
        this.decTime = decTime;
    }

    public AvatarState getState1() {
        return state1;
    }

    public void setState1(AvatarState state1) {
        this.state1 = state1;
    }

    public AvatarState getState2() {
        return state2;
    }

    public void setState2(AvatarState state2) {
        this.state2 = state2;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public boolean equalsSpec(Object o) {
        if (!(o instanceof SecurityPattern)) return false;

        SecurityPattern securityPattern = (SecurityPattern) o;
        return overhead == securityPattern.overhead &&
                size == securityPattern.size &&
                encTime == securityPattern.encTime &&
                decTime == securityPattern.decTime &&
                Objects.equals(name,securityPattern.name) &&
                Objects.equals(type,securityPattern.type) &&
                Objects.equals(nonce,securityPattern.nonce) &&
                Objects.equals(formula,securityPattern.formula) &&
                Objects.equals(key,securityPattern.key) &&
                Objects.equals(algorithm,securityPattern.algorithm);

    }

    public boolean isNonceType() {
        if (type == null) {
            return false;
        }
        return type.equals(SecurityPattern.NONCE_PATTERN);
    }

    public SecurityPattern deepClone(TMLModeling tmlm) throws TMLCheckingError {
        SecurityPattern sp = new SecurityPattern(name, type, ""+overhead, ""+size, ""+encTime, ""+decTime,
                nonce, formula, key);

        return sp;
    }

}
