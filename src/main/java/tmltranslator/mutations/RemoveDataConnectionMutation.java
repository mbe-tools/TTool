/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package tmltranslator.mutations;

import tmltranslator.TMLChannel;
import tmltranslator.TMLModeling;
import tmltranslator.TMLTask;

import java.util.ArrayList;

public class RemoveDataConnectionMutation extends DataConnectionMutation{
    String originPortName;
    String destinationPortName;
    protected RemoveDataConnectionMutation(String _connectionName) {
        super(_connectionName);
    }
    protected RemoveDataConnectionMutation(String _originPortName, String _destinationPortName) {
        super();
        originPortName = _originPortName;
        destinationPortName = _destinationPortName;
    }

    @Override
    public void apply(TMLModeling<?> _tmlModel) throws ApplyDiplodocusMutationException {
        TMLChannel channel = null;

        if (getConnectionName() != null) {
            channel = getDataConnection(_tmlModel);
        } else if (originPortName != null && destinationPortName != null) {
            String appName = "";
            if (_tmlModel.getTasks().size() != 0){
                appName = _tmlModel.getTasks().get(0).getName().split("__")[0];
            }
            channel = _tmlModel.getChannelByName(appName + "__" + originPortName + "__" + appName + "__" + destinationPortName);
        }
        if (channel == null) {
            if (getConnectionName() != null) {
                throw new ApplyDiplodocusMutationException("There is no channel named " + getConnectionName() + " in this model.");
            } else {
                throw new ApplyDiplodocusMutationException("There is no channel between ports " + originPortName + " and " + destinationPortName + " in this model.");
            }
        } else {
            _tmlModel.removeChannel(channel);
            ArrayList<TMLTask> allTasks = channel.getAllTasks();
            for (TMLTask t : allTasks) {
                t.removeTMLChannel(channel);
                t.removeReadTMLChannel(channel);
                t.removeReadTMLChannel(channel);
            }
        }
    }

    public static RemoveDataConnectionMutation createFromString(String toParse) throws ParseDiplodocusMutationException{
        RemoveDataConnectionMutation mutation = null;
        String[] tokens = DiplodocusMutationParser.tokenise(toParse);

        int index = DiplodocusMutationParser.indexOf(tokens,DiplodocusMutationParser.CONNECTION);
        if (tokens.length == index+1) {
            throw new ParseDiplodocusMutationException("Connection name missing. Expected syntax is remove data connection connectionName.");
        }
        else {
            int indexDot = DiplodocusMutationParser.indexOf(tokens,",");
            if (indexDot == -1) {
                mutation = new RemoveDataConnectionMutation(tokens[index + 1]);
            } else {
                if (tokens.length <= indexDot+1){
                    throw new ParseDiplodocusMutationException("Port name missing. Expected syntax is remove data connection originPortName," +
                            "destinationPortName");
                }
                mutation = new RemoveDataConnectionMutation(tokens[indexDot-1], tokens[indexDot+1]);
            }
        }

        mutation.setMutationType("TMLmutation");
        return mutation;
    }
}
