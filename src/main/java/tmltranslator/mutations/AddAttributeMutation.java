/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package tmltranslator.mutations;

import tmltranslator.*;

import java.util.Arrays;
import java.util.List;

public class AddAttributeMutation extends AttributeMutation {
    protected AddAttributeMutation(String _taskName, String _attributeName, String _attributeType) {
        super(_taskName);
        setAttributeName(_attributeName);
        setAttributeType(_attributeType);
    }

    protected AddAttributeMutation(String _taskName, String _attributeName, String _attributeType, String _initialValue) {
        super(_taskName);
        setAttributeName(_attributeName);
        setAttributeType(_attributeType);
        setInitialValue(_initialValue);
    }

    public TMLAttribute createElement(TMLModeling<?> _tmlModel) throws ApplyDiplodocusMutationException{
        TMLTask task = getTask(_tmlModel);
        if (task == null){
            throw new ApplyDiplodocusMutationException("Task " + this.getTaskName() + " does not exist in this model.");
        }

        for (TMLAttribute a : task.getAttributes()){
            if (a.getName() == this.getName()){
                throw new ApplyDiplodocusMutationException("An attribute named " + this.getName() + " already exists in task " + getTaskName());
            }
        }

        TMLType _type = new TMLType(getType());
        TMLAttribute attr;
        if (hasInitialValue()){
            attr = new TMLAttribute(getName(), _type, getInitialValue());
        } else {
            attr = new TMLAttribute(getName(), _type);
        }
        return attr;
    }

    @Override
    public void apply(TMLModeling<?> _tmlModel) throws ApplyDiplodocusMutationException {
        TMLAttribute attr = createElement(_tmlModel);
        getTask(_tmlModel).addAttribute(attr);
    }

    public static AddAttributeMutation createFromString(String toParse) throws ParseDiplodocusMutationException {
        String[] tokens = DiplodocusMutationParser.tokenise(toParse);
        AddAttributeMutation mutation = null;

        int index = DiplodocusMutationParser.indexOf(tokens, DiplodocusMutationParser.ATTRIBUTE);
        if (tokens.length <= index + 2) {
            throw new ParseDiplodocusMutationException("Missing attribute arguments. Expected format is add attribute type name[=val] in taskName");
        }
        String _attributeType = tokens[index + 1];
        String _attributeName = tokens[index + 2];
        String[] types = {"INT","BOOL"};
        if (!Arrays.asList(types).contains(_attributeType.toUpperCase())){
            throw new ParseDiplodocusMutationException("Missing or wrong type. Expected format is add attribute type name[=val] in taskName, and " +
                    "type is int or bool.");
        }

        index = DiplodocusMutationParser.indexOf(tokens, DiplodocusMutationParser.IN);
        if (tokens.length == index + 1 || index == -1) {
            throw new ParseDiplodocusMutationException("Missing task name. Expected format is add attribute type name[=val] in taskName");
        }
        String _taskName = tokens[index + 1];

        index = DiplodocusMutationParser.indexOf(tokens, "=");
        if (index != -1) {
            if (tokens.length == index + 1) {
                throw new ParseDiplodocusMutationException("Missing attribute initial value. Expected format is add attribute type name[=val] in " +
                        "taskName");
            }
            String _initialValue = tokens[index + 1];
            mutation = new AddAttributeMutation(_taskName, _attributeName, _attributeType, _initialValue);
        } else {
            mutation = new AddAttributeMutation(_taskName, _attributeName, _attributeType);
        }

        mutation.setMutationType("TMLmutation");
        return mutation;
    }

}
