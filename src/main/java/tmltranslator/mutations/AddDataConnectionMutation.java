/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package tmltranslator.mutations;

import tmltranslator.TMLChannel;
import tmltranslator.TMLModeling;
import tmltranslator.TMLTask;

public class AddDataConnectionMutation extends DataConnectionMutation{

    protected AddDataConnectionMutation(String _originTaskName, String _destinationTaskName, String _connectionName, String _connectionSemantics) {
        super(_originTaskName, _destinationTaskName, _connectionName);
        setConnectionSemantics(_connectionSemantics);
    }

    protected AddDataConnectionMutation(String _originTaskName, String _destinationTaskName, String _connectionName, String _connectionSemantics, int _bufferSize) {
        super(_originTaskName, _destinationTaskName, _connectionName);
        setConnectionSemantics(_connectionSemantics);
        setBufferSize(_bufferSize);
    }

    public TMLChannel createElement(TMLModeling<?> _tmlModel) throws ApplyDiplodocusMutationException{
        if (getDataConnection(_tmlModel)!=null){
            throw new ApplyDiplodocusMutationException("A data connection named " + getConnectionName() + " already exists in the current model.");
        }

        TMLChannel channel = new TMLChannel(getConnectionName(), null);
        int size = getBufferSize();

        TMLTask _originTask = getOriginTask(_tmlModel);
        TMLTask _destinationTask = getDestinationTask(_tmlModel);
        if (_originTask==null){
            throw new ApplyDiplodocusMutationException("The task named " + getOriginTaskName() + " does not exist in the current model.");
        }
        if (_destinationTask==null){
            throw new ApplyDiplodocusMutationException("The task named " + getDestinationTaskName() + " does not exist in the current model.");
        }
        channel.setOriginTask(_originTask);
        channel.setDestinationTask(_destinationTask);
        channel.setMax(8);
        if (getConnectionSemantics() == DiplodocusMutationParser.BRBW){
            channel.setType(TMLChannel.BRBW);
        }
        if (getConnectionSemantics() == DiplodocusMutationParser.BRNBW){
            channel.setType(TMLChannel.BRNBW);
        }
        if (getConnectionSemantics() == DiplodocusMutationParser.NBRNBW){
            channel.setType(TMLChannel.NBRNBW);
        }
        if (size == -1){
            channel.setSize(4);
        } else {
            channel.setSize(size);
        }
        return channel;
    }

    @Override
    public void apply(TMLModeling<?> _tmlModel) throws ApplyDiplodocusMutationException {
        TMLChannel channel = createElement(_tmlModel);
        _tmlModel.addChannel(channel);
    }

    public static AddDataConnectionMutation createFromString(String toParse) throws ParseDiplodocusMutationException{
        AddDataConnectionMutation mutation = null;
        String[] tokens = DiplodocusMutationParser.tokenise(toParse);

        int index = DiplodocusMutationParser.indexOf(tokens,DiplodocusMutationParser.BETWEEN);
        if (tokens.length <= index + 3) {
            throw new ParseDiplodocusMutationException("Missing task name. Expected syntax is add data connection connectionName between " +
                    "originTaskName and destinationTaskName");
        }
        String _originTaskName = tokens[index+1];
        String _destinationTaskName = tokens[index+3];

        if (tokens[index-1].toUpperCase().equals(DiplodocusMutationParser.CONNECTION)){
            throw new ParseDiplodocusMutationException("Missing or invalid connection name. Expected syntax is add data connection connectionName " +
                    "between originTaskName and destinationTaskName with connectionName != 'connection'");
        }
        String _connectionName = tokens[index-1];

        index = DiplodocusMutationParser.indexOf(tokens, DiplodocusMutationParser.DATA);
        if (tokens[index-1].toUpperCase().equals(DiplodocusMutationParser.ADD)){
            mutation = new AddDataConnectionMutation(_originTaskName, _destinationTaskName, _connectionName, DiplodocusMutationParser.BRBW);
        } else if (tokens[index-1].toUpperCase().equals(DiplodocusMutationParser.BRNBW)){
            mutation = new AddDataConnectionMutation(_originTaskName, _destinationTaskName, _connectionName, DiplodocusMutationParser.BRNBW);
        } else if (tokens[index-1].toUpperCase().equals(DiplodocusMutationParser.NBRNBW)){
            mutation = new AddDataConnectionMutation(_originTaskName, _destinationTaskName, _connectionName, DiplodocusMutationParser.NBRNBW);
        } else if (tokens[index-1].toUpperCase().equals(DiplodocusMutationParser.BRBW)){
            throw new ParseDiplodocusMutationException("Missing channel width. Expected syntax is add BRBW,integerValue data connection connectionName between originTaskName and destinationTaskName");
        } else if (tokens[index-2].toUpperCase().equals(DiplodocusMutationParser.BRBW) && tokens[index-1].equals(",")){
            throw new ParseDiplodocusMutationException("Missing channel width. Expected syntax is add BRBW,integerValue data connection connectionName between originTaskName and destinationTaskName");
        } else if (tokens[index-3].toUpperCase().equals(DiplodocusMutationParser.BRBW) && tokens[index-2].equals(",") && isNumeric(tokens[index-1])){
            mutation = new AddDataConnectionMutation(_originTaskName, _destinationTaskName, _connectionName, DiplodocusMutationParser.BRBW, Integer.valueOf(tokens[index-1]));
        } else if (tokens[index-3].toUpperCase().equals(DiplodocusMutationParser.BRBW) && tokens[index-2].equals(",") && !isNumeric(tokens[index-1])){
            throw new ParseDiplodocusMutationException("Channel width must be an integer written with Arabic digits.");
        }

        mutation.setMutationType("TMLmutation");
        return mutation;
    }
}
