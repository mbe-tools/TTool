/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package tmltranslator.mutations;

import tmltranslator.TMLChannel;
import tmltranslator.TMLModeling;

import java.util.List;

public abstract class DataConnectionMutation extends ConnectionMutation{
    //private Boolean BRBW, NBRNBW, BRNBW;
    protected DataConnectionMutation(String _originTaskName, String _destinationTaskName, String _connectionName) {
        super(_originTaskName, _destinationTaskName, _connectionName);
    }

    protected DataConnectionMutation(String _connectionName){
        super(_connectionName);
    }

    protected DataConnectionMutation(){
        super();
    }

    /*public void setBRBW(boolean b){
        BRBW = b;
    }

    public Boolean isBRBW(){
        return BRBW;
    }

    public void setNBRNBW(boolean b){
        NBRNBW = b;
    }

    public boolean isNBRNBW(){
        return NBRNBW;
    }

    public void setBRNBW(boolean b){
        BRNBW = b;
    }

    public boolean isBRNBW(){
        return BRNBW;
    }
     */

    public TMLChannel getDataConnection(TMLModeling<?> _tmlModel){
        List<TMLChannel> channels = _tmlModel.getChannels();
        String appName = "";
        if (_tmlModel.getTasks().size() != 0){
            appName = _tmlModel.getTasks().get(0).getName().split("__")[0];
        }
        for (TMLChannel c:channels){
            if (c.getName().equals(getConnectionName())) {
                return c;
            } else if (c.getName().equals(appName + "__" + getConnectionName())) {
                return c;
            }
        }
        return null;
    }

    public static DataConnectionMutation createFromString(String toParse) throws ParseDiplodocusMutationException{
        switch (DiplodocusMutationParser.findMutationToken(toParse)) {
            case DiplodocusMutationParser.ADD:
                return AddDataConnectionMutation.createFromString(toParse);
            case DiplodocusMutationParser.RM:
            case DiplodocusMutationParser.REMOVE:
                return RemoveDataConnectionMutation.createFromString(toParse);
            default:
                break;
        }
        return null;
    }

}
