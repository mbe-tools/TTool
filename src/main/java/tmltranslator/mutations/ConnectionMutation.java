/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package tmltranslator.mutations;

import avatartranslator.mutation.MutationParser;
import tmltranslator.TMLModeling;
import tmltranslator.TMLTask;

import java.util.LinkedList;
import java.util.List;

public abstract class ConnectionMutation extends DiplodocusMutation{
    private String connectionName;
    private String connectionSemantics;
    private String originTaskName;
    private String destinationTaskName;
    private int bufferSize = -1;
    protected ConnectionMutation(String _originTaskName, String _destinationTaskName, String _connectionName) {
        super();
        setOriginTaskName(_originTaskName);
        setDestinationTaskName(_destinationTaskName);
        setConnectionName(_connectionName);
    }

    protected ConnectionMutation(String _connectionName){
        super();
        setConnectionName(_connectionName);
    }

    protected ConnectionMutation(){
        super();
    }

    public void setConnectionName(String _connectionName){
        connectionName = _connectionName;
    }

    public void setConnectionSemantics(String _connectionSemantics){
        connectionSemantics = _connectionSemantics;
    }

    public void setOriginTaskName(String _originTaskName){
        originTaskName = _originTaskName;
    }

    public void setDestinationTaskName(String _destinationTaskName){
        destinationTaskName = _destinationTaskName;
    }

    public String getConnectionName(){
        return connectionName;
    }

    public String getConnectionSemantics(){
        return connectionSemantics;
    }

    public String getOriginTaskName(){
        return originTaskName;
    }

    public String getDestinationTaskName(){
        return destinationTaskName;
    }

    public void setBufferSize(int _bufferSize){
        bufferSize = _bufferSize;
    }

    public int getBufferSize(){
        return bufferSize;
    }

    protected TMLTask getOriginTask(TMLModeling<?> _tmlmodel){
        String appName = "";
        if (_tmlmodel.getTasks().size() != 0){
            appName = _tmlmodel.getTasks().get(0).getName().split("__")[0];
        }
        TMLTask task = _tmlmodel.getTMLTaskByName(originTaskName);
        if (task!=null){
            return task;
        }
        return _tmlmodel.getTMLTaskByName(appName + "__" + originTaskName);
    }

    protected TMLTask getDestinationTask(TMLModeling<?> _tmlmodel){
        String appName = "";
        if (_tmlmodel.getTasks().size() != 0){
            appName = _tmlmodel.getTasks().get(0).getName().split("__")[0];
        }
        TMLTask task = _tmlmodel.getTMLTaskByName(destinationTaskName);
        if (task!=null){
            return task;
        }
        return _tmlmodel.getTMLTaskByName(appName + "__" + destinationTaskName);
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    public static List<String> parseParameters(String toParse) throws ParseDiplodocusMutationException{
        List<String> _parameters = new LinkedList<>();
        String[] tokens =  MutationParser.tokenise(toParse);

        int endIndex;
        int closingBracketIndex = DiplodocusMutationParser.indexOf(tokens,")");
        if (closingBracketIndex == -1){
            throw new ParseDiplodocusMutationException("Missing ) at the end of the parameters list.");
        } else {
            endIndex = closingBracketIndex ;
        }
        for(int i = DiplodocusMutationParser.indexOf(tokens,"(") + 1; i < endIndex; i += 2) {
            if (!(tokens[i].toUpperCase().equals("INT") || tokens[i].toUpperCase().equals("BOOL"))){
                throw new ParseDiplodocusMutationException("Incorrect type provided in parameters list or incorrect list format. Expected types are" +
                        " int or bool, and expected list format is (type1,type2,...,typen).");
            }
            _parameters.add(tokens[i]);
        }

        return _parameters;
    }

    public static ConnectionMutation createFromString(String toParse) throws ParseDiplodocusMutationException{
        switch (DiplodocusMutationParser.findConnectionTypeToken(toParse)) {
            case DiplodocusMutationParser.DATA:
                return DataConnectionMutation.createFromString(toParse);
            case DiplodocusMutationParser.EVENT:
                return EventConnectionMutation.createFromString(toParse);
            case DiplodocusMutationParser.REQUEST:
                return RequestConnectionMutation.createFromString(toParse);
            default:
                break;
        }
        return null;
    }

}
