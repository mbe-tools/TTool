package tmltranslator;

import myutil.TraceManager;

import java.util.*;

public class TMLComparingMethod {
    TMLComparingMethod() {}

    public static boolean areOncommondesListsEqual(List<HwCommunicationNode> list1, List<HwCommunicationNode> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(HwCommunicationNode::getName));
        Collections.sort(list2, Comparator.comparing(HwCommunicationNode::getName));

        boolean test;



        for (int i = 0; i < list1.size(); i++) {
            TraceManager.addDev("Testing equality for " + list1.get(i).getName() + " and " + list2.get(i).getName());
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areMappedcommeltsListsEqual(List<TMLElement> list1, List<TMLElement> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if (list1 == null || list2 == null) {
            return false;
        }
        else if (list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(TMLElement::getName));
        Collections.sort(list2, Comparator.comparing(TMLElement::getName));

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areTaskListsEqual(List<TMLTask> list1, List<TMLTask> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(TMLTask::getName));
        Collections.sort(list2, Comparator.comparing(TMLTask::getName));

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areOnExecutionNodeListsEqual(List<HwExecutionNode> list1, List<HwExecutionNode> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(HwExecutionNode::getName));
        Collections.sort(list2, Comparator.comparing(HwExecutionNode::getName));

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areListsOfStringArrayEqual(List<String[]> list1, List<String[]> list2) {

        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if (list1 == null || list2 == null) {
            return false;
        } else if (list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);


        Collections.sort(list1, (x1, x2) -> {
            if (x1.length > 0 && x2.length > 0) {
                return x2[0].compareTo(x1[0]);
            }
            if (x1.length > 0) {
                return 1;
            }
            if (x2.length > 0) {
                return -1;
            }
            return x2.length - x1.length;
        });

        Collections.sort(list2, (x1, x2) -> {
            if (x1.length > 0 && x2.length > 0) {
                return x2[0].compareTo(x1[0]);
            }
            if (x1.length > 0) {
                return 1;
            }
            if (x2.length > 0) {
                return -1;
            }
            return x2.length - x1.length;
        });

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test = Arrays.equals(list1.get(i),list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areKeysMappingEqual(Map<SecurityPattern, List<HwMemory>> map1, Map<SecurityPattern, List<HwMemory>> map2) {

        if (map1 == null && map2 == null) {
            return true;
        }
        //Only one of them is null
        else if (map1 == null || map1 == null) {
            return false;
        } else if (map1.size() != map2.size()) {
            return false;
        }

        boolean test;
        for (SecurityPattern spMap1 : map1.keySet()) {
            boolean isEqual = false;
            for (SecurityPattern spMap2 : map2.keySet()) {
                if (spMap1.getName().equals(spMap2.getName())) {
                    test = areHwMemoryListsEqual(map1.get(spMap1), map2.get(spMap2));
                    if (!test) return false;
                    isEqual = true;
                    break;
                }
            }
            if (!isEqual) return false;
        }

        return true;
    }

    public static boolean areHwMemoryListsEqual(List<HwMemory> list1, List<HwMemory> list2) {
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(HwMemory::getName));
        Collections.sort(list2, Comparator.comparing(HwMemory::getName));

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areTMLActivityEltListsEqual(List<TMLActivityElement> list1, List<TMLActivityElement> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if (list1 == null || list2 == null) {
            return false;
        }
        else if (list1.size() != list2.size()) {
            return false;
        }

        for (int i = 0; i < list1.size(); i++) {
            if (!list1.get(i).equalsSpec(list2.get(i))) return false;
        }
        return true;
    }

    public static boolean areHwNodeListsEqual(List<HwNode> list1, List<HwNode> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(HwNode::getName));
        Collections.sort(list2, Comparator.comparing(HwNode::getName));

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }


    public static boolean areHwlinkListsEqual(List<HwLink> list1, List<HwLink> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(HwLink::getName));
        Collections.sort(list2, Comparator.comparing(HwLink::getName));

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean arePortListsEqual(List<TMLPort> list1, List<TMLPort> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        //Only one of them is null
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        //copying to avoid rearranging original lists
        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1, Comparator.comparing(TMLPort::getName));
        Collections.sort(list2, Comparator.comparing(TMLPort::getName));

        boolean test;

        for (int i = 0; i < list1.size(); i++) {
            test =  list1.get(i).equalsSpec(list2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areTMLChannelSetsEqual(Set<TMLChannel> channelSet1, Set<TMLChannel> channelSet2) {
        if (channelSet1 == null && channelSet2 == null) return true;
        if (channelSet1 == null || channelSet2 == null) return false;

        if(channelSet1.size() != channelSet2.size()) return false;

        List<TMLChannel> channels1 = new ArrayList<>(channelSet1);
        List<TMLChannel>  channels2 = new ArrayList<>(channelSet2);

        Collections.sort(channels1, Comparator.comparing(TMLChannel::getName));
        Collections.sort(channels2, Comparator.comparing(TMLChannel::getName));

        boolean test;

        for (int i = 0; i < channels1.size(); i++) {
            test =  channels1.get(i).equalsSpec(channels2.get(i));
            if (!test) return false;
        }

        return true;
    }

    public static boolean areTMLEventSetsEqual(Set<TMLEvent> eventSet1, Set<TMLEvent> eventSet2) {
        if (eventSet1 == null && eventSet2 == null) return true;
        if (eventSet1 == null|| eventSet2 == null) return false;

        if(eventSet1.size() != eventSet2.size()) return false;

        List<TMLEvent> events1 = new ArrayList<>(eventSet1);
        List<TMLEvent>  events2 = new ArrayList<>(eventSet2);

        Collections.sort(events1, Comparator.comparing(TMLEvent::getName));
        Collections.sort(events2, Comparator.comparing(TMLEvent::getName));

        boolean test;

        for (int i = 0; i < events1.size(); i++) {
            test =  events1.get(i).equalsSpec(events2.get(i));
            if (!test) return false;
        }
        return true;
    }

    // Check equality of two security pattern specifications
    public static boolean areSecurityPatternsEqual(SecurityPattern sp1, SecurityPattern sp2) {
        if (sp1 == null && sp2 == null) return true;
        else if (sp1 == null || sp2 == null) {
            return false;
        }
        return (sp1.equalsSpec(sp2));
    }

    // Check equality of two TMLAttribute Lists
    public static boolean areTMLAttributeListsEqual(List<TMLAttribute> attributeList1, List<TMLAttribute> attributeList2) {
        if (attributeList1 == null && attributeList2 == null) return true;
        if (attributeList1 == null || attributeList2 == null) return false;
        if(attributeList1.size() != attributeList2.size()) return false;

        for (TMLAttribute attrib : attributeList1) {
            boolean isEqualAttrib = false;
            for (TMLAttribute attrib2 : attributeList2) {
                if (attrib.equalsSpec(attrib2)) {
                    isEqualAttrib = true;
                    break;
                }
            }
            if (!isEqualAttrib) return false;
        }
        return true;
    }

    // Check if the two TMLActivityElementChannel have the same channels list.
    public static boolean areTMLChannelListsEqual(List<TMLChannel> channelList1, List<TMLChannel> channelList2) {
        if (channelList1 == null && channelList2 == null) return true;
        if (channelList1 == null || channelList2 == null) return false;
        if(channelList1.size() != channelList2.size()) return false;

        for (TMLChannel ch : channelList1) {
            boolean isEqualChan = false;
            for (TMLChannel ch2 : channelList2) {
                if (!ch.equalsSpec(ch2)) {
                    isEqualChan = true;
                    break;
                }
            }
            if (!isEqualChan) return false;
        }
        return true;
    }
}
