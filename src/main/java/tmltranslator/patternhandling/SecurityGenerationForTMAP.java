/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


package tmltranslator.patternhandling;
/**
 * Class SecurityGenerationForTMAP
 *
 * Creation: 28/09/2023
 *
 * @author Jawher JERRAY
 * @version 1.0 28/09/2023
 */

import avatartranslator.AvatarPragma;
import avatartranslator.AvatarPragmaAuthenticity;
import avatartranslator.AvatarPragmaSecret;
import avatartranslator.AvatarSpecification;
import avatartranslator.toproverif.AVATAR2ProVerif;
import common.ConfigurationTTool;
import myutil.FileUtils;
import myutil.TraceManager;
import org.apache.commons.math3.util.Pair;
import proverifspec.*;
import tmltranslator.*;
import tmltranslator.toavatarsec.TML2Avatar;
import ui.TGComponent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.concurrent.*;


public class SecurityGenerationForTMAP implements Runnable {
    //final private String  SELECT_CHOICE_HSM = "channelIndex";

    private final String appName;
    private final TMLMapping<?> tmap;
    private final String encComp;
    private final String overhead;
    private final String decComp;
    private final List<String> selectedTasks;
    private final boolean linkedHSMs;
    private int timeOutInSeconds = 0;

    private final Map<TMLTask, List<TMLTask>> taskAndItsHSMMap = new HashMap<>();
    private final Map<TMLTask, List<HwCommunicationNode>> taskHSMCommunicationNodesMap = new HashMap<>();
    private final Map<TMLTask, HwBus> taskAndHSMBusMap = new HashMap<>();
    private final Map<TMLTask, TMLActivityElement> hsmTaskOperationMap = new HashMap<>();
    private final Map<TMLChannel, SecurityPattern> channelNonceSPMap = new HashMap<>();
    private final Map<TMLChannel, TMLChannel> channelTochannelNonceMap = new HashMap<>();
    private final Map<TMLChannel, TMLChannel> channelNonceToHsmMap = new HashMap<>();
    private final Map<TMLChannel, TMLChannel> channelNonceFromHsmMap = new HashMap<>();
    private final Map<TMLTask, TMLRequest> hsmTaskRequestMap = new HashMap<>();
    private final Map<Pair<TMLTask, TMLChannel>, TMLChannel> channelToHsmMap = new HashMap<>();
    private final Map<Pair<TMLTask, TMLChannel>, TMLChannel> channelFromHsmMap = new HashMap<>();

    public SecurityGenerationForTMAP(String appName, TMLMapping<?> tmap, String encComp, String overhead, String decComp,
                                     List<String> selectedTasks, boolean isLinkedHSMs) {
        this.appName = appName;
        this.tmap = tmap;
        this.overhead = overhead;
        this.decComp = decComp;
        this.encComp = encComp;
        this.selectedTasks = selectedTasks;
        this.linkedHSMs = isLinkedHSMs;
    }

    public SecurityGenerationForTMAP(String appName, TMLMapping<?> tmap, String encComp, String overhead, String decComp,
                                     List<String> selectedTasks, boolean isLinkedHSMs, int timeOutInSeconds) {
        this.appName = appName;
        this.tmap = tmap;
        this.overhead = overhead;
        this.decComp = decComp;
        this.encComp = encComp;
        this.selectedTasks = selectedTasks;
        this.linkedHSMs = isLinkedHSMs;
        this.timeOutInSeconds = timeOutInSeconds;
    }
    
    private void proverifAnalysis(TMLMapping<?> tmap, List<TMLChannel> nonConfChans, List<TMLChannel> nonWeakAuthChans,
                                  List<TMLChannel> nonStrongAuthChans) {
        if (tmap == null) {
            TraceManager.addDev("No mapping");
            return;
        }

        //Perform ProVerif Analysis
        Object o = null;
        if (tmap.getTMLModeling().getReference() instanceof TGComponent) {
            o = ((TGComponent)(tmap.getTMLModeling().getReference())).getTDiagramPanel().tp;
        }

        TML2Avatar t2a = new TML2Avatar(tmap, false, true, o);
        AvatarSpecification avatarspec = t2a.generateAvatarSpec("1");
        if (avatarspec == null) {
            TraceManager.addDev("No avatar spec");
            return;
        }

        AVATAR2ProVerif avatar2proverif = new AVATAR2ProVerif(avatarspec);
        try {
            ProVerifSpec proverifSpec =avatar2proverif.generateProVerif(true, true, 3, true, true);

            FileUtils.saveFile(ConfigurationTTool.ProVerifCodeDirectory + "pvspec", proverifSpec.getStringSpec());
            String cmd = ConfigurationTTool.ProVerifVerifierPath + " -in pitype " + ConfigurationTTool.ProVerifCodeDirectory +  "pvspec";
            final Process[] process = new Process[1];
            final Reader[] data = new Reader[1];
            final ExecutorService executor = Executors.newSingleThreadExecutor();
            if (timeOutInSeconds > 0) {
                final Future<?> future = executor.submit(() -> {
                    try {
                        process[0] = Runtime.getRuntime().exec(cmd);
                        data[0] = new BufferedReader(new InputStreamReader(process[0].getInputStream()));
                    } catch (Exception e) {
                        TraceManager.addDev("FAILED: executing: " + cmd + ": " + e.getMessage());
                        throw new RuntimeException(e);
                    }
                });
                try {
                    future.get(timeOutInSeconds, TimeUnit.SECONDS);
                } catch (TimeoutException | InterruptedException | ExecutionException e) {
                    future.cancel(true);
                    throw new RuntimeException(e);
                } finally {
                    executor.shutdown();
                }
            } else {
                try {
                    process[0] = Runtime.getRuntime().exec(cmd);
                    data[0] = new BufferedReader(new InputStreamReader(process[0].getInputStream()));
                } catch (Exception e) {
                    TraceManager.addDev("FAILED: executing: " + cmd + ": " + e.getMessage());
                    throw new RuntimeException(e);
                }
            }
            ProVerifOutputAnalyzer pvoa = avatar2proverif.getOutputAnalyzer();
            pvoa.analyzeOutput(data[0], true);

            if (pvoa.getResults().isEmpty()) {
                TraceManager.addDev("SECGEN ERROR: No security results");
            }


            Map<AvatarPragmaSecret, ProVerifQueryResult> confResults = pvoa.getConfidentialityResults();
            Map<AvatarPragmaAuthenticity, ProVerifQueryAuthResult> authenticityResults = pvoa.getAuthenticityResults();
            for (TMLChannel ch : tmap.getTMLModeling().getSecChannelMap().keySet()) {
                if (ch.isCheckConfChannel()) {
                    List<Integer> confStatus = new ArrayList<>();
                    for (AvatarPragma pragma : tmap.getTMLModeling().getSecChannelMap().get(ch)) {
                        if (pragma instanceof AvatarPragmaSecret) {
                            AvatarPragmaSecret pragmaSecret = (AvatarPragmaSecret) pragma;
                            ProVerifQueryResult result = confResults.get(pragmaSecret);
                            if (result != null) {
                                if (!result.isProved()) {
                                    confStatus.add(1);
                                } else {
                                    int r = result.isSatisfied() ? 2 : 3;
                                    confStatus.add(r);
                                }
                            }
                        }
                    }
                    int generalConfResult = 1;
                    if (!confStatus.isEmpty()) {
                        if (confStatus.contains(3)) {
                            generalConfResult = 3;
                        } else if (confStatus.contains(1)) {
                            generalConfResult = 1;
                        } else {
                            generalConfResult = 2;
                        }
                    }
                    if (generalConfResult == 3) {
                        nonConfChans.add(ch);
                    }
                }
                if (ch.isCheckAuthChannel()) {
                    List<Integer> weakAuthStatus = new ArrayList<>();
                    List<Integer> strongAuthStatus = new ArrayList<>();
                    for (AvatarPragma pragma : tmap.getTMLModeling().getSecChannelMap().get(ch)) {
                        if (pragma instanceof AvatarPragmaAuthenticity) {
                            AvatarPragmaAuthenticity pragmaAuth = (AvatarPragmaAuthenticity) pragma;
                            ProVerifQueryAuthResult result = authenticityResults.get(pragmaAuth);
                            if (result != null) {
                                if (!result.isProved()) {
                                    strongAuthStatus.add(1);
                                } else if (result.isProved() && result.isSatisfied()) {
                                    strongAuthStatus.add(2);
                                } else if (result.isProved() && !result.isSatisfied()) {
                                    strongAuthStatus.add(3);
                                }

                                if (!result.isWeakProved()) {
                                    weakAuthStatus.add(1);
                                } else if (result.isWeakProved() && result.isWeakSatisfied()) {
                                    weakAuthStatus.add(2);
                                } else if (result.isWeakProved() && !result.isWeakSatisfied()) {
                                    weakAuthStatus.add(3);
                                }
                            }
                        }
                    }
                    int generalStrongAuthResult = 1;
                    int generalWeakAuthResult = 1;
                    if (!weakAuthStatus.isEmpty()) {
                        if (weakAuthStatus.contains(3)) {
                            generalWeakAuthResult = 3;
                        } else if (weakAuthStatus.contains(1)) {
                            generalWeakAuthResult = 1;
                        } else {
                            generalWeakAuthResult = 2;
                        }
                    }
                    if (!strongAuthStatus.isEmpty()) {
                        if (strongAuthStatus.contains(3)) {
                            generalStrongAuthResult = 3;
                        } else if (strongAuthStatus.contains(1)) {
                            generalStrongAuthResult = 1;
                        } else {
                            generalStrongAuthResult = 2;
                        }
                    }
                    if (generalWeakAuthResult == 3) {
                        nonWeakAuthChans.add(ch);
                    }

                    if (generalStrongAuthResult == 3) {
                        nonStrongAuthChans.add(ch);
                    }
                }
            }

        } catch (Exception e) {
            TraceManager.addDev("SECGEN EXCEPTION " + e);
        }
    }

    public TMLMapping<?> startThread() {
        Thread t = new Thread(this);
        t.start();
        try {
            t.join();
        } catch (Exception e) {
            TraceManager.addDev("SECGEN. Error in Security Generation Thread");
        }
        return tmap;
    }

    public void run() {
        TMLModeling<?> tmlmodel = tmap.getTMLModeling();
        //Proverif Analysis channels
        List<TMLChannel> nonConfChans = new ArrayList<>();
        List<TMLChannel> nonWeakAuthChans = new ArrayList<>();
        List<TMLChannel> nonStrongAuthChans = new ArrayList<>();

        List<TMLChannel> channels = tmlmodel.getChannels();
        for (TMLChannel channel : channels) {
            for (TMLPortWithSecurityInformation p : channel.ports) {
                channel.checkConf = channel.checkConf || channel.isSecurityGoalConf()|| p.getCheckConf();
                channel.checkAuth = channel.checkAuth || channel.isSecurityGoalWeakAuth() || channel.isSecurityGoalStrongAuth() || p.getCheckAuth();
            }
        }

        proverifAnalysis(tmap, nonConfChans, nonWeakAuthChans, nonStrongAuthChans);

        for (String task : selectedTasks) {
            String taskLongName = appName + "__" + task;
            initHSMArchDiagram(tmap.getTaskByName(taskLongName));
        }

        //With the proverif results, check which channels need to be secured

        LinkedHashSet<TMLChannel> chAddCSA = new LinkedHashSet<>();
        LinkedHashSet<TMLChannel> chAddCWA = new LinkedHashSet<>();
        LinkedHashSet<TMLChannel> chAddSA = new LinkedHashSet<>();
        LinkedHashSet<TMLChannel> chAddWA = new LinkedHashSet<>();
        LinkedHashSet<TMLChannel> chAddC = new LinkedHashSet<>();

        for (TMLChannel chan : tmlmodel.getChannels()) {
            boolean nonConf = nonConfChans.contains(chan);
            boolean nonWeakAuth = nonWeakAuthChans.contains(chan);
            boolean nonStrongAuth = nonStrongAuthChans.contains(chan);
            // add Confidentiality and Strong Authenticity
            if (chan.isSecurityGoalConf() && chan.isSecurityGoalStrongAuth() && (nonStrongAuth || nonConf)) {
                chAddCSA.add(chan);
            }
            // add Confidentiality and Weak Authenticity
            else if (chan.isSecurityGoalConf() && chan.isSecurityGoalWeakAuth() && (nonWeakAuth || nonConf)) {
                chAddCWA.add(chan);
            }
            // add Strong Authenticity
            else if (chan.isSecurityGoalStrongAuth() && nonStrongAuth) {
                chAddSA.add(chan);
            }
            // add Weak Authenticity
            else if (chan.isSecurityGoalWeakAuth() && nonWeakAuth) {
                chAddWA.add(chan);
            }
            // add Confidentiality
            else if (chan.isSecurityGoalConf() && nonConf) {
                chAddC.add(chan);
            }
        }
        for (TMLChannel ch : chAddCSA) {
            SecurityPattern spNonce = findOrCreateNonceSecurityPattern(ch);
            boolean nonceChannelExist = nonceChannelIsCreated(ch);
            TMLChannel chNonce = findOrCreateNonceChannel(ch);
            String secPatternNameInit = "se_" + ch.getChannelName();
            String secPatternName = secPatternNameInit;
            int index = 0;
            while (tmap.getSecurityPatternByName(secPatternName) != null) {
                secPatternName = secPatternNameInit + index;
                index ++;
            }

            SecurityPattern secPattern = new SecurityPattern(secPatternName, SecurityPattern.SYMMETRIC_ENC_PATTERN, overhead, "",
                    encComp, decComp, spNonce.getName(), "", "");
            handleSecurityGoalsForChannels(tmlmodel, ch, secPattern, !nonceChannelExist, spNonce, chNonce, linkedHSMs);
        }
        LinkedHashSet<TMLChannel> chAddCAndCWA = new LinkedHashSet<>(chAddCWA);
        chAddCAndCWA.addAll(chAddC);

        for (TMLChannel ch : chAddCAndCWA) {
            String secPatternNameInit = "se_" + ch.getChannelName();
            String secPatternName = secPatternNameInit;
            int index = 0;
            while (tmap.getSecurityPatternByName(secPatternName) != null) {
                secPatternName = secPatternNameInit + index;
                index ++;
            }

            SecurityPattern secPattern = new SecurityPattern(secPatternName, SecurityPattern.SYMMETRIC_ENC_PATTERN, overhead, "",
                    encComp, decComp, "", "", "");
            handleSecurityGoalsForChannels(tmlmodel, ch, secPattern, false, null, null, linkedHSMs);
        }

        for (TMLChannel ch : chAddSA) {
            SecurityPattern spNonce = findOrCreateNonceSecurityPattern(ch);
            boolean nonceChannelExist = nonceChannelIsCreated(ch);
            TMLChannel chNonce = findOrCreateNonceChannel(ch);
            String secPatternNameInit = "mac_" + ch.getChannelName();
            String secPatternName = secPatternNameInit;
            int index = 0;
            while (tmap.getSecurityPatternByName(secPatternName) != null) {
                secPatternName = secPatternNameInit + index;
                index ++;
            }

            SecurityPattern secPattern = new SecurityPattern(secPatternName, SecurityPattern.MAC_PATTERN, overhead, "",
                    encComp, decComp, spNonce.getName(), "", "");
            handleSecurityGoalsForChannels(tmlmodel, ch, secPattern, !nonceChannelExist, spNonce, chNonce, linkedHSMs);
        }

        for (TMLChannel ch : chAddWA) {
            String secPatternNameInit = "mac_" + ch.getChannelName();
            String secPatternName = secPatternNameInit;
            int index = 0;
            while (tmap.getSecurityPatternByName(secPatternName) != null) {
                secPatternName = secPatternNameInit + index;
                index ++;
            }

            SecurityPattern secPattern = new SecurityPattern(secPatternName, SecurityPattern.MAC_PATTERN, overhead, "",
                    encComp, decComp, "", "", "");
            handleSecurityGoalsForChannels(tmlmodel, ch, secPattern, false, null, null, linkedHSMs);
        }

        for (TMLTask hsmTask : hsmTaskOperationMap.keySet()) {
            buildHSMActivityDiagram(hsmTask, hsmTaskOperationMap.get(hsmTask));
            TMLTask taskOfHSM = hsmTaskRequestMap.get(hsmTask).getOriginTasks().get(0);
            updateHSMArchDiagram(hsmTask, taskAndHSMBusMap.get(taskOfHSM));
        }
    }

    private void handleSecurityGoalsForChannels(TMLModeling<?> tmlmodel, TMLChannel ch, SecurityPattern secPattern, boolean nonceGenerationNeeded,
                                                SecurityPattern spNonce, TMLChannel chNonce, boolean isDirectCommunicationHSMs) {
        tmlmodel.addSecurityPattern(secPattern);
        List<TMLWriteChannel> listWr = writeOperatorsOfChannel(ch.getOriginTask(), ch);
        List<TMLReadChannel> listRd = readOperatorsOfChannel(ch.getDestinationTask(), ch);
        TMLTask hsmTaskForOriginTask = null;
        TMLTask hsmTaskForDestinationTask = null;
        TMLChannel chToHSM;
        TMLChannel chFromHSM;
        TMLChannel chToDestinationHSM = null;
        TMLChannel chFromOriginHSM = null;
        TMLChannel chSendNonceToHSM;
        TMLChannel chReceiveNonceFromHSM;
        if (taskAndItsHSMMap.containsKey(ch.getOriginTask())) {
            hsmTaskForOriginTask = initHSMTaskDiagram(ch.getOriginTask(), "Encrypt_" + ch.getChannelName());
        }
        if (taskAndItsHSMMap.containsKey(ch.getDestinationTask())) {
            hsmTaskForDestinationTask = initHSMTaskDiagram(ch.getDestinationTask(), "Decrypt_" + ch.getChannelName());
            if (isDirectCommunicationHSMs) {
                if (hsmTaskForOriginTask != null) {
                    chToDestinationHSM = findOrCreateChannelToHSM(hsmTaskForDestinationTask, hsmTaskForOriginTask, ch);
                } else  {
                    chToDestinationHSM = findOrCreateChannelToHSM(hsmTaskForDestinationTask, ch.getOriginTask(), ch);
                }
            }
        }
        if (hsmTaskForOriginTask != null) {
            chToHSM = findOrCreateChannelToHSM(hsmTaskForOriginTask, ch.getOriginTask(), ch);
            addToSecurityTaskMap(hsmTaskForOriginTask, secPattern);
            if (!isDirectCommunicationHSMs) {
                chFromHSM = findOrCreateChannelFromHSM(hsmTaskForOriginTask, ch.getOriginTask(), ch);
            } else {
                if (hsmTaskForDestinationTask != null) {
                    chFromHSM = chToDestinationHSM;
                } else {
                    chFromHSM = findOrCreateChannelFromHSM(hsmTaskForOriginTask, ch.getDestinationTask(), ch);
                    chFromHSM.checkConf = true;
                    chFromHSM.checkAuth = true;
                    chFromHSM.setDestinationTask(ch.getDestinationTask());
                    chFromOriginHSM = chFromHSM;
                }
            }

            if (nonceGenerationNeeded) {
                if (isDirectCommunicationHSMs) {
                    chNonce.setDestinationTask(hsmTaskForOriginTask);
                    chSendNonceToHSM = chNonce;
                } else {
                    chSendNonceToHSM = findOrCreateChannelToSendNonceToHSM(chNonce, hsmTaskForOriginTask);
                }
                Pair <TMLActivityElement, TMLActivityElement> headerBottomNonceBranch = addNonceExchangeBranchHSM(hsmTaskForOriginTask, chSendNonceToHSM, spNonce,
                        false);
                try {
                    TMLActivityElement branch = addBranchHSMWithNonce(hsmTaskForOriginTask, chToHSM, chFromHSM, secPattern, true,
                            headerBottomNonceBranch.getKey(), headerBottomNonceBranch.getValue());
                    hsmTaskOperationMap.put(hsmTaskForOriginTask, branch);
                } catch (TMLCheckingError e) {
                    throw new RuntimeException(e);
                }
                for (TMLWriteChannel wr : listWr) {
                    if (isDirectCommunicationHSMs) {
                        communicationWithHSMAD(ch.getOriginTask(), wr, chToHSM,  hsmTaskRequestMap.get(hsmTaskForOriginTask));
                    } else {
                        communicationADWithHSMIncludingNonce(ch.getOriginTask(), wr, secPattern, chToHSM, chFromHSM,
                                hsmTaskRequestMap.get(hsmTaskForOriginTask), spNonce, chNonce, chSendNonceToHSM);
                    }
                }
            } else {
                try {
                    TMLActivityElement branch = addBranchHSM(hsmTaskForOriginTask, chToHSM, chFromHSM, secPattern, true);
                    hsmTaskOperationMap.put(hsmTaskForOriginTask, branch);
                } catch (TMLCheckingError e) {
                    throw new RuntimeException(e);
                }
                for (TMLWriteChannel wr : listWr) {
                    if (isDirectCommunicationHSMs) {
                        communicationWithHSMAD(ch.getOriginTask(), wr, chToHSM,  hsmTaskRequestMap.get(hsmTaskForOriginTask));
                    } else {
                        communicationWithHSMAD(ch.getOriginTask(), wr, secPattern, chToHSM, chFromHSM, hsmTaskRequestMap.get(hsmTaskForOriginTask));
                    }
                }
            }
        } else {
            addToSecurityTaskMap(ch.getOriginTask(), secPattern);
            TMLActivityElement headElem = ch.getOriginTask().getActivityDiagram().getFirst();
            if (nonceGenerationNeeded) {
                headElem = addNonceReceiveAD(ch.getOriginTask(), chNonce, spNonce, listWr, headElem);
            }
            try {
                if (chToDestinationHSM != null) {
                    addEncryptionADConnectedToOtherHSM(ch.getOriginTask(), listWr, secPattern, chToDestinationHSM, headElem);
                } else {
                    addEncryptionAD(ch.getOriginTask(), listWr, secPattern, headElem);
                }
            } catch (TMLCheckingError e) {
                throw new RuntimeException(e);
            }
        }

        if (hsmTaskForDestinationTask != null) {
            chFromHSM = findOrCreateChannelFromHSM(hsmTaskForDestinationTask, ch.getDestinationTask(), ch);
            addToSecurityTaskMap(hsmTaskForDestinationTask, secPattern);
            if (!isDirectCommunicationHSMs) {
                chToHSM = findOrCreateChannelToHSM(hsmTaskForDestinationTask, ch.getDestinationTask(), ch);
            } else {
                chToHSM = chToDestinationHSM;
                chToHSM.checkConf = true;
                chToHSM.checkAuth = true;
            }
            if (nonceGenerationNeeded) {
                if (isDirectCommunicationHSMs) {
                    chNonce.setOriginTask(hsmTaskForDestinationTask);
                    chReceiveNonceFromHSM = chNonce;
                } else {
                    chReceiveNonceFromHSM = findOrCreateChannelToReceiveNonceFromHSM(chNonce, hsmTaskForDestinationTask);
                }
                Pair <TMLActivityElement, TMLActivityElement> headerBottomNonceBranch = addNonceExchangeBranchHSM(hsmTaskForDestinationTask, chReceiveNonceFromHSM,
                        spNonce, true);
                try {
                    TMLActivityElement branch = addBranchHSMWithNonce(hsmTaskForDestinationTask, chToHSM, chFromHSM, secPattern, false,
                            headerBottomNonceBranch.getKey(), headerBottomNonceBranch.getValue());
                    hsmTaskOperationMap.put(hsmTaskForDestinationTask, branch);
                } catch (TMLCheckingError e) {
                    throw new RuntimeException(e);
                }
                for (TMLReadChannel rd : listRd) {
                    if (isDirectCommunicationHSMs) {
                        communicationWithHSMAD(ch.getDestinationTask(), rd, chFromHSM,  hsmTaskRequestMap.get(hsmTaskForDestinationTask));
                    } else {
                        communicationADWithHSMIncludingNonce(ch.getDestinationTask(), rd, secPattern, chToHSM, chFromHSM,
                                hsmTaskRequestMap.get(hsmTaskForDestinationTask), spNonce, chNonce, chReceiveNonceFromHSM);
                    }
                }
            } else {
                try {
                    TMLActivityElement branch = addBranchHSM(hsmTaskForDestinationTask, chToHSM, chFromHSM, secPattern, false);
                    hsmTaskOperationMap.put(hsmTaskForDestinationTask, branch);
                } catch (TMLCheckingError e) {
                    throw new RuntimeException(e);
                }
                for (TMLReadChannel rd : listRd) {
                    if (isDirectCommunicationHSMs) {
                        communicationWithHSMAD(ch.getDestinationTask(), rd, chFromHSM,  hsmTaskRequestMap.get(hsmTaskForDestinationTask));
                    } else {
                        communicationWithHSMAD(ch.getDestinationTask(), rd, secPattern, chToHSM, chFromHSM, hsmTaskRequestMap.get(hsmTaskForDestinationTask));
                    }
                }
            }
        } else {
            if (nonceGenerationNeeded) {
                addNonceSendAD(ch.getDestinationTask(), chNonce, spNonce, listRd, ch.getDestinationTask().getActivityDiagram().getFirst(),
                        hsmTaskForOriginTask != null);
            }
            try {
                if (chFromOriginHSM != null) {
                    addDecryptionADConnectedToOtherHSM(ch.getDestinationTask(), listRd, secPattern, chFromOriginHSM);
                } else {
                    addDecryptionAD(ch.getDestinationTask(), listRd, secPattern);
                }
            } catch (TMLCheckingError e) {
                throw new RuntimeException(e);
            }

            addToSecurityTaskMap(ch.getDestinationTask(), secPattern);
        }
        if (nonceGenerationNeeded && !isDirectCommunicationHSMs) {
            for (HwCommunicationNode commNode : tmap.getAllCommunicationNodesOfChannel(ch)) {
                if (!tmap.isCommNodeMappedOn(chNonce, commNode)) {
                    tmap.addCommToHwCommNode(chNonce, commNode);
                }
            }
        }
        Map<TMLChannel, Set<HwCommunicationNode>> chCommNodesMap = mapHSMChannelsInCommunicationNodes(hsmTaskForOriginTask,
                hsmTaskForDestinationTask, ch);
        for (TMLChannel chCommNode : chCommNodesMap.keySet()) {
            for (HwCommunicationNode commNode : chCommNodesMap.get(chCommNode)) {
                tmap.addCommToHwCommNode(chCommNode, commNode);
            }
        }
        if (isDirectCommunicationHSMs && (hsmTaskForOriginTask != null || hsmTaskForDestinationTask != null)) {
            tmap.getTMLModeling().removeChannel(ch);
            tmap.removeCommMapping(ch);
        }
    }

    private void mergeBusToCommNodeList(List<HwCommunicationNode> listCommNode, Map<TMLChannel, Set<HwCommunicationNode>> chCommNodesMap,
                                      TMLChannel ch) {
        for (HwCommunicationNode commNode : listCommNode) {
            if (!(commNode instanceof HwMemory)) {
                chCommNodesMap.get(ch).add(commNode);
            }
        }
    }

    private void mergeMemoryToCommNodeList(List<HwCommunicationNode> listCommNode, Map<TMLChannel, Set<HwCommunicationNode>> chCommNodesMap,
                                        TMLChannel ch) {
        for (HwCommunicationNode commNode : listCommNode) {
            if ((commNode instanceof HwMemory) || chCommNodesMap.get(ch).stream().noneMatch(c -> c instanceof HwMemory)) {
                chCommNodesMap.get(ch).add(commNode);
            }
        }
    }

    private HwBus getAssociatedBusOfTask(TMLTask task) {
        HwExecutionNode execNode = tmap.getHwNodeOf(task);
        for (HwLink link : tmap.getArch().getHwLinks()) {
            if (link.hwnode == execNode) {
                return link.bus;
            }
        }
        return null;
    }

    private HwMemory getAssociatedMemoryOfBus(HwBus bus) {
        for (HwLink link : tmap.getArch().getHwLinks()) {
            if (link.bus == bus && link.hwnode instanceof HwMemory) {
                return (HwMemory) link.hwnode;
            }
        }
        return null;
    }

    private Map<TMLChannel, Set<HwCommunicationNode>> mapHSMChannelsInCommunicationNodes(TMLTask hsmTaskForOriginTask,
                                                                                         TMLTask hsmTaskForDestinationTask, TMLChannel ch) {
        Map<TMLChannel, Set<HwCommunicationNode>> chCommNodesMap = new HashMap<>();
        TMLModeling<?> tmlmodel = tmap.getTMLModeling();
        if (hsmTaskForOriginTask != null) {
            TMLTask taskOfHSM = hsmTaskRequestMap.get(hsmTaskForOriginTask).getOriginTasks().get(0);
            for (TMLChannel chHSMOrigin : tmlmodel.getChannelsFromMe(hsmTaskForOriginTask)) {
                if (!chCommNodesMap.containsKey(chHSMOrigin)) {
                    chCommNodesMap.put(chHSMOrigin, new LinkedHashSet<>());
                }
                if (taskHSMCommunicationNodesMap.containsKey(taskOfHSM)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(taskOfHSM), chCommNodesMap, chHSMOrigin);
                    mergeMemoryToCommNodeList(taskHSMCommunicationNodesMap.get(taskOfHSM), chCommNodesMap, chHSMOrigin);
                }
                TMLTask corresTask = null;
                if (hsmTaskRequestMap.containsKey(chHSMOrigin.getDestinationTask())) {
                    corresTask = hsmTaskRequestMap.get(chHSMOrigin.getDestinationTask()).getOriginTasks().get(0);
                }
                if (corresTask != null && taskHSMCommunicationNodesMap.containsKey(corresTask)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(corresTask), chCommNodesMap, chHSMOrigin);
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMOrigin);
                } else if (chHSMOrigin.getDestinationTask().equals(ch.getOriginTask())) {
                    chCommNodesMap.get(chHSMOrigin).add(getAssociatedBusOfTask(chHSMOrigin.getDestinationTask()));
                } else {
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMOrigin);
                }
            }

            for (TMLChannel chHSMOrigin : tmlmodel.getChannelsToMe(hsmTaskForOriginTask)) {
                if (!chCommNodesMap.containsKey(chHSMOrigin)) {
                    chCommNodesMap.put(chHSMOrigin, new LinkedHashSet<>());
                }
                if (taskHSMCommunicationNodesMap.containsKey(taskOfHSM)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(taskOfHSM), chCommNodesMap, chHSMOrigin);
                }
                TMLTask corresTask = null;
                if (hsmTaskRequestMap.containsKey(chHSMOrigin.getOriginTask())) {
                    corresTask = hsmTaskRequestMap.get(chHSMOrigin.getOriginTask()).getOriginTasks().get(0);
                }
                if (corresTask != null && taskHSMCommunicationNodesMap.containsKey(corresTask)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(corresTask), chCommNodesMap, chHSMOrigin);
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMOrigin);
                } else if (chHSMOrigin.getOriginTask().equals(ch.getOriginTask())) {
                    mapChannelsInArch(chCommNodesMap, taskOfHSM, chHSMOrigin);
                } else {
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMOrigin);
                    mapChannelsInArch(chCommNodesMap, taskOfHSM, chHSMOrigin);
                }
            }
        }
        if (hsmTaskForDestinationTask != null) {
            TMLTask taskOfHSM = hsmTaskRequestMap.get(hsmTaskForDestinationTask).getOriginTasks().get(0);
            for (TMLChannel chHSMDestination : tmlmodel.getChannelsFromMe(hsmTaskForDestinationTask)) {
                if (!chCommNodesMap.containsKey(chHSMDestination)) {
                    chCommNodesMap.put(chHSMDestination, new LinkedHashSet<>());
                }
                if (taskHSMCommunicationNodesMap.containsKey(taskOfHSM)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(taskOfHSM), chCommNodesMap, chHSMDestination);
                    mergeMemoryToCommNodeList(taskHSMCommunicationNodesMap.get(taskOfHSM), chCommNodesMap, chHSMDestination);
                }
                TMLTask corresTask = null;
                if (hsmTaskRequestMap.containsKey(chHSMDestination.getDestinationTask())) {
                    corresTask = hsmTaskRequestMap.get(chHSMDestination.getDestinationTask()).getOriginTasks().get(0);
                }
                if (corresTask != null && taskHSMCommunicationNodesMap.containsKey(corresTask)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(corresTask), chCommNodesMap, chHSMDestination);
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMDestination);
                } else if (chHSMDestination.getDestinationTask().equals(ch.getDestinationTask())) {
                    chCommNodesMap.get(chHSMDestination).add(getAssociatedBusOfTask(chHSMDestination.getDestinationTask()));
                } else {
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMDestination);
                }
            }
            for (TMLChannel chHSMDestination : tmlmodel.getChannelsToMe(hsmTaskForDestinationTask)) {
                if (!chCommNodesMap.containsKey(chHSMDestination)) {
                    chCommNodesMap.put(chHSMDestination, new LinkedHashSet<>());
                }
                if (taskHSMCommunicationNodesMap.containsKey(taskOfHSM)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(taskOfHSM), chCommNodesMap, chHSMDestination);
                }
                TMLTask corresTask = null;
                if (hsmTaskRequestMap.containsKey(chHSMDestination.getOriginTask())) {
                    corresTask = hsmTaskRequestMap.get(chHSMDestination.getOriginTask()).getOriginTasks().get(0);
                }
                if (corresTask != null && taskHSMCommunicationNodesMap.containsKey(corresTask)) {
                    mergeBusToCommNodeList(taskHSMCommunicationNodesMap.get(corresTask), chCommNodesMap, chHSMDestination);
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMDestination);
                } else if (chHSMDestination.getOriginTask().equals(ch.getDestinationTask())) {
                    mapChannelsInArch(chCommNodesMap, taskOfHSM, chHSMDestination);
                } else {
                    mergeBusToCommNodeList(tmap.getAllCommunicationNodesOfChannel(ch), chCommNodesMap, chHSMDestination);
                    mapChannelsInArch(chCommNodesMap, taskOfHSM, chHSMDestination);
                }
            }
        }

        return chCommNodesMap;
    }

    private void mapChannelsInArch(Map<TMLChannel, Set<HwCommunicationNode>> chCommNodesMap, TMLTask taskOfHSM, TMLChannel chHSMDestination) {
        List<HwCommunicationNode> listCommNode = new ArrayList<>();
        HwBus bus = getAssociatedBusOfTask(chHSMDestination.getOriginTask());
        if (bus != null) {
            listCommNode.add(bus);
            mergeBusToCommNodeList(listCommNode, chCommNodesMap, chHSMDestination);
            HwMemory mem = getAssociatedMemoryOfBus(bus);
            if (mem != null) {
                listCommNode.clear();
                listCommNode.add(mem);
                mergeMemoryToCommNodeList(listCommNode, chCommNodesMap, chHSMDestination);
            } else {
                mergeMemoryToCommNodeList(taskHSMCommunicationNodesMap.get(taskOfHSM), chCommNodesMap, chHSMDestination);
            }
        }
    }

    private TMLTask initHSMTaskDiagram(TMLTask task, String nameHSM) {
        String hsmTaskNameInit = appName + "__" + "HSM_" + task.getTaskName() + "_" + nameHSM;
        int index = 0;
        String hsmTaskName = hsmTaskNameInit;
        while (tmap.getTaskByName(hsmTaskName) != null) {
            hsmTaskName = hsmTaskNameInit + index;
            index ++;
        }
        TMLTask hsm = new TMLTask(hsmTaskName, task.getReferenceObject(), null);
        //TMLAttribute attribute = new TMLAttribute(SELECT_CHOICE_HSM, new TMLType(TMLType.NATURAL), "0");
        //hsm.addAttribute(attribute);
        tmap.getTMLModeling().addTask(hsm);
        if (taskAndItsHSMMap.containsKey(task)) {
            if (!taskAndItsHSMMap.get(task).contains(hsm)) {
                taskAndItsHSMMap.get(task).add(hsm);
            }
        }

        String hsmRequestNameInit = appName + "__" + "req_" + hsm.getTaskName();
        int indexRequest = 0;
        String hsmRequestName = hsmRequestNameInit;
        while (tmap.getTMLModeling().getRequestByName(hsmRequestName) != null) {
            hsmRequestName = hsmRequestNameInit + indexRequest;
            indexRequest ++;
        }
        TMLRequest request = new TMLRequest(hsmRequestName, hsm.getReferenceObject());
        request.addOriginTask(task);
        request.setDestinationTask(hsm);
        hsm.setRequest(request);
        hsm.setRequested(true);
        //task.setRequest(request);
       // request.addParam(new TMLType(TMLType.NATURAL));
        tmap.getTMLModeling().addRequest(request);
        hsmTaskRequestMap.put(hsm, request);
       // taskHSMBranchesMap.put(hsm, new ArrayList<>());
        return hsm;
    }

    private void buildHSMActivityDiagram(TMLTask hsmTask, TMLActivityElement branchHeader) {
        TMLActivity hsmTaskAD = hsmTask.getActivityDiagram();
        TMLActivityElement start = hsmTaskAD.getFirst();
        if (start == null) {
            start = new TMLStartState("start", hsmTaskAD.getReferenceObject());
            hsmTaskAD.setFirst(start);
        }
        start.addNext(branchHeader);
    }

    private void updateHSMArchDiagram(TMLTask hsmTask, HwBus bus) {
        TMLArchitecture arch = tmap.getArch();
        //Add Hardware Accelerator
        HwA hwa = new HwA("HwA_" + hsmTask.getTaskName());
        arch.addHwNode(hwa);

        //Connect Bus and HWA
        HwLink linkHWAWithBus = new HwLink("link_" + hwa.getName() + "_to_" + bus.getName());
        linkHWAWithBus.bus = bus;
        linkHWAWithBus.hwnode = hwa;
        arch.addHwLink(linkHWAWithBus);
        tmap.addTaskToHwExecutionNode(hsmTask, hwa);
    }

    private void initHSMArchDiagram(TMLTask task) {
        TMLArchitecture arch = tmap.getArch();
        List<HwCommunicationNode> communicationNodesOfHSM = new ArrayList<>();
        //Find the CPU where the task is mapped to
        HwExecutionNode cpuTask = tmap.getHwNodeOf(task);

        if (cpuTask == null) {
            return;
        }

        //Add new memory
        HwMemory mem = new HwMemory("Memory_HSM_" + task.getTaskName());
        arch.addHwNode(mem);
        communicationNodesOfHSM.add(mem);

        //Add bus to be connecting to the HWA and new memory
        HwBus bus = new HwBus("Bus_HSM_" + task.getTaskName());
        bus.privacy = HwBus.BUS_PRIVATE;
        arch.addHwNode(bus);
        communicationNodesOfHSM.add(bus);
        taskAndHSMBusMap.put(task, bus);

        //Add bridge to be connecting to the 2 buses
        HwBridge bridge = new HwBridge("Bridge_HSM_" + task.getTaskName());
        arch.addHwNode(bridge);

        //get bus connected to CPU
        HwBus busOfCPUTask = null;
        for (HwLink link : tmap.getArch().getHwLinks()) {
            if (link.hwnode == cpuTask) {
                busOfCPUTask = link.bus;
                break;
            }
        }

        //Connect new Bus and Bridge
        HwLink linkBridgeWithNewBus = new HwLink("link_" + bridge.getName() + "_to_" + bus.getName());
        linkBridgeWithNewBus.bus = bus;
        linkBridgeWithNewBus.hwnode = bridge;
        arch.addHwLink(linkBridgeWithNewBus);

        //Connect the Bus of CPU and Bridge
        if (busOfCPUTask != null) {
            HwLink linkBridgeWithBus = new HwLink("link_" + bridge.getName() + "_to_" + busOfCPUTask.getName());
            linkBridgeWithBus.bus = busOfCPUTask;
            linkBridgeWithBus.hwnode = bridge;
            arch.addHwLink(linkBridgeWithBus);
            communicationNodesOfHSM.add(busOfCPUTask);
        }

        //Connect Bus and Memory
        HwLink linkMemoryWithBus = new HwLink("link_" + mem.getName() + "_to_" + bus.getName());
        linkMemoryWithBus.bus = bus;
        linkMemoryWithBus.hwnode = mem;
        arch.addHwLink(linkMemoryWithBus);

        taskHSMCommunicationNodesMap.put(task, communicationNodesOfHSM);
        List<TMLTask> listTask = new ArrayList<>();
        taskAndItsHSMMap.put(task, listTask);
    }

    private TMLActivityElement addBranchHSM(TMLTask hsmTask, TMLChannel dataCh, TMLChannel retrieveDataCh, SecurityPattern sp,
                                            boolean isEncryption) throws TMLCheckingError {
        TMLActivity hsmTaskAD = hsmTask.getActivityDiagram();
        TMLReadChannel rd = new TMLReadChannel(dataCh.getName(), hsmTaskAD.getReferenceObject());
        rd.addChannel(dataCh);
        rd.setNbOfSamples("1");
        if (!isEncryption) {
            rd.setSecurityPattern(sp);
        }
        hsmTaskAD.addElement(rd);

        TMLExecC exec = new TMLExecC(sp.getName(), hsmTaskAD.getReferenceObject());
        exec.setSecurityPattern(sp);
        exec.setDecryptionProcess(!isEncryption);
        exec.setAction(Integer.toString(sp.getEncTime()));
        hsmTaskAD.addElement(exec);
        rd.addNext(exec);

        TMLWriteChannel wr = new TMLWriteChannel(retrieveDataCh.getName(), hsmTaskAD.getReferenceObject());
        wr.addChannel(retrieveDataCh);
        hsmTaskAD.addElement(wr);
        wr.setNbOfSamples("1");
        if (isEncryption) {
            wr.setSecurityPattern(sp);
        }
        exec.addNext(wr);

        TMLStopState stop = new TMLStopState("stop", hsmTaskAD.getReferenceObject());
        hsmTaskAD.addElement(stop);
        //Connect stop and write channel
        wr.addNext(stop);

        return rd;
    }

    private TMLActivityElement addBranchHSMWithNonce(TMLTask hsmTask, TMLChannel dataCh, TMLChannel retrieveDataCh, SecurityPattern sp,
                                            boolean isEncryption, TMLActivityElement header, TMLActivityElement bottom) throws TMLCheckingError {
        TMLActivity hsmTaskAD = hsmTask.getActivityDiagram();

        TMLReadChannel rd = new TMLReadChannel(dataCh.getName(), hsmTaskAD.getReferenceObject());
        rd.addChannel(dataCh);
        rd.setNbOfSamples("1");
        if (!isEncryption) {
            rd.setSecurityPattern(sp);
        }
        hsmTaskAD.addElement(rd);
        bottom.addNext(rd);

        TMLExecC exec = new TMLExecC(sp.getName(), hsmTaskAD.getReferenceObject());
        exec.setSecurityPattern(sp);
        exec.setDecryptionProcess(!isEncryption);
        exec.setAction(Integer.toString(sp.getEncTime()));
        hsmTaskAD.addElement(exec);
        rd.addNext(exec);

        TMLWriteChannel wr = new TMLWriteChannel(retrieveDataCh.getName(), hsmTaskAD.getReferenceObject());
        wr.addChannel(retrieveDataCh);
        hsmTaskAD.addElement(wr);
        wr.setNbOfSamples("1");
        if (isEncryption) {
            wr.setSecurityPattern(sp);
        }
        exec.addNext(wr);

        TMLStopState stop = new TMLStopState("stop", hsmTaskAD.getReferenceObject());
        hsmTaskAD.addElement(stop);
        //Connect stop and write channel
        wr.addNext(stop);

        return header;
    }

    private Pair<TMLActivityElement, TMLActivityElement> addNonceExchangeBranchHSM(TMLTask hsmTask, TMLChannel nonceCh, SecurityPattern sp,
                                                                                   boolean isNonceSender) {
        TMLActivity hsmTaskAD = hsmTask.getActivityDiagram();
        if (isNonceSender) {
            TMLExecC exec = new TMLExecC(sp.getName(), hsmTaskAD.getReferenceObject());
            exec.setSecurityPattern(sp);
            exec.setAction(Integer.toString(sp.getEncTime()));
            hsmTaskAD.addElement(exec);

            TMLWriteChannel wr = new TMLWriteChannel(nonceCh.getName(), hsmTaskAD.getReferenceObject());
            wr.addChannel(nonceCh);
            hsmTaskAD.addElement(wr);
            wr.setNbOfSamples("1");
            wr.setSecurityPattern(sp);
            exec.addNext(wr);

            return new Pair<>(exec, wr);
        } else {
            TMLReadChannel rd = new TMLReadChannel(nonceCh.getName(), hsmTaskAD.getReferenceObject());
            rd.addChannel(nonceCh);
            rd.setSecurityPattern(sp);
            rd.setNbOfSamples("1");
            hsmTaskAD.addElement(rd);

            return new Pair<>(rd, rd);
        }
    }
    private void communicationADWithHSMIncludingNonce(TMLTask task, TMLActivityElementChannel comm, SecurityPattern sp, TMLChannel dataCh,
                                        TMLChannel retrieveDataCh, TMLRequest request, SecurityPattern spNonce, TMLChannel chNonce,
                                        TMLChannel chNonceWithHSM) {
        TMLActivity taskAD = task.getActivityDiagram();
        TMLSendRequest reqSend = new TMLSendRequest(request.getName(), taskAD.getReferenceObject());
        reqSend.setRequest(request);
        //reqSend.addParam(Integer.toString(indexRequest));
        taskAD.addElement(reqSend);

        TMLWriteChannel wr = new TMLWriteChannel(dataCh.getName(), taskAD.getReferenceObject());
        wr.addChannel(dataCh);
        wr.setNbOfSamples(comm.getNbOfSamples());
        taskAD.addElement(wr);

        TMLReadChannel rd = new TMLReadChannel(retrieveDataCh.getName(), taskAD.getReferenceObject());
        rd.addChannel(retrieveDataCh);
        rd.setNbOfSamples(comm.getNbOfSamples());
        taskAD.addElement(rd);

        TMLActivityElement prevComm = taskAD.getPrevious(comm);

        comm.setSecurityPattern(sp);

        if (comm instanceof TMLWriteChannel) {
            TMLWriteChannel wrNonceToHSM = new TMLWriteChannel(chNonceWithHSM.getName(), taskAD.getReferenceObject());
            wrNonceToHSM.addChannel(chNonceWithHSM);
            wrNonceToHSM.setNbOfSamples("1");
            wrNonceToHSM.setSecurityPattern(spNonce);
            taskAD.addElement(wrNonceToHSM);

            TMLReadChannel rdNonce = new TMLReadChannel(chNonce.getName(), taskAD.getReferenceObject());
            rdNonce.addChannel(chNonce);
            rdNonce.setNbOfSamples("1");
            rdNonce.setSecurityPattern(spNonce);
            taskAD.addElement(rdNonce);

            rd.setSecurityPattern(sp);

            prevComm.setNewNext(comm, rdNonce);
            rdNonce.addNext(reqSend);
            reqSend.addNext(wrNonceToHSM);
            wrNonceToHSM.addNext(wr);
            wr.addNext(rd);
            rd.addNext(comm);

        } else if (comm instanceof TMLReadChannel) {
            TMLReadChannel rdNonceFromHSM = new TMLReadChannel(chNonceWithHSM.getName(), taskAD.getReferenceObject());
            rdNonceFromHSM.addChannel(chNonceWithHSM);
            rdNonceFromHSM.setNbOfSamples("1");
            rdNonceFromHSM.setSecurityPattern(spNonce);
            taskAD.addElement(rdNonceFromHSM);

            TMLWriteChannel wrNonce = new TMLWriteChannel(chNonce.getName(), taskAD.getReferenceObject());
            wrNonce.addChannel(chNonce);
            wrNonce.setNbOfSamples("1");
            wrNonce.setSecurityPattern(spNonce);
            taskAD.addElement(wrNonce);

            wr.setSecurityPattern(sp);

            TMLActivityElement origNextComm = comm.getNextElement(0);

            prevComm.setNewNext(comm, reqSend);
            reqSend.addNext(rdNonceFromHSM);
            rdNonceFromHSM.addNext(wrNonce);
            wrNonce.addNext(comm);
            comm.setNewNext(origNextComm, wr);
            wr.addNext(rd);
            rd.addNext(origNextComm);
        }
    }

    private void communicationWithHSMAD(TMLTask task, TMLActivityElementChannel comm, SecurityPattern sp, TMLChannel dataCh,
                                        TMLChannel retrieveDataCh, TMLRequest request) {
        TMLActivity taskAD = task.getActivityDiagram();
        TMLSendRequest reqSend = new TMLSendRequest(request.getName(), taskAD.getReferenceObject());
        reqSend.setRequest(request);
        //reqSend.addParam(Integer.toString(indexRequest));
        taskAD.addElement(reqSend);

        TMLWriteChannel wr = new TMLWriteChannel(dataCh.getName(), taskAD.getReferenceObject());
        wr.addChannel(dataCh);
        wr.setNbOfSamples(comm.getNbOfSamples());
        taskAD.addElement(wr);

        TMLReadChannel rd = new TMLReadChannel(retrieveDataCh.getName(), taskAD.getReferenceObject());
        rd.addChannel(retrieveDataCh);
        rd.setNbOfSamples(comm.getNbOfSamples());
        taskAD.addElement(rd);

        TMLActivityElement prevComm = taskAD.getPrevious(comm);


        comm.setSecurityPattern(sp);
        if (comm instanceof TMLWriteChannel) {
            prevComm.setNewNext(comm, reqSend);
            rd.setSecurityPattern(sp);
            reqSend.addNext(wr);
            wr.addNext(rd);
            rd.addNext(comm);

        } else if (comm instanceof TMLReadChannel) {
            wr.setSecurityPattern(sp);
            TMLActivityElement origNextComm = comm.getNextElement(0);
            comm.setNewNext(origNextComm, reqSend);
            reqSend.addNext(wr);
            wr.addNext(rd);
            rd.addNext(origNextComm);
        }
    }

    private void communicationWithHSMAD(TMLTask task, TMLActivityElementChannel comm, TMLChannel dataCh, TMLRequest request) {
        TMLActivity taskAD = task.getActivityDiagram();
        TMLSendRequest reqSend = new TMLSendRequest(request.getName(), taskAD.getReferenceObject());
        reqSend.setRequest(request);
        taskAD.addElement(reqSend);
        TMLActivityElement prevComm = taskAD.getPrevious(comm);
        TMLActivityElement origNextComm = comm.getNextElement(0);

        if (comm instanceof TMLWriteChannel) {
            TMLWriteChannel wr = new TMLWriteChannel(dataCh.getName(), taskAD.getReferenceObject());
            wr.addChannel(dataCh);
            wr.setNbOfSamples(comm.getNbOfSamples());
            taskAD.addElement(wr);
            prevComm.setNewNext(comm, reqSend);
            reqSend.addNext(wr);
            taskAD.removeElement(comm);
            wr.addNext(origNextComm);
        } else if (comm instanceof TMLReadChannel) {
            TMLReadChannel rd = new TMLReadChannel(dataCh.getName(), taskAD.getReferenceObject());
            rd.addChannel(dataCh);
            rd.setNbOfSamples(comm.getNbOfSamples());
            taskAD.addElement(rd);
            prevComm.setNewNext(comm, reqSend);
            reqSend.addNext(rd);
            taskAD.removeElement(comm);
            rd.addNext(origNextComm);
        }
    }


    private void addDecryptionAD(TMLTask task, List<TMLReadChannel> readComms, SecurityPattern sp) throws TMLCheckingError {
        TMLActivity taskAD = task.getActivityDiagram();
        for (TMLReadChannel readComm : readComms) {
            TMLExecC exec = new TMLExecC(sp.getName(), taskAD.getReferenceObject());
            exec.setSecurityPattern(sp);
            exec.setDecryptionProcess(true);
            exec.setAction(Integer.toString(sp.getEncTime()));
            taskAD.addElement(exec);

            readComm.setSecurityPattern(sp);
            TMLActivityElement origNextComm = readComm.getNextElement(0);
            readComm.setNewNext(origNextComm, exec);
            exec.addNext(origNextComm);
        }
    }

    private void addDecryptionADConnectedToOtherHSM(TMLTask task, List<TMLReadChannel> readComms, SecurityPattern sp, TMLChannel chToDestHSM) throws TMLCheckingError {
        TMLActivity taskAD = task.getActivityDiagram();
        for (TMLReadChannel readComm : readComms) {
            TMLExecC exec = new TMLExecC(sp.getName(), taskAD.getReferenceObject());
            exec.setSecurityPattern(sp);
            exec.setDecryptionProcess(true);
            exec.setAction(Integer.toString(sp.getEncTime()));
            taskAD.addElement(exec);

            readComm.replaceChannelWith(readComm.getChannel(0), chToDestHSM);
            readComm.setName(chToDestHSM.getName());
            readComm.setSecurityPattern(sp);
            TMLActivityElement origNextComm = readComm.getNextElement(0);
            readComm.setNewNext(origNextComm, exec);
            exec.addNext(origNextComm);
        }
    }

    private void addNonceSendAD(TMLTask task, TMLChannel nonceCh, SecurityPattern spNonce, List<TMLReadChannel> readComms,
                                TMLActivityElement headerElement, boolean origTaskHasHSM) {
        TMLActivity taskAD = task.getActivityDiagram();

        TMLExecC exec = new TMLExecC(spNonce.getName(), taskAD.getReferenceObject());
        exec.setSecurityPattern(spNonce);
        exec.setAction(Integer.toString(spNonce.getEncTime()));
        taskAD.addElement(exec);
        for (TMLReadChannel readComm : readComms) {
            TMLWriteChannel wrNonce = new TMLWriteChannel(nonceCh.getName(), taskAD.getReferenceObject());
            wrNonce.addChannel(nonceCh);
            wrNonce.setNbOfSamples("1");
            wrNonce.setSecurityPattern(spNonce);
            TMLActivityElement prevOfRead = taskAD.getPrevious(readComm);
            if (readComms.size() == 1) {
                prevOfRead.setNewNext(readComm, exec);
                exec.addNext(wrNonce);
            } else {
                prevOfRead.setNewNext(readComm, wrNonce);
            }
            wrNonce.addNext(readComm);
            taskAD.addElement(wrNonce);
        }
        if (readComms.size() > 1) {
            if (origTaskHasHSM) {
                TMLActivityElement origNextComm = headerElement.getNextElement(0);
                headerElement.setNewNext(origNextComm, exec);
                exec.addNext(origNextComm);
            } else {
                TMLWriteChannel wrNonce = new TMLWriteChannel(nonceCh.getName(), taskAD.getReferenceObject());
                wrNonce.addChannel(nonceCh);
                wrNonce.setNbOfSamples("1");
                wrNonce.setSecurityPattern(spNonce);
                taskAD.addElement(wrNonce);

                TMLActivityElement origNextComm = headerElement.getNextElement(0);
                headerElement.setNewNext(origNextComm, exec);
                exec.addNext(wrNonce);
                wrNonce.addNext(origNextComm);
            }

        }
    }


    private TMLActivityElement addNonceReceiveAD(TMLTask task, TMLChannel nonceCh, SecurityPattern spNonce, List<TMLWriteChannel> writeComms,
                                   TMLActivityElement headerElement) {
        TMLActivity taskAD = task.getActivityDiagram();
        for(TMLWriteChannel writeCh : writeComms) {
            TMLReadChannel rdNonce = new TMLReadChannel(nonceCh.getName(), taskAD.getReferenceObject());
            rdNonce.addChannel(nonceCh);
            rdNonce.setNbOfSamples("1");
            rdNonce.setSecurityPattern(spNonce);
            taskAD.addElement(rdNonce);
            TMLActivityElement prevOfWrite = taskAD.getPrevious(writeCh);
            prevOfWrite.setNewNext(writeCh, rdNonce);
            rdNonce.addNext(writeCh);
        }
        if (writeComms.size() > 1) {
            TMLReadChannel rdNonce = new TMLReadChannel(nonceCh.getName(), taskAD.getReferenceObject());
            rdNonce.addChannel(nonceCh);
            rdNonce.setNbOfSamples("1");
            rdNonce.setSecurityPattern(spNonce);
            taskAD.addElement(rdNonce);

            TMLActivityElement origNextComm = headerElement.getNextElement(0);
            headerElement.setNewNext(origNextComm, rdNonce);
            rdNonce.addNext(origNextComm);
            return rdNonce;
        }
        return headerElement;
    }

    private void addEncryptionAD(TMLTask task, List<TMLWriteChannel> writeComms, SecurityPattern sp, TMLActivityElement headerElement) throws TMLCheckingError {
        TMLActivity taskAD = task.getActivityDiagram();
        TMLExecC exec = new TMLExecC(sp.getName(), taskAD.getReferenceObject());
        exec.setSecurityPattern(sp);
        exec.setDecryptionProcess(false);
        exec.setAction(Integer.toString(sp.getEncTime()));
        taskAD.addElement(exec);
        if (writeComms.size() == 1) {
            TMLActivityElement prevOfWrite = taskAD.getPrevious(writeComms.get(0));
            prevOfWrite.setNewNext(writeComms.get(0), exec);
            exec.addNext(writeComms.get(0));
            writeComms.get(0).setSecurityPattern(sp);
        } else {
            TMLActivityElement origNextHeader = headerElement.getNextElement(0);
            headerElement.setNewNext(origNextHeader, exec);
            exec.addNext(origNextHeader);
            for (TMLWriteChannel writeComm : writeComms) {
                writeComm.setSecurityPattern(sp);
            }
        }
    }

    private void addEncryptionADConnectedToOtherHSM(TMLTask task, List<TMLWriteChannel> writeComms, SecurityPattern sp, TMLChannel chToDestHSM,
                                                    TMLActivityElement headerElement) throws TMLCheckingError {
        TMLActivity taskAD = task.getActivityDiagram();
        TMLExecC exec = new TMLExecC(sp.getName(), taskAD.getReferenceObject());
        exec.setSecurityPattern(sp);
        exec.setDecryptionProcess(false);
        exec.setAction(Integer.toString(sp.getEncTime()));
        taskAD.addElement(exec);
        if (writeComms.size() == 1) {
            writeComms.get(0).replaceChannelWith(writeComms.get(0).getChannel(0), chToDestHSM);
            writeComms.get(0).setName(chToDestHSM.getName());
            TMLActivityElement prevOfWrite = taskAD.getPrevious(writeComms.get(0));
            prevOfWrite.setNewNext(writeComms.get(0), exec);
            exec.addNext(writeComms.get(0));
            writeComms.get(0).setSecurityPattern(sp);
        } else {
            TMLActivityElement origNextHeader = headerElement.getNextElement(0);
            headerElement.setNewNext(origNextHeader, exec);
            exec.addNext(origNextHeader);
            for (TMLWriteChannel writeComm : writeComms) {
                writeComm.replaceChannelWith(writeComm.getChannel(0), chToDestHSM);
                writeComm.setName(chToDestHSM.getName());
                writeComm.setSecurityPattern(sp);
            }
        }
    }

    private List<TMLReadChannel> readOperatorsOfChannel(TMLTask task, TMLChannel ch) {
        List<TMLReadChannel> elems = new ArrayList<>();
        for (TMLActivityElement elem : task.getActivityDiagram().getElements()) {
            if (elem instanceof TMLReadChannel) {
                TMLReadChannel rc = (TMLReadChannel) elem;
                for (int i = 0; i < rc.getNbOfChannels(); i++) {
                    if (rc.getChannel(i).equals(ch)) {
                        elems.add(rc);
                    }
                }
            }
        }
        return elems;
    }

    private List<TMLWriteChannel> writeOperatorsOfChannel(TMLTask task, TMLChannel ch) {
        List<TMLWriteChannel> elems = new ArrayList<>();
        for (TMLActivityElement elem : task.getActivityDiagram().getElements()) {
            if (elem instanceof TMLWriteChannel) {
                TMLWriteChannel wc = (TMLWriteChannel) elem;
                for (int i = 0; i < wc.getNbOfChannels(); i++) {
                    if (wc.getChannel(i).equals(ch)) {
                        elems.add(wc);
                    }
                }
            }
        }
        return elems;
    }

    private SecurityPattern findOrCreateNonceSecurityPattern(TMLChannel ch) {
        if (channelNonceSPMap.containsKey(ch)) {
            return channelNonceSPMap.get(ch);
        }
        SecurityPattern secPatternNonce =
                new SecurityPattern("nonce_" + ch.getDestinationTask().getTaskName() + "_" + ch.getOriginTask().getTaskName() + "_" + ch.getChannelName(),
                SecurityPattern.NONCE_PATTERN, overhead, "", encComp, decComp, "", "", "");
        channelNonceSPMap.put(ch, secPatternNonce);
        tmap.getTMLModeling().addSecurityPattern(secPatternNonce);
        return secPatternNonce;
    }

    private boolean nonceChannelIsCreated(TMLChannel ch) {
        return channelTochannelNonceMap.containsKey(ch);
    }

    private TMLChannel findOrCreateNonceChannel(TMLChannel ch) {
        if (nonceChannelIsCreated(ch)) {
            return channelTochannelNonceMap.get(ch);
        }

        String channelNameInit =
                appName + "__" + "nonceCh" + ch.getDestinationTask().getTaskName() + "_" + ch.getOriginTask().getTaskName() + "_" + ch.getChannelName();
        int index = 0;
        String channelName = channelNameInit;
        while (tmap.getChannelByName(channelName) != null) {
            channelName = channelNameInit + index;
            index ++;
        }

        TMLChannel chNonce = new TMLChannel(channelName, ch.getReferenceObject());
        chNonce.setOriginTask(ch.getDestinationTask());
        chNonce.setDestinationTask(ch.getOriginTask());
        chNonce.setPorts(new TMLPort(chNonce.getName(), chNonce.getReferenceObject()), new TMLPort(chNonce.getName(), chNonce.getReferenceObject()));
        tmap.getTMLModeling().addChannel(chNonce);

        channelTochannelNonceMap.put(ch, chNonce);
        return chNonce;
    }

    private TMLChannel findOrCreateChannelToSendNonceToHSM(TMLChannel chNonce, TMLTask hsmOfDestinationTaskNonce) {
        if (channelNonceToHsmMap.containsKey(chNonce)) {
            return channelNonceToHsmMap.get(chNonce);
        }
        TMLTask originTaskOfNonce = chNonce.getOriginTask();
        TMLTask destinationTaskOfNonce = chNonce.getDestinationTask();
        String channelNameHSMInit = appName + "__" + "dataNonceCh" + destinationTaskOfNonce.getTaskName() + "_" + originTaskOfNonce.getTaskName();
        int indexHSM = 0;
        String channelNameHSM = channelNameHSMInit;
        while (tmap.getChannelByName(channelNameHSM) != null) {
            channelNameHSM = channelNameHSMInit + indexHSM;
            indexHSM ++;
        }
        TMLChannel chNonceHSMOfDestinationTask = new TMLChannel(channelNameHSM, destinationTaskOfNonce.getReferenceObject());
        chNonceHSMOfDestinationTask.setOriginTask(destinationTaskOfNonce);
        chNonceHSMOfDestinationTask.setDestinationTask(hsmOfDestinationTaskNonce);
        chNonceHSMOfDestinationTask.setPorts(new TMLPort(chNonceHSMOfDestinationTask.getName(), chNonceHSMOfDestinationTask.getReferenceObject()),
                new TMLPort(chNonceHSMOfDestinationTask.getName(), chNonceHSMOfDestinationTask.getReferenceObject()));
        tmap.getTMLModeling().addChannel(chNonceHSMOfDestinationTask);
        channelNonceToHsmMap.put(chNonce, chNonceHSMOfDestinationTask);
        return chNonceHSMOfDestinationTask;
    }

    private TMLChannel findOrCreateChannelToReceiveNonceFromHSM(TMLChannel chNonce, TMLTask hsmOfOriginTaskNonce) {
        if (channelNonceFromHsmMap.containsKey(chNonce)) {
            return channelNonceFromHsmMap.get(chNonce);
        }
        TMLTask originTaskOfNonce = chNonce.getOriginTask(); ;
        TMLTask destinationTaskOfNonce = chNonce.getDestinationTask();
        String channelNameHSMInit = appName + "__" + "retDataNonceCh" + destinationTaskOfNonce.getTaskName() + "_" + originTaskOfNonce.getTaskName();
        int indexHSM = 0;
        String channelNameHSM = channelNameHSMInit;
        while (tmap.getChannelByName(channelNameHSM) != null) {
            channelNameHSM = channelNameHSMInit + indexHSM;
            indexHSM ++;
        }

        TMLChannel chNonceHSMOfOriginTask = new TMLChannel(channelNameHSM, hsmOfOriginTaskNonce.getReferenceObject());
        chNonceHSMOfOriginTask.setOriginTask(hsmOfOriginTaskNonce);
        chNonceHSMOfOriginTask.setDestinationTask(originTaskOfNonce);
        chNonceHSMOfOriginTask.setPorts(new TMLPort(chNonceHSMOfOriginTask.getName(), chNonceHSMOfOriginTask.getReferenceObject()),
                new TMLPort(chNonceHSMOfOriginTask.getName(), chNonceHSMOfOriginTask.getReferenceObject()));
        tmap.getTMLModeling().addChannel(chNonceHSMOfOriginTask);
        channelNonceFromHsmMap.put(chNonce, chNonceHSMOfOriginTask);
        return chNonceHSMOfOriginTask;
    }

    private TMLChannel findOrCreateChannelToHSM(TMLTask hsmTask, TMLTask task, TMLChannel ch) {
        Pair<TMLTask, TMLChannel> taskChannelPair = new Pair<>(task, ch);
        if (channelToHsmMap.containsKey(taskChannelPair)) {
            return channelToHsmMap.get(taskChannelPair);
        }
        String channelNameHSMInit = appName + "__" + "dataCh_" + task.getTaskName() + "_" + ch.getChannelName();
        int indexHSM = 0;
        String channelNameHSM = channelNameHSMInit;
        while (tmap.getChannelByName(channelNameHSM) != null) {
            channelNameHSM = channelNameHSMInit + indexHSM;
            indexHSM ++;
        }
        TMLChannel chToHSM = new TMLChannel(channelNameHSM, task.getReferenceObject());
        chToHSM.setOriginTask(task);
        chToHSM.setDestinationTask(hsmTask);
        chToHSM.setPorts(new TMLPort(chToHSM.getName(), chToHSM.getReferenceObject()),
                new TMLPort(chToHSM.getName(), chToHSM.getReferenceObject()));
        tmap.getTMLModeling().addChannel(chToHSM);
        channelToHsmMap.put(taskChannelPair, chToHSM);

        return chToHSM;
    }

    private TMLChannel findOrCreateChannelFromHSM(TMLTask hsmTask, TMLTask task, TMLChannel ch) {
        Pair<TMLTask, TMLChannel> taskChannelPair = new Pair<>(task, ch);
        if (channelFromHsmMap.containsKey(taskChannelPair)) {
            return channelFromHsmMap.get(taskChannelPair);
        }
        String channelNameHSMInit = appName + "__" + "retDataCh_" + task.getTaskName() + "_" + ch.getChannelName();
        int indexHSM = 0;
        String channelNameHSM = channelNameHSMInit;
        while (tmap.getChannelByName(channelNameHSM) != null) {
            channelNameHSM = channelNameHSMInit + indexHSM;
            indexHSM ++;
        }
        TMLChannel chFromHSM = new TMLChannel(channelNameHSM, task.getReferenceObject());
        chFromHSM.setOriginTask(hsmTask);
        chFromHSM.setDestinationTask(task);
        chFromHSM.setPorts(new TMLPort(chFromHSM.getName(), chFromHSM.getReferenceObject()),
                new TMLPort(chFromHSM.getName(), chFromHSM.getReferenceObject()));
        tmap.getTMLModeling().addChannel(chFromHSM);
        channelFromHsmMap.put(taskChannelPair, chFromHSM);

        return chFromHSM;
    }

    private void addToSecurityTaskMap(TMLTask task, SecurityPattern sec) {
        TMLModeling<?> tmlmodel = tmap.getTMLModeling();
        if (tmlmodel.getSecurityTaskMap().containsKey(sec)) {
            if (!tmlmodel.getSecurityTaskMap().get(sec).contains(task)) {
                tmlmodel.getSecurityTaskMap().get(sec).add(task);
            }
        } else {
            List<TMLTask> listTask = new ArrayList<>();
            listTask.add(task);
            tmlmodel.getSecurityTaskMap().put(sec, listTask);
        }
    }

    public TMLMapping<?> autoMapKeys() {
        if (tmap == null) {
            return null;
        }
        //Find all Security Patterns, if they don't have an associated memory at encrypt and decrypt, tmap them
        TMLModeling<?> tmlm = tmap.getTMLModeling();
        if (tmlm.getSecurityTaskMap() == null) {
            return tmap;
        }
        for (SecurityPattern sp : tmlm.getSecurityTaskMap().keySet()) {
            if (sp.getType().equals(SecurityPattern.SYMMETRIC_ENC_PATTERN) || sp.getType().equals(SecurityPattern.MAC_PATTERN) ||
                    sp.getType().equals(SecurityPattern.ASYMMETRIC_ENC_PATTERN)) {
                for (TMLTask t : tmlm.getSecurityTaskMap().get(sp)) {
                    HwExecutionNode node1 = tmap.getHwNodeOf(t);
                    boolean taskMappedToCPU = false;
                    if (node1 != null) {
                        HwExecutionNode cpuNode = node1;
                        taskMappedToCPU = true;
                        boolean keyMappedtoMem = false;
                        HwLink lastLink = null;
                        for (int i=0; i < tmap.getArch().getHwLinks().size(); i++) {
                            HwLink link = tmap.getArch().getHwLinks().get(i);
                            if (!keyMappedtoMem && link.hwnode == node1) {
                                lastLink = link;
                                if (link.bus.privacy == 1) {
                                    HwBus curBus = link.bus;
                                    boolean keyFound = false;
                                    HwMemory memNodeToMap = null;
                                    for (HwLink linkBus : tmap.getArch().getHwLinks()) {
                                        if (linkBus.bus == curBus) {
                                            if (linkBus.hwnode instanceof HwMemory) {
                                                memNodeToMap = (HwMemory) linkBus.hwnode;
                                                List<SecurityPattern> keys = tmap.getMappedPatterns(memNodeToMap);
                                                if (keys.contains(sp)) {
                                                    keyFound = true;
                                                    keyMappedtoMem = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (!keyFound) {
                                        if (memNodeToMap != null) {
                                            TraceManager.addDev("Adding " + sp.getName() + " key to " + memNodeToMap.getName());
                                            tmap.addSecurityPattern(memNodeToMap, sp);
                                        } else {
                                            HwMemory newHwMemory = new HwMemory(cpuNode.getName() + "KeysMemory");
                                            TraceManager.addDev("Creating new memory: " + newHwMemory.getName());
                                            tmap.getArch().addHwNode(newHwMemory);

                                            //Connect Bus and Memory
                                            HwLink linkNewMemWithBus = new HwLink("link_" + newHwMemory.getName() + "_to_" + curBus.getName());
                                            linkNewMemWithBus.setNodes(curBus, newHwMemory);
                                            tmap.getArch().getHwLinks().add(linkNewMemWithBus);
                                            tmap.addSecurityPattern(newHwMemory, sp);
                                            TraceManager.addDev("Adding " + sp.getName() + " key to " + newHwMemory.getName());
                                        }
                                        keyMappedtoMem = true;
                                    }
                                }
                            }
                        }
                        if (!keyMappedtoMem) {
                            if (lastLink != null) {
                                HwBus lastBusNode = lastLink.bus;
                                HwExecutionNode cpuArchiNode = cpuNode;

                                HwBridge newBridge = new HwBridge(cpuNode.getName() + "KeysBrigde");
                                tmap.getArch().addHwNode(newBridge);

                                HwBus newPrivateBus = new HwBus(cpuNode.getName() + "KeysPrivateBus");
                                newPrivateBus.privacy = HwBus.BUS_PRIVATE;
                                for (TMLElement elem : tmap.getLisMappedChannels(lastBusNode)) {
                                    if (elem instanceof TMLChannel) {
                                        tmap.addCommToHwCommNode(elem, newPrivateBus);
                                    }
                                }
                                tmap.getArch().addHwNode(newPrivateBus);

                                HwMemory memNodeToMap = new HwMemory(cpuNode.getName() + "KeysMemory");
                                TraceManager.addDev("Creating new memory: " + memNodeToMap.getName());
                                tmap.getArch().addHwNode(memNodeToMap);

                                tmap.addSecurityPattern(memNodeToMap, sp);
                                TraceManager.addDev("Adding " + sp.getName() + " key to " + memNodeToMap.getName());

                                //Connect Bus and Memory
                                HwLink newLinkBusMemory = new HwLink("Link_"+newPrivateBus.getName() + "_" + memNodeToMap.getName());
                                newLinkBusMemory.setNodes(newPrivateBus, memNodeToMap);
                                tmap.getArch().addHwLink(newLinkBusMemory);

                                //Connect new Private Bus and Bridge
                                HwLink newLinkPrivateBusBridge = new HwLink("Link_"+newPrivateBus.getName() + "_" + newBridge.getName());
                                newLinkPrivateBusBridge.setNodes(newPrivateBus, newBridge);
                                tmap.getArch().addHwLink(newLinkPrivateBusBridge);

                                //Connect Public Bus and Bridge
                                HwLink newLinkPublicBusBridge = new HwLink("Link_"+lastBusNode.getName() + "_" + newBridge.getName());
                                newLinkPublicBusBridge.setNodes(lastLink.bus, newBridge);
                                tmap.getArch().addHwLink(newLinkPublicBusBridge);

                                //Connect new Private Bus and CPU
                                HwLink newLinkPrivateBusCPU = new HwLink("Link_"+newPrivateBus.getName() + "_" + cpuArchiNode.getName());
                                newLinkPrivateBusCPU.setNodes(newPrivateBus, cpuArchiNode);
                                tmap.getArch().addHwLink(newLinkPrivateBusCPU);


                                //Disconnect Public Bus and CPU
                                HwLink linkToRemove = null;
                                for (HwLink li: tmap.getArch().getHwLinks()) {
                                    if (li.bus == lastLink.bus && li.hwnode == cpuNode) {
                                        TraceManager.addDev("Disconnect :" + li.bus.getName() + " and " + li.hwnode.getName());
                                        linkToRemove = li;
                                        break;
                                    }
                                }
                                if (linkToRemove != null) {
                                    tmap.getArch().getHwLinks().remove(linkToRemove);
                                }
                            }
                        }
                    }
                    if (!taskMappedToCPU) {
                        TraceManager.addDev(t.getTaskName() + " has to be mapped to a CPU!");
                    }
                }
            }
        }
        TraceManager.addDev("Mapping finished");
        return tmap;
    }

    public void setTimeOutInSeconds(int timeOutInSeconds) {
        this.timeOutInSeconds = timeOutInSeconds;
    }

}
