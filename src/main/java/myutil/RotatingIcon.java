/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 * 
 * ludovic.apvrille AT enst.fr
 * 
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */





package myutil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;


/**
* Class RotatingIcon
* Creation: 30/05/2024
* @version 1.1 30/05/2024
* @author Ludovic APVRILLE
*/
public final class RotatingIcon {

    private final Timer timer;
    private final JButton button;
    private final Icon icon;
    private int angle;
    private boolean rotating;

    public RotatingIcon(JButton component, Icon icon) {
        this.button = component;
        this.icon = icon;
        this.angle = 0;
        this.rotating = false;

        // Update the icon at regular intervals
        timer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                angle += 10; // Increment the angle
                if (angle >= 360) {
                    angle = 0;
                }
                updateIcon();
            }
        });
    }

    public void start() {
        if (!rotating) {
            rotating = true;
            timer.start();
        }
    }

    public void stop() {
        if (rotating) {
            rotating = false;
            timer.stop();
        }
        button.setIcon(icon);
    }

    private void updateIcon() {
        // Create a rotated version of the original icon
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = image.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.rotate(Math.toRadians(angle), icon.getIconWidth() / 2, icon.getIconHeight() / 2);
        icon.paintIcon(null, g2d, 0, 0);
        g2d.dispose();

        // Set the rotated icon on the component
        button.setIcon(new ImageIcon(image));
        button.repaint();
    }


}
