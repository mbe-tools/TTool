/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 * 
 * ludovic.apvrille AT enst.fr
 * 
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */




package ui.atd;

//import java.awt.*;

import attacktrees.*;
import avatartranslator.AvatarElement;
import avatartranslator.avatarucd.AvatarActor;
import myutil.NameChecker;
import myutil.TraceManager;
import ui.*;

import common.CDElement;
import ui.ucd.TGConnectorInclude;
import ui.ucd.TGConnectorUseCase;
import ui.ucd.UCDActor;

import java.awt.*;
import java.util.*;
import java.util.List;

//import org.w3c.dom.*;
//import org.xml.sax.*;
//import javax.xml.parsers.*;

/**
 * Class AttackTreePanel
 * Panel used for drawing attack trees
 * Creation: 03/11/2009
 * @version 1.0 03/11/2009
 * @author Ludovic APVRILLE
 */
public class AttackTreeDiagramPanel extends TDiagramPanel implements TDPWithAttributes, NameChecker.SystemWithNamedElements {

    public static String[] syntaxRulesOfAttackTree = {
            "Each attack tree should have exactly one root attack"
    };


    public static String[] syntaxRulesOfAttacks = {
    "Each attack node (including the root attack) must include a title",
    "Each attack node must include a short description, explaining what the attack node represents",
    "The title of an attack node must include only alphanumeric characters (i.e. no white-spaces)",
    "An attack node will only be connected to one parent operator node except if this is a root attack",
    "A root attack must not be connected to a parent operator",
            "Each attack should be connected by exactly one path to the root attack"};

    public static String[] syntaxRulesOfOperators = {
            "An operator node must have at least two children attack nodes",
            "An operator node must have only one parent attack node",
            "An operator node must be only one of the the following types: SEQUENTIAL, AND, or OR",
            "An operator node must not have the root attack as a child node"};

    public  AttackTreeDiagramPanel(MainGUI mgui, TToolBar _ttb) {
        super(mgui, _ttb);
        //addComponent(400, 50, TGComponentManager.EBRDD_START_STATE, false);
        /*TDiagramMouseManager tdmm = new TDiagramMouseManager(this);
        addMouseListener(tdmm);
        addMouseMotionListener(tdmm);*/
    }
    
    public boolean actionOnDoubleClick(TGComponent tgc) {
        return true;
    }
    
    public boolean actionOnAdd(TGComponent tgc) {
        return false;
    }
    public boolean actionOnValueChanged(TGComponent tgc) {
        return false;
    }
    
    public  boolean actionOnRemove(TGComponent tgc) {
        return false;
    }
    
    public String getXMLHead() {
        return "<AttackTreeDiagramPanel name=\"" + name + "\"" + sizeParam() + zoomParam() + " >";
    }
    
    public String getXMLTail() {
        return "</AttackTreeDiagramPanel>";
    }
    
    public String getXMLSelectedHead() {
        return "<AttackTreeDiagramPanelCopy name=\"" + name + "\" xSel=\"" + xSel + "\" ySel=\"" + ySel + "\" widthSel=\"" +
                widthSel + "\" heightSel=\"" + heightSel +
                "\"" + zoomParam()  + ">";
    }
    
    public String getXMLSelectedTail() {
        return "</AttackTreeDiagramPanelCopy>";
    }
    
    public String getXMLCloneHead() {
        return "<AttackTreeDiagramPanelCopy name=\"" + name + "\" xSel=\"" + 0 + "\" ySel=\"" + 0 + "\" widthSel=\""
                + 0 + "\" heightSel=\"" + 0 +
                "\"" + zoomParam()  + ">";
    }
    
    public String getXMLCloneTail() {
        return "</AttackTreeDiagramPanelCopy>";
    }
    
    public void makeGraphicalOptimizations() {
        // Segments of connector that mask components
        
        // Components over others
        
        // Position correctly guards of choice
    }
    
    public LinkedList<TGComponent> getAllAttacks() {
		LinkedList<TGComponent> list = new LinkedList<TGComponent>();
		TGComponent tgc;
		
		ListIterator iterator = getComponentList().listIterator();
		
		while(iterator.hasNext()) {
            tgc = (TGComponent)(iterator.next());
            if (tgc instanceof ATDAttack) {
				list.add(tgc);
			}
		}
		
		return list;
		
	}
	
	public boolean hasAutoConnect() {
		return false;
	}
	
	public void setConnectorsToFront() {
		TGComponent tgc;
		
		//
		
        Iterator iterator = componentList.listIterator();
        
		ArrayList<TGComponent> list = new ArrayList<TGComponent>();
		
        while(iterator.hasNext()) {
            tgc = (TGComponent)(iterator.next());
			if (!(tgc instanceof TGConnector)) {
				list.add(tgc);
			}
		}
		
		//
		for(TGComponent tgc1: list) {
			//
			componentList.remove(tgc1);
			componentList.add(tgc1);
		}
	}

    public String drawFromAttackTreeModel(AttackTree _at)  {
        Attack root = null;

        // find the root attack
        for(Attack att: _at.getAttacks()) {
            if (att.isRoot()) {
                root = att;
                break;
            }
        }

        if (root == null) {
            return "No root attack";
        }

        HashMap<AttackElement, TGComponent> mapOfComponents = new HashMap<>();

        String ret = drawAttackFromAttackTreeModel(mapOfComponents, _at, root, null, -1, 600, 100, 1200);
        drawConnectionlessAttacks(_at);
        drawCountermeasures(mapOfComponents, _at, -1);

        return ret;

    }

    public String drawAttackFromAttackTreeModel(HashMap<AttackElement, TGComponent> _mapOfComponents, AttackTree _at, Attack _att,
                                                ATDConstraint _const, int _valueConst, int _x, int _y, int _length) {
        ATDAttack attack = alreadyDrawnAttack(_att);

        int newX, newY;
        if (attack == null) {
            // We draw the attack
            newX = _x;
            newY = _y;

            attack = new ATDAttack(_x, _y, getMinX(), getMaxX(), getMinY(), getMaxY(), true, null, this);
            if ((_att.getReferenceObject() != null) && (_att.getReferenceObject() instanceof CDElement)) {
                CDElement cd = (CDElement) _att.getReferenceObject();
                attack.setUserResize(cd.getX(), cd.getY(), cd.getWidth(), cd.getHeight());
            }
            attack.setValue(_att.getName());
            attack.setRootAttack(_att.isRoot());
            attack.setDescription(_att.getDescription());

            addBuiltComponent(attack);
            _mapOfComponents.put(_att, attack);
        }

        // If the constraint is non-null, we link the attack to the constraint
        if (_const != null) {
            TGConnectingPoint p1 = attack.closerFreeTGConnectingPoint(_const.getX(), _const.getY(), false, true);
            TGConnectingPoint p2 = _const.closerFreeTGConnectingPoint(attack.getX(), attack.getY(), true, false);

            if (p1 != null && p2 != null) {
                p1.setFree(false);
                p2.setFree(false);

                TGConnector conn = null;
                Vector<Point> points = new Vector<Point>();
                conn = new ATDAttackConnector(0, 0, 0, 0, 0, 0, true, null,
                        this, p1, p2, points);
                if (_valueConst != -1) {
                    conn.setValue("" + _valueConst);
                }

                if (conn != null) {
                    addComponent(conn, 0, 0, false, true);
                }
            }
        }

        // We look for the constraint linked to this attack
        ArrayList<AttackNode> nextNodeList = new ArrayList<>();

        for(AttackNode node : _at.getAttackNodes()) {
            if (node.getResultingAttack() == _att) {
                nextNodeList.add(node);
            }
        }

        // Leaf attack?
        if (nextNodeList.isEmpty()) {
            return null;
        }

        for (AttackNode nextNode : nextNodeList) {
            // We create the ATDConstraint
            _y = _y + 70;
            ATDConstraint constraint = new ATDConstraint(_x, _y, getMinX(), getMaxX(), getMinY(), getMaxY(), true, null, this);
            //constraint.setValue(nextNode.getName());
            if ((nextNode.getReferenceObject() != null) && (nextNode.getReferenceObject() instanceof CDElement)) {
                CDElement cd = (CDElement) nextNode.getReferenceObject();
                constraint.setUserResize(cd.getX(), cd.getY(), cd.getWidth(), cd.getHeight());
            }
            constraint.setType(nextNode.getType());
            addBuiltComponent(constraint);
            _mapOfComponents.put(nextNode, constraint);

            TGConnectingPoint p1 = constraint.closerFreeTGConnectingPoint(attack.getX(), attack.getY(), false, true);
            TGConnectingPoint p2 = attack.closerFreeTGConnectingPoint(constraint.getX(), constraint.getY(), true, false);

            if (p1 != null && p2 != null) {


                TGConnector conn = null;
                Vector<Point> points = new Vector<Point>();
                conn = new ATDAttackConnector(0, 0, 0, 0, 0, 0, true, null,
                        this, p1, p2, points);
                if (_valueConst != -1) {
                    conn.setValue("" + _valueConst);
                }

                if (conn != null) {
                    p1.setFree(false);
                    p2.setFree(false);
                    addComponent(conn, 0, 0, false, true);
                }
            }

            // We now create all the sub attacks
            int size = nextNode.getInputAttacks().size();
            int currentBeg = _x - _length / 2;
            int slotSize = _length / size;
            for (int i = 0; i < size; i++) {
                Attack nextAttack = nextNode.getInputAttacks().get(i);
                Integer nextValue = nextNode.getInputValues().get(i);

                String s = drawAttackFromAttackTreeModel(_mapOfComponents, _at, nextAttack, constraint, nextValue, currentBeg + slotSize / 2, _y + 70,
                        slotSize);
                currentBeg += slotSize;

                if (s != null) {
                    return s;
                }
            }
        }

        return null;
    }

    private ATDAttack alreadyDrawnAttack(Attack attack) {
        List<TGComponent> componentList = getComponentList();

        for (TGComponent component : componentList) {
            if (component instanceof ATDAttack) {
                ATDAttack attackComponent = (ATDAttack) component;

                if (attackComponent.getValue().equals(attack.getName()) &&
                        attackComponent.getDescription().equals(attack.getDescription()) &&
                        attackComponent.isRootAttack() == attack.isRoot()) {
                    return attackComponent;
                }
            }
        }

        return null;
    }

    public void drawConnectionlessAttacks(AttackTree _at) {
        int x = 0;

        for(Attack att: _at.getAttacks()) {
            ATDAttack attack = alreadyDrawnAttack(att);

            if (attack == null) {
                attack = new ATDAttack(x, 0, getMinX(), getMaxX(), getMinY(), getMaxY(), true, null, this);
                attack.setValue(att.getName());
                attack.setRootAttack(att.isRoot());
                attack.setDescription(att.getDescription());
                addBuiltComponent(attack);

                x = x + attack.getWidth();
            }
        }
    }

    public void drawCountermeasures(HashMap<AttackElement, TGComponent> _mapOfComponents, AttackTree _at,
                                    int _valueConst) {
        // Handling countermeasures
        int xNoRel = 100;
        int yNoRel = 100;

        for(Defense def : _at.getDefenses()) {
            ATDCountermeasure atdc = new ATDCountermeasure(xNoRel, yNoRel, getMinX(), getMaxX(), getMinY(), getMaxY(), true, null, this);
            if ((def.getReferenceObject() != null) && (def.getReferenceObject() instanceof CDElement)) {
                CDElement cd = (CDElement) def.getReferenceObject();
                atdc.setUserResize(cd.getX(), cd.getY(), cd.getWidth(), cd.getHeight());
            }
            atdc.setCountermeasureName(def.getName());
            atdc.setDescription(def.getDescription());
            atdc.setEnabled(true);
            addBuiltComponent(atdc);

            yNoRel += 100;

            // Connections
            for(Attack relatedAtt: def.getRelatedAttacks()) {
                TGComponent refToAtt = _mapOfComponents.get(relatedAtt);
                if (refToAtt != null) {
                    TGConnectingPoint p1 = atdc.closerFreeTGConnectingPoint(refToAtt.getX(), refToAtt.getY(), false, true);
                    TGConnectingPoint p2 = refToAtt.closerFreeTGConnectingPoint(atdc.getX(), atdc.getY(), true, false);

                    if ((p1 != null) && (p2 != null)) {

                        TGConnector conn = null;
                        Vector<Point> points = new Vector<Point>();
                        conn = new ATDCountermeasureConnector(0, 0, 0, 0, 0, 0, true, null,
                                this, p1, p2, points);
                        if (_valueConst != -1) {
                            conn.setValue("" + _valueConst);
                        }

                        if (conn != null) {
                            p1.setFree(false);
                            p2.setFree(false);
                            addComponent(conn, 0, 0, false, true);
                        }
                    }
                }
            }
        }
    }

    public int getTotalNumberOfAttacks() {
        int count = 0;
        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttack) {
                count ++;
            }
        }
        return count;
    }

    public int getTotalNumberOfOperators() {
        int count = 0;
        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDConstraint) {
                count ++;
            }
        }
        return count;
    }

    public int getComplexity() {
        return getTotalNumberOfAttacks() + getTotalNumberOfOperators();
    }

    // returns false if the rule is not satisfied ; false otherwise
    public boolean checkAttackRule(ATDAttack attack, int ruleIndex) {

        if (ruleIndex == 0) {
            return attack.getAttackName().trim().length() > 0;

        } else if (ruleIndex == 1) {
            if (attack.hasDefaultDescription()) {
                return false;
            }
            return attack.getDescription().trim().length() > 0;

        } else if (ruleIndex == 2) {
            String name = attack.getAttackName();
            return name.matches("^[a-zA-Z0-9]+$");

        } else if (ruleIndex == 3) {
            // We check that there is only one connector starting at this attack and going to an operator node
            // Except if root attack

            if (attack.isRootAttack()) {
                return true;
            }

            int count = 0;
            for(TGComponent tgc: getAllComponentList()) {
                if (tgc instanceof ATDAttackConnector) {
                    if (((ATDAttackConnector) tgc).getTGComponent1() == attack) {
                        if (count > 0) {
                            TraceManager.addDev("Attack " + attack.getAttackName() + " count =" + count + " -> false");
                            return false;
                        }
                        if (!(((ATDAttackConnector) tgc).getTGComponent2() instanceof ATDConstraint)) {
                            TraceManager.addDev("Attack " + attack.getAttackName() + " not connected to constraint -> false");
                            return false;
                        }
                        count ++;
                    }
                }
            }
            return count == 1;

        } else if (ruleIndex == 4) {
            if (!(attack.isRootAttack())) {
                return true;
            }

            TraceManager.addDev("Attack " + attack.getAttackName() + " is a root attack");

            for(TGComponent tgc: getAllComponentList()) {
                if (tgc instanceof ATDAttackConnector) {
                    if (((ATDAttackConnector) tgc).getTGComponent1() == attack) {
                       return false;

                    }
                }
            }
            return true;


        } else if (ruleIndex == 5) {
            if (attack.isRootAttack()) {
                return true;
            }

            // Must figure out if there is only one path to a root attack
            return hasUniquePathToRootAttack(attack, new HashSet<TGComponent>());
        }



        return true;
    }

    public boolean hasUniquePathToRootAttack(TGComponent origin, HashSet<TGComponent> explored) {
        int count = 0;
        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttackConnector) {
                if (((ATDAttackConnector) tgc).getTGComponent1() == origin) {
                    TGComponent destination = ((ATDAttackConnector) tgc).getTGComponent2();
                    if (explored.contains(destination)) {
                        return false;
                    }

                    explored.add(destination);

                    boolean handled = false;
                    if (destination instanceof ATDAttack) {
                        if ( (((ATDAttack)(destination))).isRootAttack()) {
                            count ++;
                            handled = true;
                        }
                    }

                    if (!handled) {
                        boolean b = hasUniquePathToRootAttack(destination, explored);
                        if (b) {
                            count++;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }

        return count == 1;
    }

    public HashMap<ATDAttack, ArrayList<Boolean>> computeSyntaxRulesForAttacks() {
        HashMap<ATDAttack, ArrayList<Boolean>> map = new HashMap<>();

        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttack) {
                @SuppressWarnings("unchecked")
                ArrayList<Boolean> list = new ArrayList(syntaxRulesOfAttacks.length);
                for(int i=0; i<syntaxRulesOfAttacks.length; i++) {
                    boolean b = checkAttackRule((ATDAttack)tgc, i);
                    list.add(b);
                }
                map.put((ATDAttack)tgc, list);
            }
        }

        return map;
    }

    public int getNbOfSubAttacks(ATDConstraint constraint) {
        int count = 0;
        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttackConnector) {
                if (((ATDAttackConnector) tgc).getTGComponent2() == constraint) {
                    if (((ATDAttackConnector) tgc).getTGComponent1() instanceof ATDAttack) {
                        count ++;
                    }
                }
            }
        }
        return count;
    }

    public int getNumberOfAttackParentNode(ATDConstraint constraint) {
        int count = 0;
        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttackConnector) {
                if (((ATDAttackConnector) tgc).getTGComponent1() == constraint) {
                    if (((ATDAttackConnector) tgc).getTGComponent2() instanceof ATDAttack) {
                        count ++;
                    }
                }
            }
        }
        return count;
    }

    public boolean hasRootAttackAsChild(ATDConstraint constraint) {

        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttackConnector) {
                if (((ATDAttackConnector) tgc).getTGComponent2() == constraint) {
                    if (((ATDAttackConnector) tgc).getTGComponent1() instanceof ATDAttack) {
                        ATDAttack attack = (ATDAttack) ((ATDAttackConnector) tgc).getTGComponent1();
                        if (attack.isRootAttack()) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public ATDAttack getParentAttack(ATDConstraint constraint) {

        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttackConnector) {
                if (((ATDAttackConnector) tgc).getTGComponent2() == constraint) {
                    if (((ATDAttackConnector) tgc).getTGComponent1() instanceof ATDAttack) {
                        ATDAttack attack = (ATDAttack) ((ATDAttackConnector) tgc).getTGComponent1();
                        return attack;
                    }
                }
            }
        }

        return null;
    }


    public boolean checkOperatorRule(ATDConstraint constraint, int ruleIndex) {

        if (ruleIndex == 0) {
            int nb = getNbOfSubAttacks(constraint);
            return nb > 1;

        } else if (ruleIndex == 1) {
            return getNumberOfAttackParentNode(constraint) == 1;

        } else if (ruleIndex == 2) {
            return constraint.isSequence() || constraint.isAND() || constraint.isOR();

        } else if (ruleIndex == 3) {
            return !hasRootAttackAsChild(constraint);
        }
        return false;
    }
    public HashMap<ATDConstraint, ArrayList<Boolean>> computeSyntaxRulesForOperators() {
        HashMap<ATDConstraint, ArrayList<Boolean>> map = new HashMap<>();

        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDConstraint) {
                @SuppressWarnings("unchecked")
                ArrayList<Boolean> list = new ArrayList(syntaxRulesOfOperators.length);
                for(int i=0; i<syntaxRulesOfOperators.length; i++) {
                    boolean b = checkOperatorRule((ATDConstraint)tgc, i);
                    list.add(b);
                }
                map.put((ATDConstraint)tgc, list);
            }
        }

        return map;
    }

    public int getNbOfRootAttacks() {
        int count = 0;
        for(TGComponent tgc: getAllComponentList()) {
            if (tgc instanceof ATDAttack) {
                if (((ATDAttack) tgc).isRootAttack()) {
                    count++;
                }
            }
        }
        return count;
    }


    public String getStats() {
        double synCorrect = 0.0;

        StringBuffer sb = new StringBuffer();

        sb.append(CR + "# General stats" + CR);
        sb.append("- Nb of attack nodes: " + getTotalNumberOfAttacks() + CR);
        sb.append("- Nb of operator nodes: " + getTotalNumberOfOperators() + CR);
        sb.append("- Complexity (attack + operators): " + getComplexity() + CR);

        sb.append(CR + "# Syntax stats" + CR);

        boolean treeRule1 = getNbOfRootAttacks() == 1;
        if (treeRule1) {
            sb.append("- Tree has only one root attack -> OK " + CR + CR);
        } else {
            sb.append("- Tree has " + getNbOfRootAttacks() + " root attacks -> ERROR " + CR + CR);
        }

        double tva = 0.0;
        HashMap<ATDAttack, ArrayList<Boolean>> mapOfSyntaxForAttacks = computeSyntaxRulesForAttacks();
        for(ATDAttack attack: mapOfSyntaxForAttacks.keySet()) {
            sb.append("- Attack " + attack.getAttackName());
            int nbOfTrue = 0;
            ArrayList<Boolean> list = mapOfSyntaxForAttacks.get(attack);
            String notRespected = "";
            double not = list.size();
            for(int i=0; i<list.size(); i++) {
                if (list.get(i)) { nbOfTrue++; } else {
                    notRespected += " " + (i+1);
                    not --;
                }
            }
            if (nbOfTrue == syntaxRulesOfAttacks.length) {
                sb.append(" respects all syntax rules" + CR);
            } else {
                sb.append(" does NOT respect rules: " + notRespected.trim() +  ". ERROR" + CR);
            }
            tva += not / list.size();
        }

        sb.append(CR);
        double tvo = 0.0;
        HashMap<ATDConstraint, ArrayList<Boolean>> mapOfSyntaxForConstraints = computeSyntaxRulesForOperators();
        for(ATDConstraint constraint: mapOfSyntaxForConstraints.keySet()) {
            String name = constraint.getValue();
            ATDAttack attack = getParentAttack(constraint);
            if (attack != null) {
                name += " with parent attack: " + attack.getAttackName();
            }
            sb.append("- constraint " + name);
            int nbOfTrue = 0;
            ArrayList<Boolean> list = mapOfSyntaxForConstraints.get(constraint);
            String notRespected = "";
            double not = list.size();
            for(int i=0; i<list.size(); i++) {
                if (list.get(i)) { nbOfTrue++; } else {
                    notRespected += " " + (i+1);
                    not --;
                }
            }
            if (nbOfTrue == syntaxRulesOfOperators.length) {
                sb.append(" respects all syntax rules" + CR);
            } else {
                sb.append(" does NOT respect rules: " + notRespected.trim() +  ". ERROR" + CR);
            }
            tvo += not / list.size();

        }

        sb.append(CR + "# Summary of syntaxic correctness: " + CR);
        sb.append("- tva =  " + tva + CR);
        sb.append("- tvo =  " + tvo + CR);
        synCorrect = ((tva + tvo)/getComplexity());
        sb.append("- syntaxic correctness =  " + synCorrect + CR);


        sb.append(CR + "# CSV line" + CR + getName() + CSV_SEP + getComplexity() + CSV_SEP + "?" + CSV_SEP + "?" + CSV_SEP +

                "?" + CSV_SEP + "?"  + CSV_SEP + "?" + CSV_SEP + "?" + CSV_SEP + synCorrect + CR);


        sb.append(CR + CR + CR + "# Definition of Metrics" + CR + CR);
        sb.append(CR + "## Semantic correctness: (ra + ro) / complexity" + CR);
        sb.append("- ra: number of relevant attacks" + CR);
        sb.append("- ro: number of relevant operator nodes" + CR);

        sb.append(CR + "## Completeness = complexity / (complexity + ma + mo)" + CR);
        sb.append("- ma: number of attack nodes that should have been in the attack tree but were absent" + CR);
        sb.append("- mo: number of operator nodes that should have been in the attack tree but were absent" + CR);

        sb.append(CR + "## Syntaxic correctness = (tva + tvo) / complexity " + CR);
        sb.append("- tva: the sum of for each attack node, the number of respected syntax rules divided by the total number of attack node syntax " +
                "rules" + CR);
        sb.append("- tvo: the sum of for each operator node, the number of respected syntax rules divided by the total number of operator node " +
                "syntax rules" + CR);

        sb.append(CR + "## Syntax rules of attack trees" + CR);
        for(int i=0; i<syntaxRulesOfAttackTree.length; i++) {
            sb.append((i+1) + ". " + syntaxRulesOfAttackTree[i] + CR);
        }

        sb.append(CR + "## Syntax rules of attack nodes" + CR);
        for(int i=0; i<syntaxRulesOfAttacks.length; i++) {
            sb.append((i+1) + ". " + syntaxRulesOfAttacks[i] + CR);
        }

        sb.append(CR + "## Syntax rules of operator nodes" + CR);
        for(int i=0; i<syntaxRulesOfOperators.length; i++) {
            sb.append((i+1) + ". " + syntaxRulesOfOperators[i] + CR);
        }


        /*sb.append("- Each attack node (including the root attack) must include a title" + CR);
        sb.append("- Each attack node must include a short description, explaining what the attack node represents" + CR);
        sb.append("- The title of an attack node must include only alphanumeric characters (i.e. no white-spaces)" + CR);
        sb.append("- An attack node will only be connected to operator nodes" + CR);
        sb.append("- An attack node must be connected to at least one operator node" + CR);

        sb.append(CR + "## Syntax rules of operator nodes");
        sb.append("An operator node must have at least two children attack nodes" + CR);
        sb.append("An operator node must have only one parent attack node" + CR);
        sb.append("An operator node must be only one of the the following types: SEQUENTIAL, AND, or OR" + CR);
        sb.append("An operator node must not have the root attack as a child node" + CR);*/

        sb.append(CR + "## Format of CSV" + CR + "attack_tree_name,complexity,num_relevant_attack_nodes,num_relevant_operator_nodes," +
                "num_absent_attack_nodes,num_absent_operator_nodes,semantic_correctness,completeness,syntaxic_correctness" + CR);




        return sb.toString();
    }
}
