/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 * 
 * ludovic.apvrille AT enst.fr
 * 
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */




package ui.ucd;

import avatartranslator.AvatarElement;
import avatartranslator.avatarucd.AvatarActor;
import avatartranslator.avatarucd.AvatarUCDConnection;
import avatartranslator.avatarucd.AvatarUseCase;
import avatartranslator.avatarucd.AvatarUseCaseDiagram;
import myutil.NameChecker;
import myutil.TraceManager;
import ui.*;

import java.awt.*;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Vector;

/**
   * Class UseCasePanel
   * Panel for drawing a use case diagram
   * Creation: 18/02/2005
   * @version 1.0 18/02/2005
   * @author Ludovic APVRILLE
 */
public class UseCaseDiagramPanel extends TDiagramPanel implements NameChecker.SystemWithNamedElements {

    public  UseCaseDiagramPanel(MainGUI mgui, TToolBar _ttb) {
        super(mgui, _ttb);
        /*TDiagramMouseManager tdmm = new TDiagramMouseManager(this);
          addMouseListener(tdmm);
          addMouseMotionListener(tdmm);*/
    }

    public boolean actionOnDoubleClick(TGComponent tgc) {
        //
        /*if (tgc instanceof TCDTClass) {
          TCDTClass t = (TCDTClass)tgc;
          return mgui.newTClassName(t.oldValue, t.getValue());
          } else if (tgc instanceof TCDActivityDiagramBox) {
          if (tgc.getFather() instanceof TCDTClass) {
          mgui.selectTab(tgc.getFather().getValue());
          } else if (tgc.getFather() instanceof TCDTObject) {
          TCDTObject to = (TCDTObject)(tgc.getFather());
          TCDTClass t = to.getMasterTClass();
          if (t != null) {
          mgui.selectTab(t.getValue());
          }
          }
          return false; // because no change made on any diagram
          }*/
        return false;
    }

    public boolean actionOnAdd(TGComponent tgc) {
        /*if (tgc instanceof TCDTClass) {
          TCDTClass tgcc = (TCDTClass)(tgc);
          mgui.addTClass(tgcc.getClassName());
          return true;
          }*/
        return false;
    }

    public boolean actionOnRemove(TGComponent tgc) {
        /*if (tgc instanceof TCDTClass) {
          TCDTClass tgcc = (TCDTClass)(tgc);
          mgui.removeTClass(tgcc.getClassName());
          resetAllInstancesOf(tgcc);
          return true;
          }*/
        return false;
    }

    public boolean actionOnValueChanged(TGComponent tgc) {
        /*if (tgc instanceof TCDTClass) {
          return actionOnDoubleClick(tgc);
          }*/
        return false;
    }

    public String getXMLHead() {
        return "<UseCaseDiagramPanel name=\"" + name + "\"" + sizeParam() + zoomParam() + " >";
    }

    public String getXMLTail() {
        return "</UseCaseDiagramPanel>";
    }

    public String getXMLSelectedHead() {
        return "<UseCaseDiagramPanelCopy name=\"" + name + "\" xSel=\"" + xSel + "\" ySel=\"" + ySel + "\" widthSel=\"" + widthSel + "\" heightSel=\"" + heightSel +
                "\"" + zoomParam()  + ">";
    }

    public String getXMLSelectedTail() {
        return "</UseCaseDiagramPanelCopy>";
    }

    public String getXMLCloneHead() {
        return "<UseCaseDiagramPanelCopy name=\"" + name + "\" xSel=\"" + 0 + "\" ySel=\"" + 0 + "\" widthSel=\"" + 0 + "\" heightSel=\"" + 0 +
                "\"" + zoomParam()  + ">";
    }

    public String getXMLCloneTail() {
        return "</UseCaseDiagramPanelCopy>";
    }

    public void makePostLoadingProcessing() throws MalformedModelingException {
        TGComponent tgc;

        /*for(int i=0; i<componentList.size(); i++) {
          tgc = (TGComponent)(componentList.elementAt(i));
          if (tgc instanceof TCDTObject) {
          ((TCDTObject)tgc).postLoadingProcessing();
          }
          }*/
    }

    public UCDBorder getFirstUCDBorder() {
        TGComponent tgc;

        ListIterator iterator = getComponentList().listIterator();
        while(iterator.hasNext()) {
            tgc = (TGComponent)(iterator.next());
            if (tgc instanceof UCDBorder) {
                return (UCDBorder)tgc;
            }
        }

        return null;
    }

    public void drawFromAvatarUseCaseDiagram(AvatarUseCaseDiagram _ad) {

        if (_ad == null) {
            return;
        }

        TraceManager.addDev("Drawing UCD");

        int centralX = 500;
        int centralY = 500;
        int widthFrontier = 1200;
        int heightFrontier = 1000;
        int decYActor = 75;
        int decXFrontier = 200;
        int decYFrontier = 200;

        // Actors are drawn around

        HashMap<AvatarElement, TGComponent> mapOfComponents = new HashMap<>();

        int cpt = 0;
        int xActor = decXFrontier - 100;
        int yActor = decYFrontier - 100 ;
        int total = _ad.getActors().size();
        for(AvatarActor aa: _ad.getActors()) {
            UCDActor actor = new UCDActor(xActor, yActor, getMinX(), getMaxX(), getMinY(), getMaxY(), true, null, this);
            actor.setValue(aa.getName());
            cpt ++;
            yActor = yActor + heightFrontier / total / 2;
            if (cpt == total/2) {
                xActor = widthFrontier + decXFrontier + 50;
                yActor = decYFrontier - 100 ;
            }
            addBuiltComponent(actor);
            mapOfComponents.put(aa, actor);
        }

        double angle = 360 / _ad.getUseCases().size();
        double currentAngle = 0;
        int xCenter = decXFrontier + widthFrontier / 2;
        int yCenter = decYFrontier + heightFrontier / 2;

        for(AvatarUseCase auc: _ad.getUseCases()) {
            UCDUseCase useCase = new UCDUseCase((int)(xCenter + 100 * Math.cos(currentAngle)), (int)(yCenter + 100 * Math.sin(currentAngle)),
                    getMinX(), getMaxX(), getMinY(), getMaxY(), true, null, this);
            useCase.setValue(auc.getName());
            currentAngle += angle;
            addBuiltComponent(useCase);
            mapOfComponents.put(auc, useCase);
        }

        // Use cases are drawn in circle in the middle, at a random position not yet used.
        // the number of positions is determined by the number of use cases
        // If a connection exists from a use case to an actor, it is drawn as close as possible to
        // the corresponding actor

        // We first place connections with actors, then we place remaining elements

        // Connections are added
        Vector<Point> points = new Vector<Point>();
        for(AvatarUCDConnection cong: _ad.getConnections()) {
            TGConnector conn = null;

            TGComponent tgc1 = mapOfComponents.get(cong.origin);
            TGComponent tgc2 = mapOfComponents.get(cong.destination);

            if ((tgc1 != null) && (tgc2 != null)) {

                TGConnectingPoint p1 = tgc1.closerFreeTGConnectingPoint(tgc2.getX(), tgc2.getY(), false, true);
                TGConnectingPoint p2 = tgc2.closerFreeTGConnectingPoint(tgc1.getX(), tgc1.getY(), true, false);

                if (p1 != null && p2 != null) {
                    p1.setFree(false);
                    p2.setFree(false);

                    if ((cong.origin instanceof AvatarActor) || (cong.destination instanceof AvatarActor)) {
                        // Between actor and use case
                        conn = new TGConnectorUseCase(0, 0, 0, 0, 0, 0, true, null,
                                this, p1, p2, points);
                    } else {
                        conn = new TGConnectorInclude(0, 0, 0, 0, 0, 0, true, null,
                                this, p1, p2, points);
                    }
                    if (conn != null) {
                        addComponent(conn, 0, 0, false, true);
                    }
                }
            }
        }



        // Draw the frontier
        UCDBorder border = new UCDBorder(decXFrontier, decYFrontier, getMinX(), getMaxX(), getMinY(), getMaxY(), true, null, this);
        border.resize(widthFrontier, heightFrontier);
        addBuiltComponent(border);




    }

}
