/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


package ui.window;

import myutil.TraceManager;
import ui.TAttribute;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;


/**
 * Class JDialogTags
 * Dialog for managing tags
 * Creation: 20/02/2025
 *
 * @author Ludovic APVRILLE
 * @version 1.0 20/02/2025
 */
public class JDialogTags extends JDialogBase implements ActionListener, ListSelectionListener  {

    protected java.util.List<String> tags, tagsPar;

    protected JPanel panel1, panel2;

    protected Frame frame;

    protected String attrib; // "Attributes", "Gates", etc.

    // Name of attrib
    protected String name;
    protected JPanel panelName;
    protected JTextField nameField;


    // Panel1
    protected JTextField identifierText;
    protected JButton addButton;

    //Panel2
    protected JList<String> listTags;
    protected JButton upButton;
    protected JButton downButton;
    protected JButton removeButton;

    // Custom data


    /* Creates new form  */
    public JDialogTags(java.util.List<String> _tags, Frame f,
                       String title, String _attrib) {
        super(f, title, true);
        frame = f;
        tagsPar = _tags;
        tags = new LinkedList<String>();
        tags.addAll(tagsPar);

        attrib = _attrib;

        initComponents();
        myInitComponents();
        pack();
    }

    protected void myInitComponents() {
        removeButton.setEnabled(false);
        upButton.setEnabled(false);
        downButton.setEnabled(false);
    }

    protected void initComponents() {
        Container c = getContentPane();
        GridBagLayout gridbag0 = new GridBagLayout();
        GridBagLayout gridbag1 = new GridBagLayout();
        GridBagLayout gridbag2 = new GridBagLayout();
        GridBagConstraints c0 = new GridBagConstraints();
        GridBagConstraints c1 = new GridBagConstraints();
        GridBagConstraints c2 = new GridBagConstraints();

        setFont(new Font("Helvetica", Font.PLAIN, 14));
        c.setLayout(gridbag0);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        panel1 = new JPanel();
        panel1.setLayout(gridbag1);
        panel1.setBorder(new javax.swing.border.TitledBorder("Adding " + attrib + "s"));
        panel1.setPreferredSize(new Dimension(300, 250));

        panel2 = new JPanel();
        panel2.setLayout(gridbag2);
        panel2.setBorder(new javax.swing.border.TitledBorder("current list of " + attrib + "s"));
        panel2.setPreferredSize(new Dimension(300, 250));

        // first line panel1
        c1.gridwidth = 1;
        c1.gridheight = 1;
        c1.weighty = 1.0;
        c1.weightx = 1.0;
        c1.gridwidth = GridBagConstraints.REMAINDER; //end row
        c1.fill = GridBagConstraints.BOTH;
        c1.gridheight = 3;
        panel1.add(new JLabel(" "), c1);

        c1.gridwidth = GridBagConstraints.REMAINDER; //end row
        c1.gridheight = 1;
        c1.weighty = 1.0;
        c1.weightx = 1.0;
        c1.anchor = GridBagConstraints.CENTER;

        identifierText = new JTextField();
        identifierText.setColumns(15);
        identifierText.setEditable(true);
        panel1.add(identifierText, c1);


        // third line panel1
        c1.gridwidth = GridBagConstraints.REMAINDER; //end row
        c1.fill = GridBagConstraints.BOTH;
        c1.gridheight = 3;
        panel1.add(new JLabel(" "), c1);

        // fourth line panel2
        c1.gridheight = 1;
        c1.fill = GridBagConstraints.HORIZONTAL;
        addButton = new JButton("Add " + attrib);
        addButton.addActionListener(this);
        panel1.add(addButton, c1);

        // 1st line panel2
        listTags = new JList<String>(tags.toArray(new String[0]));
        //listAttribute.setFixedCellWidth(150);
        //listAttribute.setFixedCellHeight(20);
        listTags.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listTags.addListSelectionListener(this);
        JScrollPane scrollPane = new JScrollPane(listTags);
        scrollPane.setSize(300, 250);
        c2.gridwidth = GridBagConstraints.REMAINDER; //end row
        c2.fill = GridBagConstraints.BOTH;
        c2.gridheight = 5;
        c2.weighty = 10.0;
        c2.weightx = 10.0;
        panel2.add(scrollPane, c2);

        // 2nd line panel2
        c2.weighty = 1.0;
        c2.weightx = 1.0;
        c2.fill = GridBagConstraints.BOTH;
        c2.gridheight = 1;
        panel2.add(new JLabel(""), c2);

        // third line panel2
        c2.gridwidth = GridBagConstraints.REMAINDER; //end row
        c2.fill = GridBagConstraints.HORIZONTAL;
        upButton = new JButton("Up");
        upButton.addActionListener(this);
        panel2.add(upButton, c2);

        downButton = new JButton("Down");
        downButton.addActionListener(this);
        panel2.add(downButton, c2);

        removeButton = new JButton("Remove " + attrib);
        removeButton.addActionListener(this);
        panel2.add(removeButton, c2);


        //Name panel
        if (name != null) {
            GridBagLayout gbOp = new GridBagLayout();
            GridBagConstraints cOp = new GridBagConstraints();
            panelName = new JPanel();
            panelName.setLayout(gbOp);
            panelName.setBorder(new javax.swing.border.TitledBorder("Name"));
            //panelOperation.setPreferredSize(new Dimension(500, 70));

            cOp.weighty = 1.0;
            cOp.weightx = 2.0;
            cOp.gridwidth = GridBagConstraints.REMAINDER;
            cOp.fill = GridBagConstraints.BOTH;
            cOp.gridheight = 3;
            nameField = new JTextField(name);
            panelName.add(nameField, cOp);

            c0.weighty = 1.0;
            c0.weightx = 1.0;
            c0.fill = GridBagConstraints.BOTH;
            c0.gridwidth = GridBagConstraints.REMAINDER;
            c.add(panelName, c0);
        }


        // main panel;
        c0.gridwidth = 1;
        c0.gridheight = 10;
        c0.weighty = 1.0;
        c0.weightx = 1.0;
        c0.fill = GridBagConstraints.BOTH;


        c.add(panel1, c0);
        c0.gridwidth = GridBagConstraints.REMAINDER; //end row
        c.add(panel2, c0);

        c0.gridwidth = 1;
        c0.gridheight = 1;
        c0.fill = GridBagConstraints.HORIZONTAL;

        initButtons(c0, c, this);
    }

    public void actionPerformed(ActionEvent evt) {


        String command = evt.getActionCommand();

        if (evt.getSource() == addButton) {
            addTag();
        }
        // Compare the action command to the known actions.
        else if (command.equals("Save and Close")) {
            closeDialog();
        } else if (command.equals("Add " + attrib)) {
            addTag();
        } else if (command.equals("Cancel")) {
            cancelDialog();
        } else if (command.equals("Remove " + attrib)) {
            removeTag();
        } else if (command.equals("Down")) {
            downTag();
        } else if (command.equals("Up")) {
            upTag();
        }
    }


    public void addTag() {

        String value = identifierText.getText();

        if (value.length() > 0) {
            tags.add(value);
            listTags.setListData(tags.toArray(new String[0]));
            identifierText.setText("");
        } else {
            JOptionPane.showMessageDialog(frame,
                    "Bad access / type",
                    "Error",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }
    }


    public void removeTag() {
        int i = listTags.getSelectedIndex();
        if (i != -1) {
            String a = tags.get(i);
            tags.remove(i);
            listTags.setListData(tags.toArray(new String[0]));
        }
    }

    public void downTag() {
        int i = listTags.getSelectedIndex();
        if ((i != -1) && (i != tags.size() - 1)) {
            String o = tags.get(i);
            tags.remove(i);
            tags.add(i + 1, o);
            listTags.setListData(tags.toArray(new String[0]));
            listTags.setSelectedIndex(i + 1);
        }
    }

    public void upTag() {
        int i = listTags.getSelectedIndex();
        if (i > 0) {
            String o = tags.get(i);
            tags.remove(i);
            tags.add(i - 1, o);
            listTags.setListData(tags.toArray(new String[0]));
            listTags.setSelectedIndex(i - 1);
        }
    }


    public void closeDialog() {
        if (tagsPar != null) {
            tagsPar.clear();

            for (String tag : this.tags) {
                TraceManager.addDev("Adding tag:" + tag);
                tagsPar.add(tag);
            }
        }
        dispose();
    }

    public void cancelDialog() {
        dispose();
    }

    public void valueChanged(ListSelectionEvent e) {
        int i = listTags.getSelectedIndex() ;
        if (i == -1) {
            removeButton.setEnabled(false);
            upButton.setEnabled(false);
            downButton.setEnabled(false);
            identifierText.setText("");
            //initialValue.setText("");
        } else {
            String a = tags.get (i);
            identifierText.setText(a);

            removeButton.setEnabled(true);
            if (i > 0) {
                upButton.setEnabled(true);
            } else {
                upButton.setEnabled(false);
            }
            if (i != tags.size() - 1) {
                downButton.setEnabled(true);
            } else {
                downButton.setEnabled(false);
            }
        }
    }


}
