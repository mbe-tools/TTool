/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


package ui.window;

import ai.*;
import attacktrees.AttackTree;
import avatartranslator.AvatarSpecification;
import avatartranslator.avatarucd.AvatarUseCaseDiagram;
import common.ConfigurationTTool;
import help.HelpEntry;
import help.HelpManager;
import myutil.AIInterface;
import myutil.GraphicLib;
import myutil.TraceManager;
import ui.*;
import ui.atd.AttackTreeDiagramPanel;
import ui.avatarbd.AvatarBDPanel;
import ui.avatarrd.AvatarRDPanel;
import ui.avatarrd.AvatarRDRequirement;
import ui.ucd.UseCaseDiagramPanel;
import ui.util.IconManager;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Class JFrameAI
 * Creation: 01/03/2023
 * version 1.0 01/03/2021
 *
 * @author Ludovic APVRILLE
 */
public class JFrameAI extends JFrame implements ActionListener {
    private static String[] POSSIBLE_ACTIONS = {
            "Chat - Chat on any topic you like, or help the AI give a better answer on a previous question",
            "Identify requirements - Provide a system specification",
            "Classify requirements - select a requirement diagram first",
            "Identify use cases",
            "Identify properties - Select a block diagram first. You can also provide a system specification",
            "DEPRECATED - Identify system blocks (knowledge type #1) - Provide a system specification",
            "DEPRECATED - Identify system blocks (knowledge type #2) - Provide a system specification",
            "Identify system blocks - Provide a system specification",
            "DEPRECATED - Identify software blocks - Provide a system specification",
            "DEPRECATED - Identify state machines - Select a block diagram. you may provide a system specification",
            "Identify state machines and attributes - Select a block diagram. You may also provide a system specification",
            "Model mutation - A(I)MULET - Select a block diagram first",
            "Diagram coherency",
            "Diagram coherency with formal rules",
            "CAPEC tracer - Identify the possible attack patterns that an attacker could use to exploit your system " +
                    "specifications.",
            "Attack tree generator - Creates a specified amount of Attack Tree diagrams that model a possible exploitation " +
                    "on a provided system specification using a set of provided attack patterns from the CAPEC tracer.",
            "Mitigations generator - Generates a list of mitigations for each provided attack step."
    };


    private static String[] CHAT_CONTENT = {
            "My question: ",
            "System specification:\n",
            "",
            "System specification:\n",
            "System specification:\n",
            "",
            "",
            "System specification:\n",
            "",
            "",
            "System specification:\n",
            "",
            "System specification:\n\nDiagram 1: (type of diagram):\n\nDiagram 2: (type of diagram):\n\n",
            "System specification:\n\nDiagram 1: (type of diagram):\n\nDiagram 2: (type of diagram):\n\n",
            "System specification:\n",
            "System specification::\n\nAttack patterns from the CAPEC tracer:\n\n",
            "Mitigations generator - Generates a list of mitigations for each provided attack step."
    };

    private static String[] AIInteractClass = {
            "AIChat", "AIReqIdent", "AIReqClassification", "AIUseCaseDiagram", "AIDesignPropertyIdentification",
            "AIBlock",
            "AIBlockConnAttrib", "AIBlockConnAttribWithSlicing", "AISoftwareBlock", "AIStateMachine",
            "AIStateMachinesAndAttributes", "AIAmulet",
            "AIDiagramCoherency", "AIDiagramCoherencyWithFormalRules",
            "CAPECTracer", "AIAttackPatternTree", "AIAPTMitigations"
    };

    private static String[] INFOS = {
            "Chat on any topic you like",
            "Identify requirements from the specification of a system",
            "Classify requirements from a requirement diagram",
            "Identify use cases and actors from a system specification",
            "Identify the typical properties to be proven from a block diagram",
            "Identify the system blocks from a specification",
            "Identify the system " +
            "blocks from a specification (another kind of knowledge)",
            "Blocks from a specification (another kind of knowledge with slicing)",
            "Identify the software blocks from a specification",
            "Identify the state machines from a system specification and a block diagram",
            "Identify the state machines and attributes from a system " +
            "specification and a block diagram",
            "Formalize mutations to be performed on a block diagram",
            "Identify incoherencies between two diagrams",
            "Identify incoherencies between two diagrams, considering informal and formal rules",
            "Identify the possible attack patterns that an attacker could use to exploit your system specifications. " +
                    "Each identified attack pattern will have a confidence score of TTool's estimation on how " +
                    "related an attack pattern is to the provided system specifications.",
            "Using a provided system specification, create Attack Tree diagrams that " +
                    "models the steps that an attacker would need to take to exploit the given attack pattern " +
                    "on the given system specifications. This pipeline also uses the CAPEC tracer pipeline to " +
                    "extract relevant attack patterns to assist with the creation of the attack trees.",
            "Using a set of provided attack steps, output possible mitigations for each attack step that prevent " +
                    "an attacker from preforming the step. Uses the CAPEC tracer pipeline to generate a list " +
                    "of possible counters to help with identifying the mitigations."
    };

    protected JComboBox<String> listOfPossibleActions;
    protected JComboBox<String> listOfPossibleModels;
    protected ArrayList<String> keys;


    private MainGUI mgui;
    private JTextPane question, console;
    private JTabbedPane answerPane;
    private JButton appendDiagram;
    private ArrayList<ChatData> chats;

    private int currentChatIndex = -1;

    private JMenuBar menuBar;
    private JMenu help;
    private JPopupMenu helpPopup;


    private HashMap<Integer, ImageIcon> rotatedI = new HashMap<>();
    private JButton buttonClose, buttonStart, buttonApplyResponse;

    private long startTime, endTime;

    private SpinnerNumberModel numAtsModel;
    private SpinnerNumberModel numLvlsModel;

    public JFrameAI(String title, MainGUI _mgui) {
        super(title);
        mgui = _mgui;
        chats = new ArrayList<>();
        keys = new ArrayList<>();
        makeComponents();



        if (mgui.getCurrentMainTDiagramPanel() != null) {
            TraceManager.addDev("Selected TDP = " + mgui.getCurrentMainTDiagramPanel().getClass());
        }
    }

    public void setIcon(ChatData _data, Icon newIcon) {
        int index = chats.indexOf(_data);
        answerPane.setIconAt(index, newIcon);
    }

    public void makeComponents() {

        helpPopup = new JPopupMenu();
        helpPopup.add(new JLabel(IconManager.imgic7009));
        helpPopup.setPreferredSize(new Dimension(600, 900));
        menuBar = new JMenuBar();
        menuBar.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        help = new JMenu("?");
        menuBar.add(help);
        setJMenuBar(menuBar);

        help.setPreferredSize(new Dimension(30, 30));
        help.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                help();
            }
        });
        helpPopup.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"),
                "closeJlabel");
        helpPopup.getActionMap().put("closeJlabel", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                helpPopup.setVisible(false);
            }
        });

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        Container framePanel = getContentPane();
        framePanel.setLayout(new BorderLayout());

        setIconImage(IconManager.imgic152.getImage());

        // Top panel : options
        JPanel panelTop = new JPanel();
        panelTop.setLayout(new BorderLayout());
        panelTop.setBorder(new javax.swing.border.TitledBorder("Options"));

        listOfPossibleActions = new JComboBox<>(POSSIBLE_ACTIONS);
        listOfPossibleActions.addActionListener(this);
        //listOfPossibleActions.setMinimumSize(new Dimension(50, 100));
        panelTop.add(new JLabel("Selected chat:", SwingConstants.CENTER), BorderLayout.NORTH);
        panelTop.add(listOfPossibleActions, BorderLayout.CENTER);
        //panelTop.setMinimumSize(new Dimension(50, 100));

        JPanel modelSelectionPanel = new JPanel();
        modelSelectionPanel.add(new JLabel("Selected AI model:"));
        String model = "";
        if ((ConfigurationTTool.OPENAIModel != null) && (ConfigurationTTool.OPENAIModel.trim().length() > 0 )) {
            model += ConfigurationTTool.OPENAIModel + " ";
            String key = ConfigurationTTool.OPENAIKey;
            if (key == null) {
                key = "";
            }
            for(int i= 0; i<ConfigurationTTool.OPENAIModel.trim().split(" ").length; i++) {
                keys.add(key);
            }
        }
        if ((ConfigurationTTool.MistralAIModel != null) && (ConfigurationTTool.MistralAIModel.trim().length() > 0 )) {
            model += ConfigurationTTool.MistralAIModel + " ";
            String key = ConfigurationTTool.MistralAIKey;
            if (key == null) {
                key = "";
            }
            for(int i= 0; i<ConfigurationTTool.MistralAIModel.trim().split(" ").length; i++) {
                keys.add(key);
            }
        }
        model = model.trim();

        if (model.length() > 0) {
            listOfPossibleModels = new JComboBox<>(model.split(" "));
        } else {
            listOfPossibleModels = new JComboBox<>(AIInterface.MODEL_GPT_35.split(" "));
        }
        modelSelectionPanel.add(listOfPossibleModels);

        appendDiagram = new JButton("Append current diagram text");
        modelSelectionPanel.add(appendDiagram);
        appendDiagram.addActionListener(this);


        /*JPanel numAtsSelectPanel = new JPanel();
        numAtsSelectPanel.add(new JLabel("Number of attack trees to be created: "));
        numAtsModel = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
        JSpinner numAtsSpinner = new JSpinner(numAtsModel);
        numAtsSpinner.setPreferredSize(new Dimension(100, 50));
        numAtsSelectPanel.add(numAtsSpinner);
        numAtsSelectPanel.setVisible(false);

        JPanel numLvlsSelectPanel = new JPanel();
        numLvlsSelectPanel.add(new JLabel("Number of max levels per attack tree: "));
        numLvlsModel = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
        JSpinner numLvlsSpinner = new JSpinner(numLvlsModel);
        numLvlsSpinner.setPreferredSize(new Dimension(100, 50));;
        numLvlsSelectPanel.add(numLvlsSpinner);
        numLvlsSelectPanel.setVisible(false);*/

        JLabel labelAtsSpinner = new JLabel("Number of attack trees to be created: ");
        modelSelectionPanel.add(labelAtsSpinner);
        numAtsModel = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
        JSpinner numAtsSpinner = new JSpinner(numAtsModel);
        //numAtsSpinner.setPreferredSize(new Dimension(100, 50));
        modelSelectionPanel.add(numAtsSpinner);
        labelAtsSpinner.setVisible(false);
        numAtsSpinner.setVisible(false);

        JLabel labelnumLvlsSpinner = new JLabel("Number of max levels per attack tree: ");
        modelSelectionPanel.add(labelnumLvlsSpinner);
        numLvlsModel = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
        JSpinner numLvlsSpinner = new JSpinner(numLvlsModel);
        //numLvlsSpinner.setPreferredSize(new Dimension(100, 50));;
        modelSelectionPanel.add(numLvlsSpinner);
        labelnumLvlsSpinner.setVisible(false);
        numLvlsSpinner.setVisible(false);

        //numAtsSelectPanel.add(numLvlsSelectPanel, BorderLayout.SOUTH);
        //modelSelectionPanel.add(numAtsSelectPanel, BorderLayout.SOUTH);
        panelTop.add(modelSelectionPanel, BorderLayout.SOUTH);
        framePanel.add(panelTop, BorderLayout.NORTH);

        // Middle panel

        /*jta.setEditable(true);
        jta.setMargin(new Insets(10, 10, 10, 10));
        jta.setTabSize(3);
        Font f = new Font("Courrier", Font.BOLD, 12);
        jta.setFont(f);*/

        JPanel questionPanel = new JPanel(new BorderLayout());
        questionPanel.setBorder(new javax.swing.border.TitledBorder("Question"));
        //questionPanel.setMinimumSize(new Dimension(450, 550));

        question = new JTextPane();
        //question.setPreferredSize(new Dimension(400, 500));
        setOptionsJTextPane(question, true);
        JScrollPane scrollPane = new JScrollPane(question);
        //scrollPane.setMinimumSize(new Dimension(420, 500));
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        questionPanel.add(scrollPane, BorderLayout.CENTER);

        JPanel answerPanel = new JPanel(new BorderLayout());
        answerPanel.setBorder(new javax.swing.border.TitledBorder("Answer"));
        //answerPanel.setMinimumSize(new Dimension(550, 550));
        answerPane = new JTabbedPane();
        answerPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                enableDisableActions();
                // Perform your action here
            }
        });
        ;
        answerPane.addMouseListener(new JFrameAI.PopupListener(this));
        //answerPane.setMinimumSize(new Dimension(500, 500));
        addChat(getChatName());
        answerPanel.add(answerPane, BorderLayout.CENTER);

        JPanel consolePanel = new JPanel(new BorderLayout());
        consolePanel.setBorder(new javax.swing.border.TitledBorder("Console"));
        console = new JTextPane();
        //console.setPreferredSize(new Dimension(900, 150));
        inform("Select options and click on \"start\"\n");
        scrollPane = new JScrollPane(console);
        //scrollPane.setMinimumSize(new Dimension(900, 150));
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        consolePanel.add(scrollPane, BorderLayout.CENTER);

        JPanel middlePanel = new JPanel(new BorderLayout());
        JSplitPane intermediate = new JSplitPane();
        intermediate.setLeftComponent(questionPanel);
        intermediate.setRightComponent(answerPanel);
        //intermediate.setDividerLocation(0.5);

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                intermediate.setDividerLocation(0.5);
            }
        });

        /*JPanel intermediate = new JPanel(new BorderLayout());
        intermediate.add(questionPanel, BorderLayout.WEST);
        intermediate.add(answerPanel, BorderLayout.EAST);*/
        JSplitPane center = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        center.setTopComponent(intermediate);
        center.setBottomComponent(consolePanel);

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                center.setDividerLocation(0.9);
            }
        });

        middlePanel.add(center, BorderLayout.CENTER);
        //middlePanel.add(consolePanel, BorderLayout.SOUTH);

        framePanel.add(middlePanel, BorderLayout.CENTER);


        // Lower panel

        buttonClose = new JButton("Close", IconManager.imgic27);
        buttonClose.addActionListener(this);

        buttonStart = new JButton("Start", IconManager.imgic53);
        buttonStart.addActionListener(this);

        buttonApplyResponse = new JButton("Apply response", IconManager.imgic75);
        buttonApplyResponse.addActionListener(this);

        JPanel lowPart = new JPanel(new BorderLayout());
        JPanel jp = new JPanel();
        jp.add(buttonClose);
        jp.add(buttonStart);
        jp.add(buttonApplyResponse);
        lowPart.add(jp, BorderLayout.CENTER);
        framePanel.add(lowPart, BorderLayout.SOUTH);

        enableDisableActions();
        pack();

        listOfPossibleActions.addActionListener(e -> {
            String selectedAction = (String) listOfPossibleActions.getSelectedItem();
            assert selectedAction != null;

            if (question != null) {
                String content = CHAT_CONTENT[listOfPossibleActions.getSelectedIndex()];
                if (content != null) {
                    question.setText(content);
                }
            }


            if (selectedAction.equals(POSSIBLE_ACTIONS[14])) {
                modelSelectionPanel.setVisible(false);
                labelAtsSpinner.setVisible(false);
                numAtsSpinner.setVisible(false);
                numLvlsSpinner.setVisible(false);
                labelnumLvlsSpinner.setVisible(false);
//                questionPanel.setVisible(true);
            }
            else if (selectedAction.equals(POSSIBLE_ACTIONS[15])) {
                modelSelectionPanel.setVisible(true);
                labelAtsSpinner.setVisible(true);
                numAtsSpinner.setVisible(true);
                numLvlsSpinner.setVisible(true);
                labelnumLvlsSpinner.setVisible(true);
//                questionPanel.setVisible(true);
            }
            else if (selectedAction.equals(POSSIBLE_ACTIONS[16])) {
                modelSelectionPanel.setVisible(true);
                labelAtsSpinner.setVisible(false);
                numAtsSpinner.setVisible(false);
                numLvlsSpinner.setVisible(false);
                labelnumLvlsSpinner.setVisible(false);
//                questionPanel.setVisible(false);
            }
            else {
                modelSelectionPanel.setVisible(true);
                labelAtsSpinner.setVisible(false);
                numAtsSpinner.setVisible(false);
                numLvlsSpinner.setVisible(false);
                labelnumLvlsSpinner.setVisible(false);
//                questionPanel.setVisible(true);
            }
        });
    }

    private void addChat(String nameOfChat) {

        ChatData data = new ChatData();
        chats.add(data);

        //answer.setPreferredSize(new Dimension(400, 500));
        //setOptionsJTextPane(answer, false);
        JScrollPane scrollPane = new JScrollPane(data.answer);
        scrollPane.setMinimumSize(new Dimension(400, 400));
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        answerPane.add(scrollPane);
        answerPane.setTitleAt(answerPane.getTabCount() - 1, nameOfChat);
        answerPane.setIconAt(answerPane.getTabCount() - 1, IconManager.imgic154);

    }

    public void actionPerformed(ActionEvent evt) {

        TraceManager.addDev("Action performed");

        if (evt.getSource() == buttonClose) {
            close();
        } else if (evt.getSource() == buttonStart) {
            TraceManager.addDev("start!");
            start();
        } else if (evt.getSource() == buttonApplyResponse) {
            applyResponse();
        } else if (evt.getSource() == listOfPossibleActions) {
            enableDisableActions();
            printInfos();
        } else if (evt.getSource() == appendDiagram) {
            appendCurrentDiagram();
        }
    }

    private String getChatName() {
        if (!mgui.picoZebre) {
            return "Chat";
        }
        String[] names = {"pico", "zebre", "pingouin", "chien", "minou", "kitty", "chaton", "whatsapp", "Luke Skywalker",
                "macareux", "ours", "italien", "paris-brest", "belle-mère", "apéro (l'abus d'alcool est dangereux pour la santé)",
                "carpe", "crocodile", "psychologue", "dr emacs", "3615-TTool", "100 balles et 1 mars",
                "opéra (l’abus d’Alcôve est dangereux pour la santé)", "chapon", "perroquet", "chameau volant", "Alice", "oasis", "ATC RAK",
                "Adibou", "cheval de Troyes", "Twist", "GSM", "étalon", "jaseux", "walkman (l'abus d'écouteurs peut provoquer des otites)", "Blake " +
                "& Mortimer", "Knights who say Ni!", "vendeur de canapés carreautés", "Marcel Proust", "Hen", "Chocolat de Madagascar", "Zèbre de " +
                "Troie", "Télescope"};
        int x = (int) (Math.random() * names.length);
        return names[x];
    }

    private void close() {
        for (ChatData data : chats) {
            data.stopAnimation();
        }
        dispose();
    }

    private void start() {
        TraceManager.addDev("Start in JFrameAI");
        startTime = System.currentTimeMillis();
        currentChatIndex = answerPane.getSelectedIndex();
        ChatData selected = selectedChat();

        boolean chatDataMade = selected.aiChatData != null;

        if (!chatDataMade) {
            chatDataMade = selected.makeAIChatData();
        }

        if (chatDataMade) {
            TraceManager.addDev("chatDataMade in JFrameAI");
            String selectedClass = AIInteractClass[listOfPossibleActions.getSelectedIndex()];
            TraceManager.addDev("SelectedClass: " + selectedClass);
            selected.aiInteract = ai.AIInteract.getInstance("ai." + selectedClass, selected.aiChatData);


            if (selected.aiInteract == null) {
                error("Unknown selected type");
                return;
            }

            TraceManager.addDev("Selected aiinteract: " + selected.aiInteract.getClass());

            if (selected.aiInteract instanceof AIAvatarSpecificationRequired) {
                TraceManager.addDev("****** AIAvatarSpecificationRequired identified *****");
                TDiagramPanel tdp = mgui.getCurrentMainTDiagramPanel();
                boolean found = false;
                if (tdp instanceof AvatarBDPanel) {
                    found = true;
                }

                if (found) {
                    TraceManager.addDev("BD panel: ok");
                    boolean ret = mgui.checkModelingSyntax(true);
                    if (ret) {
                        TraceManager.addDev("****** Syntax checking OK *****");
                        ((AIAvatarSpecificationRequired) (selected.aiInteract)).setAvatarSpecification(mgui.gtm.getAvatarSpecification());
                    } else {
                        TraceManager.addDev("\n\n*****Syntax checking failed: no avatar spec***\n\n");
                    }
                } else {
                    TraceManager.addDev("Selected panel is not correct. tdp=" + tdp.getClass());
                    return;
                }
            }

            if (selected.aiInteract instanceof AISysMLV2DiagramContent) {
                TraceManager.addDev("****** AISysMLV2DiagramContent identified *****");
                TDiagramPanel tdp = mgui.getCurrentTDiagramPanel();
                String[] validDiagrams = ((AISysMLV2DiagramContent) (selected.aiInteract)).getValidDiagrams();
                String className = tdp.getClass().getName();

                boolean found = false;
                for (String s : validDiagrams) {
                    if (className.contains(s)) {
                        found = true;
                        break;
                    }
                }

                if (found) {
                    TraceManager.addDev("The selected diagram is valid");
                    String[] exclusions = ((AISysMLV2DiagramContent) (selected.aiInteract)).getDiagramExclusions();
                    StringBuffer sb = tdp.toSysMLV2Text(exclusions);
                    if (sb == null) {
                        error("The syntax of the selected diagram is incorrect");
                        return;
                    } else {
                        ((AISysMLV2DiagramContent) (selected.aiInteract)).setDiagramContentInSysMLV2(sb.toString());
                    }
                }
            }

            if (selected.aiInteract instanceof AIAttackPatternTree) {
                TraceManager.addDev("****** AIAttackPatternTree identified *****");
                AIAttackPatternTree aiAPT = (AIAttackPatternTree) selected.aiInteract;
                aiAPT.setMaxLevels((Integer) numLvlsModel.getNumber());
                aiAPT.setMaxNumAts((Integer) numAtsModel.getNumber());
            }

            if (selected.aiInteract instanceof AIAPTMitigations) {
                TraceManager.addDev("****** AIAPTMitigations identified *****");
                TDiagramPanel tdp = mgui.getCurrentTDiagramPanel();
                AttackTreePanelTranslator atpt = new AttackTreePanelTranslator((AttackTreeDiagramPanel) tdp);
                AttackTree at = atpt.translateToAttackTreeDataStructure();

                AIAPTMitigations aiMiti = (AIAPTMitigations) selected.aiInteract;
                aiMiti.setReferenceAT(at);
            }

            selected.aiInteract.makeRequest(question.getText());

            String info =  selected.aiInteract.getInfo();
            if (info.length() > 0) {
                inform(info + "\n");
            }

            //question.setText("Total time: " + (endTime - startTime) + " ms");

        } else {
            error("AI interface failed (no key has been set?)");
        }

    }

    private void applyResponse() {
        ChatData selectedChat = selectedChat();
        if (selectedChat.lastAnswer == null || selectedChat.aiChatData == null) {
            error("No answer to apply");
            return;
        }

        //TraceManager.addDev("Class of answer: " + selectedChat.aiInteract.getClass().getName());

        /*if (selectedChat.aiInteract instanceof ai.AIBlock) {
            applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
        } else if (selectedChat.aiInteract instanceof ai.AISoftwareBlock) {
            applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
        } else if (selectedChat.aiInteract instanceof ai.AIReqIdent) {
            applyRequirementIdentification();
        } else if (selectedChat.aiInteract instanceof ai.AIReqClassification) {
            applyRequirementClassification();
        } else if (selectedChat.aiInteract instanceof ai.AIDesignPropertyIdentification) {
            // nothing up to now :-)
        } else if (selectedChat.aiInteract instanceof ai.AIStateMachine) {
            // nothing up to now :-)
        } else if (selectedChat.aiInteract instanceof ai.AIAmulet) {
            applyMutations();
        }*/


        currentChatIndex = answerPane.getSelectedIndex();
        switch (currentChatIndex) {
            case 0:
                if (selectedChat.aiInteract instanceof ai.AIBlock) {
                    applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
                } else if (selectedChat.aiInteract instanceof ai.AIBlockConnAttrib) {
                    applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
                } else if (selectedChat.aiInteract instanceof ai.AIBlockConnAttribWithSlicing) {
                    applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
                } else if (selectedChat.aiInteract instanceof ai.AISoftwareBlock) {
                    applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
                } else if (selectedChat.aiInteract instanceof ai.AIReqIdent) {
                    applyRequirementIdentification();
                } else if (selectedChat.aiInteract instanceof ai.AIUseCaseDiagram) {
                    applyUseCaseIdentification(selectedChat.aiInteract.applyAnswer(null));
                } else if (selectedChat.aiInteract instanceof ai.AIReqClassification) {
                    applyRequirementClassification();
                } else if (selectedChat.aiInteract instanceof ai.AIDesignPropertyIdentification) {
                    // nothing up to now :-)
                } else if (selectedChat.aiInteract instanceof ai.AIStateMachine) {
                    TraceManager.addDev("Applying state machines");
                    applyIdentifyStateMachines(selectedChat.aiInteract.applyAnswer(null));
                } else if (selectedChat.aiInteract instanceof ai.AIStateMachinesAndAttributes) {
                    TraceManager.addDev("Applying state machines and attributes");
                    applyIdentifyStateMachines(selectedChat.aiInteract.applyAnswer(null));
                } else if (selectedChat.aiInteract instanceof ai.AIAmulet) {
                    applyMutations();
                }
                else if (selectedChat.aiInteract instanceof AIAttackPatternTree) {
                    applyAttackTrees(selectedChat.aiInteract.applyAnswer(null));
                }
                else if (selectedChat.aiInteract instanceof AIAPTMitigations) {
                    applyAttackTree(selectedChat.aiInteract.applyAnswer(null));
                }
                break;
            case 1:
                applyRequirementIdentification();
                break;
            case 2:
                applyRequirementClassification();
                break;
            case 3:
                applyUseCaseIdentification(selectedChat.aiInteract.applyAnswer(null));
                break;
            case 4:
                break;
            case 5:
                applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
                break;
            case 6:
                applyIdentifySystemBlocks(selectedChat.aiInteract.applyAnswer(null));
                break;
            case 7:
                applyIdentifyStateMachines(selectedChat.aiInteract.applyAnswer(null));
                break;
            case 8:
                applyMutations();
                break;
        }

        question.setText("");
    }

    private void applyIdentifyStateMachines(Object input) {
        if (input == null) {
            error("Invalid specification in answer");
            return;
        }

        TraceManager.addDev("Type of input:" + input.getClass());

        if (!(input instanceof AvatarSpecification)) {
            error("Invalid answer");
            return;
        }

        TraceManager.addDev("\n\nSpecification to draw: " + ((AvatarSpecification) input).toString() + "\n\n");

        mgui.drawAvatarSpecification((AvatarSpecification) input, false, false);
        inform("State machine of blocks added to diagram from ai answer\n");
    }

    private void applyRequirementIdentification() {

        TDiagramPanel tdp = mgui.getCurrentTDiagramPanel();
        if (!(tdp instanceof AvatarRDPanel)) {
            error("A requirement diagram must be selected first");
            return;
        }

        AvatarRDPanel rdpanel = (AvatarRDPanel) tdp;

        inform("Enhancing requirement diagram with ai answer, please wait\n");
        TraceManager.addDev("Considered JSON array: " + selectedChat().lastAnswer);
        try {
            rdpanel.loadAndUpdateFromText(selectedChat().lastAnswer);
        } catch (org.json.JSONException e) {
            TraceManager.addDev("JSON Exception: " + e.getMessage());
            inform("Answer provided by AI does not respect the JSON format necessary for TTool");
            rdpanel.repaint();
            return;
        }
        rdpanel.repaint();

        inform("Enhancing requirement diagram with ai answer: done\n");
    }

    private void applyUseCaseIdentification(Object spec) {

        TDiagramPanel tdp = mgui.getCurrentTDiagramPanel();
        if (!(tdp instanceof UseCaseDiagramPanel)) {
            error("A use case diagram must be selected first");
            return;
        }

        if (spec == null) {
            error("Empty use case diagram generated by AI");
            return;
        }

        if (!(spec instanceof AvatarUseCaseDiagram)) {
            error("Invalid use case diagram generated by AI");
            return;
        }

        UseCaseDiagramPanel ucdpanel = (UseCaseDiagramPanel) tdp;
        AvatarUseCaseDiagram ad = (AvatarUseCaseDiagram) spec;

        inform("Drawing use case diagram with ai answer, please wait\n");
        TraceManager.addDev("Considered UCD: " + ad.toString());

        ucdpanel.drawFromAvatarUseCaseDiagram(ad);
        ucdpanel.repaint();

        inform("Enhancing use case diagram with ai answer: done\n");
    }

    private void applyAttackTree(Object spec) {
        if (spec == null) {
            error("Empty attack tree diagram generated by AI");
            return;
        }

        if (!(spec instanceof AttackTree)) {
            error("Invalid attack tree diagram generated by AI");
            return;
        }
        
        AttackTree at = (AttackTree) spec;

        inform("Drawing attack tree diagram with ai answer, please wait\n");
        TraceManager.addDev("Considered AT: " + at.toString());

        mgui.drawAttackTreeDiagram(at);

        inform("Enhancing attack tree diagram with ai answer: done\n");
    }

    @SuppressWarnings("unchecked")
    private void applyAttackTrees(Object spec) {
        if (spec == null) {
            error("Empty attack tree diagram list generated by AI");
            return;
        }

        if (!(spec instanceof Iterable)) {
            error("Invalid attack tree diagram list generated by AI");
            return;
        }

        Iterable<Object> atList = (Iterable<Object>) spec;

        for (Object at : atList) {
            applyAttackTree(at);
        }
    }

    private void applyIdentifySystemBlocks(Object input) {
        if (input == null) {
            error("Invalid specification in answer");
            return;
        }

        TraceManager.addDev("Type of input:" + input.getClass());

        if (!(input instanceof AvatarSpecification)) {
            error("Invalid answer");
            return;
        }

        mgui.drawAvatarSpecification((AvatarSpecification) input, false);
        inform("System blocks added to diagram from ai answer: done\n");
    }

    private void applyRequirementClassification() {
        TDiagramPanel tdp = mgui.getCurrentTDiagramPanel();
        if (!(tdp instanceof AvatarRDPanel)) {
            error("A requirement diagram must be selected first");
            return;
        }

        AvatarRDPanel rdpanel = (AvatarRDPanel) tdp;
        inform("Enhancing requirement diagram with ai answer, please wait\n");

        ChatData selected = selectedChat();
        for (TGComponent tgc : rdpanel.getAllRequirements()) {
            AvatarRDRequirement req = (AvatarRDRequirement) tgc;
            String kind = (String) (selected.aiInteract.applyAnswer(req.getValue()));
            if (kind != null) {
                String k = JDialogRequirement.getKindFromString(kind);
                if (k != null) {
                    req.setKind(k);
                    inform("\tRequirement " + req.getValue() + " kind was set to " + k + "\n");
                } else {
                    error("Unknown kind: " + k + "\n");
                }
            }
        }
        inform("Enhancing requirement diagram: done.\n");

        rdpanel.repaint();
    }

    private void applyMutations() {
        //AvatarSpecification avspec = mgui.gtm.getAvatarSpecification();
        TDiagramPanel tdp = mgui.getCurrentTDiagramPanel();
        if (!(tdp instanceof AvatarBDPanel)) {
            error("A block diagram must be selected first");
            return;
        }

        boolean syntaxOK = mgui.checkModelingSyntax(true);

        if (!syntaxOK) {
            error("Block diagram: incorrect syntax");
            return;
        }

        AvatarSpecification avspec = mgui.gtm.getAvatarSpecification();

        if (avspec == null) {
            error("AVATAR specification not found: aborting.\n");
            return;
        }

        inform("Applying mutations to the model, please wait\n");
        avspec = (AvatarSpecification) (selectedChat().aiInteract.applyAnswer(avspec));

        if (avspec != null) {
            mgui.drawAvatarSpecification(avspec, false);
            inform("Mutations applied");
        } else {
            error("No mutations applied");
        }

    }


    private void enableDisableActions() {
        if ((answerPane != null) && (chats != null) && (buttonApplyResponse != null) && (buttonStart != null)) {
            if (answerPane.getSelectedIndex() > -1) {
                ChatData cd = chats.get(answerPane.getSelectedIndex());
                String chat = cd.lastAnswer;
                buttonApplyResponse.setEnabled(chat != null && chat.length() > 0 && !cd.doIconRotation);
                buttonStart.setEnabled(!cd.doIconRotation);
            } else {
                buttonApplyResponse.setEnabled(false);
                buttonStart.setEnabled(false);
            }
        }
    }

    private void printInfos() {
        int index = listOfPossibleActions.getSelectedIndex();
        Font tmpFont = console.getFont();
        console.setFont(tmpFont.deriveFont(Font.BOLD));
        GraphicLib.appendToPaneSafe(console, "Your selection: " + INFOS[index] + "\n", Color.darkGray);
        console.setFont(tmpFont);
    }

    private void setOptionsJTextPane(JTextPane jta, boolean _isEditable) {
        jta.setEditable(_isEditable);
        jta.setMargin(new Insets(10, 10, 10, 10));
        Font f = new Font("Courrier", Font.BOLD, 12);
        jta.setFont(f);
    }


    private ChatData selectedChat() {
        return chats.get(answerPane.getSelectedIndex());
    }

    private ChatData chatOfStart() {
        return chats.get(currentChatIndex);
    }


    private void error(String text) {
        GraphicLib.appendToPane(console, "\n****" + text + " ****\n\n", Color.red);
    }

    private void inform(String text) {
        GraphicLib.appendToPane(console, text, Color.blue);
    }

    public void help() {
        if (mgui == null) {
            TraceManager.addDev("Null mgui");
        }

        HelpManager hm = mgui.getHelpManager();
        HelpEntry he = hm.getHelpEntryWithHTMLFile("ai.html");
        mgui.openHelpFrame(he);

    }


    public void clear() {
        selectedChat().clear();
        // We must also remove the knowledge
        //selectedChat().aiinterface.clearKnowledge();
    }

    public void requestRenameTab() {
        int index = answerPane.getSelectedIndex();
        String oldName = answerPane.getTitleAt(index);
        String s = (String) JOptionPane.showInputDialog(this, "Name: ", "Renaming a tab", JOptionPane.PLAIN_MESSAGE,
                IconManager.imgic101, null,
                answerPane.getTitleAt(index));
        if ((s != null) && (s.length() > 0)) {
            // name already in use?
            if (s.compareTo(oldName) != 0) {
                if (index < answerPane.getTabCount()) {
                    answerPane.setTitleAt(index, s);
                }
            }
        }
    }

    public void removeCurrentTab() {
        int index = answerPane.getSelectedIndex();
        answerPane.remove(index);
        chats.remove(index);
    }

    public void requestMoveRightTab() {
        int index = answerPane.getSelectedIndex();
        requestMoveTabFromTo(index, index + 1);

    }

    public void requestMoveLeftTab() {
        int index = answerPane.getSelectedIndex();
        requestMoveTabFromTo(index, index - 1);
    }

    public void requestMoveTabFromTo(int src, int dst) {

        // Get all the properties
        Component comp = answerPane.getComponentAt(src);
        String label = answerPane.getTitleAt(src);
        Icon icon = answerPane.getIconAt(src);
        Icon iconDis = answerPane.getDisabledIconAt(src);
        String tooltip = answerPane.getToolTipTextAt(src);
        boolean enabled = answerPane.isEnabledAt(src);
        int keycode = answerPane.getMnemonicAt(src);
        int mnemonicLoc = answerPane.getDisplayedMnemonicIndexAt(src);
        Color fg = answerPane.getForegroundAt(src);
        Color bg = answerPane.getBackgroundAt(src);

        // Remove the tab
        answerPane.remove(src);

        // Add a new tab
        answerPane.insertTab(label, icon, comp, tooltip, dst);

        ChatData data = chats.get(src);
        chats.remove(src);
        chats.add(dst, data);

        // Restore all properties
        answerPane.setDisabledIconAt(dst, iconDis);
        answerPane.setEnabledAt(dst, enabled);
        answerPane.setMnemonicAt(dst, keycode);
        answerPane.setDisplayedMnemonicIndexAt(dst, mnemonicLoc);
        answerPane.setForegroundAt(dst, fg);
        answerPane.setBackgroundAt(dst, bg);

        answerPane.setSelectedIndex(dst);
    }

    public void appendCurrentDiagram() {
        if (question != null) {
            TDiagramPanel tdp = mgui.getCurrentTDiagramPanel();
            TraceManager.addDev("Current diagram identified");
            if (tdp != null) {
                String text = mgui.gtm.toText(tdp);
                TraceManager.addDev("Diagram text computed: " + text);
                if (text != null) {
                    question.setText((question.getText() + text));
                }
            }
        }
    }

    private class ChatData implements AIFeedback, Runnable {

        public JTextPane answer = new JTextPane();
        public TDiagramPanel tdp;
        public boolean doIconRotation = false;
        private Thread t;
        public String lastAnswer;
        public AIChatData aiChatData;
        public AIInteract aiInteract;

        public ChatData() {
        }

        public boolean makeAIChatData() {
            aiChatData = new AIChatData();
            aiChatData.feedback = this;
            int index = listOfPossibleModels.getSelectedIndex();

            return aiChatData.makeAIInterface(listOfPossibleModels.getItemAt(index), keys.get(index));
        }


        // AIFeedback methods
        public void addInformation(String text) {
            inform(text);
        }

        public void addError(String text) {
            error(text);
        }

        public void addToChat(String data, boolean user) {
            if (user) {
                GraphicLib.appendToPane(chatOfStart().answer, "\nTTool: " + data + "\n", Color.blue);
            } else {
                GraphicLib.appendToPane(chatOfStart().answer, "\nAI: " + data + "\n", Color.red);
            }
        }

        public void setAnswerText(String text) {
            lastAnswer = text;
            //GraphicLib.appendToPane(selectedChat().answer, "\nAI:" + text + "\n", Color.red);
            enableDisableActions();
            endTime = System.currentTimeMillis();
            GraphicLib.appendToPaneSafe(console, "Done. Total time: " + (endTime - startTime) + " ms\n", Color.black);
            TraceManager.addDev("Done. Total time: " + (endTime - startTime) + " ms");
        }


        public void setRunning(boolean b) {
            if (b) {
                startAnimation();
            } else {
                stopAnimation();
            }
            enableDisableActions();
        }


        // Internal methods
        public void clear() {
            answer.setText("");
            lastAnswer = "";
        }


        public void startAnimation() {
            doIconRotation = true;
            t = new Thread(this);
            t.start();
        }

        public void stopAnimation() {
            doIconRotation = false;
            if (t != null) {
                t.interrupt();
            }
            t = null;
        }


        public void run() {
            int angle = 0;

            while (doIconRotation) {
                angle = (angle - 15) % 360;
                ImageIcon rotated = rotatedI.get(angle);
                if (rotated == null) {
                    rotated = IconManager.rotateImageIcon(IconManager.imgic154, angle);
                    rotatedI.put(angle, rotated);
                }

                setIcon(this, rotated);
                try {
                    Thread.currentThread().sleep(100);
                } catch (Exception e) {
                    TraceManager.addDev("Interrupted");
                    doIconRotation = false;
                }
                ;
            }
            setIcon(this, IconManager.imgic154);
        }
    }

    // Handling popup menu AI
    private class PopupListener extends MouseAdapter /* popup menus onto tabs */ {
        private JFrameAI frameAI;
        private JPopupMenu menu;

        private JMenuItem rename, remove, moveRight, moveLeft, addNew, clear;

        private Action listener = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JMenuItem item = (JMenuItem) e.getSource();

                if (e.getSource() == rename) {
                    frameAI.requestRenameTab();
                } else if (e.getSource() == remove) {
                    frameAI.removeCurrentTab();
                } else if (e.getSource() == moveRight) {
                    frameAI.requestMoveRightTab();
                } else if (e.getSource() == remove) {
                    frameAI.requestMoveLeftTab();
                } else if (e.getSource() == addNew) {
                    addChat(getChatName());
                } else if (e.getSource() == clear) {
                    clear();
                }

            }
        };

        public PopupListener(JFrameAI _frameAI) {
            frameAI = _frameAI;
            createMenu();
        }

        public void mousePressed(MouseEvent e) {
            checkForPopup(e);
        }

        public void mouseReleased(MouseEvent e) {

            checkForPopup(e);
        }

        public void mouseClicked(MouseEvent e) {

            checkForPopup(e);
        }

        private void checkForPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                Component c = e.getComponent();
                // TraceManager.addDev("e =" + e + " Component=" + c);
                menu.show(c, e.getX(), e.getY());
            }
        }

        private void createMenu() {
            rename = createMenuItem("Rename");
            remove = createMenuItem("Remove");
            moveLeft = createMenuItem("Move to the left");
            moveRight = createMenuItem("Move to the right");
            addNew = createMenuItem("Add new chat");
            clear = createMenuItem("Clear current chat");

            menu = new JPopupMenu("Views");
            menu.add(addNew);
            menu.addSeparator();
            menu.add(clear);
            menu.addSeparator();
            menu.add(rename);
            menu.addSeparator();
            menu.add(remove);
            menu.addSeparator();
            menu.add(moveLeft);
            menu.add(moveRight);
        }

        private JMenuItem createMenuItem(String s) {
            JMenuItem item = new JMenuItem(s);
            item.setActionCommand(s);
            item.addActionListener(listener);
            return item;
        }
    }

} // Class

	