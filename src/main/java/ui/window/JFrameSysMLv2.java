/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 * 
 * ludovic.apvrille AT enst.fr
 * 
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */




package ui.window;

import avatartranslator.AvatarSpecification;
import myutil.Conversion;
import myutil.TraceManager;
import ui.SysMLv2ProviderInterface;
import ui.util.IconManager;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


/**
 * Class JFrameSysMLv2
 * Creation: 24/02/2025
 * version 1.0 24/02/2025
 * @author Ludovic APVRILLE
 */
public class JFrameSysMLv2 extends JFrame implements ActionListener {
	private static int MAX_NB_SPEC = 20;

	private ArrayList<String> theTexts;
	private SysMLv2ProviderInterface sysMLv2provider;
	private JTextPane jta;
	private JScrollPane jsp;
	private ArrayList<String> avspecs;
	private ArrayList<AvatarSpecification> originalSpecs;
	private JTextPane jtaAvspec;
	private JScrollPane jspAvspec;

	private JCheckBox differences;
	private JButton prev, next, drawFromSysML, drawFromAvatarspec;
	private JLabel infoSpec;

	// Currently shown SysML V2
	private int indexCurrentSpec = 0;

	private int specNumber = 1;





	public JFrameSysMLv2(String title, String _theText, SysMLv2ProviderInterface _sysMLv2provider, AvatarSpecification _avspec) {
		this( title, _theText, _sysMLv2provider, _avspec, null );
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
        
    public JFrameSysMLv2(String title, String _theText, SysMLv2ProviderInterface _sysMLv2provider, AvatarSpecification _avspec, ImageIcon imgic) {
		super(title);
		theTexts = new ArrayList<>();
		theTexts.add(_theText);
		sysMLv2provider = _sysMLv2provider;

		avspecs = new ArrayList<>();
		avspecs.add(_avspec.toString());
		originalSpecs = new ArrayList<>();
		originalSpecs.add(_avspec);
		
		setDefaultCloseOperation( DISPOSE_ON_CLOSE );
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		
		jta = new JTextPane();
		// Issue #35: This should not be editable because changes will not be 
		// taken into account
		jta.setEditable( false );
//		jta.setEditable(true);
		jta.setMargin(new Insets(10, 10, 10, 10));
		//jta.setTabSize(3);
		Font f = new Font("Courrier", Font.BOLD, 12); 
		jta.setFont(f);
		jsp = new JScrollPane(jta);
		//applyDiffHighlighting(jta, theTexts.get(0));

		JTabbedPane pane = new JTabbedPane();
		pane.add(jsp, "SysML");

		jtaAvspec = new JTextPane();
		// Issue #35: This should not be editable because changes will not be
		// taken into account
		jtaAvspec.setEditable( false );
//		jta.setEditable(true);
		jtaAvspec.setMargin(new Insets(10, 10, 10, 10));
		//jta.setTabSize(3);
		f = new Font("Courrier", Font.BOLD, 12);
		jtaAvspec.setFont(f);
		jspAvspec = new JScrollPane(jtaAvspec);
		//applyDiffHighlighting(jtaAvspec, avspecs.get(0));

		pane.add(jspAvspec, "Avatar Specification");
		
		framePanel.add(pane, BorderLayout.CENTER);


		JPanel jp = new JPanel();

		infoSpec = new JLabel("");
		jp.add(infoSpec);

		prev = new JButton(IconManager.imgic46);
		prev.addActionListener(this);
		jp.add(prev);
		next = new JButton(IconManager.imgic48);
		next.addActionListener(this);
		jp.add(next);


		differences = new JCheckBox("Show diff");
		differences.setSelected(true);
		differences.addActionListener(this);
		jp.add(differences);

		JButton buttonUpdate = new JButton("Update", IconManager.imgic75);
		buttonUpdate.addActionListener(this);
		jp.add(buttonUpdate);

		drawFromSysML = new JButton("Draw SysML", IconManager.imgic12);
		drawFromSysML.addActionListener(this);
		jp.add(drawFromSysML);

		drawFromAvatarspec = new JButton("Draw Avatar spec.", IconManager.imgic12);
		drawFromAvatarspec.addActionListener(this);
		jp.add(drawFromAvatarspec);




		JButton button1 = new JButton("Close", IconManager.imgic27);
		button1.addActionListener(this);
		jp.add(button1);

		framePanel.add(jp, BorderLayout.SOUTH);
        
		if ( imgic != null ) {
	        JButton button2 = new JButton(imgic);
			button2.setBorderPainted(false);
			button2.setFocusPainted(false);
			button2.setContentAreaFilled(false);
	        jp = new JPanel();
			jp.add(button2);
	        framePanel.add(jp, BorderLayout.NORTH);
		}

		diffChange();
		updateMode();
		pack();
		button1.setName("Close Configuration");
		jsp.setName("Jsp Configuration");

	}
	
 	public void actionPerformed(ActionEvent evt)  {
		String command = evt.getActionCommand();
		
		if (command.equals("Close")) {
			dispose();
			return;
		}

		if (command.equals("Update")) {
			update();
			return;
		}

		if (evt.getSource() == differences) {
			diffChange();
			return;
		}

		if (evt.getSource() == next) {
			indexCurrentSpec --;
			diffChange();
			updateMode();
			return;
		}

		if (evt.getSource() == prev) {
			indexCurrentSpec ++;
			diffChange();
			updateMode();
			return;
		}

		if (evt.getSource() == drawFromAvatarspec) {
			drawFromAvatarspec();
			return;
		}

		if (evt.getSource() == drawFromSysML) {
			drawFromSysML();
			return;
		}



	}


	public void drawFromAvatarspec() {
		if (sysMLv2provider != null) {
			AvatarSpecification as = originalSpecs.get(indexCurrentSpec);
			sysMLv2provider.drawAvatarSpecification(as, true);
		}

	}

	public void drawFromSysML() {
		if (sysMLv2provider != null) {
			sysMLv2provider.drawSysMLv2Specification(theTexts.get(indexCurrentSpec));
		}
	}

	public void update() {
		if (sysMLv2provider != null) {
			String newText = sysMLv2provider.getSysMLV2();
			AvatarSpecification newAvspec = sysMLv2provider.getAvatarSpecification();

			if ((newText != null) && (newAvspec != null)) {
				if (differences.isSelected()) {
					theTexts.add(0, newText);
					newText = newAvspec.toString();
					avspecs.add(0, newText);
					originalSpecs.add(newAvspec);
					indexCurrentSpec = 0;
					specNumber ++;

					if (theTexts.size() > MAX_NB_SPEC) {
						theTexts.remove(MAX_NB_SPEC);
						avspecs.remove(MAX_NB_SPEC);
						originalSpecs.remove(MAX_NB_SPEC);
					}
				}
			}
			diffChange();
			updateMode();
		}
	}

	public void diffChange() {
		int diff = 0;
		if (differences.isSelected() && (theTexts.size() > indexCurrentSpec + 1)) {
			diff = 1;
		}
		//TraceManager.addDev("diff=" + diff);

		String annotatedText = Conversion.generateStringLCSDiff(theTexts.get(indexCurrentSpec+diff), theTexts.get(indexCurrentSpec));
		applyDiffHighlighting(jta, jsp, annotatedText);
		annotatedText = Conversion.generateStringLCSDiff(avspecs.get(indexCurrentSpec+diff), avspecs.get(indexCurrentSpec));
		applyDiffHighlighting(jtaAvspec, jspAvspec, annotatedText);

		if (diff == 0) {
			infoSpec.setText("spec " + (specNumber - indexCurrentSpec)  + " / " + specNumber);
		} else {
			infoSpec.setText("spec " + (specNumber - indexCurrentSpec)  + " vs. " + (specNumber - indexCurrentSpec -1));
		}
	}

	private void updateMode() {
		prev.setEnabled(indexCurrentSpec < theTexts.size()-1);
		next.setEnabled(indexCurrentSpec > 0);
	}

	private static void applyDiffHighlighting(JTextPane textPane, String diffText) {
		StyledDocument doc = textPane.getStyledDocument();

		SimpleAttributeSet normalStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(normalStyle, Color.BLACK);

		SimpleAttributeSet addedStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(addedStyle, Color.GREEN);
		StyleConstants.setUnderline(addedStyle, true);

		SimpleAttributeSet removedStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(removedStyle, Color.RED);
		StyleConstants.setUnderline(removedStyle, true);

		String[] lines = diffText.split("\\n");

		try {
			for (String line : lines) {
				SimpleAttributeSet style = normalStyle;

				if (line.startsWith("+")) {
					style = addedStyle;
				} else if (line.startsWith("-")) {
					style = removedStyle;
				}

				doc.insertString(doc.getLength(), line + "\n", style);
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	private static void applyDiffHighlighting(JTextPane textPane, JScrollPane scrollPane, String diffText) {
		StyledDocument doc = textPane.getStyledDocument();

		SimpleAttributeSet normalStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(normalStyle, Color.BLACK);

		// Added lines style (green background)
		SimpleAttributeSet addedStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(addedStyle, Color.BLACK);
		StyleConstants.setBackground(addedStyle, new Color(144, 238, 144)); // Light Green

		// Removed lines style (red background)
		SimpleAttributeSet removedStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(removedStyle, Color.BLACK);
		StyleConstants.setBackground(removedStyle, new Color(255, 182, 193));

		/*SimpleAttributeSet addedStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(addedStyle, Color.GREEN);
		StyleConstants.setUnderline(addedStyle, true);

		SimpleAttributeSet removedStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(removedStyle, Color.RED);
		StyleConstants.setUnderline(removedStyle, true);*/

		// Save the current scroll position
		JViewport viewport = scrollPane.getViewport();
		Point scrollPosition = viewport.getViewPosition();

		// Clear existing text
		try {
			doc.remove(0, doc.getLength());

			String[] lines = diffText.split("\\n");

			for (String line : lines) {
				SimpleAttributeSet style = normalStyle;

				if (line.startsWith("+")) {
					style = addedStyle;
				} else if (line.startsWith("-")) {
					style = removedStyle;
				}

				doc.insertString(doc.getLength(), line + "\n", style);
			}

		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		// Restore the scroll position
		SwingUtilities.invokeLater(() -> viewport.setViewPosition(scrollPosition));
	}
	
    
} // Class 

	