/* Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 * 
 * ludovic.apvrille AT enst.fr
 * 
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */




package ui.interactivesimulation;

import myutil.GraphicLib;
import myutil.TraceManager;
import ui.ColorManager;
import ui.MainGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Class JFrameSimulationSDPanel
 * Creation: 26/05/2011
 * version 1.0 26/05/2011
 * @author Ludovic APVRILLE
 */
public	class JFrameSimulationSDPanel extends JFrame implements ActionListener {
	
    public InteractiveSimulationActions [] actions;
	
    private static String[] unitTab = {"sec", "msec", "usec", "nsec"};
    private static int[] clockDivisers = {1000000000, 1000000, 1000, 1};
    protected JComboBox<String> units;

    private List<JSimulationSDPanel> sdpanels = new ArrayList<JSimulationSDPanel>();
    protected JLabel status;
    private int nbTraces;
    private List<String> pragmas = new ArrayList<String>();
    //, buttonStart, buttonStopAndClose;
	//protected JTextArea jta;
	//protected JScrollPane jsp;
	
	public JFrameSimulationSDPanel(String _title, int _nbTraces, List<String> _pragmas) {
		super(_title);
        nbTraces = _nbTraces;
        pragmas = _pragmas;
		initActions();
		makeComponents();
		//setComponents();
                this.addComponentListener(new ComponentAdapter() {
                    @Override
                    public void componentResized(ComponentEvent e)
                    {
                        for (JSimulationSDPanel sdpanel : sdpanels) {
                            if (sdpanel != null)
                                sdpanel.resized();
                        }
                    }
                });
	}

    public JFrameSimulationSDPanel(String _title) {
        super(_title);
        nbTraces = 1;
        initActions();
        makeComponents();
        //setComponents();
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e)
            {
                for (JSimulationSDPanel sdpanel : sdpanels) {
                    if (sdpanel != null)
                        sdpanel.resized();
                }
            }
        });
    }
	
	private JLabel createStatusBar()  {
        status = new JLabel("Ready...");
	    status.setForeground(ColorManager.InteractiveSimulationText);
        status.setBorder(BorderFactory.createEtchedBorder());
        return status;
    }
	
	public void makeComponents() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
        
        // Top panel
        JPanel topPanel = new JPanel();
        JButton buttonClose = new JButton(actions[InteractiveSimulationActions.ACT_QUIT_SD_WINDOW]);
        topPanel.add(buttonClose);
        topPanel.add(new JLabel(" time unit:"));
        units = new JComboBox<>(unitTab);
        units.setSelectedIndex(1);
        units.addActionListener(this);
        topPanel.add(units);
        JButton buttonRefresh = new JButton(actions[InteractiveSimulationActions.ACT_REFRESH]);
        topPanel.add(buttonRefresh);
        framePanel.add(topPanel, BorderLayout.NORTH);
        // Simulation panel
        if (nbTraces == 1) {
            JSimulationSDPanel sdpanel = new JSimulationSDPanel(this);
            sdpanels.add(sdpanel);
            JScrollPane jsp	= new JScrollPane(sdpanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
            sdpanel.setMyScrollPanel(jsp);
            jsp.setWheelScrollingEnabled(true);
            jsp.getVerticalScrollBar().setUnitIncrement(MainGUI.INCREMENT);
            framePanel.add(jsp, BorderLayout.CENTER);
        } else {
            JTabbedPane sdpanelsPane = GraphicLib.createTabbedPane();
            for (int i = 0; i < nbTraces; i++) {
                JSimulationSDPanel sdpanel = new JSimulationSDPanel(this);
                sdpanels.add(sdpanel);
                JPanel jp01 = new JPanel();
                //GridBagLayout gridbag01 = new GridBagLayout();
                //GridBagConstraints c01 = new GridBagConstraints();
                //jp01.setLayout(gridbag01);
                jp01.setBorder(new javax.swing.border.TitledBorder("Trace " + (i+1)));
                /*c01.weighty = 1.0;
                c01.weightx = 1.0;
                c01.fill = GridBagConstraints.HORIZONTAL;
                c01.gridheight = 1;
                c01.gridwidth = 1;*/
                JLabel labelPragmaName = new JLabel("Pragma: " + pragmas.get(i));
                jp01.add(labelPragmaName, BorderLayout.NORTH);
                //c01.gridwidth = GridBagConstraints.REMAINDER;

                JScrollPane jsp	= new JScrollPane(sdpanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
                sdpanel.setMyScrollPanel(jsp);
                jsp.setWheelScrollingEnabled(true);
                jsp.getVerticalScrollBar().setUnitIncrement(MainGUI.INCREMENT);
                jp01.add(jsp, BorderLayout.CENTER);
                sdpanelsPane.add("Trace " + (i+1), jp01);
            }
            framePanel.add(sdpanelsPane, BorderLayout.CENTER);
        }


        
        // statusBar
        status = createStatusBar();
        framePanel.add(status, BorderLayout.SOUTH);
        
        // Mouse handler
        //mouseHandler = new MouseHandler(status);
        
        pack();
        
        //
        //
	}
    
    private	void initActions() {
        actions = new InteractiveSimulationActions[InteractiveSimulationActions.NB_ACTION];
        for(int	i=0; i<InteractiveSimulationActions.NB_ACTION; i++) {
            actions[i] = new InteractiveSimulationActions(i);
            actions[i].addActionListener(this);
            //actions[i].addKeyListener(this);
        }
    }
	
	
	
	public void close() {
		dispose();
		setVisible(false);
		
	}
	
	private void refresh() {
        for (JSimulationSDPanel sdpanel : sdpanels) {
            if (sdpanel != null ) {
                sdpanel.refresh();
            }
        }
	}
    
	
	public void	actionPerformed(ActionEvent evt)  {
		String command = evt.getActionCommand();
		//TraceManager.addDev("Command:" + command);
		
		if (command.equals(actions[InteractiveSimulationActions.ACT_QUIT_SD_WINDOW].getActionCommand()))  {
			close();
		} else if (command.equals(actions[InteractiveSimulationActions.ACT_REFRESH].getActionCommand()))  {
			refresh();
		} else if (evt.getSource() == units) {
            for (JSimulationSDPanel sdpanel : sdpanels) {
                if (sdpanel != null) {
                    sdpanel.setClockDiviser(clockDivisers[units.getSelectedIndex()]);
                }
            }
        }
	}

    public void setFileReference(int index, String _fileReference) {
        if (sdpanels.size() > index) {
            //TraceManager.addDev("Setting file:" + _fileReference);
            sdpanels.get(index).setFileReference(_fileReference);
        }
    }

    public void setFileReference(String _fileReference) {
        setFileReference(0, _fileReference);
    }

    public void setFileReference(int index, BufferedReader inputStream) {
        if (sdpanels.size() > index) {
            //TraceManager.addDev("Setting input stream");
            sdpanels.get(index).setFileReference(inputStream);
        }
    }

    public void setFileReference(BufferedReader inputStream) {
        setFileReference(0, inputStream);
    }
	
	public void setCurrentTime(long timeValue) {
		status.setText("time = " + timeValue);
	}
	
	public void setStatus(String _status) {
		status.setText(_status);
	}
	
	public void setNbOfTransactions(int x, long minTime, long maxTime) {
		status.setText("" + x + " transactions, min time=" + minTime + ", max time=" + maxTime);
	}
	public void setLimitEntity(boolean limit){
        for (JSimulationSDPanel sdpanel : sdpanels) {
            sdpanel.setLimitEntity(limit);
        }
	}
    
	
	
} // Class
