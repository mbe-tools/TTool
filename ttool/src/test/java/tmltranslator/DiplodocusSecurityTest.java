package tmltranslator;

import avatartranslator.AvatarSpecification;
import avatartranslator.toproverif.AVATAR2ProVerif;
import myutil.FileUtils;
import myutil.TraceManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import proverifspec.ProVerifSpec;
import test.AbstractTest;
import tmltranslator.toavatarsec.TML2Avatar;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class DiplodocusSecurityTest extends AbstractTest {
    final static String DIR_GEN = "tmltranslator/DiplodocusSecurityTest/GeneratedPvspec/";
    final static String DIR_MODELS = "tmltranslator/DiplodocusSecurityTest/Models/";
    private static String currentModel;
    private static ArrayList<String> currentTabs;
    private static HashMap<String, List<String>> goldenMap;
    static String PROVERIF_DIR;
    static String MODELS_DIR;



    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        String baseResourcesDir = getBaseResourcesDir();

        PROVERIF_DIR = baseResourcesDir + DIR_GEN;
        MODELS_DIR = baseResourcesDir + DIR_MODELS;
        RESOURCES_DIR = baseResourcesDir + DIR_MODELS;

    }

    //arguments are defined in @Parameterized.Parameters
    public DiplodocusSecurityTest(String model, ArrayList<String> tabs) {
        super();
        currentModel = model;
        currentTabs = tabs;
        goldenMap = new HashMap<>();
    }

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection models() {
        return Arrays.asList(new Object[][] {
                {"AliceAndBobHW", new ArrayList<>(Arrays.asList("SymmetricExchange", "Nonce",  "KeyExchange", "Mac", "SampleAutoSec"))},
                {"ITA01_v6", new ArrayList<>(Arrays.asList("mapAtt", "mapNoProtection"))},
                {"Rovers_SPARTA_DIPLO", new ArrayList<>(Arrays.asList("NoCountermeasureMapping", "MACMapping", "SymmetricEncryptionNonceMapping"))},
                {"ITSDemo", new ArrayList<>(Arrays.asList("Architecture", "Architecture_enc_or"))}
        });
    }


    @Before
    public void setUp() throws Exception {
        TraceManager.devPolicy = TraceManager.TO_CONSOLE;
        TraceManager.addDev("Starting test for " + currentModel + " model");
        Path path = Paths.get(PROVERIF_DIR);
        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }

        goldenMap.clear();
        //Load current golden map, which is map a tab with its golden pvspec
        for (String tab : currentTabs) {
            TraceManager.addDev("tab : " + tab);
            goldenMap.put(tab,generateGoldenList(RESOURCES_DIR + currentModel + "/" + tab + "/golden"));
        }
    }

    @Test
    public void testSecurityModels() throws Exception {

        // Test if proverif is installed in the path
        TraceManager.addDev("Testing if \"proverif\" is in the PATH");
        assertTrue(canExecute("proverif"));

        for (String tab : currentTabs) {
            TraceManager.addDev("\n\n********** Checking the security of " + tab + " ********\n");
            TMLMappingTextSpecification tmts = new TMLMappingTextSpecification(tab);
            File f = new File(RESOURCES_DIR + currentModel + "/" + tab + "/spec.tmap");
            TraceManager.addDev("Loading file: " + f.getAbsolutePath());
            String spec = null;
            try {
                spec = FileUtils.loadFileData(f);
            } catch (Exception e) {
                TraceManager.addDev("Exception executing: loading " + tab);
                fail();
            }
            TraceManager.addDev("Testing spec " + tab);
            assertNotNull(spec);
            TraceManager.addDev("Going to parse " + tab);
            boolean parsed = tmts.makeTMLMapping(spec, RESOURCES_DIR + currentModel + "/" +  tab + "/");
            assertTrue(parsed);

            TraceManager.addDev("Checking syntax " + tab);
            // Checking syntax
            TMLMapping tmap = tmts.getTMLMapping();

            TMLSyntaxChecking syntax = new TMLSyntaxChecking(tmap);
            syntax.checkSyntax();

            if (syntax.hasErrors() > 0) {
                for (TMLError error: syntax.getErrors()) {
                    TraceManager.addDev("Error: " + error.toString());
                }
            }

            assertEquals(0, syntax.hasErrors());

            // Generate ProVerif code
            TraceManager.addDev("Generating ProVerif code for " + tab);

            TML2Avatar t2a = new TML2Avatar(tmap, false, true, null);
            AvatarSpecification avatarspec = t2a.generateAvatarSpec("1");
            AVATAR2ProVerif avatar2proverif = new AVATAR2ProVerif(avatarspec);
            ProVerifSpec proverif = avatar2proverif.generateProVerif(true, true, 3, true,
                    true);
            String pvspecFilename =  currentModel + "_" + tab;

            TraceManager.addDev("Saving spec in " + PROVERIF_DIR + pvspecFilename);
            FileUtils.saveFile(PROVERIF_DIR + pvspecFilename, proverif.getStringSpec());

            TraceManager.addDev("Running Proverif");
            if (goldenMap.get(tab) == null) {
                TraceManager.addDev("The golden for : " + tab + " is null");
                fail();
            }
            Assert.assertTrue(executeAndVerifyPvspec(PROVERIF_DIR + pvspecFilename, goldenMap.get(tab)));

            TraceManager.addDev("Test for " + tab + " tab is ended");
        }
        TraceManager.addDev("Test for " + currentModel + " model is ended");
    }
}
