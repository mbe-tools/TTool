/**Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * /**
 * Class AvatarPragma
 * Creation: 20/05/2010
 * @version 1.1 01/07/2014
 * @author Ludovic APVRILLE, Raja GATGOUT
 * @see
 */

package cli;

import avatartranslator.*;
import avatartranslator.tosysmlv2.Avatar2SysML;
import avatartranslator.tosysmlv2.AvatarFromSysML;
import avatartranslator.tosysmlv2.AvatarFromSysMLError;
import graph.AUTGraph;
import myutil.FileException;
import myutil.FileUtils;
import myutil.TraceManager;
import org.junit.Test;
import test.AbstractTest;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;


public class CLIAvatar2SysMLV2Test extends AbstractTest implements InterpreterOutputInterface {

    final static String PATH_TO_TEST_FILE = "cli/input/";
    final static String PATH_TO_EXPECTED_FILE = "cli/expected/";
    final static String PATH_TO_MODELS = "cli/models/";
    private final String[] toThenFrom = {"avSysML_datatypes", "avSysML_timer", "avSysML_original", "CoffeeMachine_Avatar", "PressureController"}; //
    private final String[] okFrom = {"avSysML_timer","avSysML_timerRealNames","avSysML_original","avSysML_originalRealNames" }; //
    private final int[] okFromStates = {8,8,250,250};
    private final int[] okFromTrans = {8,8,333,333};
    private final String[] errorFrom =
            {"avSysML_syntaxErr1", "avSysML_syntaxErr2", "avSysML_missingBlockErr3", "avSysML_badFieldNameErr4",
                    "avSysML_missingSignalBindingErr5","avSysML_missingAttrDeclErr6", "avSysML_missingOutMsgErr7",
            "avSysML_missingMethDeclErr8", "avSysML_missingStateMachineErr9", "avSysML_missingStateErrA",
            "avSysML_missingChanInSndErrB", "avSysML_missingTransDeclErrC", "avSysML_missingParamSndPayloadErrD",
            "avSysML_DupChParamSndPayloadErrE", "avSysML_missingDatatypeDeclErrF", "avSysML_missingBlockErrG",
            "avSysML_missingBlockBindErrH", "avSysML_missingTimerDeclErrI", "avSysML_missingTimerOnTransErrJ",
            "avSysML_dupAttrDeclK", "avSysML_AttrTimerNameClashL", "avSysML_missingTransRecvPayloadM",
            "avSysML_missingSndPayloadN"}; //
    private final String[] errorFromMsg = {
            "ERROR (l17,c112): parser: syntax error for input symbol \"SEMICOLON\". Lexeme: ;", // 1
            "ERROR (l35,c22): parser: syntax error for input symbol \"SEMICOLON\". Lexeme: ;",
            "WARNING : blocs of relation @SYN0:B0-B1 unspecified, recovered from channel @syn:B0.in0<B1.out1",
            "ERROR (l34,c9): Field x of out-message is associated to a field (a) in in-message that does not exist (c.f. l25,c5)",
            "ERROR (l53,c5): Channel @syn:B0.out0>B1.in1 of in-message associated to out-message has only one associated signal" +
                    " (missing binding?) (c.f. l40,c5)",
            "ERROR (l105,c26): ident b is used but not declared in block B0", //6
            "ERROR (l189,c9): output profile of signal out1 has not been set",
            "",
            "",
            "ERROR (l0,c0): outgoing transitions from pre-send state must be sending transitions leading to some sending" +
                    " state (send/set timer /reset timer) (c.f. l122,c13)", // A
            "ERROR (l112,c21): sending transition should have an associated signal (c.f. l122,c13)",
            "ERROR (l131,c21): transition request in state @st:random.0 of block B0 has no associated declaration",
            "",
            "WARNING (l114,c25): channel parameter has already been set (c.f. l113,c25)", // E
            "ERROR (l67,c9): datatype Point of attribute p does not exist",
            "ERROR : block B0 has been used but has not been declared",
            "",
            "ERROR (l118,c22): timer of expire-timer state does not exist (c.f. l97,c25)",
            "ERROR (l153,c17): set-timer transition has no associated timer (c.f. l142,c25)", // J
            "ERROR (l84,c13): Attribute p is already declared  (or there is an attribute with same name) (c.f. l83,c13)",
            "ERROR (l91,c13): Timer t cannot be added: name already used for constant or attribute (c.f. l84,c13)", // L
            "ERROR (l156,c17): receive payload of transition has not been set",
            ""
    }; //
    private final boolean[] errorFromNull = {
            true,  // 1
            true,  // 2
            false, // 3
            true,  // 4
            true,  // 5
            true,  // 6
            true,  // 7
            false, // 8
            false, // 9
            true,  // A
            true,  // B
            true,  // C
            false, // D
            false, // E
            true,  // F
            true,  // G
            false, // H
            true,  // I
            true,  // J
            true,  // K
            true,  // L
            true,  // M
            false  // N
    };
    private StringBuilder outputResult;


    public CLIAvatar2SysMLV2Test() {
        //
    }


    public void exit(int reason) {
        System.out.println("Exit reason=" + reason);
        assertTrue(reason == 0);
    }
    public void printError(String error) {
        System.out.println("Error=" + error);
    }

    public void print(String s) {
        System.out.println("info from interpreter:" + s);
        outputResult.append(s);
    }

    @Test
    public void testToThenFrom() {
        String filePath = getBaseResourcesDir() + PATH_TO_TEST_FILE + "scriptavsysml_tothenfrom";
        String script;

        outputResult = new StringBuilder();

        File f = new File(filePath);
        assertTrue(myutil.FileUtils.checkFileForOpen(f));

        script = myutil.FileUtils.loadFileData(f);

        assertTrue(script.length() > 0);
        String header = "print dir\n set model resources/test/cli/models/";

        boolean show = false;
        for(String model : toThenFrom) {
            System.out.println("Testing Model " + model + ".................................");
            String modelScript = header + model + ".xml\n" + script;
            Interpreter interpret = new Interpreter(modelScript, this, show);
            interpret.clearAvatarSpecification();
            interpret.interpret();

            // Must now load the graph
            filePath = "avsysml_ori.aut";
            f = new File(filePath);
            assertTrue(myutil.FileUtils.checkFileForOpen(f));
            String data = myutil.FileUtils.loadFileData(f);

            assertTrue(data.length() > 0);
            AUTGraph graph = new AUTGraph();
            graph.buildGraph(data);
            graph.computeStates();
            int oristates = graph.getNbOfStates();
            int oritrans = graph.getNbOfTransitions();

            filePath = "avsysml_tgt.aut";
            f = new File(filePath);
            assertTrue(myutil.FileUtils.checkFileForOpen(f));
            data = myutil.FileUtils.loadFileData(f);

            assertTrue(data.length() > 0);
            graph = new AUTGraph();
            graph.buildGraph(data);
            graph.computeStates();
            int tgtstates = graph.getNbOfStates();
            int tgttrans = graph.getNbOfTransitions();


            System.out.println("states=" + oristates + ", " + tgtstates + " -- transitions=" + oritrans + ", " + tgttrans);
            assertTrue(oristates == tgtstates);
            assertTrue(oritrans == tgttrans);
        }
    }
    @Test
    public void okFrom() {
        String filePath = getBaseResourcesDir() + PATH_TO_TEST_FILE + "scriptavsysml_okfrom";
        String script;

        outputResult = new StringBuilder();

        File f = new File(filePath);
        assertTrue(myutil.FileUtils.checkFileForOpen(f));

        script = myutil.FileUtils.loadFileData(f);

        assertTrue(script.length() > 0);
        String header = "print dir\n set model resources/test/cli/models/";

        boolean show = false;
        int size = okFrom.length;
        for(int i = 0; i < size; i++) {
            String model = okFrom[i];
            System.out.println("Testing Model " + model + ".................................");
            String modelScript = header + model + ".sysml\n" + script;
            Interpreter interpret = new Interpreter(modelScript, this, show);
            interpret.clearAvatarSpecification();
            String error = interpret.interpretUntilError();
            assertTrue(error == null || error.equals(""));
            // Must now load the graph
            filePath = "avsysml_tgt.aut";
            f = new File(filePath);
            assertTrue(myutil.FileUtils.checkFileForOpen(f));
            String data = myutil.FileUtils.loadFileData(f);

            assertTrue(data.length() > 0);
            AUTGraph graph = new AUTGraph();
            graph.buildGraph(data);
            graph.computeStates();
            int tgtstates = graph.getNbOfStates();
            int tgttrans = graph.getNbOfTransitions();
            int oristates = okFromStates[i];
            int oritrans = okFromTrans[i];

            System.out.println("states=" + oristates + ", " + tgtstates + " -- transitions=" + oritrans + ", " + tgttrans);
            assertTrue(oristates == tgtstates);
            assertTrue(oritrans == tgttrans);
        }
    }
    @Test
    public void errFrom() {
        outputResult = new StringBuilder();

        String modelsDir = "resources/test/cli/models/";

        int size = errorFrom.length;
        for(int i = 0; i < size; i++) {
            String model = modelsDir + errorFrom[i] + ".sysml";

            System.out.println("Testing Model " + model + ".................................");
            AvatarFromSysML builder = new AvatarFromSysML();
            AvatarSpecification as = builder.sysMLFiletoSpec(model);
            System.out.println("Testing if null return value: (expected:" + errorFromNull[i] + ", obtained:" + (as == null) + ")");
            // expected return value ?
            assertTrue( (as == null ? errorFromNull[i] : !errorFromNull[i]));
            List<AvatarFromSysMLError> errors = builder.getErrors();
            System.out.println("Testing if errors: (expected:" + !(errorFromMsg[i] == null || errorFromMsg[i].equals("")) +
                    ", obtained:" + !(errors== null || errors.size() == 0) + ")");
            assertTrue( (errorFromMsg[i] == null || errorFromMsg[i].equals("") ? errors== null || errors.size() == 0 :
                    errors!= null && errors.size() > 0 ) );
            if (errors.size() > 0) {
                String err = errors.get(0).toString();
                System.out.println("Compare " + errorFromMsg[i] + " with " + err + " : " + errorFromMsg[i].equals(err));
                assertTrue(errorFromMsg[i].equals(err));
            }
            else  System.out.println(" : none");
            System.out.println("Test of Model " + model + " is OK ................");
        }
    }
    @Test
    public void testElseJsignalRdistr() {
        String filePath = getBaseResourcesDir() + PATH_TO_TEST_FILE + "scriptavsysml_tothenfrom";
        String script;

        outputResult = new StringBuilder();

        File f = new File(filePath);
        assertTrue(myutil.FileUtils.checkFileForOpen(f));

        script = myutil.FileUtils.loadFileData(f);

        assertTrue(script.length() > 0);
        String header = "print dir\n set model resources/test/cli/models/";

        boolean show = false;

        System.out.println("Testing Model avSysML_else_jsignal_rdistr.................................");
        String modelScript = header + "avSysML_else_jsignal_rdistr.xml\n" + script;
        Interpreter interpret = new Interpreter(modelScript, this, show);
        interpret.clearAvatarSpecification();
        interpret.interpret();

        // Must now load the graph
        filePath = "avsysml_ori.aut";
        f = new File(filePath);
        assertTrue(myutil.FileUtils.checkFileForOpen(f));
        String data = myutil.FileUtils.loadFileData(f);

        assertTrue(data.length() > 0);
        AUTGraph graph = new AUTGraph();
        graph.buildGraph(data);
        graph.computeStates();
        int oristates = graph.getNbOfStates();
        int oritrans = graph.getNbOfTransitions();

        filePath = "avsysml_tgt.aut";
        f = new File(filePath);
        assertTrue(myutil.FileUtils.checkFileForOpen(f));
        data = myutil.FileUtils.loadFileData(f);

        assertTrue(data.length() > 0);
        graph = new AUTGraph();
        graph.buildGraph(data);
        graph.computeStates();
        int tgtstates = graph.getNbOfStates();
        int tgttrans = graph.getNbOfTransitions();


        System.out.println("states=" + oristates + ", " + tgtstates + " -- transitions=" + oritrans + ", " + tgttrans);
        assertTrue(oristates == tgtstates);
        assertTrue(oritrans == tgttrans);

        filePath = "avsysml.sysml";
        AvatarFromSysML builder = new AvatarFromSysML();
        AvatarSpecification as = builder.sysMLFiletoSpec(filePath);
        assertNotNull(as);

        // test junk signals
        AvatarBlock bl = as.getBlockWithName("B2");
        assertNotNull(bl);
        AvatarSignal sg = bl.getAvatarSignalWithName("inJunk");
        assertNotNull(sg);
        assertTrue(sg.isIn());
        List<AvatarAttribute> al;
        al =sg.getListOfOriginalAttributes();
        assertTrue(al != null && al.size() == 2);
        assertTrue(al.get(0).getName().equals("x") && al.get(0).getType().equals(AvatarType.INTEGER));
        assertTrue(al.get(1).getName().equals("b") && al.get(1).getType().equals(AvatarType.BOOLEAN));
        sg = bl.getAvatarSignalWithName("outJunk");
        assertNotNull(sg);
        assertTrue(sg.isOut());
        al =sg.getListOfOriginalAttributes();
        assertTrue(al != null && al.size() == 1);
        assertTrue(al.get(0).getName().equals("b") && al.get(0).getType().equals(AvatarType.BOOLEAN));

        // test transition and random state distribution law
        bl = as.getBlockWithName("B0");
        assertNotNull(bl);
        AvatarStateMachineElement sme = null;
        for(AvatarStateMachineElement s : bl.getStateMachine().getListOfElements())
            if (s instanceof AvatarRandom) { sme = s; break; }
        // random state case:
        assertTrue(sme != null);
        assertTrue(((AvatarRandom)sme).getFunctionId() == AvatarRandom.RANDOM_LOG_NORMAL_LAW );
        assertTrue(((AvatarRandom)sme).getExtraAttribute1().equals("10"));
        assertTrue(((AvatarRandom)sme).getExtraAttribute2().equals("3") );
        assertTrue(sme.getNexts() != null && sme.getNexts().size() == 1);
        // transition case:
        sme = sme.getNext(0);
        assertTrue(sme != null);
        assertTrue(((AvatarTransition)sme).getDelayDistributionLaw() == AvatarTransition.RANDOM_LOG_NORMAL_LAW);
        assertTrue(((AvatarTransition)sme).getDelayExtra1().equals("10"));
        assertTrue(((AvatarTransition)sme).getDelayExtra2().equals("2"));
        assertTrue(((AvatarTransition)sme).getProbability() == 2);
    }

    public static long filesCompareByLine(Path path1, Path path2) throws IOException {
        try (BufferedReader bf1 = Files.newBufferedReader(path1);
             BufferedReader bf2 = Files.newBufferedReader(path2)) {

            long lineNumber = 1;
            String line1 = "", line2 = "";
            while ((line1 = bf1.readLine()) != null) {
                line2 = bf2.readLine();
                if (line2 == null || !line1.equals(line2)) {
                    return lineNumber;
                }
                lineNumber++;
            }
            if (bf2.readLine() == null) {
                return -1;
            }
            else {
                return lineNumber;
            }
        }
    }

    @Test
    public void TestOrder() {
        File f = new File("test_from.txt");
        AvatarFromSysML parser =  new AvatarFromSysML();
        AvatarSpecification as = parser.sysMLFiletoSpec(getBaseResourcesDir() + PATH_TO_MODELS + "avSysML_original.sysml");
        assertNotNull(as);
        try {FileUtils.saveFile(f,as.toString());}
        catch (FileException e) {fail();}

        f = new File("avsysml.sysml");
        Avatar2SysML builder = new Avatar2SysML(as);
        try {FileUtils.saveFile(f,builder.avatar2SysML().toString());}
        catch (FileException e) {fail();}

        f = new File("test_to.txt");
        as = parser.sysMLFiletoSpec("avsysml.sysml");
        try {FileUtils.saveFile(f,as.toString());}
        catch (FileException e) {fail();}
        try {assertTrue(filesCompareByLine(Paths.get("test_from.txt"), Paths.get("test_to.txt"))== -1);}
        catch (IOException e) {fail();}
    }

}
