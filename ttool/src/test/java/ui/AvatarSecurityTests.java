package ui;

import common.ConfigurationTTool;
import common.SpecConfigTTool;
import myutil.FileException;
import myutil.FileUtils;
import myutil.TraceManager;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;

public class AvatarSecurityTests extends AbstractUITest {

    private final static String RES_FOLDER = "ui/avatarsecuritytests/";
    private final static String INPUT_FOLDER = "input/";
    private final static String GOLDEN_SUFFIX = "_golden";
    private final static String OUTPUT_FOLDER = "output/";
    private final static String PVSPEC_SUFFIX = "_pvspec";
    private static String OUTPUT_DIR;

    private static AbstractMap<String, List<String>> MODELS = new HashMap<>();
    private static AbstractMap<String, List<String>> goldens = new HashMap<>();

    private static void initModels() {
        // Provides the filenames and the list of panels to be tested
        MODELS.put("AliceAndBob", Arrays.asList("Example", "Example DH", "Example DH with Certificate"));
        MODELS.put("TLS", Arrays.asList("NonAuthenticatedClient", "AuthenticatedClient", "NonAuthenticatedClient_lib"));
        MODELS.put("SGX_key_exchange", Arrays.asList("Design"));
        MODELS.put("MicroWaveOven_SafetySecurity_fullMethodo", Arrays.asList("Design"));
        MODELS.put("EVITA_KeyingProtocol", Arrays.asList("KeyMasterProtocol", "AsymmetricKeyDistributionProtocol"));
        MODELS.put("X3DH", Arrays.asList("protocol"));
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
        String baseResourcesDir = getBaseResourcesDir();

        RESOURCES_DIR = baseResourcesDir + RES_FOLDER;
        INPUT_DIR = RESOURCES_DIR + INPUT_FOLDER;
        OUTPUT_DIR = RESOURCES_DIR + OUTPUT_FOLDER;

        File outputDir = new File(OUTPUT_DIR);

        if (outputDir.exists()) {
            FileUtils.deleteFiles(OUTPUT_DIR);
        } else {
            boolean result = outputDir.mkdir();
            if (!result) {
                throw new FileException("Can't create output directory");
            }
        }

        initModels();

        for (String model : MODELS.keySet()) {
            for (String panel : MODELS.get(model)) {
                String tabName = panel.replace(" ", "_");
                String goldenName = model + '_' + tabName + GOLDEN_SUFFIX;
                String goldenPath = INPUT_DIR + goldenName;

                List<String> golden = generateGoldenList(goldenPath);
                goldens.put(goldenName, golden);
            }
        }
    }

    public AvatarSecurityTests() {
        super();
    }

    @Test
    public void testAvatarSecurityModel() {

        ConfigurationTTool.ProVerifVerifierHost = "localhost";
        SpecConfigTTool.ProVerifCodeDirectory = "./" + OUTPUT_DIR;
        ConfigurationTTool.ProVerifVerifierPath = "proverif";

        for (String model : MODELS.keySet()) {
            runTest(model);
        }
    }

    private void runTest(String model) {
        TraceManager.addDev("Start testing " + model);
        openModel(model);

        for (String panel : MODELS.get(model)) {
            TraceManager.addDev("Panel tested: " + panel);
            String tabName = panel.replace(" ", "_");
            mainGUI.selectPanelByName(panel);
            mainGUI.checkModelingSyntax(true, false);

            String pvspecPath = OUTPUT_DIR + model + '_' + tabName + PVSPEC_SUFFIX;
            boolean pvspecGenerated = mainGUI.gtm.generateProVerifFromAVATAR(pvspecPath, 1, true, true);
            assertTrue(pvspecGenerated);

            String goldenName = model + '_' + tabName + GOLDEN_SUFFIX;

            assertTrue(executeAndVerifyPvspec(pvspecPath, goldens.get(goldenName)));
        }
    }

}

