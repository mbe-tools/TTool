/**Copyright or (C) or Copr. GET / ENST, Telecom-Paris, Ludovic Apvrille
 *
 * ludovic.apvrille AT enst.fr
 *
 * This software is a computer program whose purpose is to allow the
 * edition of TURTLE analysis, design and deployment diagrams, to
 * allow the generation of RT-LOTOS or Java code from this diagram,
 * and at last to allow the analysis of formal validation traces
 * obtained from external tools, e.g. RTL from LAAS-CNRS and CADP
 * from INRIA Rhone-Alpes.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * /**
 * Class AvatarExpressionTest
 * Creation: 29/04/2020
 * @version 1.0 29/04/2020
 * @author Alessandro TEMPIA CALVINO
 * @see
 */


package avatartranslator;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import myutil.TraceManager;
import org.junit.Before;
import org.junit.Test;

import avatartranslator.modelchecker.SpecificationBlock;
import avatartranslator.modelchecker.SpecificationState;

import java.util.ArrayList;

public class AvatarSpecificationSyntaxCheckerTest {

    public AvatarSpecificationSyntaxCheckerTest() {
    }
    
    @Before
    public void test () {

    }
    
    @Test
    public void testDuplicateAttribute() {

        AvatarSpecification as = new AvatarSpecification("avatarspecification", null);
        AvatarBlock block1 = new AvatarBlock("block1", as, null);
        as.addBlock(block1);

        AvatarAttribute a1 = new AvatarAttribute("a1", AvatarType.INTEGER, block1, null);
        block1.addAttribute(a1);
        AvatarAttribute a2 = new AvatarAttribute("a1", AvatarType.INTEGER, block1, null);
        block1.addAttribute(a2);

        TraceManager.addDev("Number of attributes in block1:" + block1.getAttributes().size());
        assertEquals(block1.getAttributes().size(), 1);
        block1.getAttributes().add(a2);
        TraceManager.addDev("Number of attributes in block1:" + block1.getAttributes().size());
        assertEquals(block1.getAttributes().size(), 2);


        ArrayList<AvatarError> myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 1);

        AvatarAttribute a3 = new AvatarAttribute("a3", AvatarType.INTEGER, block1, null);
        block1.addAttribute(a3);
        AvatarAttribute a4 = new AvatarAttribute("a4", AvatarType.INTEGER, block1, null);
        block1.addAttribute(a4);

        assertEquals(block1.getAttributes().size(), 4);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 1);

        block1.getAttributes().add(a2);
        assertEquals(block1.getAttributes().size(), 5);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 3);

        AvatarMethod am1 = new AvatarMethod("myLovelyMethod1", null);
        block1.addMethod(am1);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 3);

        block1.getMethods().add(am1);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 4);

        AvatarMethod am2 = new AvatarMethod("a1", null);
        block1.addMethod(am2);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 7);

        AvatarSignal as1 = new AvatarSignal("myLovelySignal1", ui.AvatarSignal.IN, null);
        block1.addSignal(as1);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 7);

        block1.getMethods().add(as1);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 8);

        AvatarSignal as2 = new AvatarSignal("myLovelyMethod1", ui.AvatarSignal.IN,null);
        block1.addSignal(as2);
        myErrors = AvatarSyntaxChecker.checkSyntaxErrors(as);
        assertEquals(myErrors.size(), 10);



    }
}
