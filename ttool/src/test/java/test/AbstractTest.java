package test;

import myutil.Conversion;
import myutil.FileException;
import myutil.FileUtils;
import myutil.TraceManager;
import org.jgrapht.alg.util.Pair;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public abstract class AbstractTest {

    protected static final String TXT_EXT = ".txt";
    protected static final String XML_EXT = ".xml";
    protected static String RESOURCES_DIR = "";
    protected static String INPUT_DIR;
    protected static String EXPECTED_CODE_DIR;
    protected static String ACTUAL_CODE_DIR;
    protected static final String PROVERIF_RESULT_PREFIX = "RESULT";
    private static HashMap<String, String> alreadyMapComm;
    final static String LEFT = "left";
    final static String RIGHT = "right";

    public AbstractTest() {
        alreadyMapComm = new HashMap<>();
    }

    protected static String getBaseResourcesDir() {
        final String systemPropResDir = System.getProperty("resources_dir");

        if (systemPropResDir == null) {

            String isGradle = System.getProperty("org.gradle.test.worker");
            if (isGradle == null) {
                return "test/resources/";
            } else {
                return "resources/test/";
            }
        }

        return systemPropResDir;
    }

    public static boolean canExecute(final String exe) {
        boolean existsInPath = Stream.of(System.getenv("PATH").
                        split(Pattern.quote(File.pathSeparator)))
                .map(Paths::get)
                .anyMatch(path -> Files.exists(path.resolve(exe)));
        return existsInPath;
    }

    protected void checkResult(final String actualCode,
                               final String fileName) {
        try {
            final String expectedCode = FileUtils.loadFile(EXPECTED_CODE_DIR + fileName + TXT_EXT);

            if (!expectedCode.equals(actualCode)) {
                saveActualResults(fileName + TXT_EXT, actualCode);
            }
        } catch (FileException ex) {
            handleException(ex);
        }
    }

    protected void checkResultXml(final String actualCode,
                                  final String fileName) {

        TraceManager.addDev("Comparing with " + actualCode.substring(0, 30) + " with file: " + fileName);

        // Since this function fails because tasks are not always in the same order, it is deactivated


        try {
            final String expectedCode = FileUtils.loadFile(EXPECTED_CODE_DIR + fileName + XML_EXT);

            //FileUtils.saveFile(EXPECTED_CODE_DIR + fileName + XML_EXT, actualCode);

            if (!compareXml(actualCode, expectedCode)) {
                saveActualResults(fileName + XML_EXT, actualCode);
            }
        } catch (ParserConfigurationException | SAXException | IOException | FileException ex) {
            handleException(ex);
        }
    }

    private void saveActualResults(final String fileName,
                                   final String actualCode) {
        final String filePath = ACTUAL_CODE_DIR + fileName;
        final File fileToSave = new File(filePath);
        final File dir = fileToSave.getParentFile();

        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            FileUtils.saveFile(filePath, actualCode);
            fail("Differences were found between actual and expected code!!");
        } catch (final FileException ex) {
            handleException(ex);
        }
    }

    protected boolean compareXml(final String result,
                                 final String expected)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        documentFactory.setNamespaceAware(true);
        documentFactory.setCoalescing(true);
        documentFactory.setIgnoringElementContentWhitespace(true);
        documentFactory.setIgnoringComments(true);
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        final Document doc1 = documentBuilder.parse(new ByteArrayInputStream(result.getBytes()));
        doc1.normalizeDocument();

        final Document doc2 = documentBuilder.parse(new ByteArrayInputStream(expected.getBytes()));
        doc2.normalizeDocument();

        return doc1.isEqualNode(doc2);
    }

    protected void handleException(final Throwable th) {
        th.printStackTrace();
        fail(th.getLocalizedMessage());
    }

    protected String reworkStringForComparison(String input) {
        String ret = Conversion.replaceAllString(input, "\n", "");
        ret = Conversion.replaceAllString(ret, "\t", "");
        ret = Conversion.replaceAllString(ret, " ", "");
        return ret;


    }

    protected void monitorError(Process proc) {
        BufferedReader proc_err = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
        new Thread() {
            @Override
            public void run() {
                String line;
                try {
                    while ((line = proc_err.readLine()) != null) {
                        TraceManager.addDev("NOC executing err: " + line);
                    }
                } catch (Exception e) {
                    //System.out.println("FAILED reading errors");
                    return;
                }

            }
        }.start();
    }

    public class AbstractTestException extends Exception {
        public AbstractTestException(String message) {
            super(message);
        }
    }

    public boolean CompareTwoBufferedReaders(BufferedReader reader1, BufferedReader reader2, String _regex, String _replace) throws AbstractTestException {
        boolean areEqual;
        try {
            String line1 = reader1.readLine();
            String line2 = reader2.readLine();
            areEqual = true;
            int lineNum = 1;

            while (line1 != null || line2 != null) {
                if (!line1.equalsIgnoreCase(line2)) {
                    if (_regex != null && _regex.length() > 0) {
                        line1 = line1.replaceAll(_regex, _replace);
                        line2 = line2.replaceAll(_regex, _replace);
                    }
                    if (!line1.equalsIgnoreCase(line2)) {
                        areEqual = false;
                        break;
                    }
                }
                line1 = reader1.readLine();
                line2 = reader2.readLine();
                lineNum++;
            }

            if (areEqual) {
                TraceManager.addDev("Two buffers have same content.");
            } else {
                TraceManager.addDev("Two buffers have different content. They differ at line " + lineNum);
                TraceManager.addDev("Buffer1 has " + line1 + " and Buffer2 has " + line2 + " at line " + lineNum);
            }

            reader1.close();
            reader2.close();
        } catch (Exception e) {
            TraceManager.addDev("FAILED: executing comparison: " + e.getMessage());
            throw new AbstractTestException("FAILED: executing comparison: " + e.getMessage());
        }
        return areEqual;
    }

    public boolean executeAndVerifyPvspec(String pvspecPath, List<String> golden) {
        return executeAndVerifyPvspec(pvspecPath, golden, false);
    }

    public boolean executeAndVerifyPvspec(String pvspecPath, List<String> golden, boolean idIssue) {
        if (idIssue) {
            alreadyMapComm.clear();
        }
        String cmd = "proverif -in pitype " + pvspecPath;
        final Process[] process = new Process[1];
        final BufferedReader[] cmdOutput = new BufferedReader[1];
        int timeOutInSeconds = 600;
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        final Future<?> future = executor.submit(() -> {
            try {
                process[0] = Runtime.getRuntime().exec(cmd);
                cmdOutput[0] = new BufferedReader(new InputStreamReader(process[0].getInputStream()));
                String outputLine;
                int checked = 0;
                while ((outputLine = cmdOutput[0].readLine()) != null) {
                    if (outputLine.startsWith(PROVERIF_RESULT_PREFIX)) {
                        if (!idIssue) {
                            TraceManager.addDev("ProVerif Output : " + outputLine);
                            if (golden.contains(outputLine)) {
                                checked += 1;
                            } else {
                                return false;
                            }
                        } else {
                            TraceManager.addDev("ProVerif Output : " + outputLine);
                            assertTrue(golden.size() > checked);
                            assertTrue(equalsWithIdIssues(golden.get(checked), outputLine));
                            checked++;
                        }
                    }
                }

                if (golden.size() != 1 || !golden.get(0).isEmpty()) {
                    return golden.size() == checked;
                }
                return false;
            } catch (Exception e) {
                TraceManager.addDev("FAILED: executing: " + cmd + ": " + e.getMessage());
                throw new RuntimeException(e);
            }
        });
        try {
            future.get(timeOutInSeconds, TimeUnit.SECONDS);
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            future.cancel(true);
            throw new RuntimeException(e);
        } finally {
            executor.shutdown();
        }
        return true;
    }

    public static List<String> generateGoldenList(String goldenPath) throws IOException {
        BufferedReader goldenFile = null;
        try {
            goldenFile = new BufferedReader(new FileReader(goldenPath));
        } catch (FileNotFoundException e) {
            TraceManager.addDev("File " + goldenPath + " not found");
            throw new RuntimeException(e);
        }
        List<String> goldenList = new ArrayList<>();
        String line;
        while ((line = goldenFile.readLine()) != null) {
            if (line.startsWith(PROVERIF_RESULT_PREFIX))
                goldenList.add(line);
        }
        return goldenList;
    }

    private boolean equalsWithIdIssues(String line, String str) {
        str = str.trim();

        if (str.startsWith(line.trim()) && !line.trim().isEmpty()) {
            TraceManager.addDev("\"" + str + "\"" + " is in golden");
            return true;
        }

        String cleanStr = str.replaceAll("[0-9]+", "");
        String cleanLine = line.replaceAll("[0-9]+", "");

        if (!cleanStr.startsWith(cleanLine.trim()) || cleanLine.trim().isEmpty()) {
            TraceManager.addDev("\"" + str + "\"" + " not in golden");
            return false;
        }

        String changedStr = replaceIds(line, str);
        if (changedStr.startsWith(line.trim()) && !line.trim().isEmpty()) {
            TraceManager.addDev("\"" + changedStr + "\"" + " is in golden");
            return true;
        }

        TraceManager.addDev("\"" + str + "\"" + " not in golden");
        return false;
    }

    private String replaceIds(String line, String str) {
        String newStr = str;
        ArrayList<Pair<String, String>> lineComms = findIds(line);
        ArrayList<Pair<String, String>> strComms = findIds(str);
        if (lineComms.size() != strComms.size()) {
            return newStr;
        }
        for (int i = 0; i < lineComms.size(); i++) {
            String currentIdStr = strComms.get(i).getFirst();
            if (alreadyMapComm.containsKey(currentIdStr)) {
                newStr = newStr.replaceAll(currentIdStr, alreadyMapComm.get(currentIdStr));
            } else {
                newStr = newStr.replaceAll(currentIdStr, lineComms.get(i).getFirst());
                alreadyMapComm.put(currentIdStr, lineComms.get(i).getFirst());
            }
            TraceManager.addDev("\"" + currentIdStr + "\"" + " has been replaced by " + "\"" + alreadyMapComm.get(currentIdStr) + "\"");
        }
        return newStr;
    }

    private ArrayList<Pair<String, String>> findIds(String query) {
        ArrayList<Pair<String, String>> comms = new ArrayList<>();
        if (query.contains("==>") && query.matches(".*[0-9]+.*")) {
            int index = query.indexOf("==>");
            int lookingIndex = 0; //current query substring index
            int queryIndex = 0; //query string index
            Pattern p = Pattern.compile("[0-9]+");
            Matcher m = p.matcher(query);
            while (m.find()) {

                lookingIndex = m.start();
                queryIndex += lookingIndex;

                String idUnit = "" + query.charAt(queryIndex);
                StringBuilder id = new StringBuilder();
                int incr = 1;
                while (idUnit.matches("[0-9]")) {
                    id.append(idUnit);
                    idUnit = "" + query.charAt(queryIndex + incr);
                    incr += 1;
                }
                String position = LEFT;
                if (queryIndex > index) {
                    position = RIGHT;
                }
                queryIndex += incr;
                comms.add(new Pair<>(id.toString(), position));
                m = p.matcher(query.substring(queryIndex));
            }
        }
        return comms;
    }
}
