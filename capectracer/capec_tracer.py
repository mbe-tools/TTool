import requests
import os
import torch
import decimal
import argparse
import time
from datetime import datetime

from capec_dict import CapecDict

from sentence_transformers import SentenceTransformer, util

def get_capec_file(abs_path):
    # Send a GET request to the URL
    response = requests.get("https://capec.mitre.org/data/xml/capec_latest.xml")
    
    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Open the file in binary write mode and write the content
        with open(abs_path + "capec_latest.xml", 'wb') as file:
            file.write(response.content)

    return response

def round_dec(x, place, round_up):
    context = decimal.getcontext()
    original_rounding = context.rounding

    if round_up == True:
        context.rounding = decimal.ROUND_CEILING
    else:
        context.rounding = decimal.ROUND_FLOOR

    rounded = round(decimal.Decimal(str(x)), place)
    context.rounding = original_rounding

    return float(rounded)

def provide_att_patterns(abs_path):
    dicti = CapecDict(abs_path + "capec_latest.xml", abs_path + "system_specs.txt")
    capec_names = dicti.capec_names
    capec_descs = dicti.capec_descriptions
    capec_attack_steps = dicti.capec_execution_flows
    all_capec_sentences = dicti.capec_sentences
    spec_sentences = dicti.comparison_input_sentences

    model = SentenceTransformer('basel/ATTACK-BERT')
    spec_embeddings = model.encode(spec_sentences, convert_to_tensor=True)

    sim_scores = []

    for index, capec_sentences in enumerate(all_capec_sentences):
        capec_embeddings = model.encode(capec_sentences, convert_to_tensor=True)

        cos_scores = util.cos_sim(spec_embeddings, capec_embeddings)
        mean_cos_score = torch.mean(cos_scores).item()
        score = [index, mean_cos_score]
        sim_scores.append(score)

    # Sort the arrays based on the value of the second index in each array
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    max_score = round_dec(sim_scores[0][1], 2, True)
    min_score = 0

    with open(abs_path + 'traces.txt', 'w') as output:
        for score in sim_scores:
            if score[1] > 0:
                normalized_score = (score[1] - min_score) / (max_score - min_score)
                confidence_score = int(100 * normalized_score)

                if confidence_score > 0:
                    output.write(f'Confidence score: {confidence_score}%\n')
                    output.write(f'Name: {capec_names[score[0]]}\n')
                    output.write(f'Description:\n')
                    output.write(f'{capec_descs[score[0]]}\n')

                    if capec_attack_steps[score[0]]:
                        output.write(f'Attack Steps:\n')

                        for attack_step in capec_attack_steps[score[0]]:
                            output.write(f'{attack_step}\n')
                    output.write('\n')

def provide_mitigations(abs_path):
    dicti = CapecDict(abs_path + "capec_latest.xml", abs_path + "attack.txt")
    capec_mitigations = dicti.capec_mitigations
    all_capec_sentences = dicti.capec_sentences
    attack_sentences = dicti.comparison_input_sentences

    model = SentenceTransformer('basel/ATTACK-BERT')
    attack_embeddings = model.encode(attack_sentences, convert_to_tensor=True)

    sim_scores = []

    for index, capec_sentences in enumerate(all_capec_sentences):
        capec_embeddings = model.encode(capec_sentences, convert_to_tensor=True)

        cos_scores = util.cos_sim(attack_embeddings, capec_embeddings)
        mean_cos_score = torch.mean(cos_scores).item()
        score = [index, mean_cos_score]
        sim_scores.append(score)

    # Sort the arrays based on the value of the second index in each array
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    already_printed_mitis = []

    with open(abs_path + 'traces.txt', 'w') as output:
        for score in sim_scores:
            if score[1] > 0.4 and capec_mitigations[score[0]]:
                for mitigation in capec_mitigations[score[0]]:
                    if mitigation not in already_printed_mitis:
                        output.write(f'{mitigation}\n\n')
                        already_printed_mitis.append(mitigation)
                        
def is_file_downloaded_today(abs_path):
    file_path = abs_path + "capec_latest.xml"
    
    if not os.path.isfile(file_path):
        return False  # The file doesn't exist

    # Get the last modified time of the file
    last_modified_time = os.path.getmtime(file_path)
    last_modified_date = datetime.fromtimestamp(last_modified_time).date()

    # Get today's date
    today = datetime.now().date()

    # Compare the two dates
    return last_modified_date == today

if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description='Provides a list of most related CAPECs to a provided input.')
    parser.add_argument('--mitigations', action='store_true', help='Provide most relevant mitigations to an attack.')
    args = parser.parse_args()

    abs_path = os.path.abspath(__file__)
    last_slash_index = abs_path.rfind("/")
    abs_path = abs_path[:last_slash_index + 1]

    try:
        downloaded_capecs = is_file_downloaded_today(abs_path)

        if not downloaded_capecs:
            response = get_capec_file(abs_path)

        if downloaded_capecs or response.status_code == 200:
            if args.mitigations:
                provide_mitigations(abs_path)
            else:
                provide_att_patterns(abs_path)
        else:
            with open(abs_path + 'traces.txt', 'w') as output:
                    output.write(f"Failed to download the list of CAPECs from MITRE. Status code: {response.status_code}.")
    except Exception as e:
         with open(abs_path + 'traces.txt', 'w') as output:
                output.write(str(e))

    print("Results (or errors if any were encountered) have been published to traces.txt.")
