import re
import xmltodict

from tokens_dict import TokensDict

# Define the CapecDict class, inheriting from TokensDict
class CapecDict(TokensDict):
    def __init__(self, capec_file, input_file):
        super().__init__()

        self.capec_names = []
        self.capec_descriptions = []
        self.capec_execution_flows = []
        self.capec_mitigations = []
        self.capec_tokens = []
        self.capec_sentences = []

        self.comparison_input_tokens = []
        self.comparison_input_sentences = []

        self.parse_capecs(capec_file)
        self.parse_input(input_file)
    
    def parse_capecs(self, capec_file):
        capec_descriptions = []
        capec_names = []
        capec_execution_flows = []
        capec_mitigations = []
        
        with open(capec_file, 'r') as xml_file:
            xml_data = xml_file.read()
            xml_data = re.sub(r"<xhtml:(.*?)>", "", xml_data)
            xml_data = re.sub(r"</xhtml:(.*?)>", "", xml_data)
            capec_dict = xmltodict.parse(xml_data)

        attack_patterns = capec_dict['Attack_Pattern_Catalog']['Attack_Patterns']['Attack_Pattern']

        for attack_pattern in attack_patterns:
            if attack_pattern['@Abstraction'] == 'Standard' and \
                attack_pattern['@Status'] != 'Obsolete' and \
                attack_pattern['@Status'] !='Deprecated' and \
                "Description" in attack_pattern and \
                attack_pattern["Description"] != None:
                
                capec_names.append(attack_pattern['@Name'])
                
                description = attack_pattern["Description"]

                if "Extended_Description" in attack_pattern:
                    description = description + " " + attack_pattern["Extended_Description"]

                cleaned_desc = super().clean_text(description)
                capec_descriptions.append(cleaned_desc)
                
                attack_steps_list = []

                if "Execution_Flow" in attack_pattern:
                    attack_steps = attack_pattern["Execution_Flow"]["Attack_Step"]
                
                    if isinstance(attack_steps, list):
                        for idx, attack_step in enumerate(attack_steps):
                            attack_stp = "Step " + str(idx + 1) + ". " + attack_step["Description"] + "."
                            cleaned_attack_step = super().clean_text(attack_stp)
                            attack_steps_list.append(cleaned_attack_step)
                    else:
                        attack_stp = "Step 1. " + attack_steps["Description"] + "."
                        cleaned_attack_step = super().clean_text(attack_stp)
                        attack_steps_list.append(cleaned_attack_step)

                capec_execution_flows.append(attack_steps_list)

                mitigations_list = []

                if "Mitigations" in attack_pattern:
                    mitigations = attack_pattern["Mitigations"]["Mitigation"]

                    if isinstance(mitigations, list):
                        for mitigation in mitigations:
                            cleaned_mitigation = super().clean_text(mitigation)
                            mitigations_list.append(cleaned_mitigation)
                    else:
                        cleaned_mitigation = super().clean_text(mitigations)
                        mitigations_list.append(cleaned_mitigation)

                capec_mitigations.append(mitigations_list)

        self.capec_names = capec_names
        self.capec_descriptions = capec_descriptions
        self.capec_execution_flows = capec_execution_flows
        self.capec_mitigations = capec_mitigations
        
        capec_data = []

        for capec_index, capec_description in enumerate(self.capec_descriptions):
            capec_desc_ef = capec_description

            for attack_step in self.capec_execution_flows[capec_index]:
                attack_stp = re.sub(r'^Step [0-9]+\. ','', attack_step)
                attack_stp = re.sub(r'^\[.*\] ','', attack_stp)
                capec_desc_ef = capec_desc_ef + " " + attack_stp

            capec_data.append(capec_desc_ef)

        capec_sentences, capec_tokens = super().preprocess(capec_data)

        self.capec_tokens = capec_tokens
        self.capec_sentences = capec_sentences

    def parse_input(self, input_file):
        with open(input_file, 'r') as file:
            file_contents = file.read()

        input_texts = [super().clean_text(file_contents)]
        input_sentences, input_tokens = super().preprocess(input_texts)

        self.comparison_input_tokens = input_tokens[0]
        self.comparison_input_sentences = input_sentences[0]
