import re
import nltk
import spacy

from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import wordnet as wn

class TokensDict:
    def __init__(self):
        # Initialize NLTK
        nltk.download('punkt')
        nltk.download('wordnet')
        nltk.download('averaged_perceptron_tagger')
        nltk.download('omw-1.4')

        self.en = spacy.load('en_core_web_sm', enable=[""])
        self.stop_words = {}
        self.lemmatizer = WordNetLemmatizer()

        for stop_word in self.en.Defaults.stop_words:
            self.stop_words[hash(stop_word)] = stop_word            

        wn.ensure_loaded()

    def lemm_and_rem_sw(self, tokens):
        preprocessed_tokens = []
        tagged_tokens = nltk.pos_tag(tokens)

        for token, pos in tagged_tokens:
            hashed_token = hash(token)

            if not token.isnumeric() and self.stop_words.get(hashed_token) == None:
                if pos.startswith("N"):
                    preprocessed_tokens.append(self.lemmatizer.lemmatize(token, "n"))
                elif pos.startswith("V"):
                    preprocessed_tokens.append(self.lemmatizer.lemmatize(token, "v"))
                elif pos.startswith('J'):
                    preprocessed_tokens.append(self.lemmatizer.lemmatize(token, "a"))
                elif pos.startswith('R'):
                    preprocessed_tokens.append(self.lemmatizer.lemmatize(token, "r"))
                else:
                    preprocessed_tokens.append(token)

        return preprocessed_tokens

    def tokenize_text(self, text):
        sentences = sent_tokenize(text)
        prepro_sentences = []

        for sentence in sentences:
            # Remove punctuation using regular expression
            prepro_text = re.sub(r'[^a-zA-Z0-9\s/-]', '', sentence)
            prepro_text = re.sub(r'[-/]', ' ', prepro_text)
            prepro_text = re.sub(r'\s+', ' ', prepro_text)
            prepro_text = prepro_text.strip()
            prepro_sentences.append(prepro_text)

        word_tokens = ' '.join(prepro_sentences)
        word_tokens = word_tokens.lower()
        word_tokens = word_tokenize(word_tokens)
            
        # Tokenization using NLTK
        preprocessed_tokens = self.lemm_and_rem_sw(word_tokens)

        return prepro_sentences, preprocessed_tokens

    def preprocess(self, corpus):
        prepro_corpus_sentences = []
        prepro_corpus_tokens = []
        
        for doc in corpus:
            prepro_sentences, prepro_tokens = self.tokenize_text(doc)
            prepro_corpus_sentences.append(prepro_sentences)
            prepro_corpus_tokens.append(prepro_tokens)

        return prepro_corpus_sentences, prepro_corpus_tokens
    
    def clean_text(self, text):
        cleaned_text = text.strip()
        cleaned_text = re.sub(r'\n', ' ', cleaned_text)
        cleaned_text = re.sub(r'\.+', '.', cleaned_text)
        cleaned_text = re.sub(r'\s+', ' ', cleaned_text)
        cleaned_text = re.sub(r'^\[[^\]]*\]\s*', '', cleaned_text)

        return cleaned_text
